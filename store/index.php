<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "store-user";
include '../function/model.php';
$_SESSION['refferPage'] = $BASENAME['content'];
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Store - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element">
				<input type="text" name="product_g_a_search"  placeholder="<?php echo $TEXT['product_g_a_search']?>" class="search-100" id="product_g_a_search" value="<?php echo getStoreGASearchPlaceholder()?>" onChange="storeGASearch()">
			<div class="container-mini-left">
				<a class="title-white label-title"><?php echo $TEXT['store_g_a_all']?></a><br>
				<a href="<?php echo $BASENAME['store']?>" class="text-white-shadow"><?php echo $TEXT['store_g_a_showall']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g'?>" class="text-white-shadow"><?php echo $TEXT['store_g']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=a'?>" class="text-white-shadow"><?php echo $TEXT['store_a']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=trending'?>" class="text-white-shadow"><?php echo $TEXT['store_trending']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=bs'?>" class="text-white-shadow"><?php echo $TEXT['store_bs']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=progress'?>" class="text-white-shadow"><?php echo $TEXT['store_progress']?></a><br>
				<br>
				<a class="title-white label-title"><?php echo $TEXT['store_g']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&c=trending'?>" class="text-white-shadow"><?php echo $TEXT['store_trending']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&c=bs'?>" class="text-white-shadow"><?php echo $TEXT['store_bs']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&c=progress'?>" class="text-white-shadow"><?php echo $TEXT['store_progress']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g'?>" class="text-white-shadow"><?php echo $TEXT['store_g_all']?></a><br>
				<hr>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=action'?>" class="text-white-shadow"><?php echo $TEXT['store_g_action']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=adventure'?>" class="text-white-shadow"><?php echo $TEXT['store_g_adventure']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=arcade'?>" class="text-white-shadow"><?php echo $TEXT['store_g_arcade']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=fight'?>" class="text-white-shadow"><?php echo $TEXT['store_g_fight']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=fps'?>" class="text-white-shadow"><?php echo $TEXT['store_g_fps']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=horror'?>" class="text-white-shadow"><?php echo $TEXT['store_g_horror']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=multiplayer'?>" class="text-white-shadow"><?php echo $TEXT['store_g_multiplayer']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=racing'?>" class="text-white-shadow"><?php echo $TEXT['store_g_racing']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=rpg'?>" class="text-white-shadow"><?php echo $TEXT['store_g_rpg']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=scifi'?>" class="text-white-shadow"><?php echo $TEXT['store_g_scifi']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=shooter'?>" class="text-white-shadow"><?php echo $TEXT['store_g_shooter']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=simulation'?>" class="text-white-shadow"><?php echo $TEXT['store_g_simulation']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=stealth'?>" class="text-white-shadow"><?php echo $TEXT['store_g_stealth']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=story'?>" class="text-white-shadow"><?php echo $TEXT['store_g_story']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=strategy'?>" class="text-white-shadow"><?php echo $TEXT['store_g_strategy']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=tps'?>" class="text-white-shadow"><?php echo $TEXT['store_g_tps']?></a><br>
				<a href="<?php echo $BASENAME['store'].'?s=g&g=tycoon'?>" class="text-white-shadow"><?php echo $TEXT['store_g_tycoon']?></a><br>
			</div>
			<div class="container-big-right content-element">
				<?php echo getStoreProductView();?>
			</div>
			<center>All Product Copyright by Their Developer</center>
			<div style="height: 50px;width: 1%;"></div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>