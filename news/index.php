<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "news-user";
include '../function/model.php';
$_SESSION['refferPage'] = $BASENAME['news'];
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>News - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element">
				<input type="text" name="news_search"  placeholder="<?php echo $TEXT['news_search']?>" class="search-100" id="news_search" value="<?php echo getNewsSearchPlaceholder()?>" onChange="news_search()">
			<div class="container-mini-left">
				<a class="title-white label-title"><?php echo $TEXT['filter']?></a><br>
				<a href="<?php echo $BASENAME['news']?>" class="text-white-shadow"><?php echo $TEXT['news_all']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?f=n'?>" class="text-white-shadow"><?php echo $TEXT['news_newest']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?f=o'?>" class="text-white-shadow"><?php echo $TEXT['news_older']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?f=i'?>" class="text-white-shadow"><?php echo $TEXT['news_important']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?f=tci'?>" class="text-white-shadow"><?php echo $TEXT['news_tci']?></a><br>
				<br>
				<a class="title-white label-title"><?php echo $TEXT['category']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=game'?>" class="text-white-shadow"><?php echo $TEXT['news_game']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=app'?>" class="text-white-shadow"><?php echo $TEXT['news_app']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=dlc'?>" class="text-white-shadow"><?php echo $TEXT['news_dlc']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=content'?>" class="text-white-shadow"><?php echo $TEXT['news_content']?></a><br>
				<hr>
				<a href="<?php echo $BASENAME['news'].'?c=general'?>" class="text-white-shadow"><?php echo $TEXT['news_general']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=release'?>" class="text-white-shadow"><?php echo $TEXT['news_release']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=update'?>" class="text-white-shadow"><?php echo $TEXT['news_update']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=project'?>" class="text-white-shadow"><?php echo $TEXT['news_project']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=developer'?>" class="text-white-shadow"><?php echo $TEXT['news_developer']?></a><br>
				<a href="<?php echo $BASENAME['news'].'?c=server'?>" class="text-white-shadow"><?php echo $TEXT['news_server']?></a><br>
			</div>
			<div class="container-big-right content-element">
				<?php echo getNewsNews();?>
			</div>
			<center>All news written by each developer who write in</center>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>