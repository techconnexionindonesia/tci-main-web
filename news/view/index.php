<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "news-view-user";
include '../../function/model.php';
if (!empty($_GET['id'])) logNewsVisit($_GET['id']);
$news = getNewsArray();
$_SESSION['refferPage'] = $BASENAME['news'].'view/?id='.$news['id'];
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title><?php echo $news['title_html']?></title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>" onLoad='setImgFlag(<?php echo "`".$_SESSION['lang']."`"?>);'>
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-80-default">
			<?php if ($news['avail']){?>
				<div class="inner-big-left content-element">
					<?php echo getNewsPicLeft($news['title_photo'],$news['photos']);?>
					<h1 class="text-big text-white-shadow"><?php echo $news['title']?></h1>
					<label class="text-tiny text-gray"><?php echo $news['datetime'].' '.$TEXT['home_content_news_by1'].' '.$news['user_name'].' '.$TEXT['home_content_news_by2'].' '.$news['id_developer']?></label><br>
					<label class="text-tiny text-white-shadow"><?php echo $TEXT['category'].' : '.$news['category']?></label><br>
					<p class="text-small"><?php echo $news['text']?><br><br>
					<label class="text-tiny text-gray"><?php echo $TEXT['news_visited1'].' '.$news['visit'].' '.$TEXT['news_visited2']?></label>
					<center>
						<label class="text-low-medium text-white-shadow"><?php echo $TEXT['news_reaction']?></label><br>
						<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
							<input type="hidden" name="usr_news_id" value="<?php echo $news['id']?>">
							<input type="submit" name="usr_news_vote_yes" value="Yes" class="<?php echo getNewsBtnVoted($news['id'],'y')?>"><input type="submit" name="usr_news_vote_no" value="No" class="<?php echo getNewsBtnVoted($news['id'],'n')?>">
						</form>
					</center>
					<br><br>
					<?php if (getNewsAvailPrev()){?>
					<a href="<?php echo $BASENAME['news'].'view/?id='.($_GET['id'] - 1)?>"><button class="btn-w40-h40-square btn-transparent left"><b><?php echo '&#60;&nbsp;&nbsp;&nbsp;'.getNewsTitlePrev()?></b></button></a>
					<?php }?>
					<?php if (getNewsAvailNext()){?>
					<a href="<?php echo $BASENAME['news'].'view/?id='.($_GET['id'] + 1)?>"><button class="btn-w40-h40-square btn-transparent right"><b><?php echo getNewsTitleNext().'&nbsp;&nbsp;&nbsp;&#62;'?></b></button></a>
					<?php }?>
				</div>
				<div class="inner-small-right content-element">
					<center>
						<label class="text-high-medium text-white-shadow"><?php echo $TEXT['news_photos']?></label><hr>
						<?php echo getNewsPicRight($news['title_photo'],$news['photos']);?>
						<br><br>
						<label class="text-low-medium text-white-shadow"><?php echo $TEXT['home_content_select_language']?></label><hr>
						<a href="<?php echo $BASENAME['ch_lang'].'?lang=ID'?>"><img class="img-w45-h70-hoverable cursor-hand" style="margin-bottom:10px;margin-left:2.5px;margin-right:2.5px" src="<?php echo $BASENAME['img_html'].'icon_lang_id.png'?>" id="flagId"></a>
						<a href="<?php echo $BASENAME['ch_lang'].'?lang=EN'?>"><img class="img-w45-h70-hoverable cursor-hand" style="margin-bottom:10px;margin-left:2.5px;margin-right:2.5px" src="<?php echo $BASENAME['img_html'].'icon_lang_en.png'?>" id="flagEn"></a>
						<br><br>
						<label class="text-low-medium text-white-shadow"><?php echo $TEXT['option']?></label><hr>
						<?php if ($news['href_avail']){?>
						<a href="<?php echo $news['href']?>"><b><button class="btn-w90-h40-square btn-transparent"><?php echo $TEXT['news_open_link']?></b></button></a><br><br>
						<?php }?>
						<a href="<?php echo $news['link_developer']?>"><button class="btn-w90-h40-square btn-transparent"><b><?php echo $TEXT['open_profile_dev']?></b></button></a><br><br>
						<a href="<?php echo $news['link_writer']?>"><button class="btn-w90-h40-square btn-transparent"><b><?php echo $TEXT['news_open_profile_writer']?></b></button></a>
						<br><br>
						<label class="text-low-medium text-white-shadow"><?php echo $TEXT['news_share']?></label><hr>
						<a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $BASENAME['news'].'view/?id='.$news['id']?>&t=<?php echo $news['title_html']?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on Facebook"><img class="img-w45-h70-circle-hoverable cursor-hand" style="margin-bottom:10px;margin-left:2.5px;margin-right:2.5px" src="<?php echo $BASENAME['img_html'].'icon_sos_fb.png'?>"></a>
						<a href="https://twitter.com/share?url=<?php echo $BASENAME['news'].'view/?id='.$news['id']?>&text=<?php echo $news['title_html']?>" onclick="javascript:window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=300,width=600');return false;" target="_blank" title="Share on Twitter"><img class="img-w45-h70-circle-hoverable cursor-hand" style="margin-bottom:10px;margin-left:2.5px;margin-right:2.5px" src="<?php echo $BASENAME['img_html'].'icon_sos_tw.png'?>"></a>
					</center>
				</div>
			<?php }?>
			<center>______________________________________</center>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	<script type="text/javascript" src="<?php echo $BASENAME['js'].'news-view.js'?>"></script>