<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "topup-myprofile-user";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['my_profile_topup'];
if (empty($_SESSION['usr_id'])) goPage("login","");
if (isTopupAlready($_SESSION['usr_id'])) $loadDiv = "nonform";
else $loadDiv = "form";
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Topup - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript">
			function loadDiv(div){
				if (div == "nonform"){
					document.getElementById("nonform").style.display = "";
				}
			}
		</script>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>" onload="loadDiv('<?php echo $loadDiv?>')">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-50-default content-element">
			<label class="text-big text-white-shadow"><?php echo $TEXT2['topup_balance']?></label>
			<hr>
			<br>
			<!-- Personal Data Section -->
			<div class="textbox-w100-1">
				<label class="text-tiny text-white-shadow"><?php echo $TEXT2['data_personal']?></label>
				<hr>
				<?php $userData = getUserData($_SESSION['usr_id']);?>
				<label class="text-small text-white-shadow"><?php echo $TEXT2['data_id']?> : <?php echo $userData['id_user']?></label><br>
				<label class="text-small text-white-shadow"><?php echo $TEXT2['data_name']?> : <?php echo $userData['front_name'].' '.$userData['back_name']?></label>
			</div>
			<br>
			<!-- Balance Section -->
			<center><?php createBoxUblue1(80,100,250,$TEXT2['data_balance'],$BASENAME['img_html'].'icon_money_white.png',getSym().cvtMoney($userData['balance']));?></center>
			<br>
			<!-- Form and Nonform Section -->
			<div class="textbox-w100-1">
				<div id="nonform" style="display: none">
					<label class="text-medium text-white"><?php echo $TEXT2['in_process']?></label><hr>
					<label class="text-small"><?php echo $LONGTEXT['topup_wait']?></label>
					<br><br>
				</div>
				<label class="text-tiny text-white-shadow"><?php echo $TEXT2['topup_balance']?></label>
				<hr>
				<label class="text-small"><?php echo $LONGTEXT['topup_info']?></label><br><br>
				<a class="text-low-small" href="<?php echo $BASENAME['about_us_rule']?>"><?php echo $TEXT['about_menu_rule']?></a><br><br>
				<div id="form">
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>" enctype="multipart/form-data">
						<label class="text-tiny text-white-shadow">Please fill the form below</label>
						<hr>
						<table class="table-double-w100">
							<tr>
								<td class="sl">Channel Negara Pembayaran</td>
								<td class="sr">: 
									<select id="usr_topup_channel" name="usr_topup_channel" class="input-text-border-white-w90-h30">
										<option value="id">Indonesia</option>
									</select><br>
								</td>
							</tr>
							<tr>
								<td class="sl"><?= $TEXT2['topup_amount']?></td><td class="sr">: <span id="currencySymbol">Rp</span>&nbsp;&nbsp;<input type="number" name="usr_topup_amount" class="input-text-noborder-gray-w90-h30" placeholder="Jumlah Topup" min="30000" required><span class="text-tiny"><?= $TEXT2['topup_no_decimal']?></span></td>
							</tr>
						</table>
						<br>
						<center><input class="btn-w40-h40-square btn-green" type="submit" name="usr_topup_submit" value="Submit"></center>
					</form>
				</div>
				<br><br>
				<a href="<?php echo $BASENAME['my_profile']?>"><button class="btn-w40-h40-square btn-transparent"><- <?php echo $TEXT2['cmd_back_prevpage']?></button></a>
			</div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'topup.js'?>"></script>