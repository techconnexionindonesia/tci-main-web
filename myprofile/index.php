<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "myprofile-user";
include '../function/model.php';
$_SESSION['refferPage'] = $BASENAME['my_profile'];
if (empty($_SESSION['usr_id'])) goPage("login","");
$usr_id = $_SESSION['usr_id'];
$myData = getUserData($usr_id);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>My Profile - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element">
			<div style="width: 100%;">
				<div class="left" style="width: 22%;padding: 2%;">
					<div class="center">
						<img class="img-w90-h220-circle-hoverable" src="<?php echo $BASENAME['img_avatar'].$myData['avatar_link']?>"><br><br>
						<div class="textbox-w100-1">
							<label class="text-small text-white-shadow">Change Picture :</label><br>
							<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>" enctype="multipart/form-data">
								<input type="file" name="usr_update_avatar" accept=".jpg,.jpeg,.png,.bmp,.gif" onchange="this.form.submit()">
							</form>
						</div>
					</div>
					<br><br><br>
					<?php if (isDevAlreadySignup($_SESSION['usr_id']) && !isDevWaitValid($_SESSION['usr_id'])){?>
					<div class="content-center-big-fsfh-default content-element-blue" style="width:90%;height: 400px;padding: 5%;">
						<?php $devData = getDevDataByUsrId($_SESSION['usr_id']);?>
						<label class="text-small text-white-shadow"><?php echo $TEXT2['data_dev_related']?></label><br>
						<hr>
						<a href="<?php echo $BASENAME['developer'].'view/?id='.$devData['id_developer']?>">
						<img class="img-w100-h170" src="<?php echo $BASENAME['img_dev'].$devData['pic_logo']?>"><br>
						</a>
						<center><label class="text-small text-white-shadow"><?php echo $devData['name']?></label><br><br>
						<label class="text-big text-white-shadow"><?php echo getDevPosition($_SESSION['usr_id'])?></label></center><br><br>
						<label class="text-small text-white-shadow"><?php echo $TEXT2['data_dev_rating']?></label><br>
						<hr>
						<center><label class="text-big text-white-shadow"><?php echo calculateDevRating($devData['id_developer']).'% / '.getDevTotalVote($devData['id_developer'])?></label><br><br>
					</div>
					<?php }?>
				</div>
				<div class="left" style="width: 62%;padding: 2%;">
					<label class="text-big text-white-shadow"><?php echo $myData['front_name'].' '.$myData['back_name']?></label><br>
					<label class="text-small text-white-shadow"><?php echo $TEXT2['data_id'].' : '.$myData['id_user']?></label><br>
					<br>
					<div class="textbox-w100-1">
						<label class="text-low-small text-white-shadow left"><?php echo $TEXT2['select'].' : '?></label>&nbsp;&nbsp;&nbsp;
						<a href="<?php echo $BASENAME['my_profile']?>"><label class="text-block-hover-gray text-low-medium left" style="margin-left: 2%;margin-top: -1%;"><?php echo $TEXT2['data_profile']?></label></a>
						<a href="<?php echo $BASENAME['my_transaction']?>"><label class="text-block-hover-gray text-low-medium left" style="margin-left: 3%;margin-top: -1%;"><?php echo $TEXT2['data_transaction']?></label></a>
						<a href="<?php echo $BASENAME['my_profile_security']?>"><label class="text-block-hover-gray text-low-medium left" style="margin-left: 3%;margin-top: -1%;"><?php echo $TEXT2['data_profile_security']?></label></a>
					</div>
					<br><br>
					<div class="content-center-big-100-default content-element">
						<div class="textbox-w100-1">
							<label class="text-tiny text-white-shadow"><?php echo $TEXT2['data_personal']?></label><br>
						<hr>
						<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
						<table class="table-double-w100">
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_front_name']?></label></td><td class="sr">: <input class="input-text-noborder-gray-w90-h30 text-small" type="text" name="usr_update_frontname" value="<?php echo $myData['front_name']?>"></td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_back_name']?></label></td><td class="sr">: <input class="input-text-noborder-gray-w90-h30 text-small" type="text" name="usr_update_backname" value="<?php echo $myData['back_name']?>"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_birth']?></label></td><td class="sr">: <input class="input-text-border-white-w90-h30 text-small" type="date" name="usr_update_birth" value="<?php echo $myData['birth']?>"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_address']?></label></td><td class="sr">: <textarea class="textbox-w100-1" name="usr_update_address" style="width: 90%;"><?php echo $myData['address']?></textarea></td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_zipcode']?></label></td><td class="sr">: <input class="input-text-noborder-gray-w90-h30 text-small" type="number" name="usr_update_zipcode" value="<?php echo $myData['zipcode']?>"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_country']?></label></td><td class="sr">: <select class="input-text-border-white-w90-h30" name="usr_update_country">
									<option value="">-- <?php echo $TEXT2['cmd_select_country_update']?> --</option>
									<option value="AF">Afghanistan</option>
									<option value="AX">Åland Islands</option>
									<option value="AL">Albania</option>
									<option value="DZ">Algeria</option>
									<option value="AS">American Samoa</option>
									<option value="AD">Andorra</option>
									<option value="AO">Angola</option>
									<option value="AI">Anguilla</option>
									<option value="AQ">Antarctica</option>
									<option value="AG">Antigua and Barbuda</option>
									<option value="AR">Argentina</option>
									<option value="AM">Armenia</option>
									<option value="AW">Aruba</option>
									<option value="AU">Australia</option>
									<option value="AT">Austria</option>
									<option value="AZ">Azerbaijan</option>
									<option value="BS">Bahamas</option>
									<option value="BH">Bahrain</option>
									<option value="BD">Bangladesh</option>
									<option value="BB">Barbados</option>
									<option value="BY">Belarus</option>
									<option value="BE">Belgium</option>
									<option value="BZ">Belize</option>
									<option value="BJ">Benin</option>
									<option value="BM">Bermuda</option>
									<option value="BT">Bhutan</option>
									<option value="BO">Bolivia, Plurinational State of</option>
									<option value="BQ">Bonaire, Sint Eustatius and Saba</option>
									<option value="BA">Bosnia and Herzegovina</option>
									<option value="BW">Botswana</option>
									<option value="BV">Bouvet Island</option>
									<option value="BR">Brazil</option>
									<option value="IO">British Indian Ocean Territory</option>
									<option value="BN">Brunei Darussalam</option>
									<option value="BG">Bulgaria</option>
									<option value="BF">Burkina Faso</option>
									<option value="BI">Burundi</option>
									<option value="KH">Cambodia</option>
									<option value="CM">Cameroon</option>
									<option value="CA">Canada</option>
									<option value="CV">Cape Verde</option>
									<option value="KY">Cayman Islands</option>
									<option value="CF">Central African Republic</option>
									<option value="TD">Chad</option>
									<option value="CL">Chile</option>
									<option value="CN">China</option>
									<option value="CX">Christmas Island</option>
									<option value="CC">Cocos (Keeling) Islands</option>
									<option value="CO">Colombia</option>
									<option value="KM">Comoros</option>
									<option value="CG">Congo</option>
									<option value="CD">Congo, the Democratic Republic of the</option>
									<option value="CK">Cook Islands</option>
									<option value="CR">Costa Rica</option>
									<option value="CI">Côte d'Ivoire</option>
									<option value="HR">Croatia</option>
									<option value="CU">Cuba</option>
									<option value="CW">Curaçao</option>
									<option value="CY">Cyprus</option>
									<option value="CZ">Czech Republic</option>
									<option value="DK">Denmark</option>
									<option value="DJ">Djibouti</option>
									<option value="DM">Dominica</option>
									<option value="DO">Dominican Republic</option>
									<option value="EC">Ecuador</option>
									<option value="EG">Egypt</option>
									<option value="SV">El Salvador</option>
									<option value="GQ">Equatorial Guinea</option>
									<option value="ER">Eritrea</option>
									<option value="EE">Estonia</option>
									<option value="ET">Ethiopia</option>
									<option value="FK">Falkland Islands (Malvinas)</option>
									<option value="FO">Faroe Islands</option>
									<option value="FJ">Fiji</option>
									<option value="FI">Finland</option>
									<option value="FR">France</option>
									<option value="GF">French Guiana</option>
									<option value="PF">French Polynesia</option>
									<option value="TF">French Southern Territories</option>
									<option value="GA">Gabon</option>
									<option value="GM">Gambia</option>
									<option value="GE">Georgia</option>
									<option value="DE">Germany</option>
									<option value="GH">Ghana</option>
									<option value="GI">Gibraltar</option>
									<option value="GR">Greece</option>
									<option value="GL">Greenland</option>
									<option value="GD">Grenada</option>
									<option value="GP">Guadeloupe</option>
									<option value="GU">Guam</option>
									<option value="GT">Guatemala</option>
									<option value="GG">Guernsey</option>
									<option value="GN">Guinea</option>
									<option value="GW">Guinea-Bissau</option>
									<option value="GY">Guyana</option>
									<option value="HT">Haiti</option>
									<option value="HM">Heard Island and McDonald Islands</option>
									<option value="VA">Holy See (Vatican City State)</option>
									<option value="HN">Honduras</option>
									<option value="HK">Hong Kong</option>
									<option value="HU">Hungary</option>
									<option value="IS">Iceland</option>
									<option value="IN">India</option>
									<option value="ID">Indonesia</option>
									<option value="IR">Iran, Islamic Republic of</option>
									<option value="IQ">Iraq</option>
									<option value="IE">Ireland</option>
									<option value="IM">Isle of Man</option>
									<option value="IL">Israel</option>
									<option value="IT">Italy</option>
									<option value="JM">Jamaica</option>
									<option value="JP">Japan</option>
									<option value="JE">Jersey</option>
									<option value="JO">Jordan</option>
									<option value="KZ">Kazakhstan</option>
									<option value="KE">Kenya</option>
									<option value="KI">Kiribati</option>
									<option value="KP">Korea, Democratic People's Republic of</option>
									<option value="KR">Korea, Republic of</option>
									<option value="KW">Kuwait</option>
									<option value="KG">Kyrgyzstan</option>
									<option value="LA">Lao People's Democratic Republic</option>
									<option value="LV">Latvia</option>
									<option value="LB">Lebanon</option>
									<option value="LS">Lesotho</option>
									<option value="LR">Liberia</option>
									<option value="LY">Libya</option>
									<option value="LI">Liechtenstein</option>
									<option value="LT">Lithuania</option>
									<option value="LU">Luxembourg</option>
									<option value="MO">Macao</option>
									<option value="MK">Macedonia, the former Yugoslav Republic of</option>
									<option value="MG">Madagascar</option>
									<option value="MW">Malawi</option>
									<option value="MY">Malaysia</option>
									<option value="MV">Maldives</option>
									<option value="ML">Mali</option>
									<option value="MT">Malta</option>
									<option value="MH">Marshall Islands</option>
									<option value="MQ">Martinique</option>
									<option value="MR">Mauritania</option>
									<option value="MU">Mauritius</option>
									<option value="YT">Mayotte</option>
									<option value="MX">Mexico</option>
									<option value="FM">Micronesia, Federated States of</option>
									<option value="MD">Moldova, Republic of</option>
									<option value="MC">Monaco</option>
									<option value="MN">Mongolia</option>
									<option value="ME">Montenegro</option>
									<option value="MS">Montserrat</option>
									<option value="MA">Morocco</option>
									<option value="MZ">Mozambique</option>
									<option value="MM">Myanmar</option>
									<option value="NA">Namibia</option>
									<option value="NR">Nauru</option>
									<option value="NP">Nepal</option>
									<option value="NL">Netherlands</option>
									<option value="NC">New Caledonia</option>
									<option value="NZ">New Zealand</option>
									<option value="NI">Nicaragua</option>
									<option value="NE">Niger</option>
									<option value="NG">Nigeria</option>
									<option value="NU">Niue</option>
									<option value="NF">Norfolk Island</option>
									<option value="MP">Northern Mariana Islands</option>
									<option value="NO">Norway</option>
									<option value="OM">Oman</option>
									<option value="PK">Pakistan</option>
									<option value="PW">Palau</option>
									<option value="PS">Palestinian Territory, Occupied</option>
									<option value="PA">Panama</option>
									<option value="PG">Papua New Guinea</option>
									<option value="PY">Paraguay</option>
									<option value="PE">Peru</option>
									<option value="PH">Philippines</option>
									<option value="PN">Pitcairn</option>
									<option value="PL">Poland</option>
									<option value="PT">Portugal</option>
									<option value="PR">Puerto Rico</option>
									<option value="QA">Qatar</option>
									<option value="RE">Réunion</option>
									<option value="RO">Romania</option>
									<option value="RU">Russian Federation</option>
									<option value="RW">Rwanda</option>
									<option value="BL">Saint Barthélemy</option>
									<option value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
									<option value="KN">Saint Kitts and Nevis</option>
									<option value="LC">Saint Lucia</option>
									<option value="MF">Saint Martin (French part)</option>
									<option value="PM">Saint Pierre and Miquelon</option>
									<option value="VC">Saint Vincent and the Grenadines</option>
									<option value="WS">Samoa</option>
									<option value="SM">San Marino</option>
									<option value="ST">Sao Tome and Principe</option>
									<option value="SA">Saudi Arabia</option>
									<option value="SN">Senegal</option>
									<option value="RS">Serbia</option>
									<option value="SC">Seychelles</option>
									<option value="SL">Sierra Leone</option>
									<option value="SG">Singapore</option>
									<option value="SX">Sint Maarten (Dutch part)</option>
									<option value="SK">Slovakia</option>
									<option value="SI">Slovenia</option>
									<option value="SB">Solomon Islands</option>
									<option value="SO">Somalia</option>
									<option value="ZA">South Africa</option>
									<option value="GS">South Georgia and the South Sandwich Islands</option>
									<option value="SS">South Sudan</option>
									<option value="ES">Spain</option>
									<option value="LK">Sri Lanka</option>
									<option value="SD">Sudan</option>
									<option value="SR">Suriname</option>
									<option value="SJ">Svalbard and Jan Mayen</option>
									<option value="SZ">Swaziland</option>
									<option value="SE">Sweden</option>
									<option value="CH">Switzerland</option>
									<option value="SY">Syrian Arab Republic</option>
									<option value="TW">Taiwan, Province of China</option>
									<option value="TJ">Tajikistan</option>
									<option value="TZ">Tanzania, United Republic of</option>
									<option value="TH">Thailand</option>
									<option value="TL">Timor-Leste</option>
									<option value="TG">Togo</option>
									<option value="TK">Tokelau</option>
									<option value="TO">Tonga</option>
									<option value="TT">Trinidad and Tobago</option>
									<option value="TN">Tunisia</option>
									<option value="TR">Turkey</option>
									<option value="TM">Turkmenistan</option>
									<option value="TC">Turks and Caicos Islands</option>
									<option value="TV">Tuvalu</option>
									<option value="UG">Uganda</option>
									<option value="UA">Ukraine</option>
									<option value="AE">United Arab Emirates</option>
									<option value="GB">United Kingdom</option>
									<option value="US">United States</option>
									<option value="UM">United States Minor Outlying Islands</option>
									<option value="UY">Uruguay</option>
									<option value="UZ">Uzbekistan</option>
									<option value="VU">Vanuatu</option>
									<option value="VE">Venezuela, Bolivarian Republic of</option>
									<option value="VN">Viet Nam</option>
									<option value="VG">Virgin Islands, British</option>
									<option value="VI">Virgin Islands, U.S.</option>
									<option value="WF">Wallis and Futuna</option>
									<option value="EH">Western Sahara</option>
									<option value="YE">Yemen</option>
									<option value="ZM">Zambia</option>
									<option value="ZW">Zimbabwe</option>
								</select></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_email']?></label></td><td class="sr">: <input class="input-text-noborder-gray-w90-h30 text-small" type="email" name="usr_update_email" value="<?php echo $myData['email']?>"></td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_phone']?></label></td><td class="sr">: <input class="input-text-noborder-gray-w90-h30 text-small" type="number" name="usr_update_phone" value="<?php echo $myData['phone']?>"></td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_signature']?></label></td><td class="sr">: <input class="input-text-noborder-gray-w90-h30 text-small" type="text" name="usr_update_signature" value="<?php echo $myData['signature']?>"></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="sl"><label class="text-small text-white-shadow"><?php echo $TEXT['signup_description']?></label></td><td class="sr">: <textarea class="textbox-w100-1" name="usr_update_description" style="width: 90%;"><?php echo $myData['description']?></textarea></td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
							<tr>
								<td class="center" colspan="2"><input class="btn-w40-h40-square btn-green" type="submit" name="usr_update_submit" value="<?php echo $TEXT2['cmd_apply_update']?>"></td>
							</tr>
						</table>
						</form>
						</div>
						<br>
						<div class="textbox-w100-1" style="height: auto;">
							<div style="width: 100%;min-height: 150px;height: auto;">
								<div class="left center" style="width: 42%;padding: 2%;">
									<?php createBoxUblue1(100,120,150,$TEXT2['data_balance'],$BASENAME['img_html'].'icon_money_white.png',getSym().cvtMoney(getMyBalance()))?>
								</div>
								<div class="left center" style="width: 42%;padding: 9% 2% 2% 2%;">
									<a href="<?php echo $BASENAME['my_profile_topup']?>"><button class="btn-w90-h40-square btn-green"><?php echo $TEXT2['topup_balance']?></button></a>
								</div>
							</div>
							<br>
							<?php
							$query = mysql_query("SELECT * FROM user_user_recharge WHERE id_user = '$usr_id' LIMIT 20");
							if (mysql_num_rows($query) > 0){?>
							<table class="table-w100-h20-blue center">
								<tr>
									<th><?php echo $TEXT2['topup_balance']?></th>
									<th><?php echo $TEXT2['data_balance_e']?></th>
									<th><?php echo $TEXT2['data_datetime']?></th>
									<th><?php echo $TEXT2['data_info']?></th>
								</tr>
								<?php 
								while ($rs = mysql_fetch_array($query)) {?>
								<tr>
									<td><?php echo getSym().cvtMoney($rs['recharge'])?></td>
									<td><?php echo getSym().cvtMoney($rs['e_balance'])?></td>
									<td><?php echo $rs['datetime']?></td>
									<td><?php echo $rs['info']?></td>
								</tr>
								<?php } ?>
							</table>
							<?php }
							$query = mysql_query("SELECT COUNT(id_user) AS num , SUM(recharge) AS total FROM user_user_recharge WHERE id_user = '$usr_id'");
							$rs = mysql_fetch_array($query);?>
							<br>
							<div class="textbox-w100-1 center">
								<label class="text-medium text-white-shadow"><?php echo $TEXT2['data_total_topup'].' : '.getSym().cvtMoney($rs['total'])?></label><br>
								<label class="text-medium text-white-shadow"><?php echo $TEXT2['data_total_topup_count'].' : '.$rs['num'].'x'?></label><br>
							</div>
						</div>
						<br>
						<div class="textbox-w100-1">
							<label class="text-tiny text-white-shadow"><?php echo $TEXT2['devconsole_avt_actvote'].' '.$TEXT2['data_product']?></label><br>
							<hr><br>
							<?php
							$query = mysql_query("SELECT * FROM user_vote_stuff WHERE id_user = $usr_id LIMIT 20");
							if (mysql_num_rows($query) > 0){?>
							<table class="table-w100-h20-blue center">
								<tr>
									<th><?php echo $TEXT2['data_product']?></th>
									<th><?php echo $TEXT2['data_vote']?></th>
									<th><?php echo $TEXT2['data_datetime']?></th>
								</tr>
								<?php
								while ($rs = mysql_fetch_array($query)) {
									$x_pid = $rs['id_stuff'];
									$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
											UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
											UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
											UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
										if (mysql_num_rows($query_p) == 1){
											$rs2 = mysql_fetch_array($query_p);
											$t_p = $rs2['title'];
										}else $t_p = $x_pid;
									if ($rs['vote'] == "y") $t_v = $BASENAME['img_html'].'vote_yes.png';
									else $t_v = $BASENAME['img_html'].'vote_no.png';
									?>
									<tr>
										<td><?php echo $t_p ?></td>
										<td><img src="<?php echo $t_v ?>" style="width: 60px;height: 25px;"></td>
										<td><?php echo $rs['datetime']?></td>
									</tr>
								<?php }?>
							</table>
							<?php }?>
							<br>
							<label class="text-tiny text-white-shadow"><?php echo $TEXT2['devconsole_avt_actvote'].' '.$TEXT['home_content_news']?></label><br>
							<hr><br>
							<?php
							$query = mysql_query("SELECT * FROM user_vote_news WHERE id_user = $usr_id LIMIT 20");
							if (mysql_num_rows($query) > 0){?>
							<table class="table-w100-h20-blue center">
								<tr>
									<th><?php echo $TEXT['home_content_news']?></th>
									<th><?php echo $TEXT2['data_vote']?></th>
								</tr>
								<?php
								while ($rs = mysql_fetch_array($query)) {
									$x_pid = $rs['id_news'];
									$query_p = mysql_query("SELECT title FROM tci_news WHERE id_news = '$x_pid'") or die (mysql_error());
										if (mysql_num_rows($query_p) == 1){
											$rs2 = mysql_fetch_array($query_p);
											$t_p = $rs2['title'];
										}else $t_p = $x_pid;
									if ($rs['vote'] == "y") $t_v = $BASENAME['img_html'].'vote_yes.png';
									else $t_v = $BASENAME['img_html'].'vote_no.png';
									?>
									<tr>
										<td><?php echo getStringFromArray($t_p) ?></td>
										<td><img src="<?php echo $t_v ?>" style="width: 60px;height: 25px;"></td>
									</tr>
								<?php }?>
							</table>
							<?php }
							$query = mysql_query("SELECT COUNT(id_user) AS num FROM user_vote_stuff WHERE id_user = $usr_id
								UNION SELECT COUNT(id_user) AS num FROM user_vote_news WHERE id_user = $usr_id");
							$x_count = 0;
							while ($rs = mysql_fetch_array($query)) {
							 	$x_count = $x_count + $rs['num'];
							 } ?>
							<br>
							<div class="textbox-w100-1 center">
								<label class="text-medium text-white-shadow"><?php echo $TEXT2['data_total_vote_count'].' : '.$x_count.'x'?></label><br>
							</div>
						</div>
						<br>
						<div class="textbox-w100-1">
							<label class="text-tiny text-white-shadow"><?php echo $TEXT2['devconsole_avt_actreview']?></label><br>
							<hr><br>
							<?php
							$query = mysql_query("SELECT id_user,id_stuff,text,datetime FROM user_review,tci_game WHERE id_stuff = id_game AND id_user = '$usr_id'
								UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_dlc WHERE id_stuff = id_dlc AND id_user = '$usr_id'
								UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_content WHERE id_stuff = id_content AND id_user = '$usr_id'
								UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_app WHERE id_stuff = id_app AND id_user = '$usr_id' ORDER BY datetime DESC") or die(mysql_error());
							if (mysql_num_rows($query) > 0){
							?>
							<table class="table-w100-h20-blue text-low-tiny center">
								<tr>
									<th><?php echo $TEXT2['devconsole_avt_product']?></th>
									<th><?php echo $TEXT2['devconsole_avt_review']?></th>
									<th><?php echo $TEXT2['devconsole_avt_time']?></th>
								</tr>
								<?php
								while ($rs = mysql_fetch_array($query)) {
									$x_usrid = $rs['id_user'];
									$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid");
									if (mysql_num_rows($query_usr) == 1){
										$rs2 = mysql_fetch_array($query_usr);
										$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
									}else $t_usr = $rs['subject'];
									$x_pid = $rs['id_stuff'];
									$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
											UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
											UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
											UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
										if (mysql_num_rows($query_p) == 1){
											$rs2 = mysql_fetch_array($query_p);
											$t_p = $rs2['title'];
										}else $t_p = $x_pid;
									?>
								<tr>
									<td ><?php echo $t_p ?></td>
									<td><?php echo mb_strimwidth($rs['text'], 0, 100, "...");?></td>
									<td><?php echo $rs['datetime']?></td>
								<?php }?>
							</table>
							<?php }?>
							<br>
							<?php
							$query = mysql_query("SELECT @temp AS num FROM user_review WHERE id_user = '$usr_id'");
							$x_num = mysql_num_rows($query);?>
							<br>
							<div class="textbox-w100-1 center">
								<label class="text-medium text-white-shadow"><?php echo $TEXT2['data_total_topup_count'].' : '.$x_num.'x'?></label><br>
							</div>
						</div>
					</div>
				</div>
			</div>
			__________________________________________________________________________________________________________________________________________________________
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>