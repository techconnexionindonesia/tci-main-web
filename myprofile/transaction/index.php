<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "transaction-myprofile-user";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['my_transaction'];
if (empty($_SESSION['usr_id'])) goPage("login","");
$usr_id = $_SESSION['usr_id'];
$myData = getUserData($usr_id);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>My Transaction - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element">
			<div style="width: 100%;">
				<div class="left" style="width: 22%;padding: 2%;">
					<div class="center">
						<img class="img-w90-h190-circle-hoverable" src="<?php echo $BASENAME['img_avatar'].$myData['avatar_link']?>"><br><br>
						<div class="textbox-w100-1">
							<label class="text-small text-white-shadow">Change Picture :</label><br>
							<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>" enctype="multipart/form-data">
								<input type="file" name="usr_update_avatar" accept=".jpg,.jpeg,.png,.bmp,.gif" onchange="this.form.submit()">
							</form>
						</div>
					</div>
					<br><br><br>
					<?php if (isDevAlreadySignup($_SESSION['usr_id']) && !isDevWaitValid($_SESSION['usr_id'])){?>
					<div class="content-center-big-fsfh-default content-element-blue" style="width:90%;height: 400px;padding: 5%;">
						<?php $devData = getDevDataByUsrId($_SESSION['usr_id']);?>
						<label class="text-small text-white-shadow"><?php echo $TEXT2['data_dev_related']?></label><br>
						<hr>
						<a href="<?php echo $BASENAME['developer'].'view/?id='.$devData['id_developer']?>">
						<img class="img-w100-h170" src="<?php echo $BASENAME['img_dev'].$devData['pic_logo']?>"><br>
						</a>
						<center><label class="text-small text-white-shadow"><?php echo $devData['name']?></label><br><br>
						<label class="text-big text-white-shadow"><?php echo getDevPosition($_SESSION['usr_id'])?></label></center><br><br>
						<label class="text-small text-white-shadow"><?php echo $TEXT2['data_dev_rating']?></label><br>
						<hr>
						<center><label class="text-big text-white-shadow"><?php echo calculateDevRating($devData['id_developer']).'% / '.getDevTotalVote($devData['id_developer'])?></label><br><br>
					</div>
					<?php }?>
				</div>
				<div class="left" style="width: 62%;padding: 2%;">
					<label class="text-big text-white-shadow"><?php echo $myData['front_name'].' '.$myData['back_name']?></label><br>
					<label class="text-small text-white-shadow"><?php echo $TEXT2['data_id'].' : '.$myData['id_user']?></label><br>
					<br>
					<div class="textbox-w100-1">
						<label class="text-low-small text-white-shadow left"><?php echo $TEXT2['select'].' : '?></label>&nbsp;&nbsp;&nbsp;
						<a href="<?php echo $BASENAME['my_profile']?>"><label class="text-block-hover-gray text-low-medium left" style="margin-left: 2%;margin-top: -1%;"><?php echo $TEXT2['data_profile']?></label></a>
						<a href="<?php echo $BASENAME['my_transaction']?>"><label class="text-block-hover-gray text-low-medium left" style="margin-left: 3%;margin-top: -1%;"><?php echo $TEXT2['data_transaction']?></label></a>
						<a href="<?php echo $BASENAME['my_profile_security']?>"><label class="text-block-hover-gray text-low-medium left" style="margin-left: 3%;margin-top: -1%;"><?php echo $TEXT2['data_profile_security']?></label></a>
					</div>
					<br><br>
					<div class="content-center-big-100-default content-element">
						<br><br>
						<div class="textbox-w100-1">
							<label class="text-tiny text-white-shadow"><?php echo $TEXT2['data_product_bought']?></label><br>
							<hr>
							<?php 
							$x_avail = getUsrProductBoughtAll($_SESSION['usr_id']);
							if (!$x_avail){?>
							<label class="text-low-medium text-white-shadow"><?php echo $TEXT2['user_product_none2']?></label><br>
							<br>
							<center>
								<a href="<?php echo $BASENAME['store']?>"><button class="btn-w40-h40-square btn-transparent"><?php echo $TEXT['navbar_store']?></button></a>&nbsp;&nbsp;&nbsp;<a href="<?php echo $BASENAME['content']?>"><button class="btn-w40-h40-square btn-transparent"><?php echo $TEXT['navbar_content']?></button></a>
							</center>
							<?php }?>
							______________________________________________________________________________
						</div>
					</div>
				</div>
			</div>
			__________________________________________________________________________________________________________________________________________________________
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>