<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "dev-view-user";
include '../../function/model.php';
if (empty($_GET['id'])) goBack();
$_SESSION['refferPage'] = $BASENAME['developer'].'view/?id='.$_GET['id'];
$devData = getDevData($_GET['id']);
if ($devData['valid'] == 0) goPage("index","");
$dev_create = explode(";", $devData['create']);
$dev_stats = getDevStats($_GET['id']);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=device-width, maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title><?php echo $devData['name']?> - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element">
			<div>
				<div class="content-center-big-fs-default content-element left" style="width: 25%;margin-right: 5%;">
					<center>
						<img class="img-w100-h170" src="<?php echo $devData['pic_link']?>">
						<br><br>
							<div class="content-center-big-fsfh-default content-element-dark" style="padding-bottom: 5px;">
								<font class="text-low-medium text-white-shadow"><?php echo $devData['name']?></font><br>
								<font class="text-tiny text-white-shadow"><?php echo $devData['id_developer']?></font><br>
							</div>
					</center>
					<br>
					<table class="table-double-w100">
						<tr>
							<?php if (in_array("gamedlc", $dev_create)){?>
							<td>
								<div style="width: 100%;">
									<img src="<?php echo $BASENAME['img_html'].'check_1.png'?>" style="width: 100%;height: 45px;">
									<div style="margin-top: -40px;margin-left: 50%;"><label class="text-tiny text-white-shadow">GAME</label></div>
								</div>
							</td>
							<td>
								<div style="width: 100%;">
									<img src="<?php echo $BASENAME['img_html'].'check_1.png'?>" style="width: 100%;height: 45px;">
									<div style="margin-top: -40px;margin-left: 50%;"><label class="text-tiny text-white-shadow">DLC</label></div>
								</div>
							</td>
							<?php }?>
						</tr>
						<tr>
							<td colspan="2"><div style="height: 20px;"></td>
						</tr>
						<tr>
							<td>
								<?php if (in_array("content", $dev_create)){?>
								<div style="width: 100%;">
									<img src="<?php echo $BASENAME['img_html'].'check_1.png'?>" style="width: 100%;height: 45px;">
									<div style="margin-top: -40px;margin-left: 40%;"><label class="text-tiny text-white-shadow">CONTENT</label></div>
								</div>
								<?php }?>
							</td>
							<td>
								<?php if (in_array("app", $dev_create)){?>
								<div style="width: 100%;">
									<img src="<?php echo $BASENAME['img_html'].'check_1.png'?>" style="width: 100%;height: 45px;">
									<div style="margin-top: -40px;margin-left: 50%;"><label class="text-tiny text-white-shadow">APP</label></div>
								</div>
								<?php }?>
							</td>
						</tr>
					</table>
					<br><br>
					<center>
						<div style="width: 80%;">
							<img src="<?php echo $BASENAME['img_html'].'dev_stc.png'?>" style="width: 100%;height: 45px;">
							<div style="margin-top: -45px;margin-left: 10%;"><label class="text-medium text-white-shadow"><?php echo $devData['type_structure']?></label></div>
						</div>
						<br>
						<div style="width: 80%;">
							<img src="<?php echo $BASENAME['img_html'].'dev_plc.png'?>" style="width: 100%;height: 45px;">
							<div style="margin-top: -43px;margin-left: 10%;"><label class="text-medium text-white-shadow"><?php echo $devData['type_place']?></label></div>
						</div>
						<br><br>
						<a href="mailto:<?php echo $devData['email']?>"><button class="btn-w90-h40-square btn-transparent text-low-medium"><?php echo $TEXT2['data_email']?></button></a>
						<br><br>
						<?php if (!empty($devData['site'])){?><a href="<?php echo $devData['site']?>"><button class="btn-w90-h40-square btn-transparent text-low-medium"><?php echo $TEXT2['data_website']?></button></a><?php }?>
					</center>
				</div>
				<div class="right" style="width: 61%;">
					<div class="content-center-fsfh-default content-element" style="width: 95%;height: auto;">
						<label class="text-big text-white-shadow"><?php echo $TEXT2['data_description']?></label>
						<hr>
						<textarea class="textbox-w100-1"><?php echo $devData['description']?></textarea>
					</div>
					<div class="content-center-fsfh-default content-element" style="width: 95%;height: auto;">
						<center>
							<div style="height: 130px;">
								<div style="width:45%;" class="left"><?php echo createBoxUblue3(100,100,250,$TEXT2['data_total_product'],$BASENAME['img_html'].'icon_product_white.png',$dev_stats['total_product']);?></div>
								<span style="width: 5%;height: 1px" class="left"></span>
								<div style="width:45%;" class="left"><?php echo createBoxUblue3(100,100,250,$TEXT2['data_total_rating'],$BASENAME['img_html'].'icon_star_white.png',$dev_stats['rate_yes'].'% / '.$dev_stats['total_rate']);?></div>
							</div>
						</center>
					</div>
					<div class="content-center-fsfh-default content-element" style="width: 95%;height: auto;">
						<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
							<input class="search-100" type="text" name="usr_dev_project_search" placeholder="<?php echo $TEXT2['developer_search_by'].' '.$devData['name']?>" value="<?php echo getDevProjectSearchPlaceholder()?>">
						</form>
						<br><br>
						<!--Game Section-->
						<?php if (in_array("gamedlc", $dev_create)){?>
						<img class="img-border-shadow" src="<?php echo $BASENAME['img_html'].'icon_game.png'?>" style="width:10%;height: 50px;"><label class="text-big text-white-shadow">&nbsp;&nbsp;<?php echo $TEXT2['data_total_game'].' : '.$dev_stats['total_game']?></label>
						<hr>
						<?php echo getDevProject($_GET['id'],"game")?>
						<div style="width: 100%;height: 200px"></div>
						<br><br>
						<center><a href="<?php echo $BASENAME['store'].'?q='.$_GET['id']?>"><button class="btn-w90-h40-square btn-green"><?php echo $TEXT2['see_more']?></button></a></center>
						<?php }?>
						<!--DLC Section-->
						<?php if (in_array("gamedlc", $dev_create)){?>
						<br><br>
						<img class="img-border-shadow" src="<?php echo $BASENAME['img_html'].'icon_dlc.png'?>" style="width:10%;height: 50px;"><label class="text-big text-white-shadow">&nbsp;&nbsp;<?php echo $TEXT2['data_total_dlc'].' : '.$dev_stats['total_dlc']?></label>
						<hr>
						<?php echo getDevProject($_GET['id'],"dlc")?>
						<div style="width: 100%;height: 200px"></div>
						<br><br>
						<center><a href="<?php echo $BASENAME['content'].'?q='.$_GET['id']?>"><button class="btn-w90-h40-square btn-green"><?php echo $TEXT2['see_more']?></button></a></center>
						<?php }?>
						<!--Content Section-->
						<?php if (in_array("content", $dev_create)){?>
						<br><br>
						<img class="img-border-shadow" src="<?php echo $BASENAME['img_html'].'icon_content.png'?>" style="width:10%;height: 50px;"><label class="text-big text-white-shadow">&nbsp;&nbsp;<?php echo $TEXT2['data_total_content'].' : '.$dev_stats['total_content']?></label>
						<hr>
						<?php echo getDevProject($_GET['id'],"content")?>
						<div style="width: 100%;height: 200px"></div>
						<br><br>
						<center><a href="<?php echo $BASENAME['content'].'?q='.$_GET['id']?>"><button class="btn-w90-h40-square btn-green"><?php echo $TEXT2['see_more']?></button></a></center>
						<?php }?>
						<!--App Section-->
						<?php if (in_array("app", $dev_create)){?>
						<br><br>
						<img class="img-border-shadow" src="<?php echo $BASENAME['img_html'].'icon_app.png'?>" style="width:10%;height: 50px;"><label class="text-big text-white-shadow">&nbsp;&nbsp;<?php echo $TEXT2['data_total_app'].' : '.$dev_stats['total_app']?></label>
						<hr>
						<?php echo getDevProject($_GET['id'],"app")?>
						<div style="width: 100%;height: 200px"></div>
						<br><br>
						<center><a href="<?php echo $BASENAME['store'].'?q='.$_GET['id']?>"><button class="btn-w90-h40-square btn-green"><?php echo $TEXT2['see_more']?></button></a></center>
						<?php }?>
						_________________________________________________________________________________________
					</div>
				</div>
				__________________________________________________________________________________________________________________
				<hr>
			</div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>