<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "aboutus-user";
include '../function/model.php';
$_SESSION['refferPage'] = $BASENAME['about_us'];
$sort = initAboutProjectSort();?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>About - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'about.js'?>"></script>
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>" onload="<?php initAboutPageShow();?>;showStructure('TCI')">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element">
			<div class="container-small-left">
				<!-- About Us Button-->
				<div class="btn-w90-h40-square btn-transparent" onclick="show('about-div-aboutus')">
					<div style="float: left;margin-top: 10px;width: 80%;" class="text-small">
						<?php echo $TEXT['about_menu_about']?>
					</div>
					<div style="float: right;width: 20%;">
						<img src="<?php echo $BASENAME['img_html'].'navbar-default-avatar.png'?>" style="margin-top:3px;width: 80%;height: 35px;border-radius: 100%;">
					</div>
				</div>
				<!-- End of About Us Button-->
				<br>
				<!-- Project Button-->
				<div class="btn-w90-h40-square btn-transparent" onclick="show('about-div-project')">
					<div style="float: left;margin-top: 10px;width: 80%;" class="text-small">
						<?php echo $TEXT['about_menu_project']?>
					</div>
					<div style="float: right;width: 20%;">
						<img src="<?php echo $BASENAME['img_html'].'navbar-default-avatar.png'?>" style="margin-top:3px;width: 80%;height: 35px;border-radius: 100%;">
					</div>
				</div>
				<!-- End of Project Button-->
				<br>
				<!-- Structure Button-->
				<div class="btn-w90-h40-square btn-transparent" onclick="show('about-div-structure')">
					<div style="float: left;margin-top: 10px;width: 80%;" class="text-small">
						<?php echo $TEXT['about_menu_structure']?>
					</div>
					<div style="float: right;width: 20%;">
						<img src="<?php echo $BASENAME['img_html'].'navbar-default-avatar.png'?>" style="margin-top:3px;width: 80%;height: 35px;border-radius: 100%;">
					</div>
				</div>
				<!-- End of Structure Button-->
				<br>
				<!-- Director Button-->
				<div class="btn-w90-h40-square btn-transparent" onclick="show('about-div-director')">
					<div style="float: left;margin-top: 10px;width: 80%;" class="text-tiny">
						<?php echo $TEXT['about_menu_director']?>
					</div>
					<div style="float: right;width: 20%;">
						<img src="<?php echo $BASENAME['img_html'].'navbar-default-avatar.png'?>" style="margin-top:3px;width: 80%;height: 35px;border-radius: 100%;">
					</div>
				</div>
				<!-- End of Director Button-->
				<br>
				<!-- Rule Button-->
				<div class="btn-w90-h40-square btn-transparent" onclick="show('about-div-rule')">
					<div style="float: left;margin-top: 10px;width: 80%;" class="text-small">
						<?php echo $TEXT['about_menu_rule']?>
					</div>
					<div style="float: right;width: 20%;">
						<img src="<?php echo $BASENAME['img_html'].'navbar-default-avatar.png'?>" style="margin-top:3px;width: 80%;height: 35px;border-radius: 100%;">
					</div>
				</div>
				<!-- End of Rule Button-->
				<br>
				<!-- Join As User Button-->
				<a href="<?php echo $BASENAME['signup']?>">
					<div class="btn-w90-h40-square btn-transparent">
						<div style="float: left;margin-top: 10px;width: 80%;" class="text-tiny">
							<?php echo $TEXT['about_menu_joinasuser']?>
						</div>
						<div style="float: right;width: 20%;">
							<img src="<?php echo $BASENAME['img_html'].'navbar-default-avatar.png'?>" style="margin-top:3px;width: 80%;height: 35px;border-radius: 100%;">
						</div>
					</div>
				</a>
				<!-- End of Join As User Button-->
				<br>
				<!-- Join As Developer Button-->
				<a href="<?php echo $BASENAME['signup_developer']?>">
					<div class="btn-w90-h40-square btn-transparent">
						<div style="float: left;margin-top: 10px;width: 80%;" class="text-tiny">
							<?php echo $TEXT['about_menu_joinasdev']?>
						</div>
						<div style="float: right;width: 20%;">
							<img src="<?php echo $BASENAME['img_html'].'navbar-default-avatar.png'?>" style="margin-top:3px;width: 80%;height: 35px;border-radius: 100%;">
						</div>
					</div>
				</a>
				<!-- End of Join As Developer Button-->
			</div>
			<!-- About Us Div-->
			<div id="about-div-aboutus" class="container-middle-right content-element">
				<h1 class="text-big title-white">TECH CONNEXION INDONESIA</h1>
				<p class="text-small"><?php echo $LONGTEXT['about_tci_intro']?></p>
				<div style="float: left;margin-bottom: 20px">
					<div style="float: left;width: 50%" class="text-low-small"><?php echo $LONGTEXT['about_tci_2013']?></div>
					<div style="float: left;width: 50%"><img style="width: 100%;height: 200px" src="<?php echo $BASENAME['img_html'].'about.png'?>"></div>
				</div>
				<div style="float: left;margin-bottom: 20px">
					<div style="float: right;width: 50%" class="text-low-small"><?php echo $LONGTEXT['about_tci_2016']?></div>
					<div style="float: right;width: 50%"><img style="width: 100%;height: 200px" src="<?php echo $BASENAME['img_html'].'about.png'?>"></div>
				</div>
				<div style="float: left;margin-bottom: 20px">
					<div style="float: left;width: 50%" class="text-low-small"><?php echo $LONGTEXT['about_tci_2017']?></div>
					<div style="float: left;width: 50%;margin-bottom: 50px"><img style="width: 100%;height: 200px" src="<?php echo $BASENAME['img_html'].'about.png'?>"></div>
				</div>
				<div style="float: left;margin-bottom: 20px">
					<div style="float: right;width: 50%" class="text-low-small"><?php echo $LONGTEXT['about_tci_2018']?></div>
					<div style="float: right;width: 50%;margin-bottom: 50px"><img style="width: 100%;height: 200px" src="<?php echo $BASENAME['img_html'].'about.png'?>"></div>
				</div>
			</div>
			<!-- End of About Us Div--> 
			<!-- Our Project Div-->
			<div id="about-div-project" class="container-middle-right content-element">
				<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<input type="text" name="about_project_search"  placeholder="<?php echo $TEXT['about_project_search']?>" class="search-100" value="<?php echo getAboutProjectSearchPlaceholder()?>">
				</form>
				<div style="margin-top: 10px;"><label class="text-small text-white-shadow" style="margin-right: 2%;"><?php echo $TEXT['sort_by'].'  '?></label><label class="text-small text-block-hover-gray" style="margin-right: 2%;" onclick="window.open(basename+baseAbout+'?s=project&cs=dp','_SELF')"><?php echo $sort[0]?></label><label class="text-small text-block-hover-gray" style="margin-right: 2%;" onclick="window.open(basename+baseAbout+'?s=project&cs=p','_SELF')"><?php echo $sort[1]?></label><label class="text-small text-block-hover-gray" onclick="window.open(basename+baseAbout+'?s=project&cs=ds','_SELF')"><?php echo $sort[2]?></label></div>
				<br><br>
				<?php echo getAboutProjectProduct()?>
			</div>
			<!-- End of Our Project Div-->
			<!-- Organization Structure Div-->
			<div id="about-div-structure" class="container-middle-right content-element">
				<div id="about-div-structure-selection">
					<label class="text-low-medium text-white-shadow"><?php echo $TEXT['about_structure_select'].' '.getAboutStructureSelect()?></label><br><br>
					<?php echo getAboutStructure()?>
				</div>
			</div>
			<!-- End of Organization Structure Div-->
			<!-- Director Words Div-->
			<div id="about-div-director" class="container-middle-right content-element">
				
			</div>
			<!-- End of Director Words Div-->
			<!-- Rule Div-->
			<div id="about-div-rule" class="container-middle-right content-element">
				<h1 class="text-big title-white"><?php echo $TEXT['about_rule_title']?></h1>
				<p class="text-small"><?php echo $LONGTEXT['about_rule_intro']?></p>
				<div style="float: left;margin-bottom: 50px">
					<div style="float: right;width: 65%" class="text-low-small"><label class="text-small text-white-shadow"><?php echo $TEXT['about_rule_foruser']?></label><br><br><?php echo $LONGTEXT['about_rule_user']?></div>
					<div style="float: right;width: 35%"><img style="width: 100%;height: 200px" src="<?php echo $BASENAME['img_html'].'about.png'?>"></div>
				</div>
				<div style="float: left;margin-bottom: 20px">
					<div style="float: right;width: 65%" class="text-low-small"><label class="text-small text-white-shadow"><?php echo $TEXT['about_rule_fordev']?></label><br><br><?php echo $LONGTEXT['about_rule_dev']?></div>
					<div style="float: right;width: 35%"><img style="width: 100%;height: 200px" src="<?php echo $BASENAME['img_html'].'about.png'?>"></div>
				</div>
			</div>
			<!-- End of Rule Div-->
			__________________________________________________________________________________________________________________________________________________________
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>