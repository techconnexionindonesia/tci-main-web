<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "user-cart";
include '../function/model.php';
$_SESSION['refferPage'] = $BASENAME['cart'];
if (empty($_SESSION['usr_id'])) goPage("login");
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Cart - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'pay.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-80-default content-element">
			<center><h1 class="title-white-big"><?php echo $TEXT['cart']?></h1></center>
			<input type="text" name="usr_cart_search" id="usr_cart_search" class="search-100" onchange="cart_search()" placeholder="<?php echo $TEXT['cart_search']?>"><br><br>
			<?php echo getCart()?>
			<hr>
			<?php if (getCartNum() != 0){?>
				<p class="text-white-shadow text-small"><?php echo $TEXT['cart_balance'].' '.getSym().cvtMoney(getMyBalance());?></p>
				<p class="text-white-shadow text-small" style="margin-top:-15px;"><?php echo $TEXT['cart_total'].' '.getSym().cvtMoney(getCartPrice("all"));?></p>
				<?php if (!empty($_GET['f']) && $_GET['f'] == "b") echo '<p class="text-red text-small">'.$TEXT['cart_balance_alert'].'</p>'?>
				<hr>
				<div style="height:20px;">
						<input type="button" name="usr_cart_pay" class="right btn-green btn-option" value="<?php echo $TEXT['cart_pay']?>" onclick="payAll()">
						<input type="button" name="usr_cart_delete" class="right btn-red btn-option" value="<?php echo $TEXT['cart_delete']?>" onclick="deleteAll()">
				</div>
			<?php }else{
				echo '<center><p class="text-white-shadow text-big">'.$TEXT['cart_none'].'</p></center>';
			}?>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
		<div id="webmask">
			<div class="popup" id="div-pay">
				<center>
					<h1><?php echo $TEXT['alert']?></h1>
					<p><?php echo $TEXT['cart_alert_pay']?></p>
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<input type="hidden" name="usr_cart_pay_id" id="usr_cart_pay_id" value="">
					<input type="button" onclick="closeDiv()" value="<?php echo $TEXT['no']?>">
					<input type="submit" name="usr_cart_submit_pay" value="<?php echo $TEXT['yes']?>">
					</form>
				</center>
			</div>
			<div class="popup" id="div-delete">
				<center>
					<h1><?php echo $TEXT['alert']?></h1>
					<p><?php echo $TEXT['cart_alert_delete']?></p>
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<input type="hidden" name="usr_cart_delete_id" id="usr_cart_delete_id" value="">
					<input type="button" onclick="closeDiv()" value="<?php echo $TEXT['no']?>">
					<input type="submit" name="usr_cart_submit_delete" value="<?php echo $TEXT['yes']?>">
					</form>
				</center>
			</div>
			<div class="popup" id="div-pay-all">
				<center>
					<h1><?php echo $TEXT['alert']?></h1>
					<p><?php echo $TEXT['cart_alert_pay_all']?></p>
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<input type="button" onclick="closeDiv()" value="<?php echo $TEXT['no']?>">
					<input type="submit" name="usr_cart_submit_pay_all" value="<?php echo $TEXT['yes']?>">
					</form>
				</center>
			</div>
			<div class="popup" id="div-delete-all">
				<center>
					<h1><?php echo $TEXT['alert']?></h1>
					<p><?php echo $TEXT['cart_alert_delete_all']?></p>
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<input type="hidden" id="usr_cart_delete_id" value="">
					<input type="button" onclick="closeDiv()" value="<?php echo $TEXT['no']?>">
					<input type="submit" name="usr_cart_submit_delete_all" value="<?php echo $TEXT['yes']?>">
					</form>
				</center>
			</div>
		</div>
