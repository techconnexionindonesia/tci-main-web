<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * API Function
 * ------------------------
 * Created by Alex Andre (Project Director @TCI 2019)
 *
 * - Contain all API functions needed by Javascript
 * - Always use docblock
 *
 */

/* ======= Include Core & Library ======= */
include '../../core.php';
include '../../email_lib.php';
/* =======  End Include ======= */

$api = new api();
$core = new core();

class api{

	/*
	 * Constructor function
	 *
	 * Always run this function at first
	 *
	 * return void
	 */
	function __construct(){
		header('Content-Type: application/json');
		$this->email_lib = new email_lib();
		api::checkApi();
	}

	/*
	 * Check if request exist
	 *
	 * return void
	 */
	function checkApi(){
		if (!empty($_GET) || !empty($_POST)){
			$request = !empty($_GET) ? $_GET : $_POST;
			switch ($request['process']) {
				case 'login':
					api::login();
					break;

				case 'send_email_broadcast':
					api::emailBroadcast();
					break;

				case 'get_email_subscription_list':
					api::getEmailSubscriptionList();
					break;
				
				default:
					echo "Request not defined or wrong request";
					break;
			}
		}
	}

	/*
	 * Login function
	 *
	 * return json
	 */
	function login(){
		$request = !empty($_POST) ? $_POST : null;
		if ($request){
			$action = $request['action'];
			$table = array(TABLE_ADMIN_LOGIN);
			$select = array('*');
			$login['nip'] = $request['login_nip'];
			$login['password/ne'] = "SHA1('".$request['login_password']."')";
			$destination = $request['login_destination'];
			$additional = null;
			$rs = core::dbGet($table, $select, $login, $additional);
			if (!empty($rs['error'])){
				$result['status'] = "error";
			} else {
				$result['status'] = $rs['num_rows'] == 1 ? 'success' : 'failed';
				if ($result['status'] == 'success'){
					if ($destination == "console") $result['redirect'] = URL_CONSOLE;
					else if ($destination == "project") $result['redirect'] = URL_PROJECT;
					$_SESSION['nip'] = $login['nip'];
					$_SESSION['reffer_url'] = $result['redirect'];
				}
			}
			echo json_encode($result);
		} else {
			exit('Request not defined');
		}
	}

	/*
	 * Broadcast Email function
	 *
	 * return json
	 */
	function emailBroadcast(){
		$result['status'] = "failed";
		$request = !empty($_POST) ? $_POST : null;
		if ($request['message'] && $request['subject'] && $request['email_list']){
			$status = $this->email_lib->sendBroadcastSubscribedUser($request['subject'], $request['message'], $request['email_list']);
			$activity_text = "Mengirim pesan broadcast kepada user TCI";
			core::logActivity($activity_text);
			if ($status) $result['status'] = 'success';
		}
		echo json_encode($result);
	}

	/*
	 * Get list of user subscribed email
	 *
	 * return array
	 */
	function getEmailSubscriptionList(){
		$result = array();
		$list = $this->email_lib->getSubscribedEmail();
		if (is_array($list)){
			$result = $list;
		}
		echo json_encode($result);
	}
}