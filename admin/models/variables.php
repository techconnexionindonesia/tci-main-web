<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Variable script
 * ------------------------
 * Contain URLs needed by core
 *
 */

// Set mode to Develop or Live
#define('MODE',"develop");
#define('MODE',"sandbox");
define('MODE',"live");

// Set Base URL Path
if (MODE == "develop") define('BASEURL',"http://tci.local/");
else if (MODE == "sandbox") define('BASEURL',"https://sandbox.techconnexionindonesia.com/");
else define('BASEURL',"https://www.techconnexionindonesia.com/");

// Set Base In Server Search
$url = str_replace("models", "", __DIR__);
define('BASES',$url);

// Set Base Admin Path
define('BASEPATH',BASEURL.'admin/');

// Set Base Admin Controller
define('BASEC',BASES.'controllers/');

// Set Base Admin Model
define('BASEM',BASES.'models/');

// Set Base Admin View
define('BASEV',BASEPATH.'views/');

// Set CSS Access Link
define('CSS',BASEV.'css/style.css');

// Set Javascript Access Link
define('JS',BASEV.'js/');

// Set Javascript Jquery link
define('JQUERY',JS.'jquery-3.4.1.min.js');

// Set Images HTML Access Link
define ('IMGHTML',BASEV.'img_html/');

// Set Images Main Access Link
define ('IMG',BASEV.'img/');

/* ---------- Database Tables ---------- */

define('TABLE_ADMIN_LOGIN',"admin_login");
define('TABLE_ADMIN_ACCESS',"admin_access");
define('TABLE_ADMIN_CREW_ACTIVITY', "admin_crew_activity");

/* ---------- URL Variables ---------- */

define('URL_CONSOLE',BASEV.'console.php');
define('URL_PROJECT',BASEV.'projectmanagement.php');