<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Core function
 * ------------------------
 * Created by Alex Andre (Project Director @TCI 2019)
 *
 * - Contain all core functions needed by controller
 * - Always use docblock
 *
 */

// Define Core Identifier to access Credential Script
define('fafAFJEQjASIASFASF2133247246dagsdgADGA',"asdfaiufo23523508ash124813%!#H#%g1iu35g13%#!%!#%h1358473546yhsfas%#%@#*Y^hhOH**DGYDg");

// Include URLs Variables
include 'variables.php';
if (!defined('BASEPATH')) exit("Missing URLs system, error occured");

// Set Reporting System
if (MODE == "develop" || mode == "sandbox") {error_reporting(1);}
else {error_reporting(0);}

// Start session
if (empty($_SESSION)) session_start();

// Include Credential and make new class
require 'credential.php';
$sysCred = new credential();

// Define main class of Core
class core
{

	/* ---------- Utility Models ---------- */

	/**
	 * Get Images HTML Access link
	 *
	 * @param file string
	 * return string
	 */
	public function imgHtml($file)
	{
		$value = IMGHTML.$file;
		return $value;
	}

	/**
	 * Get Images Access link
	 *
	 * @param file string
	 * @return string
	 */
	public function img($file)
	{
		$value = IMG.$file;
		return $value;
	}

	/**
	 * Get Icon Images Dir
	 *
	 * @return string
	 */
	public function getIcon()
	{
		$value = core::imgHtml('icon.png');
		return $value;
	}

	/**
	 * Get page url
	 *
	 * @return string
	 */
	public function getPage($page){
		return BASEV.$page.'.php';
	}

	/**
	 * Check login. If not logged in will be redirect to login page
	 *
	 * @return void 
	 */
	public function checkLogin(){
		$nip = core::getMyNip();
		if (!$nip) core::redirect('login');
	}

	/**
	 * Check access. If not logged in will be redirect to login page
	 *
	 * @return void 
	 */
	public function checkAccess($page_access){
		$result = false;
		$nip = core::getMyNip();
		if ($nip) {
			$table = array(TABLE_ADMIN_ACCESS);
			$select = array('access');
			$criteria = array('nip' => $nip);
			$access = core::dbGet($table,$select,$criteria)['data'];
			if ($access){
				$data_access = explode(";", $access['access']);
				if (in_array($page_access, $data_access) || in_array("super", $data_access))
					$result = true;
			}
		}

		if (!$result)
			core::redirectLast();
	}

	/**
	 * Check loged. If logged will be redirected to last opened page
	 *
	 * @return void
	 */
	public function checkLoged(){
		$nip = core::getMyNip();
		if ($nip) core::redirectLast();
	}

	/**
	 * Redirect to view
	 *
	 * @return void
	 */
	public function redirect($page){
		$url = core::getPage($page);
		header("location:".$url);
	}

	/**
	 * Redirect to last opened page
	 *
	 * @return void
	 */
	public function redirectLast(){
		$url = $_SESSION['reffer_url'] ? $_SESSION['reffer_url'] : core::getPage('console');
		header("location:".$url);
	}

	/* ---------- Database Models ---------- */

	/**
	 * Connect to Mysql Database
	 *
	 * @return resource
	 */
	private function dbConnect(){
		$credentials = credential::getWebDatabase();
		$server = $credentials['dbweb_server'];
		$user = $credentials['dbweb_user'];
		$password = $credentials['dbweb_password'];
		$db = $credentials['dbweb_db'];
		$conn = mysql_connect($server,$user,$password);
		mysql_select_db($db);
		return $conn;
	}

	/**
	 * Get data from table
	 *
	 * @access public
	 *
	 * @param array table name of used table(s) in query
	 * @param array select the element(s) which want to get
	 * @param array || string criteria specified criteria for query
	 * @param array additional additional parameter such as order by and limit
	 *
	 * @return array
	 */
	public function dbGet($table, $select, $criteria = null, $additional = null){
		if (!$table || !$select){
			$result['error'] = "Parameters are empty";
			return $result;
		} else {
			$table = implode(",", $table);
			$select = implode(",", $select);
			$query_criteria = "";
			$query_additional = "";
			/*----- Add Criteria Section -----*/

			if (!empty($criteria)){
				if (is_array($criteria)){
					if (count($criteria) > 0){
						foreach ($criteria as $key => $value) {
							if ($query_criteria != "") $query_criteria .= 'AND ';
							$value_detailed = (is_string($value) && !stripos($key,"/ne")) ? '"'.$value.'"' : $value;
							if (stripos($key, "/ne")){
								$key = str_replace("/ne", "", $key);
							}
							$query_criteria .= $key.' = '.$value_detailed.' ';
						}
					}
				} elseif (is_string($criteria)){
					$query_criteria = $criteria;
				}
			}
			/*----- Add Additional Section -----*/
			if (!empty($additional)){
				if (is_array($additional)){
					foreach ($additional as $key => $value) {
						$query_additional .= $key.' '.$value.' ';
					}
				}
			}
			/*----- Execute Query -----*/
			$query = "SELECT ";
			if (empty($table) || empty($select)){
				$result['error'] = "Parsed parameter are invalid";
				return $result;
			} else {
				$query .= $select;
				$query .= ' FROM '.$table;
				if (!empty($query_criteria)){
					$query .= ' WHERE '.$query_criteria;
				}
				if (!empty($query_additional)){
					$query .= ' '.$query_additional;
				}
			}
			core::dbConnect();
			$result_get = mysql_query($query);
			$result_array = array();
			while($rs = mysql_fetch_array($result_get)){
				array_push($result_array, $rs);
			}

			if (mysql_error()){
				$result['error'] = mysql_error();
				return $result;
			} else {
				$result['data'] = $result_array;
				$result['num_rows'] = mysql_num_rows($result_get);
				return $result;
			}
		}
	}

	/**
	 * Insert data to table
	 *
	 * @access public
	 *
	 * @param string table name of used table in query
	 * @param array fields the element(s) which want to insert
	 *
	 * @return boolean result of inserted id or result of query
	 */
	public function dbInsert($table, $fields){
		if (!$table || !$fields || is_array($fields) != true){
			return false;
		} else {
			/* ----- Add fields section ---- */
			$query_fields = "(";
			$query_values = "(";
			foreach ($fields as $field => $value) {
				/* ---- Add Field into query ---- */
				if ($query_fields != "(") {$query_fields .= ",";}
				$query_fields .= $field;

				/* ---- Add Values into query ---- */
				if ($query_values != "(") {$query_values .= ",";}
				$query_values .= "'".$value."'";
			}
			$query_fields .= ")";
			$query_values .= ")";

			/* ---- Execute Query ---- */
			$query = "INSERT INTO ";
			$query .= $table." ";
			$query .= $query_fields;
			$query .= " VALUES ";
			$query .= $query_values;
			core::dbConnect();
			$result = mysql_query($query);
			$insert_id = mysql_insert_id();
			if ($insert_id){
				return $insert_id;
			} else {
				return $result;
			}
		}
	}
	/* --------------- Main Process ---------------- */

	/**
	 * Save admin activity log 
	 *
	 * @param string activity
	 * @return void
	 */
	public function logActivity($activity){
		if (core::getMyNip() && $activity){
			$nip = core::getMyNip();
			$criteria = array(
				'nip_crew' => $nip,
				'activity' => $activity,
				'datetime' => date('Y-m-d H:i:s')
			);
			$result = core::dbInsert(TABLE_ADMIN_CREW_ACTIVITY,$criteria);
			return $result;
		} else {
			return false;
		}
	}

	/* ------------ Get Resource Data -------------- */

	/**
	 * Get crew data
	 *
	 * @param string || null nip the crew id number to get, fill empty to get current loged crew data
	 * @return array
	 */
	public function crewDataGet($nip = null){
		if (!empty($nip) && !empty($_SESSION['crew_nip'])){
			$nip = $_SESSION['crew_nip'];
		} else if (empty($nip) && empty($_SESSION['crew_nip'])){
			exit('error getting crew data');
		}
		$data = "";
	}

	/**
	 * Get NIP from session
	 *
	 * @return string
	 */
	public function getMyNip(){
		if (!empty($_SESSION['nip'])){
			return $_SESSION['nip'];
		} else {
			return false;
		}
	}

	/**
	 * load view from html content to variable
	 *
	 * @param string file : file to load
	 * @param boolean load_to_var : load to variable or not
	 * @return string
	 */
	public function loadView($file, $data, $load_to_var){
		$path_ = core::getPage($file);
		$result = "";
		if ($load_to_var){
			$path = BASEV.$file.'.php';
			$result = file_get_contents($path);
			if ($data){
				foreach ($data as $var_name => $var_value) {
					$search_key = '/'.$var_name.'/';
					$result = str_replace($search_key, $var_value, $result);
				}
			}
		}
		return $result;
	}

	/**
	 * Get Email Template
	 *
	 * @param string template name of the template which is to be use
	 * @param array data data that will be parsed to email template
	 * @return string
	 */
	public function getEmailTemplate($template, $data){
		$result = "";
		if ($template){
			$content_template = $this->loadView('email_template/'.$template, $data, true);
			$base_template = $this->loadView('email_template/base', false, true);
			$result = str_replace('/content_template/', $content_template, $base_template);
			return $result;
		} else {
			return false;
		}
	}
}