<?php (defined('BASEPATH')) OR exit('No direct script access allowed');

 
class email_lib extends core {
 
	public function __construct() { 
	    
    }

    private function set_email_config($account){
        require_once 'library/phpmailer/Exception.php';
        require_once 'library/phpmailer/PHPMailer.php';
        require_once 'library/phpmailer/SMTP.php';
        $credential = credential::getEmailConfig($account);
	    $response = false;
        unset($this->mail);
        $this->mail = new PHPMailer\PHPMailer\PHPMailer();
        $this->mail->isHTML(true);
        // $this->mail->SMTPDebug  = 2;
	   
	    $this->mail->isSMTP();
	    $this->mail->Host     = $credential['smtp'];
	    $this->mail->SMTPAuth = true;
	    $this->mail->Username = $credential['username'];
	    $this->mail->Password = $credential['password'];
	    $this->mail->Port     = $credential['port'];
	    $this->mail->setFrom($credential['username'], $credential['name']);
	    $this->mail->addReplyTo("cs@techconnexionindonesia.com", "CS TCI");
    }

    private function set_subject($subject){
		$this->mail->Subject = $subject;
    }

    private function set_address($email){
    	$this->mail->addAddress($email);
    }

    private function set_bcc($bcc){
    	foreach ($bcc as $email) {
    		$this->mail->AddBCC($email);
    	}
    }

    private function set_content($template,$data){
    	$content = core::getEmailTemplate($template, $data);
    	$this->mail->Body = $content;
    }

    private function set_data($account, $subject, $address = false, $template = false, $data = false, $bcc = false) {
        email_lib::set_email_config($account);
    	email_lib::set_subject($subject);

    	if ($address)
    		email_lib::set_address($address);

    	if ($template && $data)
    		email_lib::set_content($template, $data);

    	if ($bcc)
    		email_lib::set_bcc($bcc);
    }

    /* ------------------------------------------------------------- */
    /* ---------------------- Method Below ------------------------- */
    /* ------------------------------------------------------------- */

    /**
     * Send broadcast message to subscribed user
     *
     * @param string subject : subject of email
     * @param string message : message of email to be send
     * @return boolean
     */
    public function sendBroadcastSubscribedUser($subject, $message, $bcc) {
    	if ($subject && $message && $bcc){
    		$template = 'free';
            $data = array('message' => $message);
            $max_bcc = 49;
            if (count($bcc) <= $max_bcc){
                email_lib::set_data('no-reply', $subject, false, $template, $data, explode(",", $bcc));
                $this->mail->send();
    		    return true;
            } else {
                return false;
            }
    	} else return false;
	}

    /* ------------------------------------------------------------- */
    /* -------------------- Non Send Function ---------------------- */
    /* ------------------------------------------------------------- */
    
    /**
     * Get email of subscribed user
     *
     * @return array
     */
    public function getSubscribedEmail() {
        $table = array('user_user','user_subscription');
        $select = array('user_user.email');
        $criteria = array('user_user.id_user/ne' => 'user_subscription.id_user');
        $result = core::dbGet($table, $select, $criteria);
        $users_email = array();
        foreach ($result['data'] as $row => $value) {
            array_push($users_email, $value['email']);
        }
        return $users_email;
    }
}
 