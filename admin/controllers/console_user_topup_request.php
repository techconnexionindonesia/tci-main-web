<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Console menu page Controller
 * ------------------------
 * Created by Alex Andre (Project Director @TCI 2019)
 *
 * - Contain all controller function for console menu page
 *
 */
require_once '../../models/core.php';
$_page_access = "user_topup";

class console_user_topup_request extends core
{
	function __construct(){
		parent::checkLogin();
		parent::checkAccess($_page_access);
	}
}

$controller = new console_user_topup_request();

?>