<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Logout menu page Controller
 * ------------------------
 * Created by Alex Andre (Project Director @TCI 2019)
 *
 * - Contain function for logout only
 *
 */
require_once '../models/core.php';

class logout extends core
{
	function __construct(){
		session_start();
		session_destroy();
		parent::redirect('login');
	}
}

$controller = new logout();

?>