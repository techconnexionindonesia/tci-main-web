<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Login menu page Controller
 * ------------------------
 * Created by Alex Andre (Project Director @TCI 2019)
 *
 * - Contain all controller function for Login menu page
 *
 */
require_once '../models/core.php';

class login extends core
{
	function __construct(){
		parent::checkLoged();
	}
}

$controller = new login();

?>