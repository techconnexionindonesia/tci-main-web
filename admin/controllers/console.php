<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Console menu page Controller
 * ------------------------
 * Created by Alex Andre (Project Director @TCI 2019)
 *
 * - Contain all controller function for console menu page
 *
 */
require_once '../models/core.php';
$_page_id = "console";
$_grant_mode = "all";
$_grant = "all";

class console extends core
{
	function __construct(){
		parent::checkLogin();
	}
}

$controller = new console();

?>