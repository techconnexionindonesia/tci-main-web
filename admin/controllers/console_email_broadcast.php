<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Console menu page Controller
 * ------------------------
 * Created by Alex Andre (Project Director @TCI 2019)
 *
 * - Contain all controller function for console menu page
 *
 */
require_once '../../models/core.php';
$_page_access = "email_broadcast";

class console_email_broadcast extends core
{
	function __construct(){
		parent::checkLogin();
		parent::checkAccess($_page_access);
	}
}

$controller = new console_email_broadcast();

?>