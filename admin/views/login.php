<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Login Page
 * ------------------------
 * Contain login function for admin only
 *
 * @TCI 2019
 *
 */

require_once '../controllers/login.php';
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>TCI Admin - Login</title>
		<meta id="Viewport" name="viewport" content="initial-scale=1, width=device-width, user-scalable=yes">
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
		<link rel="stylesheet" href="<?= CSS?>">
		<link rel="icon" href="<?= core::getIcon()?>">
		<script src="<?= JQUERY ?>"></script>
		<script type="text/javascript">const URL_SERVER = "<?=BASEURL?>";</script>
		<script type="text/javascript" src="<?= JS.'variable.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'core.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'login.js'?>"></script>
	</head>
	<body>
		<div class="main-left-full-1 element-white-transparent-rounded form-type-1">
			<br><br>
			<label class="title-medium">TCI Admin Console - Login</label>
			<br><br><br><br>
			<form action="#">
				<label class="label-small">Nomor Induk : </label><br><br>
				<input type="number" id="login_nip"><br>
				*Nomor induk yang tertera pada ID Card TCI anda
				<br><br><br><br>
				<label class="label-small">Password : </label><br><br>
				<input type="password" id="login_password"><br>
				*Masukan password administrator anda
				<br><br><br><br>
				Dengan masuk ke Administrator Console TCI, anda dapat melakukan berbagai macam manajemen fitur sesuai dengan posisi pekerjaan anda. Aktifitas pekerjaan anda pada Administrator Console TCI akan selalu terdata ke dalam basis data untuk dipantau kembali oleh Director / Supervisor.
				<br><br>
				<center>
					<button class="btn-full" id="login_button_console">Masuk ke Administrator Console</button>
					<br>
					<p id="info_bar"></p>
				</center>
			</form>
			<br>
			<label class="txt-warning" id="warning"></label>
		</div>
	</body>
</html>