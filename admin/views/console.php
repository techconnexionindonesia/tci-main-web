<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Console Menu Page
 * ------------------------
 * Contain main menu for Administrator Console
 *
 * @TCI 2019
 *
 */

require_once '../controllers/console.php';
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>TCI Admin - Console</title>
		<meta id="Viewport" name="viewport" content="initial-scale=1, width=device-width, user-scalable=yes">
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
		<link rel="stylesheet" href="<?= CSS?>">
		<link rel="icon" href="<?= core::getIcon()?>">
		<script src="<?= JQUERY ?>"></script>
		<script type="text/javascript">const URL_SERVER = "<?=BASEURL?>";</script>
		<script type="text/javascript" src="<?= JS.'variable.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'core.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'console.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'info-modal.js'?>"></script>
	</head>
	<body>
		<div class="main-left-full-1 element-white-transparent-rounded form-type-1">
			<br><br>
			<label class="title-medium">Administrator Console</label>
			<br><br><br><br>
			* Klik tombol untuk memasuki menu yang dituju
			<br><br><br><br>
			<center>
				<div id="section-main">
					<button class="btn-full info-modal-hoverable" id="console_button_user" data-infotext="info_console_button_user">Pengaturan User</button>
					<br><br>
					<button class="btn-full info-modal-hoverable" id="console_button_super" data-infotext="info_console_button_super">Pengaturan Super Admin</button>
					<br><br><hr><br>
					<button class="btn-full info-modal-hoverable btn-redirect" id="button_logout" data-infotext="info_button_logout" data-url="<?=$controller->getPage('logout')?>">LOGOUT</button>
				</div>
				<div id="section-user">
					<button class="btn-full info-modal-hoverable btn-redirect" id="console_button_topup_request" data-infotext="info_console_button_topup_request" data-url="<?=$controller->getPage('console_manage/user_topup_request')?>">Pelayanan Request Topup</button>
					<br>
					<button class="btn-full info-modal-hoverable btn-redirect" id="console_button_email_broadcast" data-infotext="info_console_button_email_broadcast" data-url="<?=$controller->getPage('console_manage/email_broadcast')?>">Broadcast Email</button>
					<br><br><hr><br>
					<button class="btn-full" id="console_button_back"><- Kembali</button>
				</div>
			</center>
		</div>

		<!-- Information Modal -->
		<div id="info-modal">
			
		</div>
		<!-- End of Information Modal -->
	</body>
</html>