<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Login Page
 * ------------------------
 * Contain login function for admin only
 *
 * @TCI 2019
 *
 */

require_once '../controllers/logout.php';
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>TCI Admin - Logout</title>
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
		<link rel="icon" href="<?= core::getIcon()?>">
	</head>
	<body>
	</body>
</html>