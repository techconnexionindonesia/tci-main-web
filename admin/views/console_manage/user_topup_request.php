<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Topup Request Page
 * ------------------------
 * Contain management function for User Topup Request
 *
 * @TCI 2019
 *
 */

require_once '../../controllers/console_user_topup_request.php';
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>TCI Admin - User Topup Request</title>
		<meta id="Viewport" name="viewport" content="initial-scale=1, width=device-width, user-scalable=yes">
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
		<link rel="stylesheet" href="<?= CSS?>">
		<link rel="icon" href="<?= core::getIcon()?>">
		<script src="<?= JQUERY ?>"></script>
		<script type="text/javascript">const URL_SERVER = "<?=BASEURL?>";</script>
		<script type="text/javascript" src="<?= JS.'variable.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'core.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'console.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'info-modal.js'?>"></script>
	</head>
	<body>
		<div class="main-center-full-1 element-white-transparent-rounded">
			<br><br>
			<label class="title-medium">Administrator Console - User Topup Request</label>
			<br><br><br><br>
			* Setiap aktifitas pada halaman ini akan tercatat ke basis data. Perhatikan setiap detail topup dan lakukan verifikasi transaksi.
			<br><br><br><br>
			<form action="#" id="form-filter" class="form-type-2">
				<input type="number" class="height-small w10 center" placeholder="Filter ID" id="user_topup_request_filter_id">
				<input type="text" class="height-small w40 center" placeholder="Filter Nama" id="user_topup_request_filter_name">
				<select class="height-small w20 input-border-transparent center" id="user_topup_request_filter_sort">
					<option value="">-- Sortir Menggunakan --</option>
					<option value="date_desc">Terbaru</option>
					<option value="date_asc">Terlama</option>
					<option value="id_asc">ID Terkecil</option>
					<option value="id_desc">ID Terbesar</option>
					<option value="name_asc">Abjad Nama terkecil</option>
					<option value="name_desc">Abjad Nama terbesar</option>
				</select>
				<button class="button-green-border-white w10 height-small" id="user_topup_request_filter_submit">Cari</button>
			</form>
			<hr><br>
			<b>Jumlah Request Topup : <span id="request_total"></span></b>
			<?php include '../console.php' ?>;
		</div>

		<!-- Information Modal -->
		<div id="info-modal">
			
		</div>
		<!-- End of Information Modal -->
	</body>
</html>