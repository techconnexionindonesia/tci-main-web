<?php
/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Console Menu Page
 * ------------------------
 * Contain main menu for Administrator Console
 *
 * @TCI 2019
 *
 */

require_once '../../controllers/console_email_broadcast.php';
?>

<!DOCTYPE HTML>
<html>
	<head>
		<title>TCI Admin - Email Broadcast</title>
		<meta id="Viewport" name="viewport" content="initial-scale=1, width=device-width, user-scalable=yes">
		<meta name="robots" content="noindex">
		<meta name="googlebot" content="noindex">
		<link rel="stylesheet" href="<?= CSS?>">
		<link rel="icon" href="<?= core::getIcon()?>">
		<script src="<?= JQUERY ?>"></script>
		<script type="text/javascript">const URL_SERVER = "<?=BASEURL?>";</script>
		<script type="text/javascript" src="<?= JS.'variable.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'core.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'console_manage/'.'email_broadcast.js'?>"></script>
		<script type="text/javascript" src="<?= JS.'info-modal.js'?>"></script>
	</head>
	<body>
		<div class="main-left-full-1 element-white-transparent-rounded">
			<br><br>
			<label class="title-medium">Email Broadcast</label>
			<br><br><br><br>
			* Silahkan isi pesan. Pesan akan ditujukan kepada semua user TCI yang mengikuti langganan notifikasi
			<br><br><br><br>
				<form action="javascript:void(0)" class="form-type-2">
					<center>
						<input type="text" class="height-small w90 center" placeholder="Subjek Pesan" id="email_broadcast_subject">
						<textarea class="w90" placeholder="Isi pesan yang akan dikirim" rows="10" id="email_broadcast_message"></textarea>
					</center>
					<div class="right">
						<button class="button-green-border-white w20 height-small" id="email_broadcast_submit">Kirim</button>
					</div>
					<div style="height: 15px"></div>
					<div class="info-scrollable">
					</div>
				</form>
				<br><br><hr><br>
				<div class="center form-type-1">
					<a href="<?=$controller->getPage('console')?>">
						<button class="btn-full" id="console_button_back"><- Kembali</button>
					</a>
				</div>
		</div>

		<!-- Information Modal -->
		<div id="info-modal">
			
		</div>
		<!-- End of Information Modal -->
	</body>
</html>