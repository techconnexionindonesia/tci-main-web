/**
 * Tech Connexion Indonesia
 * ------------------------
 * Javascript Info Modal
 * ------------------------
 * Contaion function of Info Modal interaction
 *
 */

$(document).ready(function(){
	$('#section-user').hide();

	$('#console_button_user').click(function(){
		$('#section-main').hide(500);
		$('#section-user').show(500);
	});

	$('#console_button_back').click(function(){
		$('#section-user').hide(500);

		$('#section-main').show(500);
	});
});