/**
 * Tech Connexion Indonesia
 * ------------------------
 * Javascript Login Page
 * ------------------------
 * Contaion function of ajax login and redirect
 *
 */

if (typeof jsCore === undefined){
	alert('Resource not loaded');
}

$(document).ready(function(){
	class login
	{

		/*
		 * Login Process function
		 *
		 * @param action string
		 * return void
		 */
		doLogin(action){
			if (jsLogin.isFormFilled()){
				var form = {
				login_nip : $('#login_nip').val(),
				login_password : $('#login_password').val(),
				login_destination : action
				}
				jsLogin.handleButton('disable');
				$('#info_bar').text(jsCore.txt('login_loading'));
				$('#warning').text('');
				var response = jsCore.doAjax('POST','login',form,function(err,res){
					if (!err){
						jsLogin.handleLoginResponse(res);
					}
				});
			}
		}

		/*
		 * Handle the result response of login ajax
		 *
		 * @param res array
		 * return void
		 */
		handleLoginResponse(res){
			var status = res.status;
			if (status == 'success'){
				$('#info_bar').text(jsCore.txt('login_success'));
				setTimeout(function(){
					var redirect_url = res.redirect;
					window.open(redirect_url,"_SELF");
				},2000);
			} else if (status == 'failed'){
				$('#warning').text(jsCore.txt('login_failed'));
				$('#info_bar').text('');
				jsLogin.handleButton('enable');
			}
		}

		/*
		 * Check required form
		 *
		 * return void
		 */
		 isFormFilled(){
		 	if ($('#login_nip').val() == "" || $('#login_password').val() == ""){
		 		$('#warning').html(jsCore.txt('form_not_filled'));
		 		return false;
		 	} else {
		 		return true;
		 	}
		 }

		/*
		 * Enable or Disable button
		 *
		 * return void
		 */
		handleButton(action){
			if (action == 'enable'){
				$('#login_button_console').prop('disabled',false);
				$('#login_button_project_management').prop('disabled',false);
			} else {
				$('#login_button_console').prop('disabled',true);
				$('#login_button_project_management').prop('disabled',true);
			}
		}

		 /*
		  * Bind on click event on Login Button
		  *
		  * return void
		  */
		  bindButton(){
		  	$('#login_button_console').click(function(){
		  		jsLogin.doLogin('console');
		  	});
		  	$('#login_button_project_management').click(function(){
		  		jsLogin.doLogin('project');
		  	});
		  }
	}	
	// Load JQuery
	var jsLogin = new login();
	jsLogin.bindButton();
})