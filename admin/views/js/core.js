/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Core Main Class models
 * ------------------------
 * Contain all models function used by another class
 *
 * @TCI 2019
 *
 */

class core{


	txt(txt){
		return text[txt];
	}

	doAjax(type, process, dataObject, callback){
		if (!dataObject){
			dataObject = {};
		}
		if (dataObject.process !== undefined){
			callback('process already defined', null);
		}
		dataObject.process = process;
		$.ajax({
			url: URL_AJAX,
			data: dataObject,
			dataType: 'json',
			type: type,
			success: function(res){
				callback(null, res);
			},
			error: function (jqXhr, textStatus, errorMessage) {
            	callback(errorMessage, null);
        	}
		});
	}

	doAjaxAsync(type, process, dataObject){
		if (dataObject.process !== undefined){
			return('process already defined', null);
		}
		dataObject.process = process;
		$.ajax({
			url: URL_AJAX,
			data: dataObject,
			dataType: 'json',
			async: false,
			type: type,
			success: function(res){
				return(null, res);
			},
			error: function (jqXhr, textStatus, errorMessage) {
            	return(errorMessage, null);
        	}
		});
	}
}

$(document).ready(function(){
	/* ----- Section Open Redirect ----- */
	$('.btn-redirect').click(function(){
		var url = $(this).data('url');
		if (url) window.open(url,'_SELF');
	});
});

var jsCore = new core();