/**
 * Tech Connexion Indonesia
 * ------------------------
 * Javascript Info Modal
 * ------------------------
 * Contain function of Info Modal interaction
 *
 */

 $(document).ready(function(){

 	$('.info-modal-hoverable').unbind('mouseenter').mouseenter(function(){
 		var get_text = jsCore.txt($(this).data('infotext'));
 		$('#info-modal').text(get_text);
 		$('#info-modal').show(200);
 	});
 	$('.info-modal-hoverable').unbind('mouseleave').mouseleave(function(){
 		$('#info-modal').text('');
 		$('#info-modal').hide(200);
 	});

 });