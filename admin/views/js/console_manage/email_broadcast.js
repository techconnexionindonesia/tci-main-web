/**
 * Tech Connexion Indonesia
 * ------------------------
 * Javascript Console Email Broadcast
 * ------------------------
 * Contain function to broadcast email
 *
 */

if (typeof jsCore === undefined){
	alert('Resource not loaded');
}

$(document).ready(function(){
	class email_broadcast
	{
		bcc_list = new Array();
		message = "";
		subject = "";
		max_bcc = 40;

		total_sent = 0;

		/**
		 * Bind on click event on Login Button
		 *
		 * return void
		 */
		bindButtonAndInput(){
			$('#email_broadcast_submit').click(function(){
				jsEmailBroadcast.doBroadcast();
			});
		}

		/**
		 * Add text to info section
		 *
		 * @param string text to add
		 * return void
		 */
		addInfoText(text){
			setTimeout(function(){
				$('.info-scrollable').html(text+'<br>'+$('.info-scrollable').html());
			},100);
		}

		/**
		 * Clear text to info section
		 *
		 * return void
		 */
		clearInfoText(){
			setTimeout(function(){
				$('.info-scrollable').html('');
			},100);
		}

		/**
		 * Do email broadcast
		 *
		 * return void
		 */
		doBroadcast(){
			var message = $('#email_broadcast_message').val();
			var subject = $('#email_broadcast_subject').val();
			if (message){
				$('#email_broadcast_submit').prop('disabled',true);
				jsEmailBroadcast.clearInfoText();
				jsEmailBroadcast.addInfoText("Mempersiapkan daftar broadcast . . .");
				jsEmailBroadcast.message = message;
				jsEmailBroadcast.subject = subject;
				jsCore.doAjax('POST','get_email_subscription_list',false,function(err,list){
					console.log(list);
					if (typeof list == 'object' && list.length > 0){
						jsEmailBroadcast.bcc_list = list;
						jsEmailBroadcast.sendBroadcastPerGroup();
					} else {
						jsEmailBroadcast.addInfoText("Terjadi Kesalahan");
					}
				});
			} else {
				jsEmailBroadcast.clearInfoText();
				jsEmailBroadcast.addInfoText("Harap isi pesan yang akan dikirim");
			}
		}

		/**
		 * Send email with limited bcc grouping
		 *
		 * return void
		 */
		sendBroadcastPerGroup(){
	 		var email_to_send = new Array();
	 		while(email_to_send.length < jsEmailBroadcast.max_bcc && jsEmailBroadcast.total_sent < jsEmailBroadcast.bcc_list.length){
	 			email_to_send.push(jsEmailBroadcast.bcc_list[jsEmailBroadcast.total_sent]);
	 			jsEmailBroadcast.addInfoText("("+(jsEmailBroadcast.total_sent+1)+") "+jsEmailBroadcast.bcc_list[jsEmailBroadcast.total_sent]);
	 			jsEmailBroadcast.total_sent++;
	 		}
	 		if (email_to_send.length > 0){
	 			var form = {
	 				subject: jsEmailBroadcast.subject,
	 				message: jsEmailBroadcast.message,
	 				email_list: email_to_send.join(",")
	 			}
	 			jsCore.doAjax('POST','send_email_broadcast',form,function(err,result){
	 				if (!err){
	 					jsEmailBroadcast.addInfoText("Status Pengiriman : "+(result.status == 'success' ? "Berhasil" : "Gagal"));
	 				} else {
	 					jsEmailBroadcast.addInfoText("Terjadi Kesalahan");
	 				}
	 				if (jsEmailBroadcast.total_sent >= jsEmailBroadcast.bcc_list.length){
	 					jsEmailBroadcast.addInfoText("------------------------------------");
	 					jsEmailBroadcast.addInfoText("Broadcast telah diproses ke : "+jsEmailBroadcast.total_sent+" user");
	 					jsEmailBroadcast.addInfoText("==========================");
	 					jsEmailBroadcast.clearAfterBroadcast();
	 				} else {
	 					jsEmailBroadcast.addInfoText("------------------------------------");
	 					jsEmailBroadcast.addInfoText("Mempersiapkan daftar kembali");
	 					jsEmailBroadcast.callSendBroadcast();
	 				}
	 			});
	 		}
		}

		/**
		 * Call send broadcast again
		 *
		 * return void
		 */
		callSendBroadcast(){
			jsEmailBroadcast.sendBroadcastPerGroup();
		}

		/**
		 * Clear form and variable
		 *
		 * return void
		 */
		clearAfterBroadcast(){
			jsEmailBroadcast.subject = "";
			jsEmailBroadcast.message = "";
			jsEmailBroadcast.subject = new Array();
			jsEmailBroadcast.total_sent = 0;
			$('#email_broadcast_message').val("");
			$('#email_broadcast_subject').val("");
			$('#email_broadcast_submit').prop('disabled',false);
		}
	}

	var jsEmailBroadcast = new email_broadcast();
	jsEmailBroadcast.bindButtonAndInput();
});