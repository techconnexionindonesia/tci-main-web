/**
 *
 * Tech Connexion Indonesia
 * ------------------------
 * Variable models
 * ------------------------
 * Contain all constant variables
 *
 * @TCI 2019
 *
 */

const URL_BASE = URL_SERVER + "admin/";
const URL_AJAX = URL_BASE + "models/api/v1/api.php";

var text = {
	form_not_filled: "Form belum diisi lengkap",
	login_success: "Login berhasil, sedang mengarahkan . . .",
	login_failed: "Gagal melakukan login, cek kembali form anda",
	login_loading: "Sedang login . . .",

	info_button_logout: "Logout dan kembali ke menu login",
	info_console_button_user: "Anda dapat melakukan fungsi pengaturan user dalam menu ini seperti pelayanan topup dsb",
	info_console_button_super: "Pengaturan super admin dikhususkan bagi administrator tingkat atas. Berfungsi untuk melakukan pengaturan khusus seperti menambah dan menghapus serta melakukan pengubahan data web",
	info_console_button_topup_request: "Menu untuk menerima atau menolak request topup dari user",
	info_console_button_email_broadcast: "Menu untuk melakukan broadcast email ke semua user TCI yang berlangganan notifikasi"
};