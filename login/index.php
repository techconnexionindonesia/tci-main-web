<?php
//Index Page - Login - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "login-user";
include '../function/model.php';
if (!empty($_SESSION['usr_id'])) goBack();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Login - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-50-center content-element">
			<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>" class="form-big">
			<h1 class="title-white-big"><?php echo $TEXT['login_title']?></h1>
			<br>
			<input type="text" name="usr_login_username" placeholder="<?php echo $TEXT['login_ph_username']?>" required>
			<br>
			<input type="password" name="usr_login_password" placeholder="<?php echo $TEXT['login_ph_password']?>" required>
			<br><br>
			<input type="submit" name="usr_login_submit" value="<?php echo $TEXT['submit']?>" class="btn-green">
			<br>
			<?php echo getWrongLoginLabel()?>
			<p><a href="<?php echo $BASENAME['signup']?>"><?php echo $TEXT['login_signup']?></a></p>
			</form>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>