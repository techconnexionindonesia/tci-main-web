<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "product-dev";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['devconsole_product'];
if (empty($_SESSION['usr_id'])) goPage("login","");
if (!isDevAlreadySignup($_SESSION['usr_id'])) goPage("devconsole_signup"."");
if (isDevWaitValid($_SESSION['usr_id'])) goPage("devconsole_wait"."");
if (empty($_SESSION['dev_id'])) goPage("devconsole_login","");
$devData = getDevData($_SESSION['dev_id']);
$sort = initDevProductSort();
if (!empty($_GET['id'])) $product = getDevProductData($_GET['id']);
;?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Product - Devconsole</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'devconsole-product.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg3.jpg'?>" onload="setIdDeveloper(`<?php echo $_SESSION['dev_id']?>`);setRateExchange(`<?php echo getRateExchange()?>`)">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element-blue">
			<div style="width: 100%;height: 120px;">
				<div class="left" style="width: 20%;">
					<img src="<?php echo $BASENAME['img_html'].'icon_devlogo.png'?>" style="width: 100%;height: 120px;"><hr>
				</div>
				<div class="right" style="width: 80%;padding-top: 30px;">
					<img class="img-w20-h120-border-hoverable right cursor-hand" style="margin-left: 2%;margin-top: -30px;" src="<?php echo $devData['pic_link']?>">
					<a href="<?php echo $BASENAME['devconsole_config']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_config']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_activity']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_activity']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_finance']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_finance']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_product']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_product']?></label></a>
				</div>
			</div>
			<div class="content-center-big-100-default content-element-dark">
				<div style="width: 100%;height: auto;">
					<!-- Left Section-->
					<div class="left" style="width: 40%;height: auto;">
						<div class="content-center-big-100-default content-element-dark">
							<?php
							//Section edit mode
							if (!empty($product['mode']) && $product['mode'] == "edit"){?>
								<font class="text-big"><?php echo $TEXT['statistic']?></font><br><br>
								<font class="text-small"><?php echo $TEXT2['devconsole_stas_totalbuyer'].' : '.$product['total_buyer']?></font><br>
								<font class="text-small"><?php echo $TEXT2['devconsole_stas_totaldownload'].' : '.$product['total_download'].' x'?></font><br>
								<font class="text-small"><?php echo $TEXT2['devconsole_stas_totalincome'].' : '.getSym().$product['total_income']?></font><br>
								<font class="text-small"><?php echo $TEXT['store_rating1'].' : '.$product['vote_yes'].'% '.$TEXT['store_rating2'].' '.$product['vote_total'].' '.$TEXT['store_rating3']?></font><br>
								<font class="text-small"><?php echo $TEXT2['devconsole_stas_totalreview'].' : '.$product['total_review'].' x'?></font><br>
								<font class="text-small"><?php echo $TEXT['project_published'].' : '.$product['date_published']?></font><br><br>
								<center><a href="<?php echo $product['link_store']?>"><button class="btn-w90-h40-square btn-transparent2">STORE</button></a></center><br>
								<hr>
							<?php }?>
							<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>" enctype="multipart/form-data" onsubmit="submitted()">
							<input type="hidden" name="dev_ps_mode" id="dev_ps_mode" value="<?php if (!empty($product['mode'])) echo $product['mode'];else echo 'add'?>">
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_type'].' : '?></font><br>
							<select class="input-text-border-white-w90-h30 text-small center" name="dev_ps_type" id="dev_ps_type" required>
								<?php 
								if (empty($product['type'])) $product['type'] = "";
								echo getDevAvailableSelectCreate($_SESSION['dev_id'],$product['type'])?>
							</select><font class="text-tiny" id="label_type_disabled">*Disabled</font><br><br>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_idproduct'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_idproduct_d" id="dev_ps_idproduct_d" disabled required><input type="hidden" name="dev_ps_idold" id="dev_ps_idold" value="<?php if (!empty($product['id'])) echo $product['id']?>"><br>
							<font class="text-low-tiny"><?php echo '*'.$TEXT['automatic']?></font><br><br>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_code'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_code" id="dev_ps_code" value="<?php if (!empty($product['code_project'])) echo $product['code_project']?>" required><br><br>
							<font class="text-low-medium" id="label_idgame"><?php echo $TEXT2['devconsole_idgame'].' : '?><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_idgame" id="dev_ps_idgame" value="<?php if (!empty($product['id_content']) || !empty($product['id_dlc'])) echo $product['id_game_parent']?>"><br><br></font>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_title'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_title" id="dev_ps_title" value="<?php if (!empty($product['title'])) echo $product['title']?>" required><br><br>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_subtitle_id'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_subtitle_id" id="dev_ps_subtitle_id" value="<?php if (!empty($product['sub_title_id'])) echo $product['sub_title_id']?>" required><br><br>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_subtitle_en'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_subtitle_en" id="dev_ps_subtitle_en" value="<?php if (!empty($product['sub_title_en'])) echo $product['sub_title_en']?>" required><br><br>
							<input type="hidden" name="dev_ps_subtitle" id="dev_ps_subtitle" value="">
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_gc'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_gc" id="dev_ps_gc" value="<?php if (!empty($product['gc'])) echo $product['gc']?>" required><br>
							<font class="text-tiny"><?php echo $TEXT2['devconsole_gc_info']?></font><br><br>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_tags'].' : '?></font><br>
							<textarea class="textarea-w90-h50-transparent-inner text-tiny" name="dev_ps_tags" id="dev_ps_tags"><?php if (!empty($product['tags'])) echo $product['tags']?></textarea><br><br>
							<font class="text-tiny" id="dev_ps_picreplaceinfo"><?php echo $TEXT2['devconsole_picreplace_info']?><br></font>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_pictitle'].' : '?></font><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_pictitle" id="dev_ps_pictitle"><br><br>
							<label id="dev_ps_picprogress"><font class="text-low-medium"><?php echo $TEXT2['devconsole_picprogress'].' : '?></font><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_picprogress"><br>
							<font class="text-tiny"><?php echo $TEXT2['devconsole_picprogress_info']?></font><br><br></label>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_picsub'].' : '?></font><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_picsub1" id="dev_ps_picsub1"><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_picsub2" id="dev_ps_picsub2"><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_picsub3" id="dev_ps_picsub3"><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_picsub4" id="dev_ps_picsub4"><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_picsub5" id="dev_ps_picsub5"><br>
							<input type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_ps_picsub6" id="dev_ps_picsub6"><br><br>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_detail_id'].' : '?></font><br>
							<textarea class="textarea-w90-h50-transparent-inner text-tiny" name="dev_ps_detail_id" id="dev_ps_detail_id" required><?php if (!empty($product['detail_id'])) echo $product['detail_id']?></textarea><br><br>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_detail_en'].' : '?></font><br>
							<textarea class="textarea-w90-h50-transparent-inner text-tiny" name="dev_ps_detail_en" id="dev_ps_detail_en" required><?php if (!empty($product['detail_en'])) echo $product['detail_en']?></textarea>
							<input type="hidden" name="dev_ps_detail" id="dev_ps_detail"><br><br>
							<hr>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_pricing']?></font><br><br>
							<font class="text-small"><?php echo $TEXT2['devconsole_pricedollar'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_pricedollar" id="dev_ps_pricedollar" value="<?php if (isset($product['price'])) echo $product['price']?>" required><br>
							<font class="text-tiny"><?php echo $TEXT2['devconsole_pricedollar_info']?></font><br><br>
							<font class="text-small"><?php echo $TEXT2['devconsole_pricerupiah'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_pricerupiah" id="dev_ps_pricerupiah" disabled><br>
							<font class="text-tiny"><?php echo '*'.$TEXT['automatic']?></font><br><br>
							<font class="text-low-small"><?php echo $TEXT2['devconsole_pricestoredollar'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_pricestoredollar" id="dev_ps_pricestoredollar" disabled><br>
							<font class="text-tiny"><?php echo '*'.$TEXT['automatic']?></font><br><br>
							<font class="text-low-small"><?php echo $TEXT2['devconsole_pricestorerupiah'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_pricestorerupiah" id="dev_ps_pricestorerupiah" disabled><br>
							<font class="text-tiny"><?php echo '*'.$TEXT['automatic']?></font><br><br>
							<font class="text-small"><?php echo $TEXT2['devconsole_priceexchange'].getRateExchange()?></font><br>
							<hr>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_progressing']?></font><br><br>
							<font class="text-small"><?php echo $TEXT2['devconsole_progressdatestarted'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="date" name="dev_ps_datestarted" id="dev_ps_datestarted" value="<?php if (!empty($product['date_started'])) echo $product['date_started']?>" required><br>
							<label><input type="checkbox" name="dev_ps_notfinish" id="dev_ps_notfinish" <?php if (!empty($product['dev_ps_datepublished']) && $product['dev_ps_datepublished'] == "0000-00-00") echo "selected" ?>> <font class="text-tiny"><?php echo $TEXT2['devconsole_progressnotfinish']?></font></label><br><br>
							<font class="text-small"><?php echo $TEXT2['devconsole_progresspercentage'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="number" min="0" max="100" name="dev_ps_progress" id="dev_ps_progress" value="<?php if (!empty($product['progress'])) echo $product['progress']?>" required><br>
							<font class="text-tiny"><?php echo $TEXT2['devconsole_progresspercentage_info']?></font><br><br>
							<font class="text-small"><?php echo $TEXT2['devconsole_progresslabel'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-small center" type="text" name="dev_ps_label" id="dev_ps_label" value="<?php if (!empty($product['label'])) echo $product['label']?>" required><br><br>
							<label><input type="checkbox" name="dev_ps_showpublic" id="dev_ps_showpublic" <?php if (!empty($product['show_public']) && $product['show_public'] == 1) echo "checked"?>>  <font class="text-small"><?php echo $TEXT2['devconsole_progressshow']?></font></label><br>
							<hr>
							<font class="text-low-medium"><?php echo $TEXT2['devconsole_fileset']?></font><br>
							<font class="text-tiny" id="dev_ps_filereplace_info"><?php echo $TEXT2['devconsole_filereplace_info']?><br></font><br>							
							<font class="text-small"><?php echo $TEXT2['devconsole_fileproduct'].' : '?></font><br>
							<input type="file" accept=".exe,.zip,.rar" name="dev_ps_file" id="dev_ps_file"><br>
							<font class="text-tiny"><?php echo $TEXT2['devconsole_fileproduct_info']?></font><br><br>
							<font class="text-small"><?php echo $TEXT2['devconsole_filelink'].' : '?></font><br>
							<input class="input-text-border-white-w90-h30 text-tiny center" type="text" name="dev_ps_link" id="dev_ps_link"><br>
							<font class="text-tiny"><?php echo $TEXT2['devconsole_filelink_info']?></font><br>
							<hr>
							<input class="btn-green" style="margin-right: 12%;width: 25%;height: 30px;" type="submit" name="dev_ps_submit_add" id="dev_ps_submit_add" value="<?php echo $TEXT['addupdate']?>"><?php if (!empty($product['mode']) && $product['mode'] == "edit"){?><a href="<?php echo $BASENAME['devconsole_product']?>"><input class="btn-transparent cursor-hand" style="margin-right: 12%;width: 25%;height: 30px;" type="button" value="<?php echo $TEXT['reset']?>"></a><?php } else {?><input class="btn-transparent cursor-hand" style="margin-right: 12%;width: 25%;height: 30px;" type="reset" name="dev_ps_reset" id="dev_ps_reset" value="<?php echo $TEXT['reset']?>"><?php }?><input class="btn-red" style="width: 25%;height: 30px;" type="submit" name="dev_ps_submit_delete" id="dev_ps_submit_delete" onclick="if (!confirm(`<?php echo $TEXT2['devconsole_product_delete_confirm']?>`)) return false;" value="<?php echo $TEXT['delete']?>">
							</form>
						</div>
					</div>
					<!-- End of Left Section-->
					<div class="container-middle-right content-element-dark right" style="margin-top:50px;width: 55%;height: auto">
						<input class="search-100" type="text" name="devconsole_product_search" placeholder="<?php echo $TEXT2['devconsole_product_search']?>" value="<?php echo getSearchPlaceholder()?>" onchange="devconsoleProduct_search()" id="devconsole_product_search"><br>
						<div style="margin-top: 10px;"><label class="text-small text-white-shadow" style="margin-right: 2%;"><?php echo $TEXT['sort_by'].'  '?></label><label class="text-low-small text-block-hover-gray" style="margin-right: 5%;" onclick="window.open(basename+baseDevConsoleProduct+'?cs=dp','_SELF')"><?php echo $sort[0]?></label><label class="text-low-small text-block-hover-gray" style="margin-right: 5%;" onclick="window.open(basename+baseDevConsoleProduct+'?cs=p','_SELF')"><?php echo $sort[1]?></label><label class="text-low-small text-block-hover-gray" onclick="window.open(basename+baseDevConsoleProduct+'?cs=ds','_SELF')"><?php echo $sort[2]?></label><br>
						<?php echo $TEXT2['devconsole_productclickinfo']?></div>
						<?php echo getDevProduct($_SESSION['dev_id'])?>
					</div>
				</div>
				__________________________________________________________________________________________________________________________________________________
			</div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>