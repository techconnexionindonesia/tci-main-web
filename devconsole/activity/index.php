<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "activity-dev";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['devconsole_activity'];
if (empty($_SESSION['usr_id'])) goPage("login","");
if (!isDevAlreadySignup($_SESSION['usr_id'])) goPage("devconsole_signup"."");
if (isDevWaitValid($_SESSION['usr_id'])) goPage("devconsole_wait"."");
if (empty($_SESSION['dev_id'])) goPage("devconsole_login","");
$devData = getDevData($_SESSION['dev_id']);
$dev_id = $_SESSION['dev_id'];
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Activity - Devconsole</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'devconsole-activity.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg3.jpg'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element-blue">
			<div style="width: 100%;height: 120px;">
				<div class="left" style="width: 20%;">
					<img src="<?php echo $BASENAME['img_html'].'icon_devlogo.png'?>" style="width: 100%;height: 120px;"><hr>
				</div>
				<div class="right" style="width: 80%;padding-top: 30px;">
					<img class="img-w20-h120-border-hoverable right cursor-hand" style="margin-left: 2%;margin-top: -30px;" src="<?php echo $devData['pic_link']?>">
					<a href="<?php echo $BASENAME['devconsole_config']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_config']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_activity']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_activity']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_finance']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_finance']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_product']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_product']?></label></a>
				</div>
			</div>
			<br>
			<br>
			<hr>
			<label class="text-low-medium left" style="margin-right: 2%;"><?php echo $TEXT2['select'].' : '?></label>
			<label class="text-block-hover-gray2 text-low-medium left" style="margin-right: 2%;"><?php echo $TEXT['activity']?></label>
			<label class="text-block-hover-gray2 text-low-medium left" style="margin-right: 2%;" onclick="alert('News feature coming soon! , once available, you could make news from here')"><?php echo $TEXT['news']?></label>
			<br>
			<br>
			<hr>
			<div>
				<div class="content-center-big-45-center content-element-dark left" style="margin-right: 1%;height: 400px;">
					<font class="text-big text-white-shadow"><?php echo $TEXT2['devconsole_avt_acttransaction']?></font><br><br>
					<?php $query = mysql_query("SELECT * FROM user_user_transaction WHERE id_developer = '$dev_id' ORDER BY datetime DESC LIMIT 11") or die (mysql_error());?>					
					<table class="table-w100-h20-blue cursor-hand text-low-tiny">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_user']?></th>
							<th><?php echo $TEXT2['devconsole_avt_product']?></th>
							<th><?php echo $TEXT2['devconsole_avt_price']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_usrid = $rs['id_user'];
							$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid LIMIT 13");
							if (mysql_num_rows($query_usr) == 1){
								$rs2 = mysql_fetch_array($query_usr);
								$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
							}else $t_usr = $x_usrid;
							$x_pid = $rs['id_stuff'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
								UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
								UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
								UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
							if (mysql_num_rows($query_p) == 1){
								$rs2 = mysql_fetch_array($query_p);
								$t_p = $rs2['title'];
							}else $t_p = $x_pid;
						?>
						<tr>
							<td><?php echo $t_usr ?></td>
							<td><?php echo $t_p ?></td>
							<td><?php echo getSym().cvtMoney($rs['price'])?></td>
							<td><?php echo $rs['datetime']?></td>
						</tr>
					<?php }?>
					</table>
					<br>
					<button class="btn-green btn-w40-h40-square" onclick="showFullTransaction()"><?php echo $TEXT2['devconsole_avt_more_transaction']?></button>
				</div>
				<div class="content-center-big-45-center content-element-dark left" style="height: 400px;">
					<font class="text-big text-white-shadow"><?php echo $TEXT2['devconsole_avt_actdeveloper']?></font><br><br>
					<?php
					$query = mysql_query("SELECT * FROM dev_activity WHERE id_developer='$dev_id' ORDER BY datetime DESC LIMIT 6")
					?>
					<table class="table-w100-h20-blue cursor-hand text-low-tiny">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_admin']?></th>
							<th><?php echo $TEXT2['devconsole_avt_activity']?></th>
							<th><?php echo $TEXT2['devconsole_avt_target']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_usrid = $rs['subject'];
							$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid");
							if (mysql_num_rows($query_usr) == 1){
								$rs2 = mysql_fetch_array($query_usr);
								$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
							}else $t_usr = $rs['subject'];
							$x_pid = $rs['object'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
									UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
									UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
									UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
								if (mysql_num_rows($query_p) == 1){
									$rs2 = mysql_fetch_array($query_p);
									$t_p = $rs2['title'];
								}else $t_p = $x_pid;
							?>
							<tr>
								<td><?php echo $t_usr ?></td>
								<td><?php echo getStringFromArray($rs['doing'])?></td>
								<td><?php echo $t_p ?></td>
								<td><?php echo $rs['datetime']?></td>
							</tr>
						<?php }?>
					</table>
					<br>
					<button class="btn-green btn-w40-h40-square" onclick="showFullActivity()"><?php echo $TEXT2['devconsole_avt_more_admin']?></button>
				</div>
				<div class="content-center-big-45-center content-element-dark left" style="margin-right: 1%;height: 400px;">
					<font class="text-big text-white-shadow"><?php echo $TEXT2['devconsole_avt_actvote']?></font><br><br>
					<?php
					$query = mysql_query("SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_game WHERE id_stuff = id_game AND tci_game.id_developer = '$dev_id'
						UNION SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_dlc WHERE id_stuff = id_dlc AND tci_dlc.id_developer = '$dev_id'
						UNION SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_content WHERE id_stuff = id_content AND tci_content.id_developer = '$dev_id'
						UNION SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_app WHERE id_stuff = id_app AND tci_app.id_developer = '$dev_id' ORDER BY datetime DESC LIMIT 7") OR die (mysql_error());?>
					<table class="table-w100-h30-blue cursor-hand text-low-tiny">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_product']?></th>
							<th><?php echo $TEXT2['devconsole_avt_vote']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_pid = $rs['id_stuff'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
									UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
									UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
									UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
								if (mysql_num_rows($query_p) == 1){
									$rs2 = mysql_fetch_array($query_p);
									$t_p = $rs2['title'];
								}else $t_p = $x_pid;
							if ($rs['vote'] == "y") $t_v = $BASENAME['img_html'].'vote_yes.png';
							else $t_v = $BASENAME['img_html'].'vote_no.png';
							?>
							<tr>
								<td><?php echo $t_p ?></td>
								<td><img src="<?php echo $t_v ?>" style="width: 70%;height: 25px;"></td>
								<td><?php echo $rs['datetime']?></td>
							</tr>
						<?php }?>
					</table>
					<br>
					<button class="btn-green btn-w40-h40-square" onclick="showFullVote()"><?php echo $TEXT2['devconsole_avt_more_vote']?></button>
				</div>
				<div class="content-center-big-45-center content-element-dark left" style="height: 400px;">
					<font class="text-big text-white-shadow"><?php echo $TEXT2['devconsole_avt_actreview']?></font><br><br>
					<?php
					$query = mysql_query("SELECT id_user,id_stuff,text,datetime FROM user_review,tci_game WHERE id_stuff = id_game AND id_developer = '$dev_id'
						UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_dlc WHERE id_stuff = id_dlc AND id_developer = '$dev_id'
						UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_content WHERE id_stuff = id_content AND id_developer = '$dev_id'
						UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_app WHERE id_stuff = id_app AND id_developer = '$dev_id' ORDER BY datetime DESC limit 11") or die(mysql_error());
					?>
					<table class="table-w100-h20-blue text-low-tiny">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_product']?></th>
							<th><?php echo $TEXT2['devconsole_avt_review']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_pid = $rs['id_stuff'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
									UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
									UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
									UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
								if (mysql_num_rows($query_p) == 1){
									$rs2 = mysql_fetch_array($query_p);
									$t_p = $rs2['title'];
								}else $t_p = $x_pid;
							?>
						<tr>
							<td><?php echo $t_p ?></td>
							<td><?php echo mb_strimwidth($rs['text'], 0, 63, "...");?></td>
							<td><?php echo $rs['datetime']?></td>
						<?php }?>
					</table>
					<br>
					<button class="btn-green btn-w40-h40-square" onclick="showFullReview()"><?php echo $TEXT2['devconsole_avt_more_review']?></button>
				</div>
			</div>
			_________________<br>
			* Real Time Updated<br>
			_________________
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
		<div id="webmask">
			<div class="content-center-big-80-default content-element-blue" id="div-transaction" style="height: 550px;overflow-y: scroll;">
				<button class="btn-w30-h40-square btn-transparent text-low-medium" onclick="hide()"><?php echo $TEXT2['devconsole_avt_close']?></button>
				<br><br>
				<center>
					<?php $query = mysql_query("SELECT * FROM user_user_transaction WHERE id_developer = '$dev_id' ORDER BY datetime DESC") or die (mysql_error());?>					
					<table class="table-w100-h20-blue cursor-hand text-low-tiny center">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_user']?></th>
							<th><?php echo $TEXT2['devconsole_avt_product']?></th>
							<th><?php echo $TEXT2['devconsole_avt_price']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
							<th><?php echo $TEXT2['devconsole_avt_info']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_usrid = $rs['id_user'];
							$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid LIMIT 13");
							if (mysql_num_rows($query_usr) == 1){
								$rs2 = mysql_fetch_array($query_usr);
								$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
							}else $t_usr = $x_usrid;
							$x_pid = $rs['id_stuff'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
								UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
								UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
								UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
							if (mysql_num_rows($query_p) == 1){
								$rs2 = mysql_fetch_array($query_p);
								$t_p = $rs2['title'];
							}else $t_p = $x_pid;
						?>
						<tr>
							<td><?php echo $t_usr ?></td>
							<td><?php echo $t_p ?></td>
							<td><?php echo getSym().cvtMoney($rs['price'])?></td>
							<td><?php echo $rs['datetime']?></td>
							<td><?php echo $rs['info']?></td>
						</tr>
					<?php }?>
					</table>
				</center>
			</div>			
			<div class="content-center-big-80-default content-element-blue" id="div-activity"  style="height: 550px;overflow-y: scroll;">
				<button class="btn-w30-h40-square btn-transparent text-low-medium" onclick="hide()"><?php echo $TEXT2['devconsole_avt_close']?></button>
				<br><br>
				<center>
					<?php
					$query = mysql_query("SELECT * FROM dev_activity WHERE id_developer='$dev_id' ORDER BY datetime DESC")
					?>
					<table class="table-w100-h20-blue cursor-hand text-low-tiny center">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_admin']?></th>
							<th><?php echo $TEXT2['devconsole_avt_activity']?></th>
							<th><?php echo $TEXT2['devconsole_avt_target']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
							<th><?php echo $TEXT2['devconsole_avt_info']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_usrid = $rs['subject'];
							$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid");
							if (mysql_num_rows($query_usr) == 1){
								$rs2 = mysql_fetch_array($query_usr);
								$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
							}else $t_usr = $rs['subject'];
							$x_pid = $rs['object'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
									UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
									UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
									UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
								if (mysql_num_rows($query_p) == 1){
									$rs2 = mysql_fetch_array($query_p);
									$t_p = $rs2['title'];
								}else $t_p = $x_pid;
							?>
							<tr>
								<td><?php echo $t_usr ?></td>
								<td><?php echo getStringFromArray($rs['doing'])?></td>
								<td><?php echo $t_p ?></td>
								<td><?php echo $rs['datetime']?></td>
								<td><?php echo $rs['info']?></td>
							</tr>
						<?php }?>
					</table>
				</center>
			</div>
			<div class="content-center-big-50-default content-element-blue" id="div-vote"  style="height: 550px;overflow-y: scroll;">
				<button class="btn-w30-h40-square btn-transparent text-low-medium" onclick="hide()"><?php echo $TEXT2['devconsole_avt_close']?></button>
				<br><br>
				<center>
					<?php
					$query = mysql_query("SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_game WHERE id_stuff = id_game AND tci_game.id_developer = '$dev_id'
						UNION SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_dlc WHERE id_stuff = id_dlc AND tci_dlc.id_developer = '$dev_id'
						UNION SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_content WHERE id_stuff = id_content AND tci_content.id_developer = '$dev_id'
						UNION SELECT id_stuff,id_user,vote,datetime FROM user_vote_stuff,tci_app WHERE id_stuff = id_app AND tci_app.id_developer = '$dev_id' ORDER BY datetime DESC") OR die (mysql_error());?>
					<table class="table-w100-h30-blue cursor-hand text-low-tiny center">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_product']?></th>
							<th><?php echo $TEXT2['devconsole_avt_vote']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_pid = $rs['id_stuff'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
									UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
									UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
									UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
								if (mysql_num_rows($query_p) == 1){
									$rs2 = mysql_fetch_array($query_p);
									$t_p = $rs2['title'];
								}else $t_p = $x_pid;
							if ($rs['vote'] == "y") $t_v = $BASENAME['img_html'].'vote_yes.png';
							else $t_v = $BASENAME['img_html'].'vote_no.png';
							?>
							<tr>
								<td><?php echo $t_p ?></td>
								<td><img src="<?php echo $t_v ?>" style="width: 70%;height: 25px;"></td>
								<td><?php echo $rs['datetime']?></td>
							</tr>
						<?php }?>
					</table>
				</center>
			</div>
			<div class="content-center-big-80-default content-element-blue" id="div-review"  style="height: 550px;overflow-y: scroll;">
				<button class="btn-w30-h40-square btn-transparent text-low-medium" onclick="hide()"><?php echo $TEXT2['devconsole_avt_close']?></button>
				<br><br>
				<center>
					<?php
					$query = mysql_query("SELECT id_user,id_stuff,text,datetime FROM user_review,tci_game WHERE id_stuff = id_game AND id_developer = '$dev_id'
						UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_dlc WHERE id_stuff = id_dlc AND id_developer = '$dev_id'
						UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_content WHERE id_stuff = id_content AND id_developer = '$dev_id'
						UNION SELECT id_user,id_stuff,text,datetime FROM user_review,tci_app WHERE id_stuff = id_app AND id_developer = '$dev_id' ORDER BY datetime DESC") or die(mysql_error());
					?>
					<table class="table-w100-h20-blue text-low-tiny center">
						<tr>
							<th><?php echo $TEXT2['devconsole_avt_user']?></th>
							<th><?php echo $TEXT2['devconsole_avt_product']?></th>
							<th><?php echo $TEXT2['devconsole_avt_review']?></th>
							<th><?php echo $TEXT2['devconsole_avt_time']?></th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_usrid = $rs['id_user'];
							$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid");
							if (mysql_num_rows($query_usr) == 1){
								$rs2 = mysql_fetch_array($query_usr);
								$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
							}else $t_usr = $rs['subject'];
							$x_pid = $rs['id_stuff'];
							$query_p = mysql_query("SELECT title FROM tci_game WHERE id_game = '$x_pid'
									UNION SELECT title FROM tci_dlc WHERE id_dlc = '$x_pid'
									UNION SELECT title FROM tci_content WHERE id_content = '$x_pid'
									UNION SELECT title FROM tci_app WHERE id_app = '$x_pid'") or die (mysql_error());
								if (mysql_num_rows($query_p) == 1){
									$rs2 = mysql_fetch_array($query_p);
									$t_p = $rs2['title'];
								}else $t_p = $x_pid;
							?>
						<tr>
							<td><?php echo $t_usr ?></td>
							<td ><?php echo $t_p ?></td>
							<td><?php echo mb_strimwidth($rs['text'], 0, 100, "...");?></td>
							<td><?php echo $rs['datetime']?></td>
						<?php }?>
					</table>
				</center>
			</div>
		</div>
		<script type="text/javascript">hide()</script>
	</body>
</html>