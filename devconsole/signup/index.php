<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "signup-dev";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['devconsole_signup'];
if (empty($_SESSION['usr_id'])) goPage("login","");
if (isDevAlreadySignup($_SESSION['usr_id'])) goPage("devconsole"."");
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Signup - Devconsole</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg3.jpg'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element-blue">
			<div style="width: 100%;height: 120px;"><div class="left" style="width: 20%;"><img src="<?php echo $BASENAME['img_html'].'icon_devlogo.png'?>" style="width: 100%;height: 120px;"><hr></div><div class="right" style="width: 80%;padding-top: 30px;"><label class="right text-big text-white-shadow"><?php echo $TEXT['devconsole_signup']?></label></div></div>
			<div class="content-center-big-100-default content-element-dark">
			<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>" enctype="multipart/form-data">
				<div class="left" style="width: 50%;">
				<table border="0" class="table-double-w80">
				<?php
				if (!empty($_SESSION['usr_id'])){
				$usr_id = $_SESSION['usr_id'];
				$myData = getUserData($usr_id);
				}?>
					<label class="text-medium"><?php echo $TEXT['signup_personal']?></label><br><br>
					<tr>
						<td class="sl"><?php echo $TEXT['signup_name'].'</td><td class="sr"> : '.$myData['front_name'].' '.$myData['back_name']?></td>
					</tr>
					<tr>
						<td class="sl"><?php echo $TEXT['id'].'</td><td class="sr"> : '.$myData['id_user']?><input type="hidden" name="dev_signup_usr_id" value="<?php echo $myData['id_user']?>"></td>
					</tr>
					<tr>
						<td class="sl"><?php echo $TEXT['signup_password']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30" type="password" name="dev_signup_usr_password" placeholder="<?php echo $TEXT['signup_password_re']?>" required></td>
					</tr>
				</table>
				</div>
				<div class="left" style="width: 50%;">
					<label class="text-medium"><?php echo $TEXT['devconsole_createdev']?></label><br><br>
					<table class="table-double-w80">
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_iddev']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" minlength="3" name="dev_signup_iddev" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_password']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="password" minlength="6" name="dev_signup_password" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['signup_password_re']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="password" name="dev_signup_repassword" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['signup_name']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_signup_name" minlength="3" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_createp']?><br><br></td><td class="sr"> : <input type="checkbox" name="dev_signup_createp[]" value="gamedlc"><?php echo $TEXT['gamedlc']?></input>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="dev_signup_createp[]" value="content"><?php echo $TEXT['content']?></input><br>&nbsp;&nbsp;<input type="checkbox" name="dev_signup_createp[]" value="app"><?php echo $TEXT['app']?></input></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_type']?><br><br><br></td><td class="sr">&rarr; <?php echo $TEXT['devconsole_structure'].' : '?><select name="dev_signup_structure">
								<option value="single"><?php echo $TEXT['devconsole_typestc_single']?></option>
								<option value="team"><?php echo $TEXT['devconsole_typestc_team']?></option>
								<option value="company"><?php echo $TEXT['devconsole_typestc_company']?></option>
							</select><br><br>
								&rarr; <?php echo $TEXT['devconsole_place'].' : '?><select name="dev_signup_place">
								<option value="home"><?php echo $TEXT['devconsole_typeplc_home']?></option>
								<option value="office"><?php echo $TEXT['devconsole_typeplc_office']?></option>
								<option value="any"><?php echo $TEXT['devconsole_typeplc_any']?></option>
							</select></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['signup_email']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="email" name="dev_signup_email" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['signup_description']?></td><td class="sr"> : <textarea name="dev_signup_description" minlength="100" class="textarea-w90-h50-transparent-inner text-tiny" required></textarea><br>&nbsp;&nbsp;* <?php echo $TEXT['alert_en']?></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_logo']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_signup_logo" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_site']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_signup_site"></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bank']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_signup_bank"></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bankname']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_signup_bankname"></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bankno']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="number" name="dev_signup_bankno"></td>
						</tr>
						<tr>
							<td class="sl" colspan="2"><input type="checkbox" name="dev_signup_rule" required> <a style="color: lightblue;" href="<?php echo $BASENAME['about_us_rule']?>"><?php echo $TEXT['devconsole_agree']?></a></td>
						</tr>
						<tr>
							<td class="sl" colspan="2"><br><font class="text-lightred"><?php if (!empty($_GET['err'])) echo $TEXT['alert'].' : '.getDevErrorTxt()?></font></td>
						</tr>
						<tr style="height: 80px;">
							<td class="sl" colspan="2"><center><input class="btn-w40-h40-square btn-green" type="submit" name="dev_signup_submit" value="<?php echo $TEXT['submit']?>"></center></td>
						</tr>
					</table>
				</div>
			</form>
			_
			</div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>