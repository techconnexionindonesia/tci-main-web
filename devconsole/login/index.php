<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "login-dev";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['devconsole_login'];
if (empty($_SESSION['usr_id'])) goPage("login","");
if (!isDevAlreadySignup($_SESSION['usr_id'])) goPage("devconsole_signup"."");
if (isDevWaitValid($_SESSION['usr_id'])) goPage("devconsole_wait"."");
if (!empty($_SESSION['dev_id'])) goPage("devconsole","");
$devData = getDevDataByUsrId($_SESSION['usr_id']);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Login - Devconsole</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg3.jpg'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element-blue">
			<div style="width: 100%;height: 120px;">
				<div class="left" style="width: 20%;">
					<img src="<?php echo $BASENAME['img_html'].'icon_devlogo.png'?>" style="width: 100%;height: 120px;"><hr>
				</div>
				<div class="right" style="width: 80%;padding-top: 30px;">
					<img class="img-w20-h120-border-hoverable right cursor-hand" style="margin-left: 2%;margin-top: -30px;" src="<?php echo $devData['pic_link']?>">
					<a href="<?php echo $BASENAME['devconsole_config']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_config']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_activity']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_activity']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_finance']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_finance']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_product']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_product']?></label></a>
				</div>
			</div>
			<div class="content-center-big-50-default content-element-dark">
				<center><h1 class="title-white-big"><?php echo $TEXT['login_title']?></h1></center>
				<br>
				<div class="textbox">
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<table class="table-double-w100">
						<tr>
							<td class="sl"><?php echo $TEXT2['data_password_dev']?></td>
							<td class="sr">: <input class="input-text-noborder-gray-w90-h30" type="password" name="dev_login_password" placeholder="<?php echo $TEXT['login_ph_password']?>"></td>
						</tr>
						<tr>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td colspan="2"><center><input class="btn-w40-h40-square btn-green" type="submit" name="dev_login_submit" value="<?php echo $TEXT['submit']?>"></center></td>
						</tr>
					</table>
					</form>
				</div>
			</div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>