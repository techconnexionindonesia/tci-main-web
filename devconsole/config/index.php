<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "config-dev";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['devconsole_config'];
if (empty($_SESSION['usr_id'])) goPage("login","");
if (!isDevAlreadySignup($_SESSION['usr_id'])) goPage("devconsole_signup"."");
if (isDevWaitValid($_SESSION['usr_id'])) goPage("devconsole_wait"."");
if (empty($_SESSION['dev_id'])) goPage("devconsole_login","");
$devData = getDevData($_SESSION['dev_id']);
$dev_id = $_SESSION['dev_id'];
$dev_position = getDevPosition($_SESSION['usr_id']);
$dev_create = explode(";", $devData['create']);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Configuration - Devconsole</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg3.jpg'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element-blue">
			<div style="width: 100%;height: 120px;">
				<div class="left" style="width: 20%;">
					<img src="<?php echo $BASENAME['img_html'].'icon_devlogo.png'?>" style="width: 100%;height: 120px;"><hr>
				</div>
				<div class="right" style="width: 80%;padding-top: 30px;">
					<img class="img-w20-h120-border-hoverable right cursor-hand" style="margin-left: 2%;margin-top: -30px;" src="<?php echo $devData['pic_link']?>">
					<a href="<?php echo $BASENAME['devconsole_config']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_config']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_activity']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_activity']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_finance']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_finance']?></label></a>
					<a href="<?php echo $BASENAME['devconsole_product']?>"><label class="text-block-hover-gray2 text-medium right" style="margin-left: 2%;"><?php echo $TEXT['devconsole_product']?></label></a>
				</div>
			</div>
			<br>
			<br>
			<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>" enctype="multipart/form-data">
			<div style="height: 800px;">
				<div class="left" style="width: 30%;height: 800px; margin-right: 10%;"><!-- Left Section-->
					<div class="content-center-big-100-default content-element-dark left">
						<table border="0" class="table-double-w100">
						<?php
						if (!empty($_SESSION['usr_id'])){
						$usr_id = $_SESSION['usr_id'];
						$myData = getUserData($usr_id);
						}?>
							<label class="text-medium">User Verification</label><br><br>
							<tr>
								<td class="sl"><?php echo $TEXT['signup_name'].'</td><td class="sr"> : '.$myData['front_name'].' '.$myData['back_name']?></td>
							</tr>
							<tr>
								<td class="sl"><?php echo $TEXT['id'].'</td><td class="sr"> : '.$myData['id_user']?></td>
							</tr>
							<tr>
								<td class="sl"><?php echo $TEXT['signup_password']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30" type="password" name="dev_config_usr_password" placeholder="<?php echo $TEXT['signup_password_re']?>" required></td>
							</tr>
						</table>
						<br><br>
						<table border="0" class="table-double-w100">
							<label class="text-medium">Developer Verification</label><br><br>
							<tr>
								<td class="sl"><?php echo $TEXT['devconsole_iddev'].'</td><td class="sr"> : '.$devData['id_developer']?></td>							
							</tr>
							<tr>
								<td class="sl"><?php echo $TEXT['signup_password']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30" type="password" name="dev_config_dev_password_old" placeholder="<?php echo $TEXT['signup_password_re']?>" required></td>
							</tr>
						</table>
						<br>
						<label class="text-tiny"><?php echo $TEXT2['devconsole_verification_info']?></label><br><br>
					</div>
					<?php if ($dev_position == "owner"){?>
					<div class="content-center-fsfh-default content-element-dark left" style="width:100%;height: 220px;">
						<label class="text-medium"><?php echo $TEXT2['devconsole_logincontrol']?></label><br><br>
						<?php echo $TEXT2['devconsole_password_new'].' :'?><br>
						<input class="input-text-noborder-gray-w90-h30" type="password" name="dev_config_dev_password_new" placeholder="<?php echo $TEXT2['devconsole_password_new']?>">
						<br><br>
						<?php echo $TEXT['signup_password_re'].' :'?><br>
						<input class="input-text-noborder-gray-w90-h30" type="password" name="dev_config_dev_password_new_re" placeholder="<?php echo $TEXT['signup_password_re']?>">
						<br><br><?php echo $TEXT2['devconsole_logincontrol_info'];?>
					</div>
				<?php }?>
				</div>
				<div class="content-center-big-50-default content-element-dark left">
					<label class="text-medium"><?php echo $TEXT2['devconsole_devdata']?></label><br><br>
					<table class="table-double-w100">
						<tr>
							<td class="sl"><?php echo $TEXT['signup_name']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_config_name" minlength="3" value="<?php echo $devData['name']?>" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_createp']?><br><br></td><td class="sr"> : <input type="checkbox" name="dev_config_createp[]" value="gamedlc" <?php if (in_array("gamedlc", $dev_create)) echo "checked"?>><?php echo $TEXT['gamedlc']?></input>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox" name="dev_config_createp[]" value="content" <?php if (in_array("content", $dev_create)) echo "checked"?>><?php echo $TEXT['content']?></input><br>&nbsp;&nbsp;<input type="checkbox" name="dev_config_createp[]" value="app" <?php if (in_array("app", $dev_create)) echo "checked"?>><?php echo $TEXT['app']?></input></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_type']?><br><br><br></td><td class="sr">&rarr; <?php echo $TEXT['devconsole_structure'].' : '?><select name="dev_config_structure">
								<option value="single" <?php if ($devData['type_structure'] == "single") echo "selected"?>><?php echo $TEXT['devconsole_typestc_single']?></option>
								<option value="team" <?php if ($devData['type_structure'] == "team") echo "selected"?>><?php echo $TEXT['devconsole_typestc_team']?></option>
								<option value="company" <?php if ($devData['type_structure'] == "company") echo "selected"?>><?php echo $TEXT['devconsole_typestc_company']?></option>
							</select><br><br>
								&rarr; <?php echo $TEXT['devconsole_place'].' : '?><select name="dev_config_place">
								<option value="home" <?php if ($devData['type_place'] == "home") echo "selected"?>><?php echo $TEXT['devconsole_typeplc_home']?></option>
								<option value="office" <?php if ($devData['type_place'] == "office") echo "selected"?>><?php echo $TEXT['devconsole_typeplc_office']?></option>
								<option value="any" <?php if ($devData['type_place'] == "any") echo "selected"?>><?php echo $TEXT['devconsole_typeplc_any']?></option>
							</select></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['signup_email']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="email" name="dev_config_email" value="<?php echo $devData['email']?>" required></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['signup_description']?></td><td class="sr"> : <textarea name="dev_config_description" minlength="100" class="textarea-w90-h50-transparent-inner text-tiny" required><?php echo $devData['description']?></textarea><br>&nbsp;&nbsp;* <?php echo $TEXT['alert_en']?></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_logo']?><br></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="file" accept=".jpg,.jpeg,.png,.bmp,.gif" name="dev_config_logo"><br><font class="text-tiny">&nbsp;&nbsp;<?php echo $TEXT2['devconsole_picchange_info']?></font></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_site']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_config_site" value="<?php echo $devData['site']?>"></td>
						</tr>
						<tr>
							<td colspan="2"><br><br><b><?php echo $TEXT2['devconsole_bankdata']?></b><hr></td>
						</tr>
						<!-- Bank Owner Section-->
						<?php if ($dev_position == "owner"){?>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bank']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_config_bank" value="<?php echo $devData['bank']?>"></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bankname']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="text" name="dev_config_bankname" value="<?php echo $devData['bank_name']?>"></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bankno']?></td><td class="sr"> : <input class="input-text-noborder-gray-w90-h30 text-low-small" type="number" name="dev_config_bankno" value="<?php echo $devData['bank_no']?>"></td>
						</tr>
						<?php }?>
						<!-- Bank Admin Section-->
						<?php if ($dev_position == "admin"){?>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bank']?></td><td class="sr"> : <?php echo $devData['bank']?></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bankname']?></td><td class="sr"> : <?php echo $devData['bank_name']?></td>
						</tr>
						<tr>
							<td class="sl"><?php echo $TEXT['devconsole_bankno']?></td><td class="sr"> : <?php echo $devData['bank_no']?></td>
						</tr>
						<?php }?>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td colspan="2" class="text-tiny"><?php echo $TEXT['devconsole_bankchange_info']?></td>
						</tr>
						<tr>
							<td class="sl" colspan="2"><br><font class="text-lightred"><?php if (!empty($_GET['err'])) echo $TEXT['alert'].' : '.getDevErrorTxt()?></font></td>
						</tr>
					</table>
				</div>
				</div>
				<center><input class="btn-w40-h40-square btn-green" type="submit" name="dev_config_submit" value="<?php echo $TEXT['submit']?>"></center></center>
				<br><br><br>
				<font class="text-big title-white-shadow"><?php echo $TEXT['devconsole_config_admin']?></font>
				<hr>
			</form>
				<!--Administration Section-->
				<?php if ($dev_position == "owner"){?>
				<div class="content-center-big-50-default content-element-dark">
					<?php if ($devData['admin_count'] < 5){?>
					<font class="text-low-medium"><?php echo $TEXT2['devconsole_add_admin_username'].' :'?></font>
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<input class="input-text-noborder-gray-w90-h30 text-low-small" style="margin-right: 2%;" type="text" name="dev_admin_username" placeholder="Input Username"><input class="btn-green" style="width: 7%;" type="submit" name="dev_admin_add_submit" value="OK">
					</form>
					<br><br><br><br>
					<?php }?>
					<font class="text-low-medium"><?php echo $TEXT2['devconsole_administrator_list']?></font>
					<hr><?php echo $TEXT2['devconsole_administrator_num_info']?><br><br>
					<?php
					$query = mysql_query("SELECT * FROM dev_admin WHERE id_developer = '$dev_id'");?>
					<table class="table-w100-h20-blue center" border="0">
						<tr>
							<th><?php echo $TEXT2['administrator']?></th>
							<th><?php echo $TEXT2['confirm']?></th>
							<th>Action</th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_usrid = $rs['id_user'];
							$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid");
							if (mysql_num_rows($query_usr) == 1){
								$rs2 = mysql_fetch_array($query_usr);
								$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
							}else $t_usr = $rs['id_user'];
							if ($rs['confirm'] == 1) $t_c = $TEXT2['confirm_1'];
							else $t_c = $TEXT2['confirm_0'];?>
						<tr>
							<td><?php echo $t_usr?></td>
							<td><?php echo $t_c?></td>
							<td>
								<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
								<input type="hidden" name="dev_admin_id" value="<?php echo $rs['id_user']?>">
								<input class="btn-red" style="width: 70%;" type="submit" name="dev_admin_delete_submit" value="<?php echo $TEXT['delete']?>" onclick="if (!confirm('Are you Sure?')) return false">
								</form>
							</td>
						</tr>
						<?php }?>
					</table>
				</div>
				<?php }?>
				<!--Administration for Admin Section-->
				<?php if ($dev_position == "admin"){?>
				<div class="content-center-big-50-default content-element-dark">
					<font class="text-low-medium">Administrator List</font>
					<hr><br>
					<?php
					$query = mysql_query("SELECT * FROM dev_admin WHERE id_developer = '$dev_id'");?>
					<table class="table-w100-h20-blue center" border="0">
						<tr>
							<th>Administrator</th>
							<th>Confirm</th>
							<th>Action</th>
						</tr>
						<?php
						while ($rs = mysql_fetch_array($query)) {
							$x_usrid = $rs['id_user'];
							$query_usr = mysql_query("SELECT front_name,back_name FROM user_user WHERE id_user = $x_usrid");
							if (mysql_num_rows($query_usr) == 1){
								$rs2 = mysql_fetch_array($query_usr);
								$t_usr = $rs2['front_name'].' '.$rs2['back_name'];
							}else $t_usr = $rs['subject'];
							if ($rs['confirm'] == 1) $t_c = "Confirmed";
							else $t_c = "Not yet";?>
						<tr>
							<td><?php echo $t_usr?></td>
							<td><?php echo $t_c?></td>
							<td>
								<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
								<?php if ($rs['id_user'] == $_SESSION['usr_id']){?><input class="btn-red" style="width: 70%;" type="submit" name="dev_admin_resign_submit" value="<?php echo $TEXT['delete']?>" onclick="if (confirm('Please Confirm') == 0) return false;"><?php }?>
								</form>
							</td>
						</tr>
						<?php }?>
					</table>
				</div>
				<?php }?>
			</div>
		</div>
	</body>
</html>