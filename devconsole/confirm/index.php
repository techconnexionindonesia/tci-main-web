<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "confirm-dev";
include '../../function/model.php';
$_SESSION['refferPage'] = $BASENAME['devconsole_confirm'];
if (empty($_SESSION['usr_id'])) goPage("login","");
if (!isDevAlreadySignup($_SESSION['usr_id'])) goPage("devconsole_signup"."");
if (!isDevWaitValid($_SESSION['usr_id'])) goPage("devconsole"."");
if (getDevPosition($_SESSION['usr_id']) != "admin") goPage("devconsole_wait","");
$devData = getDevDataByUsrId($_SESSION['usr_id']);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Confirm - Devconsole</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg3.jpg'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element-blue">
			<div style="width: 100%;height: 120px;"><div class="left" style="width: 20%;"><img src="<?php echo $BASENAME['img_html'].'icon_devlogo.png'?>" style="width: 100%;height: 120px;"><hr></div><div class="right" style="width: 80%;padding-top: 30px;"><label class="right text-big text-white-shadow"><?php echo $TEXT['devconsole_signup']?></label></div></div>
			<div class="content-center-big-50-default content-element-dark">
				<center><h1 class="title-white-big"><?php echo $TEXT2['title_congratulation']?></h1></center>
				<div class="textbox">
					<center>
					<?php echo $TEXT2['devconsole_admin_invitation']?><br><b><?php echo $devData['name']?></b><br><br>
					<?php echo $TEXT2['devconsole_admin_confirm']?><br><b><?php echo $devData['email']?></b><br><br>
					<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
					<table class="table-double-w100">
						<tr>
							<td class="center"><input class="btn-w90-h40-square btn-green" type="submit" name="dev_confirm_submit_accept" value="<?php echo $TEXT2['cmd_accept']?>"></td>
							<td class="center"><input class="btn-w90-h40-square btn-red" type="submit" name="dev_confirm_submit_reject" value="<?php echo $TEXT2['cmd_reject']?>"></td>
						</tr>
					</table>
					</form>
					</center>
				</div>
			</div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>