<?php
//Index Page - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "index-user";
include 'function/model.php';
$_SESSION['refferPage'] = $BASENAME['index'];
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Tech Connexion Indonesia</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>" id="btnselected"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="home-slideshow">
			<?php echo getHomeSlideShow();?>
		</div>
		<div class="content-left-right">
			<div class="inner content-element">
				<h3 class="title-white"><?php echo $TEXT['home_content_onprogress']?></h3>
				<?php echo getHomeProgressSlide();?>
				<br><br>
				<?php echo getHomeProgressList();?>
			</div>
			<div class="inner content-element">
				<h3 class="title-white"><?php echo $TEXT['home_content_recentactivities']?></h3>
				<?php echo getHomeRecentActivities();?>
			</div>
		</div>
		<div class="content-center content-element">
			<div style="float: left;">
				<h3 class="title-white"><?php echo $TEXT['home_content_trending']?></h3>
				<?php echo getHomeProductTrending();?>
			</div>
			<div style="float: left;">
			<h3 class="title-white"><?php echo $TEXT['home_content_news']?></h3>
			<?php echo getHomeNews();?>
			</div>
		</div>
		<div class="content-left-right content-element">
			<div class="inner">
				<h3 class="title-white"><?php echo $TEXT['home_content_select_language']?></h3>
				<a href="<?php echo $BASENAME['ch_lang'].'?lang=ID'?>"><img src="<?php echo $BASENAME['img_html'].'icon_lang_id.png'?>" class="flag-icon"></a>
				<a href="<?php echo $BASENAME['ch_lang'].'?lang=EN'?>"><img src="<?php echo $BASENAME['img_html'].'icon_lang_en.png'?>" class="flag-icon"></a>
				<br><br>
				<center><a href="https://www.techconnexionindonesia.com/itd/license"><button class="btn-w90-h40-square btn-transparent">License ITD</button></a></center>
			</div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
		<div class="webmask" id="webmask">
			<div class="content-center-big-50-default content-element" style="display: none;height: 550px;overflow-y: scroll;" id="div-recentactivity">
				<button class="btn-w30-h40-square btn-transparent text-low-medium" onclick="closeRecentActivity()"><?php echo $TEXT2['devconsole_avt_close']?></button>
				<br><br>
				<table class="table-w100-h20-blue center" style="color: white;">
					<tr>
						<th><?php echo $TEXT['activity']?></th>
						<th><?php echo $TEXT2['data_datetime']?></th>
					</tr>
					<?php
					$query = mysql_query("SELECT * FROM tci_recent_activity ORDER BY datetime DESC LIMIT 100");
					while ($rs = mysql_fetch_array($query)) {?>
					<tr>
						<td><a href="<?php echo $rs['link']?>" style="color: white;"><?php echo getStringFromArray($rs['text'])?></a></td>
						<td><?php echo $rs['datetime']?></td>
					</tr>
					<?php }?>
				</table>
			</div>
		</div>
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'home.js'?>"></script>
	</body>
</html>