<?php

/*Forms function
--Tech Connexion Indonesia Website--
--Created by TCI Programming Division--
--Tech Connexion Indonesia Group--
--(c) Andreas Alexander Kaawoan (Executive Director) TCI Group--
--Process with function PHP 5 Language--
*/
session_start();
$_SESSION['thisPage'] = "payment-ipaymu";
require '../model.php';
require '../variable.php';
require '../credentials/credentials.php';


if ($_SERVER['REMOTE_ADDR'] != '120.89.93.249'){
	die('Not allowed');
}

if (!empty($_POST['sid']) && !empty($_POST['trx_id']) && !empty($_POST['status']) && !empty($_POST['via'])){
	connectDb();
	$sessionId = $_POST['sid'];
	$trx_id = $_POST['trx_id'];
	$status = $_POST['status'];
	$via = $_POST['via'];

	// Check status
	$approved_status = array('berhasil','gagal');
	if (!in_array($status, $approved_status)){
		die();
	}

	$query = mysql_query("SELECT * FROM user_topup_request WHERE id_topup_token = '".$sessionId."'");
	if ($query){
		$request = mysql_fetch_array($query);

		$url_check_api = $SITE['ipaymu_check_transaction'];
		$header = array("Content-Type: application/x-www-form-urlencoded");
        $data_post_curl = array(
            'key' => $credentials_ipaymu_key,
            'id' => $trx_id,
            'format' => 'json'
        );
        $result_curl = doCurl($url_check_api,$header,$data_post_curl,'POST');
        $result_data = json_decode($result_curl['data'], true);
        $result_status = json_decode($result_curl['code'],true);
        if ($result_status == 200){
        	$data_transaction = $result_data;
        	/* Convert IDR to USD */
        	$exchange = getRateExchange();
        	$converted_amount = cvtMoneyIdrToUsd($data_transaction['Nominal']);
        	$converted_amount = $converted_amount + 0.01;
        	/* Convert status */
        	if ($status == 'berhasil'){
        		$status = 'sucessfull';
        	} else {
        		$status = 'failed';
        	}
        	$data_save_transaction = array(
        		'id_topup_token' => $sessionId,
        		'id_payment' => $trx_id,
        		'id_user' => $request['id_user'],
        		'channel_country' => $request['channel_country'],
        		'currency' => $request['currency'],
        		'channel_gateway' => $request['channel_gateway'],
        		'amount' => $converted_amount,
        		'status' => $status,
        		'via' => $via,
        		'info' => '',
        		'date_requested' => $request['date_request'],
        		'date_paid' => $data_transaction['WaktuBayar']
        	);
        	$id_topup = topupSaveHistory($data_save_transaction);
        	if ($status == 'sucessfull'){
        		$topup_info = "Topup RP".$data_transaction['Nominal']." -> $".$converted_amount." w currency ".$exchange;
        		topupIncreaseBalance($id_topup,$request['id_user'],$converted_amount,$topup_info);
        		$user_balance_usd = getBalanceById($request['id_user']);
        		$user_balance_idr = cvtMoneyCurr($user_balance_usd,'IDR');
        		$data_email = array(
        			'id_topup' => $id_topup,
        			'id_user' => $request['id_user'],
        			'via' => $via,
        			'amount' => $data_transaction['Nominal'],
        			'balance_usd' => $user_balance_usd,
        			'balance_idr' => $user_balance_idr
        		);
        		$email_message = getEmailTemplateSuccess($data_email);
        		$subject = 'Topup Saldo TCI Berhasil';
        		$user_email = getUserData($request['id_user'])['email'];
        		emailSend('topup@techconnexionindonesia.com',$user_email,$subject,$email_message);
        	} else if ($status == 'failed'){
        		$data_email = array(
        			'id_topup' => $id_topup,
        			'id_user' => $request['id_user'],
        			'via' => $via,
        			'amount' => $data_transaction['Nominal'],
        		);
        		$email_message = getEmailTemplateFailed($data_email);
        		$subject = 'Topup Saldo TCI Gagal';
        		$user_email = getUserData($request['id_user'])['email'];
        		emailSend('topup@techconnexionindonesia.com',$user_email,$subject,$email_message);
        		$query_remove_request = mysql_query("DELETE FROM user_topup_request WHERE id_topup_token = '".$sessionId."'");
        	}
        }
	}
}

function getEmailTemplateSuccess($data){
	require '../variable.php';
	$tempalate = '
		<div style="width: 600px; height: auto; border: 2px solid darkblue; text-align: center;padding:5%;border-radius:20px;margin:auto">
			<img src="'.$BASENAME['img_html'].'navbar-logo.png'.'" style="width:200px;height:100px">
			<h4 style="text-align: center; font-size: 30px;"><strong>TOPUP BERHASIL !</strong></h4>
			<p style="font-size: 20px">Anda berhasil melakukan Topup Saldo akun website<br /> <strong>TECH CONNEXION INDONESIA</strong><br /> dengan data sebagai berikut :<p><br>
	  		<hr>
			ID Topup : <b>'.$data['id_topup'].'</b><br>
			ID User : <b>'.$data['id_user'].'</b><br>
			Metode Pembayaran : <b>'.$data['via'].'</b><br>
			Jumlah Topup : <b>Rp'.number_format($data['amount']).'</b><br>
			Saldo akun : <b>$'.$data['balance_usd'].' (Rp'.$data['balance_idr'].')</b><hr><br><br>
	  		<p style="font-size: 20px">Terima kasih telah melakukan topup saldo di website<br> Tech Connexion Indonesia<p><br>
	  		Kunjungi Website <br>
	  		<a href="'.$BASENAME['server'].'">Tech Connexion Indonesia</a><br>
	  		sekarang untuk melakukan pembelian produk game dan aplikasi<br><br>
	  		Untuk bantuan, hubungi <a href="mailto:cs@techconnexionindonesia.com">cs@techconnexionindonesia.com</a>
		</div>
	';
	return $tempalate;
}

function getEmailTemplateFailed($data){
	require '../variable.php';
	$tempalate = '
		<div style="width: 600px; height: auto; border: 2px solid darkblue; text-align: center;padding:5%;border-radius:20px;margin:auto">
			<img src="'.$BASENAME['img_html'].'navbar-logo.png'.'" style="width:200px;height:100px">
			<h4 style="text-align: center; font-size: 30px;"><strong>TOPUP GAGAL</strong></h4>
			<p style="font-size: 20px">Topup yang anda lakukan di website<br /> <strong>TECH CONNEXION INDONESIA</strong><br /> dengan data sebagai berikut :<p><br>
	  		<hr>
			ID Topup : <b>'.$data['id_topup'].'</b><br>
			ID User : <b>'.$data['id_user'].'</b><br>
			Metode Pembayaran : <b>'.$data['via'].'</b><br>
			Jumlah Topup : <b>Rp'.number_format($data['amount']).'</b><br>
	  		<p style="font-size: 20px">Telah gagal diproses. Silahkan untuk topup kembali di website<br> Tech Connexion Indonesia<p><br>
	  		Kunjungi Website <br>
	  		<a href="'.$BASENAME['server'].'">Tech Connexion Indonesia</a><br>
	  		untuk melakukan topup dan membeli produk game dan aplikasi<br><br>
	  		Untuk bantuan, hubungi <a href="mailto:cs@techconnexionindonesia.com">cs@techconnexionindonesia.com</a>
		</div>
	';
	return $tempalate;
}