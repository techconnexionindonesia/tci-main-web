<?php

/*Forms function
--Tech Connexion Indonesia Website--
--Created by TCI Programming Division--
--Tech Connexion Indonesia Group--
--(c) Andreas Alexander Kaawoan (Executive Director) TCI Group--
--Process with function PHP 5 Language--
*/
session_start();
include 'model.php';
//include the main function

//Checking submit name
if (!empty($_POST['usr_login_submit'])) login_user();
if (!empty($_POST['usr_signup_submit'])) signup_user();
if (!empty($_POST['usr_signup_success_submit'])) signup_success();
if (!empty($_POST['usr_store_ATC'])) add_to_cart();
if (!empty($_POST['usr_store_comment_submit'])) submit_review();
if (!empty($_POST['usr_store_comment_delete'])) delete_review();
if (!empty($_POST['usr_cart_submit_pay_all'])) cart_system("pay","all");
if (!empty($_POST['usr_cart_submit_pay'])) cart_system("pay","single");
if (!empty($_POST['usr_cart_submit_delete_all'])) cart_system("delete","all");
if (!empty($_POST['usr_cart_submit_delete'])) cart_system("delete","single"); 
if (!empty($_POST['usr_store_vote_yes']) || !empty($_POST['usr_store_vote_no'])) store_vote_product();
if (!empty($_POST['usr_news_vote_yes']) || !empty($_POST['usr_news_vote_no'])) news_vote_product();
if (isset($_POST['about_project_search'])) about_project_search();
if (!empty($_POST['dev_signup_submit'])) signup_dev();
if (!empty($_POST['dev_ps_submit_add'])) devconsole_product_system("addupdate");
if (!empty($_POST['dev_ps_submit_delete'])) devconsole_product_system("delete");
if (!empty($_POST['dev_admin_resign_submit'])) funcDevAdminResign(); //Direct Function
if (!empty($_POST['dev_admin_add_submit'])) devconsole_add_admin();
if (!empty($_POST['dev_admin_delete_submit'])) devconsole_delete_admin();
if (!empty($_POST['dev_config_submit'])) devconsole_update();
if (isset($_POST['usr_dev_project_search'])) dev_project_search();
if (!empty($_POST['usr_topup_submit'])) topup_request();
if (!empty($_FILES['usr_update_avatar'])) update_user('avatar');
if (!empty($_POST['usr_update_submit'])) update_user('general');
if (!empty($_POST['usr_update_security_submit'])) update_user('security');
if (!empty($_POST['dev_login_submit'])) devconsole_login();
if (!empty($_POST['dev_confirm_submit_accept'])) funcDevAdminConfirm('accept'); //Direct Function
if (!empty($_POST['dev_confirm_submit_reject'])) funcDevAdminConfirm('reject'); //Direct Function
if (!empty($_POST['itd_submit'])) itd_license();

//Main Function
//Sorted by a-Z
function about_project_search(){
	connectDb();
	$q = $_POST['about_project_search'];
	$_SESSION['ext_aboutProjectSearch'] = $q;
	$_SESSION['ext_aboutProjectSort'] = "";
	goPage("about_us_project",'');
}

function add_to_cart(){
	if (!empty($_SESSION['usr_id'])){
		$usr_id = $_SESSION['usr_id'];
		$stuff_id = mysql_real_escape_string($_POST['usr_store_stuff_id']);
		funcAddToCart($usr_id,$stuff_id);
	}else goPage("login","");
}

function cart_system($a,$b){
	if (!empty($_SESSION['usr_id'])){
		if ($a == "pay"){
			if ($b == "all") funcCartSystem("pay","all");
			else if ($b == "single"){
				if (!empty($_POST['usr_cart_pay_id'])){
					$id_stuff = mysql_real_escape_string($_POST['usr_cart_pay_id']);
					funcCartSystem("pay",$id_stuff);
				}else goPage("cart","");
			}
		}else if ($a == "delete"){
			if ($b == "all") funcCartSystem("delete","all");
			else if ($b == "single"){
				if (!empty($_POST['usr_cart_delete_id'])){
					$id_stuff = mysql_real_escape_string($_POST['usr_cart_delete_id']);
					funcCartSystem("delete",$id_stuff);
				}else goPage("cart","");
			}
		}
	}else goPage("login");
}

function delete_review(){
	$dr['id'] = mysql_real_escape_string($_POST['usr_store_comment_id']);
	deleteReview($dr);
}

function dev_project_search(){
	connectDb();
	$q = $_POST['usr_dev_project_search'];
	$_SESSION['ext_devProjectSearch'] = $q;
	goBack();
}

function devconsole_add_admin(){
	connectDb();
	$username = mysql_real_escape_string($_POST['dev_admin_username']);
	$usr_id_add = getUserIdByUsername($username);
	if ($usr_id_add == false){
	 	goBackParam("?err=username");
	 	exit;
	}
	funcDevAdminAdd($usr_id_add);
}

function devconsole_delete_admin(){
	$id_admin = mysql_real_escape_string($_POST['dev_admin_id']);
	funcDevAdminDelete($id_admin);
}

function devconsole_login(){
	if (empty($_SESSION['usr_id'])) {
		goPage("error","?err=data");
		exit;
	}
	if (isDevAlreadySignup($_SESSION['usr_id'])){
		$x_dev = getDevDataByUsrId($_SESSION['usr_id']);
		$dev_id = $x_dev['id_developer'];
		$password = mysql_real_escape_string($_POST['dev_login_password']);
		funcLoginDev($dev_id,$password);
	}else goPage("error","?err=data");
}

function devconsole_product_system($mode){
	include 'variable.php';
	connectDb();
	if ($mode == "addupdate"){
		$p['mode'] = mysql_real_escape_string($_POST['dev_ps_mode']);
		$p['type'] = mysql_real_escape_string($_POST['dev_ps_type']);
		switch ($p['type']) {
			case 'game':
				$x_type_num = "1";
				break;
			
			case 'dlc':
				$x_type_num = "2";
				break;

			case 'content':
				$x_type_num = "3";
				break;

			case 'app':
				$x_type_num = "4";
				break;

			default:
				goBackParam("?err=err");
				exit;
				break;
		}
		$p['id_developer'] = $_SESSION['dev_id'];
		if (isset($_POST['dev_ps_idold'])) $p['id_old'] = mysql_real_escape_string($_POST['dev_ps_idold']);
		$p['code_project'] = mysql_real_escape_string($_POST['dev_ps_code']);
		if (isset($_POST['dev_ps_idgame'])) $p['id_game_parent'] = mysql_real_escape_string($_POST['dev_ps_idgame']);
		if ($p['type'] == "game" || $p['type'] == "app"){
			$p['id'] = $p['id_developer'].$x_type_num.$p['code_project'];
		}else $p['id'] = $p['id_developer'].$x_type_num.$p['id_game_parent'].$p['code_project'];
		if ($p['mode'] == "add" && isProductExist($p['id'])) {
			goBackParam("?err=id");
			exit;
		}else if ($p['mode'] == "edit" && ($p['id_old'] != $p['id'] && isProductExist($p['id']))){
			goBackParam("?err=id");
		}
		$p['title'] = mysql_real_escape_string($_POST['dev_ps_title']);
		$p['sub_title'] = mysql_real_escape_string($_POST['dev_ps_subtitle']);
		$p['gc'] = mysql_real_escape_string($_POST['dev_ps_gc']);
		$p['tags'] = mysql_real_escape_string($_POST['dev_ps_tags']);
		//Section checking image and upload them
		$x_type_approved = array("jpg","jpeg","png","bmp","gif");
		if ($_FILES['dev_ps_pictitle']['size'] != 0) {
			$x_pictitle = $_FILES['dev_ps_pictitle'];
			$x_type = strtolower(pathinfo($x_pictitle['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_pictitle['tmp_name']) == false) goBackParam("?err=imgnot");
		 	else if ($x_pictitle['size'] > 1000000) goBackParam("?err=imgsize");
		 	$x_name = $p['id'].'title'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_title'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_pictitle['tmp_name'],	$x_path);
		}
		if ($_FILES['dev_ps_picsub1']['size'] != 0) {
			$x_picsub1 = $_FILES['dev_ps_picsub1'];
			$x_type = strtolower(pathinfo($x_picsub1['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_picsub1['tmp_name']) == false) goBackParam("?err=imgnot");
			else if ($x_picsub1['size'] > 1000000) goBackParam("?err=imgsize");
			$x_name = $p['id'].'sub1'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_sub1'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_picsub1['tmp_name'],	$x_path);
		}
		if ($_FILES['dev_ps_picsub2']['size'] != 0) {
			$x_picsub2 = $_FILES['dev_ps_picsub2'];
			$x_type = strtolower(pathinfo($x_picsub2['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_picsub2['tmp_name']) == false) goBackParam("?err=imgnot");
			else if ($x_picsub2['size'] > 1000000) goBackParam("?err=imgsize");
			$x_name = $p['id'].'sub2'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_sub2'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_picsub2['tmp_name'],	$x_path);
		}
		if ($_FILES['dev_ps_picsub3']['size'] != 0) {
			$x_picsub3 = $_FILES['dev_ps_picsub3'];
			$x_type = strtolower(pathinfo($x_picsub3['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_picsub3['tmp_name']) == false) goBackParam("?err=imgnot");
			else if ($x_picsub3['size'] > 1000000) goBackParam("?err=imgsize");
			$x_name = $p['id'].'sub3'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_sub3'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_picsub3['tmp_name'],	$x_path);
		}
		if ($_FILES['dev_ps_picsub4']['size'] != 0) {
			$x_picsub4 = $_FILES['dev_ps_picsub4'];
			$x_type = strtolower(pathinfo($x_picsub4['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_picsub4['tmp_name']) == false) goBackParam("?err=imgnot");
			else if ($x_picsub4['size'] > 1000000) goBackParam("?err=imgsize");
			$x_name = $p['id'].'sub4'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_sub4'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_picsub4['tmp_name'],	$x_path);
		}
		if ($_FILES['dev_ps_picsub5']['size'] != 0) {
			$x_picsub5 = $_FILES['dev_ps_picsub5'];
			$x_type = strtolower(pathinfo($x_picsub5['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_picsub5['tmp_name']) == false) goBackParam("?err=imgnot");
			else if ($x_picsub5['size'] > 1000000) goBackParam("?err=imgsize");
			$x_name = $p['id'].'sub5'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_sub5'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_picsub5['tmp_name'],	$x_path);
		}
		if ($_FILES['dev_ps_picsub6']['size'] != 0) {
			$x_picsub6 = $_FILES['dev_ps_picsub6'];
			$x_type = strtolower(pathinfo($x_picsub6['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_picsub6['tmp_name']) == false) goBackParam("?err=imgnot");
			else if ($x_picsub6['size'] > 1000000) goBackParam("?err=imgsize");
			$x_name = $p['id'].'sub6'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_sub6'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_picsub6['tmp_name'],	$x_path);
		}
		if ($_FILES['dev_ps_picprogress']['size'] != 0) {
			$x_picprogress = $_FILES['dev_ps_picprogress'];	
			$x_type = strtolower(pathinfo($x_picprogress['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)) goBackParam("?err=imgtype");
			else if (getimagesize($x_picprogress['tmp_name']) == false) goBackParam("?err=imgnot");
			else if ($x_picprogress['size'] > 1000000) goBackParam("?err=imgsize");
			$x_name = $p['id'].'progress'.'.'.$x_type;
		 	$x_dir = "../img/product/".$p['id_developer'].'/'.$p['id'].'/';
		 	$x_path = $x_dir.$x_name;
			$p['pic_progress'] = $x_name;
			if (!is_dir($x_dir)) mkdir($x_dir);
			move_uploaded_file($x_picprogress['tmp_name'],	$x_path);
		}
		//End of Image checking
		$p['detail'] = mysql_real_escape_string($_POST['dev_ps_detail']);
		$x_price = floatval(mysql_real_escape_string($_POST['dev_ps_pricedollar']));
		if (!is_float($x_price) || $x_price < 0) goBackParam("?err=price");
		$fee_gateway = $x_price > 0 ? 0.31 : 0;
		$p['price'] = cvtMoneyCurr(($x_price+($x_price*10/100) + $fee_gateway),"USD");
		$x_datestarted = strtotime(mysql_real_escape_string($_POST['dev_ps_datestarted']));
		if (!empty($_POST['dev_ps_notfinish'])){
			$p['date_published'] = strtotime("0000-00-00");
		}else{
			if ($x_datestarted > strtotime(date("Y-m-d"))) goBackParam("?err=date");
			$p['date_published'] = date("Y-m-d");
		}
		$p['date_started'] = date("Y-m-d",$x_datestarted);
		$p['progress'] = mysql_real_escape_string($_POST['dev_ps_progress']);
		$p['label'] = mysql_real_escape_string($_POST['dev_ps_label']);
		if (!empty($_POST['dev_ps_showpublic'])) $p['show_public'] = true;
		else $p['show_public'] = false;	
		if ($_FILES['dev_ps_file']['size'] != 0){
			if ($_FILES['dev_ps_file']['size'] > 5242880) goBackParam("?err=filesize");
			else{
				$x_rand = rand(1000000,100000000000);
				$x_rand1 = rand(1000000,100000000000);
				$x_rand11 = rand(1000000,100000000000);
				$x_rand2 = rand(1000000,100000000000);
				$x_rand3 = rand(1000000,100000000000);
				$x_rand4 = rand(1000000,100000000000);
				$x_rand5 = rand(1000000,100000000000);
				$x_rand6 = rand(1000000,100000000000);
				$x_type = strtolower(pathinfo($_FILES['dev_ps_file']['name'], PATHINFO_EXTENSION));
				$x_type_approved = array("rar","zip","exe");
				if (!in_array($x_type, $x_type_approved)) goBackParam("?err=filetype");
				$x_name = $x_rand2.$x_rand3.$x_rand4.$x_rand5.$x_rand6.'.'.$x_type;
				$x_to_dir = $x_rand.$x_rand1.$x_rand11.'/';
				$x_path = "../product/".$p['id_developer'].'/'.$p['id'].'/';
				$x_path2 = $x_path.$x_to_dir;
				if (!is_dir($x_path)) mkdir($x_path);
				if (!is_dir($x_path2)) mkdir($x_path2);
				move_uploaded_file($_FILES['dev_ps_file']['tmp_name'], $x_path2.$x_name);
				$p['link'] = $BASENAME['product'].$p['id_developer'].'/'.$p['id'].'/'.$x_to_dir.$x_name;
			}
		}else if (!empty($_POST['dev_ps_link'])){
			if (getRemoteSize($_POST['dev_ps_link']) > 0) $p['link'] = $_POST['dev_ps_link'];
			else goBackParam("?err=link");
		}
		if ($p['mode'] == "add") funcDevProductAdd($p);
		else if ($p['mode'] == "edit") funcDevProductEdit($p);
	}else if ($mode == "delete"){
		$p['mode'] = mysql_real_escape_string($_POST['dev_ps_mode']);
		$p['type'] = mysql_real_escape_string($_POST['dev_ps_type']);
		switch ($p['type']) {
			case 'game':
				$x_type_num = "1";
				break;
			
			case 'dlc':
				$x_type_num = "2";
				break;

			case 'content':
				$x_type_num = "3";
				break;

			case 'app':
				$x_type_num = "4";
				break;

			default:
				goBackParam("?err=err");
				exit;
				break;
		}
		$p['id_developer'] = $_SESSION['dev_id'];
		if (isset($_POST['dev_ps_idold'])) $p['id_old'] = mysql_real_escape_string($_POST['dev_ps_idold']);
		$p['code_project'] = mysql_real_escape_string($_POST['dev_ps_code']);
		if (isset($_POST['dev_ps_idgame'])) $p['id_game_parent'] = mysql_real_escape_string($_POST['dev_ps_idgame']);
		if ($p['type'] == "game" || $p['type'] == "app"){
			$p['id'] = $p['id_developer'].$x_type_num.$p['code_project'];
		}else $p['id'] = $p['id_developer'].$x_type_num.$p['id_game_parent'].$p['code_project'];
		funcDevProductDelete($p);
	}
}

function devconsole_update(){
	include 'variable.php';
	if (!empty($_POST['dev_config_dev_password_new'])){
		if ($_POST['dev_config_dev_password_new'] != $_POST['dev_config_dev_password_new_re']){ 
			goBackParam("?err=pw");
			exit;
		}
		$u_data['dev_password'] = mysql_real_escape_string($_POST['dev_config_dev_password_new']);
	}
	$u_data['usr_id'] = $_SESSION['usr_id'];
	$u_data['usr_password'] = mysql_real_escape_string($_POST['dev_config_usr_password']);
	$u_data['dev_id'] = $_SESSION['dev_id'];
	$u_data['dev_password_old'] = mysql_real_escape_string($_POST['dev_config_dev_password_old']);
	$u_data['dev_name'] = mysql_real_escape_string($_POST['dev_config_name']);
	if (!empty($_POST['dev_config_createp'])) $u_data['dev_createp'] = $_POST['dev_config_createp'];
	else {
		goBackParam("?err=create");
		exit;
	}
	$u_data['dev_typestc'] = mysql_real_escape_string($_POST['dev_config_structure']);
	$u_data['dev_typeplc'] = mysql_real_escape_string($_POST['dev_config_place']);
	$u_data['dev_email'] = mysql_real_escape_string($_POST['dev_config_email']);
	$u_data['dev_description'] = mysql_real_escape_string($_POST['dev_config_description']);
	if ($_FILES['dev_config_logo']['size'] > 0){
		$x_rand = rand(100,1000000000);
		$x_type = pathinfo($_FILES['dev_config_logo']['name'], PATHINFO_EXTENSION);
		$x_name = $x_rand.'.'.$x_type;
		if (getimagesize($_FILES['dev_config_logo']['tmp_name']) == false) goBackParam("?err=imgnot");
		if ($_FILES['dev_config_logo']['size'] > 1000000) goBackParam("?err=imgsize");
		move_uploaded_file($_FILES['dev_config_logo']['tmp_name'],	'../temp_dir/'.$x_name);
		$u_data['dev_logo'] = $x_name;
	}
	$u_data['dev_site'] = mysql_real_escape_string($_POST['dev_config_site']);
	if (!empty($_POST['dev_config_bank'])) $u_data['dev_bank'] = mysql_real_escape_string($_POST['dev_config_bank']);
	if (!empty($_POST['dev_config_bankname'])) $u_data['dev_bankname'] = mysql_real_escape_string($_POST['dev_config_bankname']);
	if (!empty($_POST['dev_config_bankno'])) $u_data['dev_bankno'] = mysql_real_escape_string($_POST['dev_config_bankno']);
	funcUpdateDev($u_data);
}

function itd_license(){
	$usr_id = $_SESSION['usr_id'];
	$itd_id = mysql_real_escape_string($_POST['itd_id']);
	$itd_layout = mysql_real_escape_string($_POST['itd_layout']);
	$query = mysql_query("SELECT * FROM user_user_transaction WHERE id_user = $usr_id AND id_stuff = 'TCI2INDOTRAINDISPCTA'");
	if (mysql_num_rows($query) == 1){
		mysql_connect("sql169.main-hosting.eu","u827725805_tci","tciproitdgame");
		mysql_select_db("u827725805_tci");
		$query = mysql_query("SELECT * FROM itd_layout_approval WHERE id = $itd_id");
		if (mysql_num_rows($query) != 0) mysql_query("UPDATE itd_layout_approval SET expired = '-'");
		else {
			$query = mysql_query("SELECT * FROM itd_user WHERE id=$itd_id");
			$rs = mysql_fetch_array($query);
			$itd_nama = $rs['nama'];
			mysql_query("INSERT INTO itd_layout_approval values($itd_id,'$itd_nama','CTA','-')") or die (mysql_error());
		}
	}else {
		goBackParam("?err=notbuy");
	}
	goBackParam("?succ=1");
}

function login_user(){
	$username = mysql_real_escape_string($_POST['usr_login_username']);
	$password = mysql_real_escape_string($_POST['usr_login_password']);
	funcLoginUser($username,$password);
}

function news_vote_product(){
	if (!empty($_SESSION['usr_id'])){
		$n_id = mysql_real_escape_string($_POST['usr_news_id']);
		if (!empty($_POST['usr_news_vote_yes'])){
			funcVoteNews($n_id,'y');
		}else if (!empty($_POST['usr_news_vote_no'])){
			funcVoteNews($n_id,'n');
		}
	}else{
		goPage("login","");
	}
}

function signup_success(){
	if (!empty($_POST['usr_signup_subscription'])) subscribeUser();
	$_SESSION['signup_success'] = NULL;
	goBack();
}

function signup_dev(){
	include 'variable.php';
	if (mysql_real_escape_string($_POST['dev_signup_password']) != mysql_real_escape_string($_POST['dev_signup_repassword'])) goPage("devconsole_signup","?err=pw");
	$s_data['usr_id'] = mysql_real_escape_string($_POST['dev_signup_usr_id']);
	$s_data['usr_password'] = mysql_real_escape_string($_POST['dev_signup_usr_password']);
	$s_data['dev_id'] = mysql_real_escape_string($_POST['dev_signup_iddev']);
	$s_data['dev_password'] = mysql_real_escape_string($_POST['dev_signup_password']);
	$s_data['dev_name'] = mysql_real_escape_string($_POST['dev_signup_name']);
	if (!empty($_POST['dev_signup_createp'])) $s_data['dev_createp'] = $_POST['dev_signup_createp'];
	else goBackParam("?err=create");
	$s_data['dev_typestc'] = mysql_real_escape_string($_POST['dev_signup_structure']);
	$s_data['dev_typeplc'] = mysql_real_escape_string($_POST['dev_signup_place']);
	$s_data['dev_email'] = mysql_real_escape_string($_POST['dev_signup_email']);
	$s_data['dev_description'] = mysql_real_escape_string($_POST['dev_signup_description']);
	$x_rand = rand(100,1000000000);
	$x_type = pathinfo($_FILES['dev_signup_logo']['name'], PATHINFO_EXTENSION);
	$x_name = $x_rand.'.'.$x_type;
	if (getimagesize($_FILES['dev_signup_logo']['tmp_name']) == false) goBackParam("?err=imgnot");
	if ($_FILES['dev_signup_logo']['size'] > 1000000) goBackParam("?err=imgsize");
	move_uploaded_file($_FILES['dev_signup_logo']['tmp_name'],	'../temp_dir/'.$x_name);
	$s_data['dev_logo'] = $x_name;
	$s_data['dev_site'] = mysql_real_escape_string($_POST['dev_signup_site']);
	$s_data['dev_bank'] = mysql_real_escape_string($_POST['dev_signup_bank']);
	$s_data['dev_bankname'] = mysql_real_escape_string($_POST['dev_signup_bankname']);
	$s_data['dev_bankno'] = mysql_real_escape_string($_POST['dev_signup_bankno']);
	funcSignupDev($s_data);
}

function signup_user(){
	if(mysql_real_escape_string($_POST['usr_signup_password']) != mysql_real_escape_string($_POST['usr_signup_password_re'])) goPage("signup","?re=true");
	$s_data['username'] = mysql_real_escape_string($_POST['usr_signup_username']);
	$s_data['password'] = mysql_real_escape_string($_POST['usr_signup_password']);
	$s_data['password_re'] = mysql_real_escape_string($_POST['usr_signup_password_re']);
	$s_data['front_name'] = mysql_real_escape_string($_POST['usr_signup_frontname']);
	$s_data['back_name'] = mysql_real_escape_string($_POST['usr_signup_backname']);
	$s_data['birth'] = mysql_real_escape_string($_POST['usr_signup_birth']);
	$s_data['address'] = mysql_real_escape_string($_POST['usr_signup_address']);
	$s_data['zipcode'] = mysql_real_escape_string($_POST['usr_signup_zipcode']);
	$s_data['country'] = mysql_real_escape_string($_POST['usr_signup_country']);
	$s_data['email'] = mysql_real_escape_string($_POST['usr_signup_email']);
	$s_data['phone'] = mysql_real_escape_string($_POST['usr_signup_phonecode']).mysql_real_escape_string($_POST['usr_signup_phone']);
	$s_data['question_1'] = mysql_real_escape_string($_POST['usr_signup_question1']);
	$s_data['question_2'] = mysql_real_escape_string($_POST['usr_signup_question2']);
	$s_data['answer_1'] = mysql_real_escape_string($_POST['usr_signup_answer1']);
	$s_data['answer_2'] = mysql_real_escape_string($_POST['usr_signup_answer2']);
	$s_data['signature'] = mysql_real_escape_string($_POST['usr_signup_signature']);
	$s_data['description'] = mysql_real_escape_string($_POST['usr_signup_description']);
	if ($_FILES['usr_signup_img']['size'] > 0){
		$x_rand = rand(100,1000000000);
		$x_type_approved = array("jpg","jpeg","png","bmp","gif");
		$x_type = pathinfo($_FILES['usr_signup_img']['name'], PATHINFO_EXTENSION);
		if (!in_array($x_type, $x_type_approved)) goPage("signup","?err=imgtype");
		$x_name = $x_rand.'.'.$x_type;
		if (getimagesize($_FILES['usr_signup_img']['tmp_name']) == false) goPage("signup","?err=imgnot");
		if ($_FILES['usr_signup_img']['size'] > 1000000) goPage("signup","?err=imgsize");
		move_uploaded_file($_FILES['usr_signup_img']['tmp_name'],	'../temp_dir/'.$x_name);
		$s_data['img'] = $x_name;
		$s_data['img_type'] = $x_type;
		$s_data['img_avail'] = true;
	}else{
		$s_data['img_avail'] = false;
	}
	funcSignupUser($s_data);
}

function store_vote_product(){
	if (!empty($_SESSION['usr_id'])){
		$pr_id = mysql_real_escape_string($_POST['usr_store_id']);
		if (!empty($_POST['usr_store_vote_yes'])){
			funcVoteStuff($pr_id,'y');
		}else if (!empty($_POST['usr_store_vote_no'])){
			funcVoteStuff($pr_id,'n');
		}
	}else{
		goPage("login","");
	}
}

function submit_review(){
	if (!empty($_SESSION['usr_id'])){
		$sr['pr_id'] = mysql_real_escape_string($_POST['usr_store_id']);
		$sr['text'] = mysql_real_escape_string($_POST['usr_store_review_text']);
		$sr['datetime'] = date("Y-m-d H:i:s");
		funcSubmitReview($sr);
	}
}

function topup_request(){
	if (empty($_SESSION['usr_id'])){
		goPage("login","");
		exit;
	}
	// if (isTopupAlready($_SESSION['usr_id'])){
	// 	goBack();
	// 	exit;
	// }
	$topup['channel'] = mysql_real_escape_string($_POST['usr_topup_channel']);
	$topup['amount'] = intval($_POST['usr_topup_amount']);
	if ($topup['channel'] != 'id' || is_int($topup['amount']) == false ){
		goBackParam('?err=error');
	}
	funcTopupRequest($topup);
}

function update_user($m){ //m : mode -> "general","avatar","security"
	$data['id_user'] = $_SESSION['usr_id'];
	if ($m == "avatar"){
		if ($_FILES['usr_update_avatar']['size'] > 0){
			$x_pic = $_FILES['usr_update_avatar'];
			$x_type_approved = array("jpg","jpeg","png","bmp","gif");
			$x_type = strtolower(pathinfo($x_pic['name'], PATHINFO_EXTENSION));
			if (!in_array($x_type, $x_type_approved)){ 
				goBackParam("?err=imgtype");
				exit;
			}
			else if (getimagesize($x_pic['tmp_name']) == false){ 
				goBackParam("?err=imgnot");
				exit;
			}
		 	else if ($x_pic['size'] > 2000000) {
		 		goBackParam("?err=imgsize");
		 		exit;
		 	}
		 	$x_avail = false;
	 		$x_name = $_SESSION['usr_id'].'_av.'.$x_type;
		 	$x_dir = "../img/avatar/";
		 	$x_path = $x_dir.$x_name;
			move_uploaded_file($x_pic['tmp_name'],	$x_path);
			$data['avatar'] = $x_name;
		}
	}else if ($m == "general"){
		$data['front_name'] = mysql_real_escape_string($_POST['usr_update_frontname']);
		$data['back_name'] = mysql_real_escape_string($_POST['usr_update_backname']);
		$data['birth'] = mysql_real_escape_string($_POST['usr_update_birth']);
		$data['address'] = mysql_real_escape_string($_POST['usr_update_address']);
		$data['zipcode'] = mysql_real_escape_string($_POST['usr_update_zipcode']);
		$data['country'] = mysql_real_escape_string($_POST['usr_update_country']);
		$data['email'] = mysql_real_escape_string($_POST['usr_update_email']);
		$data['phone'] = mysql_real_escape_string($_POST['usr_update_phone']);
		$data['signature'] = mysql_real_escape_string($_POST['usr_update_signature']);
		$data['description'] = mysql_real_escape_string($_POST['usr_update_description']);
	}else if ($m == "security"){
		$data['username'] = mysql_real_escape_string($_POST['usr_update_username']);
		$data['password_old'] = mysql_real_escape_string($_POST['usr_update_password_old']);
		$data['password_new'] = mysql_real_escape_string($_POST['usr_update_password_new']);
		$data['password_new_re'] = mysql_real_escape_string($_POST['usr_update_password_new_re']);
		$data['question1'] = mysql_real_escape_string($_POST['usr_update_question1']);
		$data['answer1'] = mysql_real_escape_string($_POST['usr_update_answer1']);
		$data['question2'] = mysql_real_escape_string($_POST['usr_update_question2']);
		$data['answer2'] = mysql_real_escape_string($_POST['usr_update_answer2']);
		if ($data['password_new'] != ""){
			if ($data['password_new'] != $data['password_new_re']){
				goBackParam("?err=pwnotsame");
				exit;
			}
		}
	}
	funcUpdateUser($m,$data);
}