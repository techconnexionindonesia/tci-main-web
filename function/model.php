<?php

/*models function
--Tech Connexion Indonesia Website--
--Created by TCI Programming Division--
--Tech Connexion Indonesia Group--
--(c) Andreas Alexander Kaawoan (Executive Director) TCI Group--
--Process with function PHP 5 Language--
*/

require 'variable.php';
//Get variable data for variable processing such as basename

//Set the default timezone to Jakarta (GMT+7)
//date_default_timezone_set("UTC");
//End of set default timezone

paging(); //Run Paging first
checkCurrency(); //Check if currency session is set
checkLanguage(); //Then check if the session language is set
logStatistic("web_visit");
if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
else include 'lang/ID.php';
//function collections
// ----------SEPERATOR----------
// BEGINNING OF MAIN FUNCTION (sorted by a-Z)
function accountingIncrease($amount){
    $query = mysql_query("UPDATE admin_accounting_central SET real_balance = real_balance + ".$amount);
    return $query;
}

function balanceSystem($id,$p,$b) {// Balance System ; Parameter Id for user Id, P for process type: "-" for minus balance, "+" for add balance, "c" for change balance, B for the balance to change or add or minus
    connectDb();
    $usr_id = mysql_real_escape_string($id);
    $p = mysql_real_escape_string($p);
    $b = formatMoney(mysql_real_escape_string($b));
    switch ($p) {
        case '-':
            if (!mysql_query("UPDATE user_user_balance SET balance = balance - $b, total_out = total_out + $b WHERE id_user = $usr_id")) goPage("error","?err=data");
            break;

        case '+':
            if (!mysql_query("UPDATE user_user_balance SET balance = balance + $b, total_in = total_in + $b WHERE id_user = $usr_id")) goPage("error","?err=data");
            break;

        case 'c':
            if (!mysql_query("UPDATE user_user_balance SET balance = $b WHERE id_user = $usr_id")) goPage("error","?err=data");
            break;
        
        default:
            goPage("error","?err=data");
            break;
    }
}

function calculateDevRating($dev_id){
    connectDb();
    $id = mysql_real_escape_string($dev_id);
    $qu_y = mysql_query("SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_game WHERE id_stuff = id_game AND id_developer='$id' AND vote = 'y'
        UNION SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_dlc WHERE id_stuff = id_dlc AND id_developer = '$id' AND vote = 'y'
        UNION SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_content WHERE id_stuff = id_content AND id_developer = '$id' AND vote = 'y'
        UNION SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_app WHERE id_stuff = id_app AND id_developer = '$id' AND vote = 'y'
        ");
    $qu_total = mysql_query("SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_game WHERE id_stuff = id_game AND id_developer='$id'
        UNION SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_dlc WHERE id_stuff = id_dlc AND id_developer = '$id'
        UNION SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_content WHERE id_stuff = id_content AND id_developer = '$id'
        UNION SELECT COUNT(id_stuff) AS num FROM user_vote_stuff,tci_app WHERE id_stuff = id_app AND id_developer = '$id'");
    $v_y = 0;
    $v_t = 0;
    while ($rs = mysql_fetch_array($qu_y)) {
        $v_y = $v_y + intval($rs['num']);
    }
    while ($rs = mysql_fetch_array($qu_total)) {
        $v_t = $v_t + intval($rs['num']);
    }
    if ($v_t != 0){
        $x = $v_y / $v_t;
        if (strlen($x) != 1) $x = substr($x, 0, 4);
        $result = number_format($x * 100);
    }else $result = 0;
    return $result;
}

function calculateRating($uid){
	connectDb();
	$id = mysql_real_escape_string($uid);
	$qu_y = mysql_query("SELECT count(vote) AS vote_yes FROM user_vote_stuff WHERE id_stuff='$id' AND vote='y'");
	$qu_total = mysql_query("SELECT count(vote) as vote_total FROM user_vote_stuff WHERE id_stuff='$id'");
	$rs_y = mysql_fetch_array($qu_y);
	$rs_t = mysql_fetch_array($qu_total);
	$v_y = $rs_y['vote_yes'];
	$v_t = $rs_t['vote_total'];
	if ($v_t != 0){
		$x = $v_y / $v_t;
		if (strlen($x) != 1) $x = substr($x, 0, 4);
		$result = number_format($x * 100);
	}else $result = 0;
	return $result;
}

function checkCurrency(){
	if (empty($_SESSION['curr'])){
		$_SESSION['curr'] = "IDR";
	}
}

function checkLanguage(){ //check if the Language session exist
	if (empty($_SESSION['lang'])){
		$_SESSION['lang'] = "ID"; //set if null
	}
}

function checkProductBuy($prid){
	$result = false;
	if (!empty($_SESSION['usr_id'])){
		$pr_id = mysql_real_escape_string($prid);
		$usr_id = $_SESSION['usr_id'];
		$query = mysql_query("SELECT * FROM user_user_transaction WHERE id_stuff='$pr_id' AND id_user='$usr_id'");
		if (mysql_num_rows($query) == 1){
			$result = true;
		}
	}
	return $result;
}

function connectDb(){ //connect to MySQL Database
	require 'variable.php';
    require 'credentials/credentials.php';
	$conServer = $CONX['server_main'];
	$conUser = $CONX['user_main'];
	$conPassword = $CONX['password_main'];
	$conDb = $CONX['db_main'];
	$con = mysql_connect($conServer,$conUser,$conPassword);
	$conDb = mysql_select_db($conDb);
	if (!$con || !$conDb) goPage("error","?err=db");
}

function createBoxUblue1($w,$h,$f,$t,$i,$d){ //W : Width,H : Height,F : Font-Size,T : Title,I : Icon(Only from img_html),D : Data
    include 'variable.php';
    $i_h = intval($h) * 70 / 100;
    $i_h = $i_h.'px';
    $w = $w.'%';
    $h = $h.'px';
    $f = $f.'%';
    echo 
    "<div class='box-ublue-1' style='width: $w;height: $h;'>
    <center>
        <label class='text-white-shadow' style='margin-top:-100%;'>$t</label>
        <hr>
        <div class='left' style='width:30%;height:70%'><img src='".$i."' style='width:100%;height:$i_h;'></div><div class='left' style='width:70%;height:$i_h;margin-top:3%;'><label class='text-white-shadow' style='font-size:$f;'>$d</label></div>
    </center>
    </div>";
}

function createBoxUblue3($w,$h,$f,$t,$i,$d){ //W : Width,H : Height,F : Font-Size,T : Title,I : Icon(Only from img_html),D : Data
    include 'variable.php';
    $i_h = intval($h) * 70 / 100;
    $i_h = $i_h.'px';
    $w = $w.'%';
    $h = $h.'px';
    $f = $f.'%';
    echo 
    "<div class='box-ublue-3' style='width: $w;height: $h;'>
    <center>
        <label class='text-white-shadow' style='margin-top:-100%;'>$t</label>
        <hr>
        <div class='left' style='width:30%;height:70%'><img src='".$i."' style='width:100%;height:$i_h;'></div><div class='left' style='width:70%;height:$i_h;margin-top:3%;'><label class='text-white-shadow' style='font-size:$f;'>$d</label></div>
    </center>
    </div>";
}

function createStatistic(){
    connectDb();
    $x_date = date("Y-m");
    $query = mysql_query("SELECT * FROM tci_statistic WHERE month = '$x_date'");
    if (mysql_num_rows($query) == 0){
        $query = mysql_query("SELECT * FROM dev_dev WHERE valid = 1");
        $query2 = mysql_query("SELECT * FROM tci_news");
        $query3 = mysql_query("SELECT @temp FROM tci_game
            UNION SELECT @temp FROM tci_dlc
            UNION SELECT @temp FROM tci_content
            UNION SELECT @temp FROM tci_app");
        $query4 = mysql_query("SELECT * FROM user_user");
        $x_tot_dev = mysql_num_rows($query);
        $x_tot_news = mysql_num_rows($query2);
        $x_tot_product = mysql_num_rows($query3);
        $x_tot_user = mysql_num_rows($query4);
        mysql_query("INSERT INTO tci_statistic values(0,0,0,$x_tot_dev,0,$x_tot_news,0,0,0,0,0,$x_tot_product,0,0,0,0,0,0,$x_tot_user,0,0,0,'$x_date')") or die (mysql_error());
    }
}

function cvtCodeCountry($code){

$code = strtoupper($code);

$countryList = array(
        'AF' => 'Afghanistan',
        'AX' => 'Aland Islands',
        'AL' => 'Albania',
        'DZ' => 'Algeria',
        'AS' => 'American Samoa',
        'AD' => 'Andorra',
        'AO' => 'Angola',
        'AI' => 'Anguilla',
        'AQ' => 'Antarctica',
        'AG' => 'Antigua and Barbuda',
        'AR' => 'Argentina',
        'AM' => 'Armenia',
        'AW' => 'Aruba',
        'AU' => 'Australia',
        'AT' => 'Austria',
        'AZ' => 'Azerbaijan',
        'BS' => 'Bahamas the',
        'BH' => 'Bahrain',
        'BD' => 'Bangladesh',
        'BB' => 'Barbados',
        'BY' => 'Belarus',
        'BE' => 'Belgium',
        'BZ' => 'Belize',
        'BJ' => 'Benin',
        'BM' => 'Bermuda',
        'BT' => 'Bhutan',
        'BO' => 'Bolivia',
        'BA' => 'Bosnia and Herzegovina',
        'BW' => 'Botswana',
        'BV' => 'Bouvet Island (Bouvetoya)',
        'BR' => 'Brazil',
        'IO' => 'British Indian Ocean Territory (Chagos Archipelago)',
        'VG' => 'British Virgin Islands',
        'BN' => 'Brunei Darussalam',
        'BG' => 'Bulgaria',
        'BF' => 'Burkina Faso',
        'BI' => 'Burundi',
        'KH' => 'Cambodia',
        'CM' => 'Cameroon',
        'CA' => 'Canada',
        'CV' => 'Cape Verde',
        'KY' => 'Cayman Islands',
        'CF' => 'Central African Republic',
        'TD' => 'Chad',
        'CL' => 'Chile',
        'CN' => 'China',
        'CX' => 'Christmas Island',
        'CC' => 'Cocos (Keeling) Islands',
        'CO' => 'Colombia',
        'KM' => 'Comoros the',
        'CD' => 'Congo',
        'CG' => 'Congo the',
        'CK' => 'Cook Islands',
        'CR' => 'Costa Rica',
        'CI' => 'Cote Ivoire',
        'HR' => 'Croatia',
        'CU' => 'Cuba',
        'CY' => 'Cyprus',
        'CZ' => 'Czech Republic',
        'DK' => 'Denmark',
        'DJ' => 'Djibouti',
        'DM' => 'Dominica',
        'DO' => 'Dominican Republic',
        'EC' => 'Ecuador',
        'EG' => 'Egypt',
        'SV' => 'El Salvador',
        'GQ' => 'Equatorial Guinea',
        'ER' => 'Eritrea',
        'EE' => 'Estonia',
        'ET' => 'Ethiopia',
        'FO' => 'Faroe Islands',
        'FK' => 'Falkland Islands (Malvinas)',
        'FJ' => 'Fiji the Fiji Islands',
        'FI' => 'Finland',
        'FR' => 'France, French Republic',
        'GF' => 'French Guiana',
        'PF' => 'French Polynesia',
        'TF' => 'French Southern Territories',
        'GA' => 'Gabon',
        'GM' => 'Gambia the',
        'GE' => 'Georgia',
        'DE' => 'Germany',
        'GH' => 'Ghana',
        'GI' => 'Gibraltar',
        'GR' => 'Greece',
        'GL' => 'Greenland',
        'GD' => 'Grenada',
        'GP' => 'Guadeloupe',
        'GU' => 'Guam',
        'GT' => 'Guatemala',
        'GG' => 'Guernsey',
        'GN' => 'Guinea',
        'GW' => 'Guinea-Bissau',
        'GY' => 'Guyana',
        'HT' => 'Haiti',
        'HM' => 'Heard Island and McDonald Islands',
        'VA' => 'Holy See (Vatican City State)',
        'HN' => 'Honduras',
        'HK' => 'Hong Kong',
        'HU' => 'Hungary',
        'IS' => 'Iceland',
        'IN' => 'India',
        'ID' => 'Indonesia',
        'IR' => 'Iran',
        'IQ' => 'Iraq',
        'IE' => 'Ireland',
        'IM' => 'Isle of Man',
        'IL' => 'Israel',
        'IT' => 'Italy',
        'JM' => 'Jamaica',
        'JP' => 'Japan',
        'JE' => 'Jersey',
        'JO' => 'Jordan',
        'KZ' => 'Kazakhstan',
        'KE' => 'Kenya',
        'KI' => 'Kiribati',
        'KP' => 'Korea',
        'KR' => 'Korea',
        'KW' => 'Kuwait',
        'KG' => 'Kyrgyz Republic',
        'LA' => 'Lao',
        'LV' => 'Latvia',
        'LB' => 'Lebanon',
        'LS' => 'Lesotho',
        'LR' => 'Liberia',
        'LY' => 'Libyan Arab Jamahiriya',
        'LI' => 'Liechtenstein',
        'LT' => 'Lithuania',
        'LU' => 'Luxembourg',
        'MO' => 'Macao',
        'MK' => 'Macedonia',
        'MG' => 'Madagascar',
        'MW' => 'Malawi',
        'MY' => 'Malaysia',
        'MV' => 'Maldives',
        'ML' => 'Mali',
        'MT' => 'Malta',
        'MH' => 'Marshall Islands',
        'MQ' => 'Martinique',
        'MR' => 'Mauritania',
        'MU' => 'Mauritius',
        'YT' => 'Mayotte',
        'MX' => 'Mexico',
        'FM' => 'Micronesia',
        'MD' => 'Moldova',
        'MC' => 'Monaco',
        'MN' => 'Mongolia',
        'ME' => 'Montenegro',
        'MS' => 'Montserrat',
        'MA' => 'Morocco',
        'MZ' => 'Mozambique',
        'MM' => 'Myanmar',
        'NA' => 'Namibia',
        'NR' => 'Nauru',
        'NP' => 'Nepal',
        'AN' => 'Netherlands Antilles',
        'NL' => 'Netherlands the',
        'NC' => 'New Caledonia',
        'NZ' => 'New Zealand',
        'NI' => 'Nicaragua',
        'NE' => 'Niger',
        'NG' => 'Nigeria',
        'NU' => 'Niue',
        'NF' => 'Norfolk Island',
        'MP' => 'Northern Mariana Islands',
        'NO' => 'Norway',
        'OM' => 'Oman',
        'PK' => 'Pakistan',
        'PW' => 'Palau',
        'PS' => 'Palestinian Territory',
        'PA' => 'Panama',
        'PG' => 'Papua New Guinea',
        'PY' => 'Paraguay',
        'PE' => 'Peru',
        'PH' => 'Philippines',
        'PN' => 'Pitcairn Islands',
        'PL' => 'Poland',
        'PT' => 'Portugal, Portuguese Republic',
        'PR' => 'Puerto Rico',
        'QA' => 'Qatar',
        'RE' => 'Reunion',
        'RO' => 'Romania',
        'RU' => 'Russian Federation',
        'RW' => 'Rwanda',
        'BL' => 'Saint Barthelemy',
        'SH' => 'Saint Helena',
        'KN' => 'Saint Kitts and Nevis',
        'LC' => 'Saint Lucia',
        'MF' => 'Saint Martin',
        'PM' => 'Saint Pierre and Miquelon',
        'VC' => 'Saint Vincent and the Grenadines',
        'WS' => 'Samoa',
        'SM' => 'San Marino',
        'ST' => 'Sao Tome and Principe',
        'SA' => 'Saudi Arabia',
        'SN' => 'Senegal',
        'RS' => 'Serbia',
        'SC' => 'Seychelles',
        'SL' => 'Sierra Leone',
        'SG' => 'Singapore',
        'SK' => 'Slovakia (Slovak Republic)',
        'SI' => 'Slovenia',
        'SB' => 'Solomon Islands',
        'SO' => 'Somalia, Somali Republic',
        'ZA' => 'South Africa',
        'GS' => 'South Georgia and the South Sandwich Islands',
        'ES' => 'Spain',
        'LK' => 'Sri Lanka',
        'SD' => 'Sudan',
        'SR' => 'Suriname',
        'SJ' => 'Svalbard & Jan Mayen Islands',
        'SZ' => 'Swaziland',
        'SE' => 'Sweden',
        'CH' => 'Switzerland, Swiss Confederation',
        'SY' => 'Syrian Arab Republic',
        'TW' => 'Taiwan',
        'TJ' => 'Tajikistan',
        'TZ' => 'Tanzania',
        'TH' => 'Thailand',
        'TL' => 'Timor-Leste',
        'TG' => 'Togo',
        'TK' => 'Tokelau',
        'TO' => 'Tonga',
        'TT' => 'Trinidad and Tobago',
        'TN' => 'Tunisia',
        'TR' => 'Turkey',
        'TM' => 'Turkmenistan',
        'TC' => 'Turks and Caicos Islands',
        'TV' => 'Tuvalu',
        'UG' => 'Uganda',
        'UA' => 'Ukraine',
        'AE' => 'United Arab Emirates',
        'GB' => 'United Kingdom',
        'US' => 'United States of America',
        'UM' => 'United States Minor Outlying Islands',
        'VI' => 'United States Virgin Islands',
        'UY' => 'Uruguay, Eastern Republic of',
        'UZ' => 'Uzbekistan',
        'VU' => 'Vanuatu',
        'VE' => 'Venezuela',
        'VN' => 'Vietnam',
        'WF' => 'Wallis and Futuna',
        'EH' => 'Western Sahara',
        'YE' => 'Yemen',
        'ZM' => 'Zambia',
        'ZW' => 'Zimbabwe'
    );

    if( !$countryList[$code] ) return $code;
    else return $countryList[$code];
}

function cvtMoney($money){
	connectDb();
	$curr = getCurr();
	if ($curr == "USD"){
		return number_format($money,2);
	}else if ($curr == "IDR"){
		$result = 0;
		$query = mysql_query("SELECT kurs_rupiah FROM tci_config");
		$rs = mysql_fetch_array($query);
		$kurs = $rs['kurs_rupiah'];
		$result = number_format($money * $kurs,0,",",".");
		return $result;
	}
}

function cvtMoneyCurr($money,$curr){
    connectDb();
    if ($curr == "USD"){
        return number_format($money,2);
    }else if ($curr == "IDR"){
        $result = 0;
        $query = mysql_query("SELECT kurs_rupiah FROM tci_config");
        $rs = mysql_fetch_array($query);
        $kurs = $rs['kurs_rupiah'];
        $result = number_format($money * $kurs,0,",",".");
        return $result;
    }
}

function cvtMoneyIdrToUsd($money){
    connectDb();
    $result = 0;
    $query = mysql_query("SELECT kurs_rupiah FROM tci_config");
    $rs = mysql_fetch_array($query);
    $kurs = $rs['kurs_rupiah'];
    $result = number_format($money / $kurs,2);
    return $result;
}

 function cvtSize($bytes)
    {
        if ($bytes >= 1073741824)
        {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        }
        elseif ($bytes >= 1048576)
        {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        }
        elseif ($bytes >= 1024)
        {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        }
        elseif ($bytes > 1)
        {
            $bytes = $bytes . ' bytes';
        }
        elseif ($bytes == 1)
        {
            $bytes = $bytes . ' byte';
        }
        else
        {
            $bytes = '0 bytes';
        }

        return $bytes;
}

function deleteDir($dirPath) {
    if (is_dir($dirPath)) {   
        if (substr($dirPath, strlen($dirPath) - 1, 1) != '/') {
            $dirPath .= '/';
        }
        $files = glob($dirPath . '*', GLOB_MARK);
        foreach ($files as $file) {
            if (is_dir($file)) {
                deleteDir($file);
            } else {
                unlink($file);
            }
        }
        rmdir($dirPath);
    }
}

function deleteReview($r){
	if (!empty($_SESSION['usr_id'])){
		$usr_id = $_SESSION['usr_id'];
		$r_id = mysql_real_escape_string($r['id']);
		$query = mysql_query("DELETE FROM user_review WHERE id_review=$r_id AND id_user='$usr_id'");
		goBack();
	}	
}

function disconnectDb(){ //disconnect from MySQL Database
	$con = mysql_close();
}

/** --- Centrailized CURL function --- **/
function doCurl($url, $header = false, $data = false, $method = false){
    $ch = curl_init();

    if ($header)
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

    if ($data){
        $data_to_post = http_build_query($data);
    }
    else $data_to_post = null;

    if ($method == "GET" && $data){
        curl_setopt($ch, CURLOPT_URL, $url.'?'.$data_to_post);
    } else {
        curl_setopt($ch, CURLOPT_URL, $url);
    }

    curl_setopt($ch, CURLOPT_HEADER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
    curl_setopt($ch, CURLOPT_VERBOSE, 1);
    
    if ($method == 'POST' && $data_to_post){
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_to_post);
    }

    $output = curl_exec($ch); 

    $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
    $result['data'] = substr($output, $header_size);
    $result['code'] = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);      
    return $result;
}

function downloadFile($id){ //id : Product Id
    connectDb();
    if (empty($_SESSION['usr_id'])){
        goPage("login","");
    }
    $id = mysql_real_escape_string($id);
    $query = mysql_query("SELECT title,id_developer,link FROM tci_game WHERE id_game = '$id'
        UNION SELECT title,id_developer,link FROM tci_dlc WHERE id_dlc = '$id'
        UNION SELECT title,id_developer,link FROM tci_content WHERE id_content = '$id'
        UNION SELECT title,id_developer,link FROM tci_app WHERE id_app = '$id'");
    if (mysql_num_rows($query) == 1){
        $rs = mysql_fetch_array($query);
        $usr_id = $_SESSION['usr_id'];
        $query = mysql_query("SELECT * FROM user_user_transaction WHERE id_user = $usr_id AND id_stuff = '$id'");
        if (isDevAlreadySignup($usr_id)){
            $x_dev = getDevDataByUsrId($usr_id);
        }else{
            $x_dev = array();
            $x_dev['id_developer'] = "";
        }
        if (mysql_num_rows($query) == 1 || $x_dev['id_developer'] == $rs['id_developer']){
            $x_dir = $rs['link'];
            $x_type = strtolower(pathinfo($x_dir, PATHINFO_EXTENSION));
            mysql_query("UPDATE tci_stuff_download SET num = num + 1 WHERE id_stuff = '$id'");
            header('Content-Description: File Transfer');
            header('Content-Type: application/octet-stream');
            header('Content-Disposition: attachment; filename="' . $rs['title'].'.'.$x_type.'"');
            header('Content-Transfer-Encoding: binary');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Pragma: public');
            header('Content-Length: ' . getRemoteSize($x_dir));
            while (ob_get_level()) ob_end_clean();
            ob_clean();
            flush();
            readfile($x_dir);            
        }else{
            goBackParam("#notBuy");
        }
    }else{
        goBackParam("#notExist");
    }
}

function emailSend($from,$to,$subject,$text){
    require 'plugins/phpmailer/Exception.php';
    require 'plugins/phpmailer/PHPMailer.php';
    require 'plugins/phpmailer/SMTP.php';
    require 'variable.php';
    require 'credentials/credentials.php';
    $response = false;
    $mail = new PHPMailer\PHPMailer\PHPMailer();
   

    // SMTP configuration
    $mail->isSMTP();
    $mail->Host     = 'smtp.hostinger.com'; //sesuaikan sesuai nama domain hosting/server yang digunakan
    $mail->SMTPAuth = true;
    $mail->Username = $from; // user email
    $mail->Password = $email_password[$from]; // password email
    $mail->Port     = 587;

    $mail->setFrom($from, $email_name[$from]); // user email
    $mail->addReplyTo($from, $email_name[$from]); //user email

    // Add a recipient
    $mail->addAddress($to); //email tujuan pengiriman email

    // Email subject
    $mail->Subject = $subject; //subject email

    // Set email format to HTML
    $mail->isHTML(true);

    // Email body content
    $mail->Body = $text;

    // Send email
    $mail->send();
}

function formatMoney($money){
    return number_format($money,2);
}

//Start of FORM FUNCTION

function funcAddToCart($usr_id,$stuff_id){
	connectDb();
	$query = mysql_query("SELECT * FROM user_cart WHERE id_user='$usr_id' AND id_stuff='$stuff_id'");
	if (mysql_num_rows($query) != 1){
		$query = mysql_query("INSERT INTO user_cart values('$usr_id','$stuff_id')");
		if (!$query) goPage("error","?err=data");
	}else goPage("error","?err=data");
	goBack();	
}

function funcCartSystem($a,$b,$is_from_handle = false){ //$a is proccess type such as pay or delete;$b is for id or "all" when processing all cart
    connectDb();
    if (!empty($_SESSION['usr_id'])){
        $usr_id = $_SESSION['usr_id'];
        if ($a == "pay"){
            $c_balance = getMyBalance();
            $c_total = getCartPrice($b);
            if ($c_balance < $c_total) goBackParam("?f=b");
            else{
                $myData = getUserData($_SESSION['usr_id']);
                if ($b == "all"){
                    $query = mysql_query("SELECT id_game AS id,id_developer,title,price FROM tci_game,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_game.id_game
                                UNION SELECT id_app AS id,id_developer,title,price FROM tci_app,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_app.id_app
                                UNION SELECT id_dlc AS id,id_developer,title,price FROM tci_dlc,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_dlc.id_dlc
                                UNION SELECT id_content AS id,id_developer,title,price FROM tci_content,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_content.id_content");
                    while ($rs = mysql_fetch_array($query)){
                        $c_f_balance = $c_balance;
                        if ($rs['id_developer'] != "TCI") {
                            $c_p_real_price = formatMoney($rs['price']);
                            $fee_gateway = $c_p_real_price > 0 ? 0.31 : 0;
                            $c_p_price = formatMoney((100/(100+10) * ($c_p_real_price - $fee_gateway)));
                            $c_p_dividen = formatMoney(($c_p_real_price * 10 / 100) + $fee_gateway);
                        }else{
                            $c_p_real_price = formatMoney($rs['price']);
                        }
                        $c_e_balance = formatMoney($c_f_balance-$c_p_real_price);
                        $c_id_stuff = $rs['id'];
                        $c_id_dev = $rs['id_developer'];
                        $c_title_stuff = $rs['title'];
                        if ($c_id_dev != "TCI"){
                            $txt_log = $myData['front_name'].' '.$myData['back_name'].' / ID '.$myData['id_user'].' Dividen 10% & $0,31 (Payment Gateway Fee) dari pembelian '.$rs['title'].' / '.$rs['id'].' seharga $'.$rs['price'];
                            if (!mysql_query("INSERT INTO user_user_transaction values($usr_id,$c_f_balance,$c_p_price,$c_e_balance,'$c_id_stuff','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                            if (!mysql_query("INSERT INTO user_real_transaction values($usr_id,$c_f_balance,$c_p_real_price,$c_e_balance,'$c_id_stuff','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                            if (!mysql_query("UPDATE dev_balance SET balance = balance + $c_p_price , total_in = total_in + $c_p_price WHERE id_developer = '$c_id_dev'")) goPage("error","?err=data");
                            if ($c_p_real_price > 0){
                                if (!mysql_query("INSERT INTO admin_accounting_log_income values($c_p_dividen,'$txt_log',NOW())")) goPage("error","?err=data");
                                if (!mysql_query("UPDATE admin_accounting_central SET balance = balance + $c_p_dividen")) goPage("error","?err=data");
                            }
                        }else{
                            $txt_log = $myData['front_name'].' '.$myData['back_name'].' / ID '.$myData['id_user'].' Pemasukan dari produk TCI : '.$rs['title'].' / '.$rs['id'];
                            if (!mysql_query("INSERT INTO user_user_transaction values($usr_id,$c_f_balance,$c_p_real_price,$c_e_balance,'$c_id_stuff','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                            if (!mysql_query("INSERT INTO user_real_transaction values($usr_id,$c_f_balance,$c_p_real_price,$c_e_balance,'$c_id_stuff','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                            if (!mysql_query("UPDATE dev_balance SET balance = balance + $c_p_real_price , total_in = total_in + $c_p_real_price WHERE id_developer = '$c_id_dev'")) goPage("error","?err=data");
                            if ($c_p_real_price > 0){
                                if (!mysql_query("INSERT INTO admin_accounting_log_income values($c_p_real_price,'$txt_log',NOW())")) goPage("error","?err=data");
                                if (!mysql_query("UPDATE admin_accounting_central SET balance = balance + $c_p_real_price")) goPage("error","?err=data");
                            }
                        }
                        $c_balance = $c_e_balance;
                        funcCartSystem("delete",$c_id_stuff,true);
                        logUserActivity("Melakukan pembelian produk ID : ".$c_id_stuff);
                        $x_d = getUserData($usr_id);
                        $x_n = $x_d['front_name'].' '.$x_d['back_name'];
                        include 'lang/ID.php';
                        $txtLogID = $x_n.' '.$TEXT['cart_bought'].' '.$c_title_stuff;
                        include 'lang/EN.php';
                        $txtLogEN = $x_n.' '.$TEXT['cart_bought'].' '.$c_title_stuff;
                        $txtLog = "ID/".$txtLogID.';EN/'.$txtLogEN;
                        logRecentActivity("$txtLog",$_SESSION['usr_link']);
                        logStatistic("product_transaction");
                        logStatistic("user_activity");
                    }
                }else{
                    $query = mysql_query("SELECT id_game AS id,id_developer,title,price FROM tci_game WHERE id_game = '$b'
                        UNION SELECT id_dlc AS id,id_developer,title,price FROM tci_dlc WHERE id_dlc = '$b'
                        UNION SELECT id_content AS id,id_developer,title,price FROM tci_content WHERE id_content = '$b'
                        UNION SELECT id_app AS id,id_developer,title,price FROM tci_app WHERE id_app = '$b'");
                    if (!mysql_num_rows($query)==1)goPage("error","?err=data");
                    $rs = mysql_fetch_array($query);
                    $c_id_dev = $rs['id_developer'];
                    if ($c_id_dev != "TCI") {
                            $c_p_real_price = formatMoney($rs['price']);
                            $fee_gateway = $c_p_real_price > 0 ? 0.31 : 0;
                            $c_p_price = formatMoney((100/(100+10) * ($c_p_real_price - $fee_gateway)));
                            $c_p_dividen = formatMoney(($c_p_real_price * 10 / 100) + $fee_gateway);
                        }else{
                            $c_p_real_price = formatMoney($c_total);
                        }
                    $c_e_balance = formatMoney($c_balance-$c_total);
                    if ($c_id_dev != "TCI"){
                        $txt_log = $myData['front_name'].' '.$myData['back_name'].' / ID '.$myData['id_user'].' Dividen 10% & $0,31 (Payment Gateway Fee) dari pembelian '.$rs['title'].' / '.$rs['id'].' seharga $'.$rs['price'];
                        if(!mysql_query("INSERT INTO user_user_transaction values($usr_id,$c_balance,$c_p_price,$c_e_balance,'$b','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                        if(!mysql_query("INSERT INTO user_real_transaction values($usr_id,$c_balance,$c_p_real_price,$c_e_balance,'$b','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                        if (!mysql_query("UPDATE dev_balance SET balance = balance + $c_p_price , total_in = total_in + $c_p_price WHERE id_developer = '$c_id_dev'")) goPage("error","?err=data");
                        if ($c_p_real_price > 0){
                            if (!mysql_query("INSERT INTO admin_accounting_log_income values($c_p_dividen,'$txt_log',NOW())")) goPage("error","?err=data");
                            if (!mysql_query("UPDATE admin_accounting_central SET balance = balance + $c_p_dividen")) goPage("error","?err=data");
                        }
                    }else{
                        $txt_log = $myData['front_name'].' '.$myData['back_name'].' / ID '.$myData['id_user'].' Pemasukan dari produk TCI : '.$rs['title'].' / '.$rs['id'];
                        if(!mysql_query("INSERT INTO user_user_transaction values($usr_id,$c_balance,$c_p_real_price,$c_e_balance,'$b','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                        if(!mysql_query("INSERT INTO user_real_transaction values($usr_id,$c_balance,$c_p_real_price,$c_e_balance,'$b','$c_id_dev',NOW(),'')")) goPage("error","?err=data");
                        if (!mysql_query("UPDATE dev_balance SET balance = balance + $c_p_real_price , total_in = total_in + $c_p_real_price WHERE id_developer = '$c_id_dev'")) goPage("error","?err=data");
                        if ($c_p_real_price){
                            if (!mysql_query("INSERT INTO admin_accounting_log_income values($c_p_real_price,'$txt_log',NOW())")) goPage("error","?err=data");
                            if (!mysql_query("UPDATE admin_accounting_central SET balance = balance + $c_p_real_price")) goPage("error","?err=data");
                        }
                    }
                    $query = mysql_query("SELECT title FROM tci_game,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff =  '$b' AND user_cart.id_stuff = tci_game.id_game
                                UNION SELECT title FROM tci_app,user_cart WHERE user_cart.id_user = $usr_id AND  user_cart.id_stuff =  '$b' AND user_cart.id_stuff = tci_app.id_app
                                UNION SELECT title FROM tci_dlc,user_cart WHERE user_cart.id_user = $usr_id AND  user_cart.id_stuff =  '$b' AND user_cart.id_stuff = tci_dlc.id_dlc
                                UNION SELECT title FROM tci_content,user_cart WHERE user_cart.id_user = $usr_id AND  user_cart.id_stuff =  '$b' AND user_cart.id_stuff = tci_content.id_content");
                    if (!$query)goPage("error","?err=data");
                    $rs = mysql_fetch_array($query);
                    $c_title_stuff = $rs['title'];
                    $c_balance = $c_e_balance;
                    funcCartSystem("delete",$b,true);
                    logUserActivity("Melakukan pembelian produk ID : ".$b);
                    $x_d = getUserData($usr_id);
                    $x_n = $x_d['front_name'].' '.$x_d['back_name'];
                    include 'lang/ID.php';
                    $txtLogID = $x_n.' '.$TEXT['cart_bought'].' '.$c_title_stuff;
                    include 'lang/EN.php';
                    $txtLogEN = $x_n.' '.$TEXT['cart_bought'].' '.$c_title_stuff;
                    $txtLog = "ID/".$txtLogID.';EN/'.$txtLogEN;
                    logRecentActivity("$txtLog",$_SESSION['usr_link']);
                    logStatistic("product_transaction");
                    logStatistic("user_activity");
                }
                balanceSystem($usr_id,'-',$c_total);
                goPage("my_transaction","");
            }
        }else if ($a == "delete"){
            if ($b == "all"){
                if (!$query = mysql_query("DELETE FROM user_cart WHERE user_cart.id_user = $usr_id")) goPage("error","?err=data");
                if (!$is_from_handle){
                    goPage("cart","");
                }
            }else{
                if (!$query = mysql_query("DELETE FROM user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = '$b'")) goPage("error","?err=data");
                if (!$is_from_handle){
                    goPage("cart","");
                }
            }
        }
    }
}

function funcDevAdminAdd($usr_id_add){
    connectDb();
    $dev_id = $_SESSION['dev_id'];
    $usr_id_add = mysql_real_escape_string($usr_id_add);
    $usr_id = $_SESSION['usr_id'];
    $dev_position = getDevPosition($usr_id);
    if ($dev_position == "owner"){
        if (!isDevAlreadySignup($usr_id_add)){
            mysql_query("INSERT INTO dev_admin values ('$dev_id','$usr_id_add',0)") or die (mysql_error());
            $result = "#success";
        }else $result = "?err=hassdev";
    }else $result = "?err=err";
    logStatistic("dev_activity");
    goBackParam($result);
}

function funcDevAdminConfirm($d){ //d : Decision -> accept , reject
    include 'variable.php';
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    connectDb();
    if (!empty($_SESSION['usr_id'])){
        $x_dev = getDevDataByUsrId($_SESSION['usr_id']);
        $usr_id = $_SESSION['usr_id'];
        $usrData = getUserData($usr_id);
        $dev_id = $x_dev['id_developer'];
        $_SESSION['dev_id'] = $dev_id;
        if ($d == "accept") {
            mysql_query("UPDATE dev_admin SET confirm = 1 WHERE id_developer = '$dev_id' AND id_user = $usr_id");
            include 'lang/ID.php';
            $l_doing_id = $TEXT2['devconsole_admin_joined'];
            include 'lang/EN.php';
            $l_doing_en = $TEXT2['devconsole_admin_joined'];
            $l_doing = joinTextLang($l_doing_id,$l_doing_en);
            logDevActivity($l_doing,$dev_id,"");
            include 'lang/ID.php';
            $l_recent_id = $usrData['front_name'].' '.$usrData['back_name'].' '.$TEXT2['devconsole_admin_joined'].' '.$dev_id;
            include 'lang/EN.php';
            $l_recent_en = $usrData['front_name'].' '.$usrData['back_name'].' '.$TEXT2['devconsole_admin_joined'].' '.$dev_id;
            $l_recent = joinTextLang($l_recent_id,$l_recent_en);
            $l_link = $BASENAME['developer'].'view/?id='.$dev_id;
            logRecentActivity($l_recent,$l_link);
        }
        else if ($d == "reject") {
            mysql_query("DELETE FROM dev_admin WHERE id_developer = '$dev_id' AND id_user = $usr_id");
            mysql_query("UPDATE dev_admin SET confirm = 1 WHERE id_developer = '$dev_id' AND id_user = $usr_id");
            include 'lang/ID.php';
            $l_doing_id = $TEXT2['devconsole_admin_reject'];
            include 'lang/EN.php';
            $l_doing_en = $TEXT2['devconsole_admin_reject'];
            $l_doing = joinTextLang($l_doing_id,$l_doing_en);
            logDevActivity($l_doing,$dev_id,"");
        }
        $_SESSION['dev_id'] = null;
    }
    logStatistic("dev_activity");
    goPage("devconsole","");
}

function funcDevAdminDelete($id_admin){
    connectDb();
    $dev_id = $_SESSION['dev_id'];
    $id_admin = mysql_real_escape_string($id_admin);
    $usr_id = $_SESSION['usr_id'];
    $dev_position = getDevPosition($usr_id);
    if ($dev_position == "owner"){
        $query = mysql_query("SELECT @temp FROM dev_admin WHERE id_developer = '$dev_id' AND id_user = '$id_admin'");
        if (mysql_num_rows($query) == 1){
            $query = mysql_query("DELETE FROM dev_admin WHERE id_user='$id_admin'") or die (mysql_error());
        }else goBackParam("?err=err");
    }else goBackParam("?err=err");
    logStatistic("dev_activity");
    goBackParam("#success");
}

function funcDevAdminResign(){
    connectDb();
    if (!empty($_SESSION['usr_id']) && getDevPosition($_SESSION['usr_id']) == "admin"){
        $usr_id = $_SESSION['usr_id'];
        $x_dev = getDevDataByUsrId($usr_id);
        $dev_id = $x_dev['id_developer'];
        mysql_query("DELETE FROM dev_admin WHERE id_developer = '$dev_id' AND id_user = $usr_id") or die(mysql_error());
        logStatistic("dev_activity");
    }
    goPage("index","");
}

function funcDevProductAdd($p){
    include 'variable.php';
    connectDb();
    $p_id_developer = $p['id_developer'];
    $p_type = $p['type'];
    $p_id = $p['id'];
    $p_code_project = $p['code_project'];
    if (!empty($p['id_game_parent'])) $p_id_game_parent = $p['id_game_parent'];
    $p_title = $p['title'];
    $p_sub_title = $p['sub_title'];
    $p_gc = $p['gc'];
    $p_tags = $p['tags'];
    $p_pic_title = $p['pic_title'];
    $x_pic_sub[0] = $p['pic_sub1'];
    $x_pic_sub[1] = $p['pic_sub2'];
    $x_pic_sub[2] = $p['pic_sub3'];
    if (!empty($p['pic_sub4'])) $x_pic_sub[3] = $p['pic_sub4'];
    if (!empty($p['pic_sub5'])) $x_pic_sub[4] = $p['pic_sub5'];
    if (!empty($p['pic_sub6'])) $x_pic_sub[5] = $p['pic_sub6'];
    if (!empty($p['pic_progress'])){
        $p_pic_progress = $p['pic_progress'];
    }else $p_pic_progress = "";
    $p_detail = $p['detail'];
    $p_price = $p['price'];
    $p_date_started = $p['date_started'];
    $p_date_published = $p['date_published'];
    $p_progress = $p['progress'];
    $p_label = $p['label'];
    $p_show_public = $p['show_public'];
    $p_link = mysql_real_escape_string($p['link']);
    // Join section for Pic Sub
    $x_picsub_length = count($x_pic_sub);
    $p_pic_sub = "";
    for ($xx=0; $xx < $x_picsub_length; $xx++) { 
        if ($p_pic_sub == "") $p_pic_sub = $x_pic_sub[$xx];
        else $p_pic_sub = $p_pic_sub.';'.$x_pic_sub[$xx];
    }
    //Section insert to product
    switch ($p_type) {
        case 'game':
            $x_table_progress = "tci_game_progress";
            mysql_query("INSERT INTO tci_game values ('$p_id','$p_code_project','$p_title','$p_sub_title','$p_id_developer','$p_gc','$p_tags','$p_pic_title','$p_pic_sub','$p_pic_progress','$p_detail','$p_price','$p_link')") or die (goBackParam("?err=err"));
            break;
        
        case 'dlc':
            $x_table_progress = "tci_dlc_progress";
            mysql_query("INSERT INTO tci_dlc values ('$p_id','$p_id_game_parent','$p_code_project','$p_title','$p_sub_title','$p_id_developer','$p_gc','$p_tags','$p_pic_title','$p_pic_sub','$p_pic_progress','$p_detail','$p_price','$p_link')") or die  (goBackParam("?err=err"));
            break;

        case 'content':
            $x_table_progress = "tci_content_progress";
            mysql_query("INSERT INTO tci_content values ('$p_id','$p_id_game_parent','$p_title','$p_sub_title','$p_id_developer','$p_gc','$p_tags','$p_pic_title','$p_pic_sub','$p_pic_progress','$p_detail','$p_price','$p_link')") or die  (goBackParam("?err=err"));
            break;

        case 'app':
            $x_table_progress = "tci_app_progress";
            mysql_query("INSERT INTO tci_app values ('$p_id','$p_code_project','$p_title','$p_sub_title','$p_id_developer','$p_gc','$p_tags','$p_pic_title','$p_pic_sub','$p_pic_progress','$p_detail','$p_price','$p_link')") or die  (goBackParam("?err=err"));
            break;
    }
    //Section insert to progress
    mysql_query("INSERT INTO $x_table_progress VALUES ('$p_id','$p_date_started','$p_date_published','$p_progress','$p_label','$p_show_public')") or die (goBackParam("?err=err"));
    //Section insert to stuff download
    mysql_query("INSERT INTO tci_stuff_download VALUES ('$p_id','0')") or die(goBackParam("?err=err"));
    //Section add log dev activity
    include 'lang/ID.php';
    $l_doing_id = $TEXT2['devconsole_log_add_p'];
    include 'lang/EN.php';
    $l_doing_en = $TEXT2['devconsole_log_add_p'];
    $l_doing = joinTextLang($l_doing_id,$l_doing_en);
    logDevActivity($l_doing,$p_id,"");
    if ($p_show_public == true){
        $x_dev = getDevData($p_id_developer);
        include 'lang/ID.php';
        $l_recent_id = $x_dev['name'].' '.$TEXT2['devconsole_log_add_p'].' '.$p_title;
        include 'lang/EN.php';
        $l_recent_en = $x_dev['name'].' '.$TEXT2['devconsole_log_add_p'].' '.$p_title;
        $l_recent = joinTextLang($l_recent_id,$l_recent_en);
        if ($p_type == "game" || $p_type == "app") $l_link = $BASENAME['store'].'view/?id='.$p_id;
        if ($p_type == "content" || $p_type == "dlc") $l_link = $BASENAME['content'].'view/?id='.$p_id;
        logRecentActivity($l_recent,$l_link);
    }
    logStatistic("dev_activity");
    logStatistic("product_add");
    goBackParam("#success");
}

function funcDevProductDelete($p){
    connectDb();
    if ($p['mode'] != "edit") {
        echo $p['mode'];
        exit;
    }
    $p_id_developer = $p['id_developer'];
    $p_id = $p['id_old'];
    $x_p = getDevProductData($p_id);
    $p_show_public = $x_p['show_public'];
    if (isProductExist($p_id)){
        $query = mysql_query("SELECT @temp FROM tci_game WHERE id_game = '$p_id' AND id_developer = '$p_id_developer'
            UNION SELECT @temp FROM tci_dlc WHERE id_dlc = '$p_id' AND id_developer = '$p_id_developer'
            UNION SELECT @temp FROM tci_content WHERE id_content = '$p_id' AND id_developer = '$p_id_developer'
            UNION SELECT @temp FROM tci_app WHERE id_app = '$p_id' AND id_developer = '$p_id_developer'") or die (goBackParam("?err=err"));
        if (mysql_num_rows($query) == 1){
            $x_path_img = "../img/product/".$p_id_developer.'/'.$p_id.'/';
            $x_path_product = "../product/".$p_id_developer.'/'.$p_id.'/';
            deleteDir($x_path_img);
            deleteDir($x_path_product);
            switch ($p['type']) {
                case 'game':
                    mysql_query("DELETE FROM tci_game WHERE id_game = '$p_id'") or die (goBackParam("?err=err"));
                    mysql_query("DELETE FROM tci_game_progress WHERE id_game = '$p_id'") or die (goBackParam("?err=err"));
                    break;
                
                case 'dlc':
                    mysql_query("DELETE FROM tci_dlc WHERE id_dlc = '$p_id'") or die (goBackParam("?err=err"));
                    mysql_query("DELETE FROM tci_dlc_progress WHERE id_dlc = '$p_id'") or die (goBackParam("?err=err"));
                    break;
                
                case 'content':
                    mysql_query("DELETE FROM tci_content WHERE id_content = '$p_id'") or die (goBackParam("?err=err"));
                    mysql_query("DELETE FROM tci_content_progress WHERE id_content = '$p_id'") or die (goBackParam("?err=err"));
                    break;
                
                case 'app':
                    mysql_query("DELETE FROM tci_app WHERE id_app = '$p_id'") or die (goBackParam("?err=err"));
                    mysql_query("DELETE FROM tci_app_progress WHERE id_app = '$p_id'") or die (goBackParam("?err=err"));
                    break;
            }
            mysql_query("DELETE FROM tci_stuff_download WHERE id_stuff = '$p_id'") or die (goBackParam("?err=err"));
            mysql_query("DELETE FROM user_cart WHERE id_stuff = '$p_id'") or die (goBackParam("?err=err"));
            mysql_query("DELETE FROM user_review WHERE id_stuff = '$p_id'") or die (goBackParam("?err=err"));
            mysql_query("DELETE FROM user_vote_stuff WHERE id_stuff = '$p_id'") or die (goBackParam("?err=err"));
            //Section add log dev activity
            include 'lang/ID.php';
            $l_doing_id = $TEXT2['devconsole_log_del_p'];
            include 'lang/EN.php';
            $l_doing_en = $TEXT2['devconsole_log_del_p'];
            $l_doing = joinTextLang($l_doing_id,$l_doing_en);
            logDevActivity($l_doing,$p_id,"");
            if ($p_show_public == true){
                $x_dev = getDevData($p_id_developer);
                include 'lang/ID.php';
                $l_recent_id = $x_dev['name'].' '.$TEXT2['devconsole_log_del_p'].' '.$x_p['title'];
                include 'lang/EN.php';
                $l_recent_en = $x_dev['name'].' '.$TEXT2['devconsole_log_del_p'].' '.$x_p['title'];
                $l_recent = joinTextLang($l_recent_id,$l_recent_en);
                if ($p_type == "game" || $p_type == "app") $l_link = $BASENAME['store'].'view/?id='.$p_id;
                if ($p_type == "content" || $p_type == "dlc") $l_link = $BASENAME['content'].'view/?id='.$p_id;
                logRecentActivity($l_recent,$l_link);
            }
            logStatistic("dev_activity");
            logStatistic("prouct_delete");
            logStatisticMin("product_total");
            goBackParam("#success");
        }else goBackParam("?err=err");
    }else goBackParam("?err=err");
}

function funcDevProductEdit($p){
    include 'variable.php';
    connectDb();
    $p_type = $p['type'];
    switch ($p_type) {
        case 'game':
            $x_q = "tci_game";
            $x_qp = "tci_game_progress";
            $x_qi = "tci_game.id_game";
            $x_qpi = "tci_game_progress.id_game";
            break;
        
        case 'dlc':
            $x_q = "tci_dlc";
            $x_qp = "tci_dlc_progress";
            $x_qi = "tci_dlc.id_dlc";
            $x_qpi = "tci_dlc_progress.id_dlc";
            break;

        case 'content':
            $x_q = "tci_content";
            $x_qp = "tci_content_progress";
            $x_qi = "tci_content.id_content";
            $x_qpi = "tci_content_progress.id_content";
            break;

        case 'app':
            $x_q = "tci_app";
            $x_qp = "tci_app_progress";
            $x_qi = "tci_app.id_app";
            $x_qpi = "tci_app_progress.id_app";
            break;
    }
    $p_id_developer = $p['id_developer'];
    $p_id = $p['id'];
    $p_id_old = $p['id_old'];
    $p_code_project = $p['code_project'];
    if (!empty($p['id_game_parent'])) $p_id_game_parent = $p['id_game_parent'];
    $p_title = $p['title'];
    $p_sub_title = $p['sub_title'];
    $p_gc = $p['gc'];
    $p_tags = $p['tags'];
    if (!empty($p['pic_title'])) $p_pic_title = $p['pic_title'];
    if (!empty($p['pic_progress'])) $p_pic_progress = $p['pic_progress'];
    if (!empty($p['pic_sub1'])) $x_pic_sub[0] = $p['pic_sub1'];
    if (!empty($p['pic_sub2'])) $x_pic_sub[1] = $p['pic_sub2'];
    if (!empty($p['pic_sub3'])) $x_pic_sub[2] = $p['pic_sub3'];
    if (!empty($p['pic_sub4'])) $x_pic_sub[3] = $p['pic_sub4'];
    if (!empty($p['pic_sub5'])) $x_pic_sub[4] = $p['pic_sub5'];
    if (!empty($p['pic_sub6'])) $x_pic_sub[5] = $p['pic_sub6'];
    $p_detail = $p['detail'];
    $p_price = $p['price'];
    $p_date_started = $p['date_started'];
    $p_date_published = $p['date_published'];
    $p_progress = $p['progress'];
    $p_label = $p['label'];
    if (!empty($p['show_public'])) $p_show_public = true;
    else $p_show_public = false;
    if (!empty($p['link'])) $p_link = $p['link'];
    //Section check ID exist
    if (!isProductExist($p_id_old)){
        goBackParam("?err=err");
        exit;
    }
    $query = mysql_query("SELECT @temp FROM tci_game WHERE id_game = '$p_id_old' AND id_developer = '$p_id_developer'
            UNION SELECT @temp FROM tci_dlc WHERE id_dlc = '$p_id_old' AND id_developer = '$p_id_developer'
            UNION SELECT @temp FROM tci_content WHERE id_content = '$p_id_old' AND id_developer = '$p_id_developer'
            UNION SELECT @temp FROM tci_app WHERE id_app = '$p_id_old' AND id_developer = '$p_id_developer'") or die (goBackParam("?err=err"));
    if (mysql_num_rows($query) == 1){
        //Section check image
        $x_path = "../img/product/".$p_id_developer.'/'.$p_id_old.'/';
        $query = mysql_query("SELECT pic_title,pic_progress,pic_sub FROM $x_q WHERE $x_qi = '$p_id_old'");
        $rs = mysql_fetch_array($query);
        //Check Pic Title
        if (!empty($p_pic_title) && ($rs['pic_title'] != $p_pic_title)){
            if ($rs['pic_title'] != ""){
                $x_img = $x_path.$rs['pic_title'];
                if (is_file($x_img)) unlink($x_img);
            }
        }
        if (!empty($p_pic_progress) && ($rs['pic_progress'] != $p_pic_progress)){
            //Check Pic Progress
            if ($rs['pic_progress'] != ""){
                $x_img = $x_path.$rs['pic_progress'];
                if (is_file($x_img)) unlink($x_img);
            }
        }
        //Rename for Pic Title and Progress
        if (empty($p_pic_title) && $rs['pic_title'] != ""){
            if ($p_id != $p_id_old){
                $x_img = $x_path.$rs['pic_title'];
                $x_type = strtolower(pathinfo($x_img, PATHINFO_EXTENSION));
                $x_newname = $p_id.'title.'.$x_type;
                $x_newpath = $x_path.$x_newname;
                if (is_file($x_img)) rename($x_img, $x_newpath);
                $p_pic_title = $x_newname;
            }
        }
        if (empty($p_pic_progress) && $rs['pic_progress'] != ""){
            if ($p_id != $p_id_old){
                $x_img = $x_path.$rs['pic_progress'];
                $x_type = strtolower(pathinfo($x_img, PATHINFO_EXTENSION));
                $x_newname = $p_id.'progress.'.$x_type;
                $x_newpath = $x_path.$x_newname;
                if (is_file($x_img)) rename($x_img, $x_newpath);
                $p_pic_progress = $x_newname;
            }
        }
        //Check Pic Sub
        $p_pic_sub = "";
        if ($rs['pic_sub'] != ""){
            echo 'pic sub not empty';
            $x_dsub = explode(";", $rs['pic_sub']);
            $x_string = 'sub';
            for ($xx=1; $xx <= 6; $xx++) { 
                $x_strsub = $x_string.strval($xx);
                if (!empty($x_pic_sub[$xx-1])){
                    foreach ($x_dsub as $x_esub) {
                        if ($x_esub != $x_pic_sub[$xx-1] && stripos($x_esub, $x_strsub)){
                            $x_img = $x_path.$x_esub;
                            unlink($x_img);
                            $x_ch = true;
                        }else if ($x_esub == $x_pic_sub[$xx-1]){
                            $x_ch = false;
                        }
                    }
                    if (empty($x_ch)) $x_ch = true;
                    if (!empty($x_ch) && $x_ch == true){
                            if ($xx == 1) $p_pic_sub = $x_pic_sub[$xx-1];
                            else $p_pic_sub = $p_pic_sub.";".$x_pic_sub[$xx-1];
                        }
                }else{
                    foreach ($x_dsub as $x_esub) {
                        echo $x_esub.'<br>';
                        if (stripos($x_esub, $x_strsub)){
                            if ($p_id != $p_id_old){
                                $x_img = $x_path.$x_esub;
                                $x_type = strtolower(pathinfo($x_img, PATHINFO_EXTENSION));
                                $x_newname = $p_id.$x_strsub.'.'.$x_type;
                                $x_newpath = $x_path.$x_newname;
                                rename($x_img, $x_newpath);
                                if ($xx == 1) $p_pic_sub = $x_newname;
                                else $p_pic_sub = $p_pic_sub.";".$x_newname;
                            }else{
                                if ($xx == 1) $p_pic_sub = $x_esub;
                                else $p_pic_sub = $p_pic_sub.";".$x_esub;
                            }
                        }
                    }
                }
            }
        }
        //Check and join section for posted pic sub
        if ($p_pic_sub == ""){
            $xx = 0;
            while ($xx < 6) {
                if (!empty($x_pic_sub[$xx])){
                    if ($xx == 0) $p_pic_sub = $x_pic_sub[$xx];
                    else $p_pic_sub = $p_pic_sub.';'.$x_pic_sub[$xx];
                }
                $xx++;
            }
        }
        echo "check<br>";
        echo $p_pic_sub;
        //Check folder pic to change
        if ($p_id != $p_id_old){
            $x_dir_old = "../img/product/".$p_id_developer."/".$p_id_old."/";
            $x_dir_new = "../img/product/".$p_id_developer."/".$p_id."/";
            if (is_dir($x_dir_old)) rename($x_dir_old, $x_dir_new);
        }
        //Section check product link
        $query = mysql_query("SELECT link FROM $x_q WHERE $x_qi = '$p_id_old'");
        $rs = mysql_fetch_array($query);
        $x_link = $rs['link'];
        if ($x_link != "" && !empty($p_link)){ // If link from DB not empty , do below                        
            if (stripos($x_link, substr($BASENAME['server'], 1))){                
                $x_olink = str_replace($BASENAME['server'], "../", $x_link);
                $x_elink = explode($p_id_old, $x_olink);
                $x_efol = explode("/", $x_elink[1]);
                $x_odir = $x_elink[0].$p_id_old.'/'.$x_efol[1].'/';
                if (is_dir($x_odir)) deleteDir($x_odir);
            }
        }else if ($x_link != "" && empty($p_link)){
            if (stripos($x_link, substr($BASENAME['server'], 1))){
                if ($p_id != $p_id_old){
                    $x_olink = str_replace($BASENAME['server'], "../", $x_link);
                    $x_elink = explode($p_id_old, $x_olink);
                    $x_odir = $x_elink[0].$p_id_old.'/';
                    $x_efol = explode("/", $x_elink[1]);
                    $x_ndir = $x_elink[0].$p_id.'/';
                    $x_ndir2 = $x_elink[0].$p_id.'/'.$x_efol[1].'/';
                    $x_nlink = $x_elink[0].$p_id.$x_elink[1];
                    if (is_dir($x_odir)){
                        if (!is_dir($x_ndir)) mkdir($x_ndir);
                        if (!is_dir($x_ndir2)) mkdir($x_ndir2);
                        rename($x_olink, $x_nlink);
                        deleteDir($x_odir);
                    }
                    $p_link = $BASENAME['product'].$p_id_developer.'/'.$p_id.$x_elink[1];
                }
            }
        }
        //Section query update
        switch ($p_type) {
            case 'game':
                mysql_query("UPDATE $x_q SET code_project = '$p_code_project' , title = '$p_title' , sub_title = '$p_sub_title' , genre = '$p_gc' , tags = '$p_tags' , detail = '$p_detail' , price = '$p_price' WHERE $x_qi = '$p_id_old'") or die (goBackParam("?err=err"));
                break;
            
            case 'dlc':
                mysql_query("UPDATE $x_q SET id_game_parent = '$p_id_game_parent' , code_project = '$p_code_project' , title = '$p_title' , sub_title = '$p_sub_title' , category = '$p_gc' , tags = '$p_tags' , detail = '$p_detail' , price = '$p_price' WHERE $x_qi = '$p_id_old'") or die (goBackParam("?err=err"));
                break;
            
            case 'content':
                mysql_query("UPDATE $x_q SET id_game_parent = '$p_id_game_parent' , title = '$p_title' , sub_title = '$p_sub_title' , category = '$p_gc' , tags = '$p_tags' , detail = '$p_detail' , price = '$p_price' WHERE $x_qi = '$p_id_old'")  or die (goBackParam("?err=err"));
                break;
            
            case 'app':
                mysql_query("UPDATE $x_q SET code_project = '$p_code_project' , title = '$p_title' , sub_title = '$p_sub_title' , category = '$p_gc' , tags = '$p_tags' , detail = '$p_detail' , price = '$p_price' WHERE $x_qi = '$p_id_old'")  or die (goBackParam("?err=err"));
                break;
        }
        mysql_query("UPDATE $x_qp SET date_started = '$p_date_started' , date_published = '$p_date_published' , progress = '$p_progress' , label = '$p_label' , show_public = '$p_show_public' WHERE $x_qpi = '$p_id_old'")  or die (goBackParam("?err=err"));
        //Section query image
        if (!empty($p_pic_title)) mysql_query("UPDATE $x_q SET pic_title = '$p_pic_title' WHERE $x_qi = '$p_id_old'")  or die (goBackParam("?err=err"));
        if (!empty($p_pic_progress)) mysql_query("UPDATE $x_q SET pic_progress = '$p_pic_progress' WHERE $x_qi = '$p_id_old'")  or die (goBackParam("?err=err"));
        if (!empty($p_pic_sub)) mysql_query("UPDATE $x_q SET pic_sub = '$p_pic_sub' WHERE $x_qi = '$p_id_old'")  or die (goBackParam("?err=err"));
        //Section query file
        if (!empty($p_link)) mysql_query("UPDATE $x_q SET link = '$p_link' WHERE $x_qi = '$p_id_old'")  or die (goBackParam("?err=err"));
        //End of query update section
        //Now let's begin the check for change id section . Hufftt i'm really work hard for all the whole this code
        if ($p_id != $p_id_old){
            mysql_query("UPDATE $x_q SET $x_qi = '$p_id' WHERE $x_qi = '$p_id_old'"); //Change id in table product
            mysql_query("UPDATE $x_qp SET $x_qpi = '$p_id' WHERE $x_qpi = '$p_id_old'"); //Change id in table progress
            mysql_query("UPDATE tci_stuff_download SET id_stuff = '$p_id' WHERE id_stuff = '$p_id_old'"); //Change id in table stuff download
            mysql_query("UPDATE user_cart SET id_stuff = '$p_id' WHERE id_stuff = '$p_id_old'"); //Change id in table cart
            mysql_query("UPDATE user_review SET id_stuff = '$p_id' WHERE id_stuff = '$p_id_old'"); //Change id in table review
            //Section replace string in table User User Activity
            $query = mysql_query("SELECT activity FROM user_user_activity WHERE activity LIKE '%".$p_id_old."%'") or die(mysql_error());
            while ($rs = mysql_fetch_array($query)) {
                $x_r = $rs['activity'];
                $x_c = str_replace($p_id_old, $p_id, $x_r);
                mysql_query("UPDATE user_user_activity SET activity = '$x_c' WHERE activity = '$x_r'") or die (mysql_error());
            }
            mysql_query("UPDATE user_user_transaction SET id_stuff = '$p_id' WHERE id_stuff = '$p_id_old'") or die (mysql_error());
            mysql_query("UPDATE user_vote_stuff SET id_stuff = '$p_id' WHERE id_stuff = '$p_id_old'") or die (mysql_error());
        }
        //Section add log dev activity
        include 'lang/ID.php';
        $l_doing_id = $TEXT2['devconsole_log_upd_p'];
        include 'lang/EN.php';
        $l_doing_en = $TEXT2['devconsole_log_upd_p'];
        $l_doing = joinTextLang($l_doing_id,$l_doing_en);
        logDevActivity($l_doing,$p_id,"");
        if ($p_show_public == true){
            $x_dev = getDevData($p_id_developer);
            include 'lang/ID.php';
            $l_recent_id = $x_dev['name'].' '.$TEXT2['devconsole_log_upd_p'].' '.$p_title;
            include 'lang/EN.php';
            $l_recent_en = $x_dev['name'].' '.$TEXT2['devconsole_log_upd_p'].' '.$p_title;
            $l_recent = joinTextLang($l_recent_id,$l_recent_en);
            if ($p_type == "game" || $p_type == "app") $l_link = $BASENAME['store'].'view/?id='.$p_id;
            if ($p_type == "content" || $p_type == "dlc") $l_link = $BASENAME['content'].'view/?id='.$p_id;
            logRecentActivity($l_recent,$l_link);
        }
        logStatistic("dev_activity");
        logStatistic("product_update");
        goBackParam("#success");
    }else goBackParam("?err=err");
}

function funcLoginDev($i,$p){
    include 'variable.php';
    connectDb();
    $query = mysql_query("SELECT id_developer FROM dev_login WHERE id_developer='$i' AND password=SHA1('$p')");
    $num = mysql_num_rows($query);
    if ($num == 1){
        $rs = mysql_fetch_array($query);
        $_SESSION['dev_id'] = $rs['id_developer'];
        goPage("devconsole","");
    }else{
        goPage("devconsole_login","?err=pw");
    }
}

function funcLoginUser($u,$p){
    include 'variable.php';
	connectDb();
	$query = mysql_query("SELECT id_user FROM user_login WHERE username='$u' AND password=SHA1('$p')");
	$num = mysql_num_rows($query);
	$rs = mysql_fetch_array($query);
	$u_id = $rs['id_user'];
	if ($num == 1){
		$query = mysql_query("SELECT id_user,admin_link,id_developer FROM user_user WHERE id_user='$u_id'");
		if (mysql_num_rows($query) != 1) goPage("error","?err=data");		
		$rs = mysql_fetch_array($query);
		$_SESSION['usr_id'] = $rs['id_user'];
        $_SESSION['usr_link'] = $BASENAME['user'].'view/?id='.$rs['id_user'];
		$u_admLink = $rs['admin_link'];
		$u_idDev = $rs['id_developer'];
		if ($rs['admin_link'] != ""){
			$query = mysql_query("SELECT position FROM admin_crew WHERE nip_crew='$u_admLink'");
			if (mysql_num_rows($query) == 1){
				$rs = mysql_fetch_array($query);
				$_SESSION['adm_nip'] = $u_admLink;
				$_SESSION['adm_position'] = $rs['position'];
			}
		}
	goBack();
	}else{
		goPage("login","?false=true");
	}
}

function funcSignupDev($data){
    include 'variable.php';
    connectDb();
    $usr_id = $data['usr_id'];
    $usr_password = $data['usr_password'];
    $query = mysql_query("SELECT * FROM user_login WHERE id_user = $usr_id AND password = SHA1('$usr_password')");
    if (mysql_num_rows($query) == 1){
        $dev_id = $data['dev_id'];
        $dev_name = $data['dev_name'];
        $dev_logo = "../temp_dir/".$data['dev_logo'];
        $query = mysql_query("SELECT * FROM dev_dev WHERE id_developer = '$dev_id' OR name = '$dev_name'");
        if (mysql_num_rows($query) == 0){
            $dev_password = $data['dev_password'];
            $dev_createp = $data['dev_createp'];
            if ($dev_createp == NULL) goBackParam("?err=create");
            $dev_typestc = $data['dev_typestc'];
            $dev_typeplc = $data['dev_typeplc'];
            $dev_email = $data['dev_email'];
            $dev_description = $data['dev_description'];
            $dev_site = $data['dev_site'];
            $dev_bank = $data['dev_bank'];
            $dev_bankname = $data['dev_bankname'];
            $dev_bankno = $data['dev_bankno'];
            $dev_ecreatep = "";
            //section for explode Create Product
            foreach ($dev_createp as $xx) {
                if ($dev_ecreatep == "") $dev_ecreatep = $xx;
                else $dev_ecreatep = $dev_ecreatep.";".$xx;
            }
            //end section
            //section for upload Logo
            $x_allowed_type = array("jpg","jpeg","png","bmp","gif");
            $x_type = strtolower(pathinfo($dev_logo, PATHINFO_EXTENSION));
            if (!in_array($x_type, $x_allowed_type)) goBackParam("?err=imgtype");
            $x_logoname = $dev_id.'.'.$x_type;
            rename($dev_logo, "../img/dev/".$x_logoname);
            //end section
            if (!mysql_query("INSERT INTO dev_dev values('$dev_id','$usr_id','$dev_name','$dev_ecreatep','$dev_typestc','$dev_typeplc','$dev_email','$dev_description','$x_logoname','$dev_site','$dev_bank','$dev_bankname','$dev_bankno','0')")) goPage("error","?err=db");
            if (!mysql_query("INSERT INTO dev_login values('$dev_id',SHA1('$dev_password'))")) goPage("error","?err=db");
            if (!mysql_query("INSERT INTO dev_balance values('$dev_id','0','0','0')")) goPage("error","");
            $x_dir_img = "../img/product/".$dev_id.'/';
            $x_dir_product = "../product/".$dev_id.'/';
            if (!is_dir($x_dir_img)) mkdir($x_dir_img);
            if (!is_dir($x_dir_product)) mkdir($x_dir_product);
            logStatistic("dev_activity");
            logStatistic("dev_signup");
            logStatistic("dev_total");
            goPage("devconsole_wait","");
        }else{
            unlink($dev_logo);
            goBackParam("?err=exist");
        }
    }else{
        unlink($dev_logo);
        goPage("devconsole_signup","?err=usrpw");
    }
}

function funcSignupUser($data){
	include 'variable.php';
	connectDb();
	$s_username = $data['username'];
	$query = mysql_query("SELECT id_user FROM user_login WHERE username='$s_username'");
	if (mysql_num_rows($query) == 0){
		$s_password = $data['password'];
		$s_frontname = $data['front_name'];
		$s_backname = $data['back_name'];
		$s_birth = $data['birth'];
		$s_address = $data['address'];
		$s_zipcode = $data['zipcode'];
		$s_country = $data['country'];
		$s_email = $data['email'];
		$s_phone = $data['phone'];
		$s_question1 = $data['question_1'];
		$s_question2 = $data['question_2'];
		$s_answer1 = $data['answer_1'];
		$s_answer2 = $data['answer_2'];
		$s_signature = $data['signature'];
		$s_description = $data['description'];
		if(!$query = mysql_query("INSERT INTO user_login values(NULL,'$s_username',SHA1('$s_password'))")) goPage("error","?err=data");
		$query = mysql_query("SELECT id_user FROM user_login WHERE username='$s_username' AND password=SHA1('$s_password')");
		$rs = mysql_fetch_array($query);
		$s_id = $rs['id_user'];
        if ($data['img_avail']){
            if (is_file("../temp_dir/".$data['img'])){
                rename("../temp_dir/".$data['img'], "../img/avatar/".$s_id.'_av.'.$data['img_type']);
                $s_avatarlink = $s_id.'_av.'.$data['img_type'];
            }else goPage("error","?err=data");
        }else{
            $s_avatarlink = "default.png";
        }
		$_SESSION['usr_link'] = $BASENAME['user'].'view/?id='.$s_id;
		if(!$query = mysql_query("INSERT INTO user_user values('$s_id','$s_frontname','$s_backname','$s_birth','$s_address','$s_zipcode','$s_country','$s_email','$s_phone','$s_question1','$s_question2','$s_answer1','$s_answer2','$s_signature','$s_description',NOW(),'$s_avatarlink','','')")) goPage("error","?err=data");
		if(!$query = mysql_query("INSERT INTO user_user_balance values('$s_id','0','0','0')")) goPage("error","?err=data");
		$usrActivityText = "Registrasi pertama kali";
		include 'lang/ID.php';
		$recentActivityTextId = $s_frontname.' '.$s_backname.' '.$TEXT['signup_joined'];
		include 'lang/EN.php';
		$recentActivityTextEn = $s_frontname.' '.$s_backname.' '.$TEXT['signup_joined'];
		$txt_logRecentActivity = "ID/".$recentActivityTextId.";EN/".$recentActivityTextEn;
		logUserActivity($usrActivityText);
		logRecentActivity($txt_logRecentActivity,$_SESSION['usr_link']);
		$_SESSION['usr_id'] = $rs['id_user'];
		$_SESSION['signup_success'] = true;
		goPage("signup_success","");
        logStatistic("user_signup");
        logStatistic("user_total");
        logStatistic("user_activity");
	}else{
		goPage("signup","?username=true");
	}
}

function funcSubmitReview($sr){
	connectDb();
	$sr_prid = mysql_real_escape_string($sr['pr_id']);
	$sr_usr = $_SESSION['usr_id'];
	$sr_text = mysql_real_escape_string($sr['text']);
	$sr_datetime = $sr['datetime'];
	if (checkProductBuy($sr_prid)){
		$query = mysql_query("INSERT INTO user_review values(NULL,'$sr_prid','$sr_usr','$sr_text','$sr_datetime')");
		if (!$query) goPage("error","?err=data");
        logStatistic("product_review");
        logStatistic("user_activity");
		goBack();
	}else{
		goBackParam("&err=buy");
	}
}

function funcTopupRequest($topup){
    require 'variable.php';
    require 'credentials/credentials.php';
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    if (empty($_SESSION['usr_id'])){
        goPage("login","");
        exit;
    }
    $id_user = $_SESSION['usr_id'];
    $country_channel = $topup['channel'];
    $topup_amount = $topup['amount'];

    if ($country_channel == 'id' && $topup_amount >= 30000){
        $topup_currency = "IDR";
        $topup_gateway = "Ipaymu";
        $url_api_payment = $SITE['ipaymu_api_payment'];
        $product_name = 'Topup Saldo Akun TCI';
        $product_comments = 'Payment untuk Topup saldo akun TCI';
        $user_data = getUserData($_SESSION['usr_id']);
        /* Change 62 to 0 */
        $user_phone = $user_data['phone'];
        if (substr($user_phone, 0,2) == '62'){
            $user_phone = substr_replace($user_phone, "0", 0, 2);
        }

        $header = array("Content-Type: application/x-www-form-urlencoded");
        $data_post_curl = array(
            'key' => $credentials_ipaymu_key,
            'action' => 'payment',
            'product' => $product_name,
            'price' => $topup_amount,
            'quantity' => 1,
            'expired' => 2,
            'comments' => $product_comments,
            'ureturn' => $BASENAME['my_profile_topup'].'?success=true',
            'unotify' => $BASENAME['dr_response_payment_ipaymu'],
            'ucancel' => $BASENAME['my_profile_topup'].'?cancel=true',
            'buyer_name' => $user_data['front_name'].' '.$user_data['back_name'],
            'buyer_phone' => $user_phone,
            'buyer_email' => $user_data['email'],
            'format' => 'json'
        );
        $result_curl = doCurl($url_api_payment,$header,$data_post_curl,'POST');
        $data_payment = json_decode($result_curl['data'],true);
        $status_curl = json_decode($result_curl['code']);
        $url_payment = $data_payment['url'];
        if ($status_curl == 200 && $data_payment['sessionID']){
            /* Save data to database table user_topup_request */
            $id_session = $data_payment['sessionID'];
            $query = mysql_query("INSERT INTO user_topup_request (id_topup_token,id_user,channel_country,currency,channel_gateway,amount,date_request) values ('".$id_session."','".$id_user."','".$country_channel."','".$topup_currency."','".$topup_gateway."','".$topup_amount."',NOW())");
            if ($query){
                header('location:'.$url_payment);
            }
        } else {
            goPage("error","?err=data");
        }
    } else {
        goPage("error","?err=data");
    }
}

function funcUpdateDev($data){
    include 'variable.php';
    connectDb();
    $usr_id = $data['usr_id'];
    $usr_password = $data['usr_password'];
    $dev_id = $data['dev_id'];
    $dev_password_old = $data['dev_password_old'];
    $query = mysql_query("SELECT * FROM user_login WHERE id_user = $usr_id AND password = SHA1('$usr_password')");
    $query2 = mysql_query("SELECT * FROM dev_login WHERE id_developer = '$dev_id' AND password = SHA1('$dev_password_old')");
    if (mysql_num_rows($query) == 1 && mysql_num_rows($query2) == 1){
        $dev_name = $data['dev_name'];
        if (!empty($data['dev_logo'])) $dev_logo = "../temp_dir/".$data['dev_logo'];
        $query = mysql_query("SELECT name FROM dev_dev WHERE id_developer = '$dev_id'");
        $rs = mysql_fetch_array($query);
        $x_name = $rs['name'];
        $query = mysql_query("SELECT * FROM dev_dev WHERE name = '$dev_name' AND NOT name = '$x_name'");
        if (mysql_num_rows($query) == 0){
            if (!empty($data['dev_password'])) $dev_password = $data['dev_password'];
            $dev_createp = $data['dev_createp'];
            if ($dev_createp == NULL) goBackParam("?err=create");
            $dev_typestc = $data['dev_typestc'];
            $dev_typeplc = $data['dev_typeplc'];
            $dev_email = $data['dev_email'];
            $dev_description = $data['dev_description'];
            $dev_site = $data['dev_site'];
            if (!empty($data['dev_bank'])) $dev_bank = $data['dev_bank'];
            if (!empty($data['dev_bankname'])) $dev_bankname = $data['dev_bankname'];
            if (!empty($data['dev_bankno'])) $dev_bankno = $data['dev_bankno'];
            $dev_ecreatep = "";
            //section for explode Create Product
            foreach ($dev_createp as $xx) {
                if ($dev_ecreatep == "") $dev_ecreatep = $xx;
                else $dev_ecreatep = $dev_ecreatep.";".$xx;
            }
            //end section
            //section for upload Logo
            if (!empty($dev_logo)){
                //Remove old logo
                $query = mysql_query("SELECT pic_logo FROM dev_dev WHERE id_developer = '$dev_id'");
                $rs = mysql_fetch_array($query);
                $x_pic_old = "../img/dev/".$rs['pic_logo'];
                $x_allowed_type = array("jpg","jpeg","png","bmp","gif");
                $x_type = strtolower(pathinfo($dev_logo, PATHINFO_EXTENSION));
                if (!in_array($x_type, $x_allowed_type)) goBackParam("?err=imgtype");
                $x_logoname = $dev_id.'.'.$x_type;
                unlink($x_pic_old);
                rename($dev_logo, "../img/dev/".$x_logoname);
            }
            //end section
            //Query update section
            mysql_query("UPDATE dev_dev SET  name = '$dev_name' , dev_dev.create = '$dev_ecreatep' , type_structure = '$dev_typestc' , type_place = '$dev_typeplc' , email = '$dev_email' , description = '$dev_description' , site = '$dev_site' WHERE id_developer = '$dev_id'") or die (mysql_error());
            if (!empty($x_logoname)) mysql_query("UPDATE dev_dev SET pic_logo = '$x_logoname' WHERE id_developer = '$dev_id'") or die (mysql_error());
            if (!empty($dev_bank)) mysql_query("UPDATE dev_dev SET bank = '$dev_bank' , bank_name = '$dev_bankname' , bank_no = '$dev_bankno'") or die (mysql_error());
            if (!empty($dev_password)) mysql_query("UPDATE dev_login SET password = SHA1('$dev_password') WHERE id_developer = '$dev_id'") or die (mysql_error());
            logStatistic("dev_activity");
            goBackParam("#success");
        }else{
            unlink($dev_logo);
            goBackParam("?err=exist");
        }
    }else{
        unlink($dev_logo);
        goBackParam("?err=usrpw");
    }
}

function funcUpdateUser($m,$data){ // M : Mode , Data : Array for the data
    connectDb();
    $u_usr_id = $data['id_user'];
    if ($m == "avatar"){
        $u_avatar = $data['avatar'];
        $query = mysql_query("SELECT avatar_link FROM user_user WHERE id_user = $u_usr_id");
        $rs = mysql_fetch_array($query);
        if ($rs['avatar_link'] != "" && $rs['avatar_link'] != $u_avatar){
            unlink("../img/avatar/".$rs['avatar_link']);
        }
        mysql_query("UPDATE user_user SET avatar_link = '$u_avatar' WHERE id_user = $u_usr_id");
    }else if ($m == "general"){
        $u_frontname = $data['front_name'];
        $u_backname = $data['back_name'];
        $u_birth = $data['birth'];
        $u_address = $data['address'];
        $u_zipcode = $data['zipcode'];
        $u_country = $data['country'];
        $u_email = $data['email'];
        $u_phone = $data['phone'];
        $u_signature = $data['signature'];
        $u_description = $data['description'];
        mysql_query("UPDATE user_user SET front_name = '$u_frontname' , back_name = '$u_backname' , birth = '$u_birth' , address = '$u_address' , zipcode = '$u_zipcode' , email = '$u_email' , phone = '$u_phone' , signature = '$u_signature' , description = '$u_description' WHERE id_user = '$u_usr_id'");
        if ($u_country != ""){
            mysql_query("UPDATE user_user SET country = '$u_country' WHERE id_user = '$u_usr_id'");
        }
    }else if ($m == "security"){
        $u_username = $data['username'];
        $u_password_old = $data['password_old'];
        $query = mysql_query("SELECT * FROM user_login WHERE id_user = $u_usr_id AND password = SHA1('$u_password_old')");
        if (mysql_num_rows($query) == 1){
            $query = mysql_query("SELECT * FROM user_login WHERE username = '$u_username' AND NOT id_user = '$u_usr_id'");
            if (mysql_num_rows($query) != 1){
                $u_password_new = $data['password_new'];
                $u_question1 = $data['question1'];
                $u_answer1 = $data['answer1'];
                $u_question2 = $data['question2'];
                $u_answer2 = $data['answer2'];
                mysql_query("UPDATE user_login SET username = '$u_username' WHERE id_user = $u_usr_id");
                if ($u_password_new != ""){
                    mysql_query("UPDATE user_login SET password = SHA1('$u_password_new') WHERE id_user = $u_usr_id");
                }
                mysql_query("UPDATE user_user SET question_1 = '$u_question1' , answer_1 = '$u_answer1' , question_2 = '$u_question2' , answer_2 = '$u_answer2' WHERE id_user = $u_usr_id");
            }else{
                goBackParam("?err=username");
                exit;
            }
        }else{
            goBackParam("?err=pw"); 
            exit;
        }
    }
    logStatistic("user_update");
    logStatistic("user_activity");
    goBack();
}

function funcVoteNews($vid,$vv){
    include 'variable.php';
    connectDb();
    $usr_id = $_SESSION['usr_id'];
    $id = mysql_real_escape_string($vid);
    $v = mysql_real_escape_string($vv);
    $ann = false;
    $query = mysql_query("SELECT * FROM user_vote_news WHERE id_news='$id' AND id_user='$usr_id'");
    if (mysql_num_rows($query) == 1){
        $rs = mysql_fetch_array($query);
        if ($rs['vote'] != $v){
            $query = mysql_query("UPDATE user_vote_news SET vote='$v' WHERE id_news='$id' AND id_user='$usr_id'");
            $ann = true;
        }
    }else{
        $query = mysql_query("INSERT INTO user_vote_news values('$id','$usr_id','$v')");
        if (!$query) goPage("error","?err=data");
        $ann = true;
    }
    if ($ann) {
        $x_d = getUserData($_SESSION['usr_id']);
        $x_n = $x_d['front_name'].' '.$x_d['back_name'];
        $n_link = $BASENAME['news'].'view/?id='.$id;
        $usrActivityText = "Melakukan voting pada news : ".$id;
        include 'lang/ID.php';
        $recentActivityTextId = $x_n.' '.$TEXT['news_voted'];
        include 'lang/EN.php';
        $recentActivityTextEn = $x_n.' '.$TEXT['news_voted'];
        $txt_logRecentActivity = "ID/".$recentActivityTextId.";EN/".$recentActivityTextEn;
        logUserActivity($usrActivityText);
        logRecentActivity($txt_logRecentActivity,$n_link);
        logStatistic("news_vote");
        logStatistic("user_activity");
    }
    goBack(); 
}

function funcVoteStuff($vid,$vv){
	connectDb();
	$usr_id = $_SESSION['usr_id'];
	$id = mysql_real_escape_string($vid);
	$v = mysql_real_escape_string($vv);
	$query = mysql_query("SELECT * FROM user_user_transaction WHERE id_user='$usr_id' AND id_stuff='$id'");
	if (mysql_num_rows($query) == 1){
		$query = mysql_query("SELECT * FROM user_vote_stuff WHERE id_stuff='$id' AND id_user='$usr_id'");
		if (mysql_num_rows($query) == 1){
			$rs = mysql_fetch_array($query);
			if ($rs['vote'] != $v){
				$query = mysql_query("UPDATE user_vote_stuff SET vote='$v',datetime=NOW() WHERE id_stuff='$id' AND id_user='$usr_id'");
			}
		}else{
			$query = mysql_query("INSERT INTO user_vote_stuff values('$id','$usr_id','$v',NOW())");
			if (!$query) goPage("error","?err=data");
		}
        $x_d = getUserData($_SESSION['usr_id']);
        $x_n = $x_d['front_name'].' '.$x_d['back_name'];
        $x_p = getDevProductData($id);
        $p_link = $BASENAME['store'].'view/?id='.$id;
        $usrActivityText = "Melakukan voting pada produk : ".$x_p['title'];
        include 'lang/ID.php';
        $recentActivityTextId = $x_n.' '.$TEXT2['label_store_voted'].' : '.$x_p['title'];
        include 'lang/EN.php';
        $recentActivityTextEn = $x_n.' '.$TEXT2['label_store_voted'].' : '.$x_p['title'];
        $txt_logRecentActivity = "ID/".$recentActivityTextId.";EN/".$recentActivityTextEn;
        logUserActivity($usrActivityText);
        logRecentActivity($txt_logRecentActivity,$p_link);
        logStatistic("product_vote");
        logStatistic("user_activity");
		goBack(); 
	}else{
		goBackParam("&err=buy");
	}
}

//End of FORM FUNCTION

function getCurr(){
	if (!empty($_SESSION['curr'])){
		return $_SESSION['curr'];
	}else{
		checkCurr();	
		getCurr();
	}
}

function getLang(){ //check what language is used
	if (!empty($_SESSION['lang'])){
		return $_SESSION['lang'];
	}else{
		checkLanguage();	
		getLang();
	} 
}

function goBack(){
	if (!empty($_SESSION['refferPage'])){
		header("location:".$_SESSION['refferPage']);
	}else{
		goPage("index","");
	}
}

function goBackParam($p){
	if (!empty($_SESSION['refferPage'])){
		header("location:".$_SESSION['refferPage'].$p);
	}else{
		goPage("index","");
	}
}

function goPage($page,$var){ //just like header()
	include 'variable.php';
	header("location:".$BASENAME[$page].$var);
    die();
}

function initAboutPageShow(){
    if (!empty($_GET['s'])){
        switch ($_GET['s']) {
            case 'intro':
                echo "show('about-div-aboutus')";
                break;
            
            case 'project':
                echo "show('about-div-project')";
                break;

            case 'structure':
                echo "show('about-div-structure')";
                break;

            case 'director':
                echo "show('about-div-director')";
                break;

            case 'rule':
                echo "show('about-div-rule')";
                break;

            default:
                echo "show('about-div-aboutus')";
                break;
        }
    }else echo "show('about-div-aboutus')";
}

function initAboutProjectSort(){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    if (empty($_SESSION['ext_aboutProjectSort']) && empty($_SESSION['ext_aboutProjectSearch'])) $_SESSION['ext_aboutProjectSort'] = "datePublishedDesc";
    if ((!empty($_GET['s']) && $_GET['s'] == "project") && !empty($_GET['cs'])){
        switch ($_GET['cs']) {
            case 'dp':
                if ($_SESSION['ext_aboutProjectSort'] == "datePublishedDesc") $_SESSION['ext_aboutProjectSort'] = "datePublishedAsc";
                else $_SESSION['ext_aboutProjectSort'] = "datePublishedDesc";
                $_SESSION['ext_aboutProjectSearch'] = null;
                break;
            
            case 'p':
                if ($_SESSION['ext_aboutProjectSort'] == "popularityDesc") $_SESSION['ext_aboutProjectSort'] = "popularityAsc";
                else $_SESSION['ext_aboutProjectSort'] = "popularityDesc";
                $_SESSION['ext_aboutProjectSearch'] = null;
                break;

            case 'ds':
                if ($_SESSION['ext_aboutProjectSort'] == "dateStartedDesc") $_SESSION['ext_aboutProjectSort'] = "dateStartedAsc";
                else $_SESSION['ext_aboutProjectSort'] = "dateStartedDesc";
                $_SESSION['ext_aboutProjectSearch'] = null;
                break;

            default:
                if (empty($_SESSION['ext_aboutProjectSearch'])){
                    if ($_SESSION['ext_aboutProjectSort'] == "datePublishedDesc") $_SESSION['ext_aboutProjectSort'] = "datePublishedAsc";
                    else $_SESSION['ext_aboutProjectSort'] = "datePublishedDesc";
                }
                break;
        }
        goPage('about_us_project','');
    }
    $result[0] = $TEXT['about_project_sort_dp'];
    $result[1] = $TEXT['about_project_sort_p'];
    $result[2] = $TEXT['about_project_sort_ds'];
    switch ($_SESSION['ext_aboutProjectSort']) {
        case 'datePublishedDesc':
            $result[0] = '&bull; '.$TEXT['about_project_sort_dp'].' &darr;';
            break;

        case 'datePublishedAsc':
            $result[0] = '&bull; '.$TEXT['about_project_sort_dp'].' &uarr;';
            break;

        case 'popularityDesc':
            $result[1] = '&bull; '.$TEXT['about_project_sort_p'].' &darr;';
            break;

        case 'popularityAsc':
            $result[1] = '&bull; '.$TEXT['about_project_sort_p'].' &uarr;';
            break;

        case 'dateStartedDesc':
            $result[2] = '&bull; '.$TEXT['about_project_sort_ds'].' &darr;';
            break;

        case 'dateStartedAsc':
            $result[2] = '&bull; '.$TEXT['about_project_sort_ds'].' &uarr;';
            break;
    }
    return $result;
}

function initDevProductSort(){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    if (empty($_SESSION['ext_devProductSort']) && empty($_GET['q']) && empty($_GET['id'])) $_SESSION['ext_devProductSort'] = "datePublishedDesc";
    else if (!empty($_GET['q']) || !empty($_GET['id'])) $_SESSION['ext_devProductSort'] = "";
    if (!empty($_GET['cs'])){
        switch ($_GET['cs']) {
            case 'dp':
                if ($_SESSION['ext_devProductSort'] == "datePublishedDesc") $_SESSION['ext_devProductSort'] = "datePublishedAsc";
                else $_SESSION['ext_devProductSort'] = "datePublishedDesc";
                break;
            
            case 'p':
                if ($_SESSION['ext_devProductSort'] == "popularityDesc") $_SESSION['ext_devProductSort'] = "popularityAsc";
                else $_SESSION['ext_devProductSort'] = "popularityDesc";
                break;

            case 'ds':
                if ($_SESSION['ext_devProductSort'] == "dateStartedDesc") $_SESSION['ext_devProductSort'] = "dateStartedAsc";
                else $_SESSION['ext_devProductSort'] = "dateStartedDesc";
                break;
        }
    }
    $result[0] = $TEXT['about_project_sort_dp'];
    $result[1] = $TEXT['about_project_sort_p'];
    $result[2] = $TEXT['about_project_sort_ds'];
    switch ($_SESSION['ext_devProductSort']) {
        case 'datePublishedDesc':
            $result[0] = '&bull; '.$TEXT['about_project_sort_dp'].' &darr;';
            break;

        case 'datePublishedAsc':
            $result[0] = '&bull; '.$TEXT['about_project_sort_dp'].' &uarr;';
            break;

        case 'popularityDesc':
            $result[1] = '&bull; '.$TEXT['about_project_sort_p'].' &darr;';
            break;

        case 'popularityAsc':
            $result[1] = '&bull; '.$TEXT['about_project_sort_p'].' &uarr;';
            break;

        case 'dateStartedDesc':
            $result[2] = '&bull; '.$TEXT['about_project_sort_ds'].' &darr;';
            break;

        case 'dateStartedAsc':
            $result[2] = '&bull; '.$TEXT['about_project_sort_ds'].' &uarr;';
            break;
    }
    return $result;
}

function isCrewMatchPosition($stc_id,$stc_pos,$crew_pos){ // Structure id, Structure position, Crew position, all in code
    $result = false;
    $pos_1 = explode(";", $crew_pos);
    for ($x=0; $x < count($pos_1); $x++) { 
        $pos_2 = explode("/", $pos_1[$x]);
        $xx = 0;
        if ($pos_2[0] == $stc_id && $pos_2[2] == $stc_pos) $result = true;
    }
    return $result;
}

function isDevAlreadySignup($usr_id){
    connectDb();
    $result = false;
    $query = mysql_query("SELECT * FROM dev_dev WHERE id_owner = $usr_id");
    $query2 = mysql_query("SELECT * FROM dev_admin WHERE id_user = $usr_id");
    if (mysql_num_rows($query) == 1 || mysql_num_rows($query2) == 1) $result = true;
    return $result;
}

function isDevWaitValid($usr_id){
    connectDb();
    $result = false;
    $query = mysql_query("SELECT * FROM dev_dev WHERE id_owner = $usr_id AND valid = 0");
    $query2 = mysql_query("SELECT * FROM dev_admin WHERE id_user = $usr_id AND confirm = 0");
    if (mysql_num_rows($query) == 1 || mysql_num_rows($query2) == 1) $result = true;
    return $result;
}

function isProductExist($uid){ // Product Id
    connectDb();
    $id = mysql_real_escape_string($uid);
    $query = mysql_query("SELECT @temp FROM tci_game WHERE id_game = '$id'
        UNION SELECT @temp FROM tci_dlc WHERE id_dlc = '$id'
        UNION SELECT @temp FROM tci_content WHERE id_content = '$id'
        UNION SELECT @temp FROM tci_app WHERE id_app = '$id'") or die (mysql_error());
    if (mysql_num_rows($query) > 0) return true;
    else return false;
}

function isTopupAlready($usr_id){
    connectDb();
    $usr_id = mysql_real_escape_string($usr_id);
    $query = mysql_query("SELECT * FROM user_topup_request WHERE id_user = $usr_id AND date_request > NOW() - INTERVAL 2 HOUR");
    if (mysql_num_rows($query) > 0) return true;
    else return false;
}

function isUserExist($usr_id){
    connectDb();
    $usr_id = mysql_real_escape_string($usr_id);
    $query = mysql_query("SELECT * FROM user_user WHERE id_user = $usr_id");
    if (mysql_num_rows($query) == 1) return true;
    else return false;
}

function joinTextLang($id,$en){
    $result = "ID/".$id.';EN/'.$en;
    return $result;
}

function logDevActivity($doing,$object,$info){
    connectDb();
    if (empty($_SESSION['dev_id']) || empty($_SESSION['usr_id'])) goPage("error","?err=data");
    $a = $_SESSION['dev_id'];
    $b = $_SESSION['usr_id'];
    $c = mysql_real_escape_string($doing);
    $d = mysql_real_escape_string($object);
    $e = mysql_real_escape_string($info);
    mysql_query("INSERT INTO dev_activity values('$a','$b','$c','$d','$e',NOW())") or die (goPage("error","?err=db"));
}

function logNewsVisit($id){
    $n_id = mysql_real_escape_string($id);
    $query = mysql_query("UPDATE tci_news_visit SET num = num + 1 WHERE id_news = $n_id");
    logStatistic("news_visit");
    logStatistic("user_activity");
}

function logRecentActivity($text,$link){
	connectDb();
	$query = mysql_query("INSERT INTO tci_recent_activity values(NULL,'$text',NOW(),'$link')");
	if (!$query) goPage("error","?err=data");
}

function logStatistic($var){
    connectDb();
    $x_date = date("Y-m");
    $query = mysql_query("SELECT * FROM tci_statistic WHERE month = '$x_date'");
    if (mysql_num_rows($query) == 0) createStatistic();
    mysql_query("UPDATE tci_statistic SET $var = $var + 1 WHERE month = '$x_date'") or die (mysql_error());
    mysql_query("UPDATE tci_statistic SET web_activity = web_activity + 1 WHERE month = '$x_date'") or die (mysql_error());
}

function logStatisticMin($var){
    connectDb();
    $x_date = date("Y-m");
    $query = mysql_query("SELECT * FROM tci_statistic WHERE month = '$x_date'");
    if (mysql_num_rows($query) == 0) createStatistic();
    mysql_query("UPDATE tci_statistic SET $var = $var - 1 WHERE month = '$x_date'") or die (mysql_error());
    mysql_query("UPDATE tci_statistic SET web_activity = web_activity + 1 WHERE month = '$x_date'") or die (mysql_error());
}

function logUserActivity($activity, $usr_id = null){
	connectDb();
    if (!empty($_SESSION['usr_id'])){
        $usr_id = $_SESSION['usr_id'];
    }
	$query = mysql_query("INSERT INTO user_user_activity values(NULL,'$usr_id','$activity',NOW())");
	if (!$query) goPage("error","?err=data");
}

function paging(){ //paging function , check if the page had admin position specific ,so user without having same position will be rejected to enter the page
	connectDb();
	$thisPage = $_SESSION['thisPage'];
	$query = mysql_query("SELECT * FROM tci_paging WHERE pagename='$thisPage'");
	if (!mysql_num_rows($query) == 0){
		$rs = mysql_fetch_array($query);
		if (!$rs['level_position'] == ""){
			if (getBooleanAdmin()){
				$level_position = explode(";", $rs['level_position']);
				$level_position_num = count($level_position);
				$xx = 0;
				$result = false;
				while ($xx < $level_position_num){
					if ($level_position[$xx] == getMyAdminPosition()) $result = true;
					$xx++;
				}
				if ($result == false) goPage("restricted-admin","");
			}else goPage("restricted","");
		}
	}else goPage("error","?err=paging");
}

function selectLanguage(){
	if (!empty($_GET['lang'])){
		if ($_GET['lang'] == "ID") {
            $_SESSION['lang'] = "ID";
            $_SESSION['curr'] = "IDR";
        }
		else if ($_GET['lang'] == "EN") {
            $_SESSION['lang'] = "EN";
            $_SESSION['curr'] = "USD";
        }
		goBack();
	}
}

function subscribeUser(){
	connectDb();
	$usr_id = $_SESSION['usr_id'];
	$query = mysql_query("INSERT INTO user_subscription values('$usr_id')");
	if (!$query) goPage("error","?err=data");
}

function topupSaveHistory($data_save_transaction){
    $query_insert_history = mysql_query("INSERT INTO user_topup_history (id_payment,id_user,channel_country,currency,channel_gateway,amount,status,via,info,date_requested,date_paid) values ('".$data_save_transaction['id_payment']."','".$data_save_transaction['id_user']."','".$data_save_transaction['channel_country']."','".$data_save_transaction['currency']."','".$data_save_transaction['channel_gateway']."','".$data_save_transaction['amount']."','".$data_save_transaction['status']."','".$data_save_transaction['via']."','".$data_save_transaction['info']."','".$data_save_transaction['date_requested']."','".$data_save_transaction['date_paid']."')");
    $id_topup = mysql_insert_id();
    $query_remove_request = mysql_query("DELETE FROM user_topup_request WHERE id_topup_token = '".$data_save_transaction['id_topup_token']."'");
    return $id_topup;
}

function topupIncreaseBalance($id_topup,$usr_id,$topup_amount,$info = ""){
    include 'variable.php';
    $query = mysql_query("SELECT * FROM user_user_balance WHERE id_user = '".$usr_id."'");
    $data_balance = mysql_fetch_array($query);
    $currenct_balance = $data_balance['balance'];
    $end_balance = $data_balance['balance'] + $topup_amount;
    $query = mysql_query("INSERT INTO user_user_recharge (id_topup,id_user,f_balance,recharge,e_balance,datetime,info) values ('".$id_topup."','".$usr_id."','".$currenct_balance."','".$topup_amount."','".$end_balance."',NOW(),'".$info."')")  or die(mysql_error().'asdasd');
    $query = mysql_query("UPDATE user_user_balance SET balance = balance + ".$topup_amount." , total_in = total_in + ".$topup_amount." WHERE id_user = '".$usr_id."'")  or die(mysql_error());
    accountingIncrease($topup_amount);
    $text_activity = "Melakukan Topup sebesar $".$topup_amount." dengan ID Topup : ".$id_topup;
    logUserActivity($text_activity,$usr_id);
    $user_data = getUserData($usr_id);
    $user_name = $user_data['front_name'].' '.$user_data['back_name'];
    $user_link = $BASENAME['user'].'view/?id='.$usr_id;
    include 'lang/ID.php';
    $recentActivityTextId = $user_name.' '.$TEXT2['topup_just_did'];
    include 'lang/EN.php';
    $recentActivityTextEn = $user_name.' '.$TEXT2['topup_just_did'];
    $txt_logRecentActivity = "ID/".$recentActivityTextId.";EN/".$recentActivityTextEn;
    logRecentActivity($txt_logRecentActivity,$user_link);
    logStatistic("user_topup");
}

function uniteGenreCategory($arr){
	$result = "";
	if (!empty($arr['genre'])) $result = $arr['genre'];
    else if (!empty($arr['category'])) $result = $arr['category'];
    return $result;
}

function uniteId($arr){
    $result = "";
    if (!empty($arr['id_dlc'])) $result = $arr['id_dlc'];
    else if (!empty($arr['id_content'])) $result = $arr['id_content'];
    else if (!empty($arr['id_game'])) $result = $arr['id_game'];
    else if (!empty($arr['id_app'])) $result = $arr['id_app'];
    return $result;
}

// END OF MAIN FUNCTION
// -----------SEPERATOR-----------
// GETTER - FUNCTION (Sorted by a-Z)

function getAboutProjectProduct(){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    if (!empty($_SESSION['ext_aboutProjectSearch'])){
        $x = mysql_real_escape_string($_SESSION['ext_aboutProjectSearch']);
        $q = explode(" ", $x);
        $num = count($q);
        $xx = 1;
        while ($xx <= $num){
            if ($xx == 1){
                $to_add_g = "(tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_a = "(tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_c = "(tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_d = "(tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
            }
            else if ($xx != 1){
                $to_add_g = $to_add_g." AND (tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_a = $to_add_a." AND (tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_c = $to_add_c." AND (tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_d = $to_add_d." AND (tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
            }
            $xx++;
        }
        mysql_query("SET @ptype:='game'");
        $query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label FROM tci_game,tci_game_progress WHERE tci_game.id_game = tci_game_progress.id_game AND tci_game.id_developer = 'tci' AND (".$to_add_g.")
        UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label FROM tci_dlc,tci_dlc_progress WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_developer = 'tci' AND (".$to_add_d.")
        UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label FROM tci_content,tci_content_progress WHERE tci_content.id_content = tci_content_progress.id_content AND tci_content.id_developer = 'tci' AND (".$to_add_c.")
        UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label FROM tci_app,tci_app_progress WHERE tci_app.id_app = tci_app_progress.id_app AND tci_app.id_developer = 'tci' AND (".$to_add_a.") LIMIT 10") or die (mysql_error());
    }else{
        mysql_query("SET @ptype:='game'");
        switch ($_SESSION['ext_aboutProjectSort']) {
            case 'datePublishedDesc':
                $to_add = "ORDER BY date_published DESC";
                break;
            
            case 'datePublishedAsc':
                $to_add = "ORDER BY date_published ASC";
                break;

            case 'dateStartedDesc':
                $to_add = "ORDER BY date_started DESC";
                break;

            case 'dateStartedAsc':
                $to_add = "ORDER BY date_started ASC";
                break;

            case 'popularityDesc':
                $to_add = "ORDER BY num DESC";
                break;

            case 'popularityAsc':
                $to_add = "ORDER BY num ASC";
                break;
        }
        $query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label,tci_game_progress.date_published AS date_published, tci_game_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game.id_game = tci_game_progress.id_game AND tci_game_progress.show_public = 1 AND tci_game.id_developer = 'tci' AND tci_stuff_download.id_stuff = tci_game.id_game
            UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label,tci_dlc_progress.date_published AS date_published, tci_dlc_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc_progress.show_public = 1 AND tci_dlc.id_developer = 'tci' AND tci_stuff_download.id_stuff = tci_dlc.id_dlc
            UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label,tci_content_progress.date_published AS date_published, tci_content_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content.id_content = tci_content_progress.id_content AND tci_content_progress.show_public = 1 AND tci_content.id_developer = 'tci' AND tci_stuff_download.id_stuff = tci_content.id_content
            UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label,tci_app_progress.date_published AS date_published, tci_app_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app.id_app = tci_app_progress.id_app AND tci_app_progress.show_public = 1 AND tci_app.id_developer = 'tci' AND tci_stuff_download.id_stuff = tci_app.id_app  ".$to_add."; ") or die(mysql_error());
    }
    $num = mysql_num_rows($query);
    if (!$num < 1){
        $result = '';
        while ($rs = mysql_fetch_array($query)){
            $sl_type = $rs['@ptype'];
            $sl_pic = $rs['pic_title'];
            $sl_id = $rs['id'];
            $sl_title = $rs['title'];
            $sl_iddeveloper = $rs['id_developer'];
            $sl_progress = $rs['progress'];
            $sl_label = $rs['label'];
            if ($rs['@ptype'] == "game") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs['@ptype'] == "dlc") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs['@ptype'] == "content") {
                $sl_link = $BASENAME['content'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['content'];
            }
            else if ($rs['@ptype'] == "app") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            $sl_linkdev = $BASENAME['developer'].'view/?id='.$sl_iddeveloper;
            $result = $result.  '<div class="progress-slide">
                                    <a href="'.$sl_link.'"><img src="'.$BASENAME['img_product'].$sl_iddeveloper.'/'.$sl_id.'/'.$sl_pic.'"></a>
                                    <a href="'.$sl_linktype.'"><button class="labeling label-type">'.$TEXT['home_progressslide_type'].$sl_type.'</button></a><br>
                                    <a href="'.$sl_link.'"><button class="labeling label-title">'.$sl_title.'</button></a><br>
                                    <a href="'.$sl_linkdev.'"><button class="labeling label-dev">'.$TEXT['home_progressslide_dev'].$sl_iddeveloper.'</button></a><br>
                                    <div class="progress">
                                        <button class="label-progress" style="width:'.$sl_progress.'%'.';min-width:15%;">'.$sl_progress.'%'.'</button>
                                    </div>
                                    <button class="labeling label-label">'.$TEXT['home_progressslide_status'].$sl_label.'</button>
                                </div>
                                ';
        }
        return $result;
    }
    else return "";
}   

function getAboutProjectSearchPlaceholder(){
    if (!empty($_SESSION['ext_aboutProjectSearch'])){
        return $_SESSION['ext_aboutProjectSearch'];
    }else{
        return "";
    }
}

function getAboutStructure(){
    include 'variable.php';
    connectDb();
    $result = "";
    $query = mysql_query("SELECT * FROM tci_structure");
    while ($rs = mysql_fetch_array($query)){
        $stc_id = $rs['id_structure'];
        $stc_title = $rs['title'];
        $stc_img = $BASENAME['img_structure'].$rs['pic_structure'];
        $result = $result.'
        <div id="'.$stc_id.'" class="div-structures">
        <img class="img-w100-h400" src="'.$stc_img.'">';
        if (mysql_num_rows($query) > 0){
            $result = $result.'<br><br>';
            $stc_sort = explode(";", $rs['sort']);
            for ($x=0; $x < count($stc_sort); $x++) { 
                $query2 = mysql_query("SELECT * FROM admin_crew");
                 while ($rs2 = mysql_fetch_array($query2)){
                    $stc_pos = $stc_sort[$x];
                    $crew_pos = $rs2['code_position'];
                    $crew_npos = $rs2['position'];
                    if (isCrewMatchPosition($stc_id,$stc_pos,$crew_pos)){
                        $crew_name = $rs2['name'];
                        $crew_img = $BASENAME['img_crew'].$rs2['pic'];
                        $crew_namepos = getCrewMatchPosition($stc_id,$stc_pos,$crew_pos,$crew_npos);
                        $result = $result.'
                            <div class="box-w30-h250-blue left text-low-small text-white-shadow cursor-hand">
                                <img src="'.$crew_img.'"><br>
                                <center>
                                <label>'.$crew_name.'</label><br><br>
                                <label>'.$crew_namepos[1].'</label><br>
                                <label>'.$crew_namepos[2].'</label>
                                </center>
                            </div>
                        ';
                    }
                }
            }
        }
        $result = $result.'</div>';
    }
    return $result;
}

function getAboutStructureSelect(){
    connectDb();
    $result = "<select class='select-w30-h30-gray' id='about-structure-selection' onchange='structureSelection()'>";
    //Take primary structure first
    $query = mysql_query("SELECT * FROM tci_structure WHERE id_structure='TCI'");
    if (mysql_num_rows($query) == 1){
        $rs = mysql_fetch_array($query);
        $result = $result.'
            <option value="TCI">'.$rs['title'].'</option>
        ';
    }
    $query = mysql_query("SELECT * FROM tci_structure WHERE NOT id_structure='TCI'");
    if (mysql_num_rows($query) != 0){
        while($rs = mysql_fetch_array($query)){
            $stc_id = $rs['id_structure'];
            $stc_title = $rs['title'];
            $result = $result.'
                <option value="'.$stc_id.'">'.$stc_title.'</option>
            ';
        }
    }
    return $result.'</select>';
}

function getAddedATC($id){
	$result = false;
	if (!empty($_SESSION['usr_id'])){
		$pr_id = mysql_real_escape_string($id);
		$usr_id = $_SESSION['usr_id'];
		$query = mysql_query("SELECT * FROM user_cart WHERE id_user='$usr_id' AND id_stuff='$pr_id'");
		if (mysql_num_rows($query) == 1) $result = true;
	}
	return $result;
}

function getAvailATC($id){
	if (!empty($_SESSION['usr_id'])){
		$pr_id = mysql_real_escape_string($id);
		$usr_id = $_SESSION['usr_id'];
		$query = mysql_query("SELECT id_user AS tmp FROM user_cart WHERE id_user='$usr_id' AND id_stuff = '$pr_id'
								UNION SELECT id_user AS tmp FROM user_user_transaction WHERE id_user='$usr_id' AND id_stuff = '$pr_id'");
        if (mysql_num_rows($query) == 1) $result = false;
		else $result = true;
	}else{
		$result = true;
	}
	return $result;
}

function getAvailDl($id){
	if (!empty($_SESSION['usr_id'])){
		$pr_id = mysql_real_escape_string($id);
		$usr_id = $_SESSION['usr_id'];
		$query = mysql_query("SELECT * FROM user_user_transaction WHERE user_user_transaction.id_user='$usr_id' AND user_user_transaction.id_stuff='$pr_id'");
        //check for matching Id and Developer
		if (mysql_num_rows($query) == 1) $result = true;
		else $result = false;
	}else{
		$result = false;
	}
	return $result;
}

function getBalanceById($usr_id){
    if (!empty($usr_id)){
        if (!$query = mysql_query("SELECT balance FROM user_user_balance WHERE id_user = $usr_id")) goPage("error","?err=data");
        $rs = mysql_fetch_array($query);
        $result = formatMoney($rs['balance']);
        return $result;
    }
}

function getBooleanAdmin(){ //check if user is admin
	if (!empty($_SESSION['adm_nip'])) return true; //return true if yes
	else return false; //return false if wrong
}

function getBooleanHeaderNews(){ //check if header news is exist
	connectDb();
	$query = mysql_query("SELECT * FROM tci_config");
	$rs = mysql_fetch_array($query);
	if ($rs['header_news'] == "") return false;
	else return true;
}

function getCart(){
	connectDb();
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	include 'variable.php';
	$result = "";
	if (!empty($_SESSION['usr_id'])){
		if (empty($_GET['q'])){
			$usr_id = $_SESSION['usr_id'];
			$sql = "SELECT @ptype:='game' AS type, tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.price AS price FROM tci_game,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff = tci_game.id_game
                UNION SELECT @ptype:='app' AS type, tci_app.id_app AS id, tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.price AS price FROM tci_app,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff =  tci_app.id_app
                UNION SELECT @ptype:='dlc' AS type, tci_dlc.id_dlc AS id, tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.price AS price FROM tci_dlc,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff=tci_dlc.id_dlc
                UNION SELECT @ptype:='content' AS type, tci_content.id_content AS id, tci_content.title AS title,tci_content.sub_title AS subtitle,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.price AS price FROM tci_content,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff=tci_content.id_content";
		}else{
            $usr_id = $_SESSION['usr_id'];
            $q = mysql_real_escape_string($_GET['q']);
            $sql = "SELECT @ptype:='game' AS type, tci_game.id_game AS id, tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.price AS price FROM tci_game,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff = tci_game.id_game AND (tci_game.id_game LIKE '%$q%' OR tci_game.title LIKE '%$q%')
                UNION SELECT @ptype:='app' AS type, tci_app.id_app AS id, tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.price AS price FROM tci_app,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff =  tci_app.id_app AND (tci_app.id_app LIKE '%$q%' OR tci_app.title LIKE '%$q%')
                UNION SELECT @ptype:='dlc' AS type, tci_dlc.id_dlc AS id, tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.price AS price FROM tci_dlc,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff=tci_dlc.id_dlc AND (tci_dlc.id_dlc LIKE '%$q%' OR tci_dlc.title LIKE '%$q%')
                UNION SELECT @ptype:='content' AS type, tci_content.id_content AS id, tci_content.title AS title,tci_content.sub_title AS subtitle,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.price AS price FROM tci_content,user_cart WHERE user_cart.id_user=$usr_id AND user_cart.id_stuff=tci_content.id_content AND (tci_content.id_content LIKE '%$q%' OR tci_content.title LIKE '%$q%')";
        }
        $query = mysql_query($sql);
        if (mysql_num_rows($query) >= 1){
            while ($rs = mysql_fetch_array($query)){
                $pr_id = $rs['id'];
                $pr_title = $rs['title'];
                $pr_subtitle = getStringFromArray($rs['sub_title']);
                $pr_iddeveloper = $rs['id_developer'];
                $pr_price = $rs['price'];
                $pr_pic = $BASENAME['img_product'].$pr_iddeveloper.'/'.$pr_id.'/'.$rs['pic_title'];
                if ($rs['type'] == "game" || $rs['type'] == "app"){
                    $pr_link = $BASENAME['store'].'view/?id='.$pr_id;
                }else $pr_link = $BASENAME['store'].'view/?id='.$pr_id;
                $result =   $result.'
                            <div class="product-box">
                            <div class="pic-title">
                                <a href="'.$pr_link.'"><img src="'.$pr_pic.'">
                            </div>
                                <div class="inner">
                                <h5 class="text-big-black">'.$pr_title.'</h5> 
                                <label="text-gray title">'.$TEXT['developed_by'].' '.$pr_iddeveloper.'</label><br>
                                <label="text-gray sub-title">'.$pr_subtitle.'</label>
                                <script type="text/javascript">
                                var prid = '.$pr_id.';
                                </script>
                                <div style="margin-top:10px;"><div class="left"><label class="text-green">'.getSym().cvtMoney($pr_price).'</label></div></a><div class="right" style="width:60%"><button class="btn-red" onclick="deleteCart(`'.$pr_id.'`)">'.$TEXT['cart_delete_single'].'</button>        <button class="btn-green" onclick="payCart(`'.$pr_id.'`)" style="margin-left: 5%;">'.$TEXT['cart_pay_single'].' '.getSym().cvtMoney($pr_price).'</button></div></div>
                                </div>
                            </div>
                            ';
                
            }
        }
	}
    return $result;
}

function getCartNum(){
    if (!empty($_SESSION['usr_id'])){
        $usr_id = $_SESSION['usr_id'];
        if (!$query = mysql_query("SELECT count(id_stuff) AS num FROM user_cart WHERE id_user = $usr_id")) goPage("error","?err=data");
        $rs = mysql_fetch_array($query);
        $result = $rs['num'];
        return $result;
    }
}

function getCartPrice($p){ // P parameter is Product. "all" for all product in cart, specific id for specific product
    connectDb();
    if (!empty($_SESSION['usr_id'])){
        $usr_id = $_SESSION['usr_id'];
        if ($p == "all"){
            $query = mysql_query("SELECT SUM(tci_game.price) AS price FROM tci_game,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_game.id_game
                                UNION SELECT SUM(tci_app.price) AS price FROM tci_app,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_app.id_app
                                UNION SELECT SUM(tci_dlc.price) AS price FROM tci_dlc,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_dlc.id_dlc
                                UNION SELECT SUM(tci_content.price) AS price FROM tci_content,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff = tci_content.id_content");
        }else{
            $query = mysql_query("SELECT SUM(tci_game.price) AS price FROM tci_game,user_cart WHERE user_cart.id_user = $usr_id AND user_cart.id_stuff =  '$p' AND user_cart.id_stuff = tci_game.id_game
                                UNION SELECT SUM(tci_app.price) AS price FROM tci_app,user_cart WHERE user_cart.id_user = $usr_id AND  user_cart.id_stuff =  '$p' AND user_cart.id_stuff = tci_app.id_app
                                UNION SELECT SUM(tci_dlc.price) AS price FROM tci_dlc,user_cart WHERE user_cart.id_user = $usr_id AND  user_cart.id_stuff =  '$p' AND user_cart.id_stuff = tci_dlc.id_dlc
                                UNION SELECT SUM(tci_content.price) AS price FROM tci_content,user_cart WHERE user_cart.id_user = $usr_id AND  user_cart.id_stuff =  '$p' AND user_cart.id_stuff = tci_content.id_content");
        }
        if (!$query) goPage("error","?err=data");
        $price = 0;
        while ($rs = mysql_fetch_array($query)) {
            $price = $price + $rs['price'];
        }
        $price = formatMoney($price);
        return $price;
    }else goPage("error","?err=data");
}

function getContentDlcGameList(){
    include 'variable.php';
    connectDb();
    if (!$query = mysql_query("SELECT id_game_parent FROM tci_dlc")) goPage("error","?err=data");
    $result = "";
    while ($rs = mysql_fetch_array($query)){
        $result = $result.  '<a href="'.$BASENAME['content'].'?s=d&g='.$rs['id_game'].'" class="text-white-shadow">'.$TEXT['content_trending'].'</a><br>';
    }
    return $result;
}

function getContentProductArray(){
    include 'variable.php';
    $result['avail'] = false;
    if (!empty($_GET['id'])){
        $pr_id = mysql_real_escape_string($_GET['id']);
        $query = mysql_query("SELECT @type:='content' AS type FROM tci_content WHERE tci_content.id_content ='$pr_id'
            UNION SELECT @type:='dlc' AS type FROM tci_dlc WHERE tci_dlc.id_dlc='$pr_id'");
        if (mysql_num_rows($query) == 1){
            $rs = mysql_fetch_array($query);
            $pr_type = $rs['type'];
            if ($pr_type == "content"){
                $query = mysql_query("SELECT * FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content.id_content='".$pr_id."' AND tci_content_progress.id_content='".$pr_id."' AND tci_stuff_download.id_stuff='".$pr_id."'");
            }else if ($pr_type == "dlc"){
                $query = mysql_query("SELECT * FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc.id_dlc='".$pr_id."' AND tci_dlc_progress.id_dlc='".$pr_id."' AND tci_stuff_download.id_stuff='".$pr_id."'");
            }
            $rs = mysql_fetch_array($query);
            $rs['id'] = uniteId($rs);
            $rs['gc'] = uniteGenreCategory($rs);
            $rs['linkPic'] = $BASENAME['img_product'].$rs['id_developer'].'/'.$rs['id'].'/'.$rs['pic_title'];
            $rs['avail'] = true;
            $rs['avail_ATC'] = getAvailATC($rs['id']);
            $rs['ATC_added'] = getAddedATC($rs['id']);
            $rs['avail_dl'] = getAvailDl($rs['id']);
            $rs['vote_yes'] = calculateRating($rs['id']);
            $rs['vote_total'] = getTotalVote($rs['id']);
            $rs['title_html'] = $rs['title'].' - TCI';
            $result = $rs;
        }else $result['title_html'] = 'No product found - TCI';
    }else $result['title_html'] = 'No product found - TCI';
    return $result;
}

function getContentProductView(){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    $result = "";
    $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff
                UNION SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY date_published DESC";
    if (empty($_GET['s']) && empty($_GET['q'])){
        $sql = $sql;
    }else if (!empty($_GET['s']) && empty($_GET['q'])){
        if ($_GET['s'] == "c"){
            if (empty($_GET['c']) && empty($_GET['g'])){
                $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff ORDER BY date_published DESC";
            }else if(!empty($_GET['c']) && empty($_GET['g'])){
                if ($_GET['c'] == "trending"){
                    $str_to_add = "";
                    $rs = mysql_fetch_array(mysql_query("SELECT trending_id FROM tci_config"));
                    $idf = explode(";", $rs['trending_id']);
                    $num_idf = count($idf);
                    $xx = 1;
                    if ($num_idf > 0){
                        while ($xx <= $num_idf){
                            if ($xx == 1) $str_to_add = "tci_content.id_content = '".$idf[$xx-1]."' ";
                            else $str_to_add = $str_to_add."OR tci_content.id_content = '".$idf[$xx-1]."'";
                            $xx++;
                        }
                        $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff AND (".$str_to_add.") ORDER BY date_published";
                    }else{
                        $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff ORDER BY date_published DESC";
                    }
                }else if ($_GET['c'] == "bs"){
                    $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff ORDER BY date_published DESC";
                }else if ($_GET['c'] == "progress"){
                    $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND NOT tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff ORDER BY tci_content_progress.date_started DESC";
                }
            }else if(!empty($_GET['g']) && empty($_GET['c'])){
                $game = mysql_real_escape_string($_GET['g']);
                $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff AND tci_content.id_game_parent LIKE '%$game%' ORDER BY date_published DESC";
            }
        }else if ($_GET['s'] == "d"){
            if (empty($_GET['c']) && empty($_GET['g'])){
                $sql = "SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY date_published DESC";
            }else if(!empty($_GET['c']) && empty($_GET['g'])){
                if ($_GET['c'] == "trending"){
                    $str_to_add = "";
                    $rs = mysql_fetch_array(mysql_query("SELECT trending_id FROM tci_config"));
                    $idf = explode(";", $rs['trending_id']);
                    $num_idf = count($idf);
                    $xx = 1;
                    if ($num_idf > 0){
                        while ($xx <= $num_idf){
                            if ($xx == 1) $str_to_add = "tci_dlc.id_dlc = '".$idf[$xx-1]."' ";
                            else $str_to_add = $str_to_add."OR tci_dlc.id_dlc = '".$idf[$xx-1]."'";
                            $xx++;
                        }
                        $sql = "SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff AND (".$str_to_add.") ORDER BY date_published DESC";
                    }else{
                        $sql = "SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY date_published DESC";
                    }
                }else if ($_GET['c'] == "bs"){
                    $sql = "SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY download DESC";
                }else if ($_GET['c'] == "progress"){
                    $sql = "SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND NOT tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY tci_dlc_progress.date_started DESC";
                }
            }else if(!empty($_GET['g']) && empty($_GET['c'])){
                $game = mysql_real_escape_string($_GET['g']);
                $sql = "SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff AND tci_dlc.id_game_parent LIKE '%$game%' ORDER BY date_published DESC";
            }
        }else if ($_GET['s'] == "trending"){
            $str_to_add = "";
            $rs = mysql_fetch_array(mysql_query("SELECT trending_id FROM tci_config"));
            $idf = explode(";", $rs['trending_id']);
            $num_idf = count($idf);
            $xx = 1;
            if ($num_idf > 0){
                while ($xx <= $num_idf){
                    if ($xx == 1) {
                        $str_to_add_c = "tci_content.id_content = '".$idf[$xx-1]."' ";
                        $str_to_add_d = "tci_dlc.id_dlc = '".$idf[$xx-1]."' ";
                    }
                    else {
                        $str_to_add_c = $str_to_add_c."OR tci_content.id_content = '".$idf[$xx-1]."'";
                        $str_to_add_d = $str_to_add_d."OR tci_dlc.id_dlc = '".$idf[$xx-1]."' ";
                    }
                    $xx++;
                }
                $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff AND (".$str_to_add_c.")
                UNION SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff AND (".$str_to_add_d.") ORDER BY date_published DESC";
            }
        }else if ($_GET['s'] == "bs"){
            $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff
                UNION SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY download DESC";
        }else if ($_GET['s'] == "newest"){
            $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff
                UNION SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY date_published DESC";
        }else if ($_GET['s'] == "progress"){
            $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download, tci_content_progress.date_started AS date_started FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content_progress.show_public = 1 AND NOT tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff
             UNION SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download, tci_dlc_progress.date_started AS date_started FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc_progress.show_public = 1 AND NOT tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff ORDER BY date_started DESC";
        }
    }else if (empty($_GET['s']) && !empty($_GET['q'])){
        $x = mysql_real_escape_string($_GET['q']);
        $q = explode(" ", $x);
        $num = count($q);
        $xx = 1;
        while ($xx <= $num){
            if ($xx == 1){
                $to_add_c = "(tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_d = "(tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
            }
            else if ($xx != 1){
                $to_add_c = $to_add_c." AND (tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_d = $to_add_d." AND (tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
            }
            $xx++;
        }
        $sql = "SELECT @type:='content' AS type,tci_content.id_content AS id,tci_content.id_game_parent AS id_game,tci_content.title AS title,tci_content.sub_title AS sub_title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content.pic_progress AS pic_progress,tci_content.price AS price,tci_content_progress.progress AS progress,tci_content_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_content,tci_content_progress,tci_stuff_download WHERE (tci_content_progress.show_public = 1 AND tci_content.id_content = tci_content_progress.id_content AND tci_content.id_content = tci_stuff_download.id_stuff) AND (".$to_add_c.")
        UNION SELECT @type:='dlc' AS type,tci_dlc.id_dlc AS id,tci_dlc.id_game_parent AS id_game,tci_dlc.title AS title,tci_dlc.sub_title AS sub_title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc.pic_progress AS pic_progress,tci_dlc.price AS price,tci_dlc_progress.progress AS progress,tci_dlc_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE (tci_dlc_progress.show_public = 1 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_dlc = tci_stuff_download.id_stuff) AND (".$to_add_d.") LIMIT 10";
    }
    $query = mysql_query($sql);
    while ($rs = mysql_fetch_array($query)){
        $pr_pic = $rs['pic_title'];
        $pr_id = $rs['id'];
        $pr_idgame = $rs['id_game'];
        $pr_title = $rs['title'];
        $pr_subtitle = getStringFromArray($rs['sub_title']);
        $pr_iddeveloper = $rs['id_developer'];
        $pr_progress = $rs['progress'];
        $pr_price = $rs['price'];
        $pr_datepublished = $rs['date_published'];
        $pr_download = $rs['download'];
        $pr_link = $BASENAME['content'].'view/?id='.$pr_id;
        $result =   $result.'
                            <div class="product-box">
                            <div class="pic-title">
                                <a href="'.$pr_link.'"><img src="'.$BASENAME['img_product'].$pr_iddeveloper.'/'.$pr_id.'/'.$pr_pic.'">
                            </div>
                                <div class="inner">
                                <h5 class="text-big-black">'.$pr_idgame.' : '.$pr_title.'</h5> 
                                <label="text-gray title">'.$TEXT['developed_by'].' '.$pr_iddeveloper.'</label><br>
                                <label="text-gray sub-title">'.$pr_subtitle.'</label>
                                <h4 class="text-green">'.getSym().cvtMoney($pr_price).'</h4>
                                </div>
                                </a>
                            </div>
                            ';
    }
    return $result;
}

function getContentSearchPlaceholder(){
    if (!empty($_GET['q'])){
        return $_GET['q'];
    }else{
        return "";
    }
}

function getCrewMatchPosition($stc_id,$stc_pos,$crew_pos,$crew_npos){ // Structure id, Structure position, Crew position, all in code
    $result = ["","",""];
    $pos_1 = explode(";", $crew_pos);
    $npos_1 = explode(";", $crew_npos);
    for ($x=0; $x < count($pos_1); $x++) { 
        $pos_2 = explode("/", $pos_1[$x]);
        $npos_2 = explode("/", $npos_1[$x]);
        $xx = 0;
        while ($xx < count($pos_2)) {
            if ($pos_2[0] == $stc_id && $pos_2[2] == $stc_pos) $result = [$npos_2[0],$npos_2[1],$npos_2[2]];
            $xx++;
        }
    }
    return $result;
}

function getDevAvailableSelectCreate($dev_id,$product_type){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    connectDb();
    $result = "";
    if (!empty($dev_id)){
        $query = mysql_query("SELECT dev_dev.create FROM dev_dev WHERE id_developer = '$dev_id'");
        if (mysql_num_rows($query) == 1){
            $rs = mysql_fetch_array($query);
            $x_create = explode(";", $rs['create']);
            foreach ($x_create as $x_select) {
                if ($x_select == "gamedlc"){
                    $x_game_s = "";
                    $x_dlc_s = "";
                    if ("game" == $product_type) $x_game_s = "selected";
                    else if ("dlc" == $product_type) $x_dlc_s = "selected";
                    $result = $result.'
                        <option value="game" '.$x_game_s.'>'.$TEXT['game'].'</option>
                        <option value="dlc" '.$x_dlc_s.'>'.$TEXT['dlc'].'</option>
                    ';
                }else{
                    $x_type_s = "";
                    if ($x_select == $product_type) $x_type_s = "selected";
                    $result = $result.'
                    <option value="'.$x_select.'" '.$x_type_s.'>'.$TEXT[$x_select].'</option>
                ';
                }
            }
        }
    }
    return $result;
}

function getDevData($dev_id){
    connectDb();
    include 'variable.php';
    $result = null;
    if (!empty($dev_id)){
        $query = mysql_query("SELECT * FROM dev_dev WHERE id_developer = '$dev_id'");
        if (mysql_num_rows($query) == 1){
            $result = mysql_fetch_array($query);
            $result['pic_link'] = $BASENAME['img_dev'].$result['pic_logo'];
            $query2 = mysql_query("SELECT * FROM dev_admin WHERE id_developer = '$dev_id'");
            $result['admin_count'] = mysql_num_rows($query2);
        }
    }
    return $result;
}

function getDevDataByUsrId($usr_id){
    $result = null;
    $query = mysql_query("SELECT * FROM dev_dev WHERE id_owner = $usr_id");
    $query2 = mysql_query("SELECT * FROM dev_admin WHERE id_user = $usr_id");
    if (mysql_num_rows($query) == 1){
        $rs = mysql_fetch_array($query);
        $result = getDevData($rs['id_developer']);
    }
    if (mysql_num_rows($query2) == 1){
        $rs = mysql_fetch_array($query2);
        $result = getDevData($rs['id_developer']);
    }
    if (mysql_num_rows($query) != 1 && mysql_num_rows($query2) != 1){
        $result = false;
    }
    return $result;
}

function getDevErrorTxt(){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    $result = "";
    if (!empty($_GET['err'])){
        if ($_GET['err'] == "usrpw") $result = $TEXT['devconsole_err_usrpw'];
        else if ($_GET['err'] == "devpw") $result = $TEXT['devconsole_err_usrpw'];
        else if ($_GET['err'] == "exist") $result = $TEXT['devconsole_err_exist'];
        else if ($_GET['err'] == "create") $result = $TEXT['devconsole_err_create'];
        else if ($_GET['err'] == "imgnot") $result = $TEXT['devconsole_err_imgnot'];
        else if ($_GET['err'] == "imgsize") $result = $TEXT['devconsole_err_imgsize'];
        else if ($_GET['err'] == "imgtype") $result = $TEXT['devconsole_err_imgtype'];
    }
    return $result;
}

function getDevPosition($usr_id){
    connectDb();
    $usr_id = mysql_real_escape_string($usr_id);
    $query = mysql_query("SELECT @temp FROM dev_dev WHERE id_owner = $usr_id");
    $query2 = mysql_query("SELECT @temp FROM dev_admin WHERE id_user = $usr_id");
    if (mysql_num_rows($query) == 1) return "owner";
    else if (mysql_num_rows($query2) == 1) return "admin";
}

function getDevProduct($dev_id){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    if (!empty($_GET['q']) || !empty($_GET['id'])){
        if (!empty($_GET['q'])){
            $x = mysql_real_escape_string($_GET['q']);
            $q = explode(" ", $x);
            $num = count($q);
            $xx = 1;
            while ($xx <= $num){
                if ($xx == 1){
                    $to_add_g = "(tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
                    $to_add_a = "(tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
                    $to_add_c = "(tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                    $to_add_d = "(tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
                }
                else if ($xx != 1){
                    $to_add_g = $to_add_g." AND (tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
                    $to_add_a = $to_add_a." AND (tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
                    $to_add_c = $to_add_c." AND (tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                    $to_add_d = $to_add_d." AND (tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
                }
                $xx++;
            }
        }else if (!empty($_GET['id'])){
            $x = mysql_real_escape_string($_GET['id']);
            $to_add_g = "tci_game.id_game = '$x'";
            $to_add_a = "tci_app.id_app = '$x'";
            $to_add_c = "tci_content.id_content = '$x'";
            $to_add_d = "tci_dlc.id_dlc = '$x'";
        }
        mysql_query("SET @ptype:='game'");
        $query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label FROM tci_game,tci_game_progress WHERE tci_game.id_game = tci_game_progress.id_game AND tci_game.id_developer = '$dev_id' AND (".$to_add_g.")
        UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label FROM tci_dlc,tci_dlc_progress WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_developer = '$dev_id' AND (".$to_add_d.")
        UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label FROM tci_content,tci_content_progress WHERE tci_content.id_content = tci_content_progress.id_content AND tci_content.id_developer = '$dev_id' AND (".$to_add_c.")
        UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label FROM tci_app,tci_app_progress WHERE tci_app.id_app = tci_app_progress.id_app AND tci_app.id_developer = '$dev_id' AND (".$to_add_a.") LIMIT 10") or die (mysql_error());
    }else{
        mysql_query("SET @ptype:='game'");
        switch ($_SESSION['ext_devProductSort']) {
            case 'datePublishedDesc':
                $to_add = "ORDER BY date_published DESC";
                break;
            
            case 'datePublishedAsc':
                $to_add = "ORDER BY date_published ASC";
                break;

            case 'dateStartedDesc':
                $to_add = "ORDER BY date_started DESC";
                break;

            case 'dateStartedAsc':
                $to_add = "ORDER BY date_started ASC";
                break;

            case 'popularityDesc':
                $to_add = "ORDER BY num DESC";
                break;

            case 'popularityAsc':
                $to_add = "ORDER BY num ASC";
                break;
        }
        $query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label,tci_game_progress.date_published AS date_published, tci_game_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game.id_game = tci_game_progress.id_game AND tci_game.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_game.id_game
            UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label,tci_dlc_progress.date_published AS date_published, tci_dlc_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_dlc.id_dlc
            UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label,tci_content_progress.date_published AS date_published, tci_content_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content.id_content = tci_content_progress.id_content AND tci_content.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_content.id_content
            UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label,tci_app_progress.date_published AS date_published, tci_app_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app.id_app = tci_app_progress.id_app AND tci_app.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_app.id_app  ".$to_add."; ") or die(mysql_error());
    }
    $num = mysql_num_rows($query);
    if (!$num < 1){
        $result = '';
        while ($rs = mysql_fetch_array($query)){
            $sl_type = $rs['@ptype'];
            $sl_pic = $rs['pic_title'];
            $sl_id = $rs['id'];
            $sl_title = $rs['title'];
            $sl_iddeveloper = $rs['id_developer'];
            $sl_progress = $rs['progress'];
            $sl_label = $rs['label'];
            if ($rs['@ptype'] == "game") {
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs['@ptype'] == "dlc") {
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs['@ptype'] == "content") {
                $sl_linktype = $BASENAME['content'];
            }
            else if ($rs['@ptype'] == "app") {
                $sl_linktype = $BASENAME['store'];
            }
            $sl_link = $BASENAME['devconsole_product'].'?id='.$sl_id;
            $sl_linkdev = $BASENAME['devconsole'];
            $result = $result.  '<div class="progress-slide">
                                    <a href="'.$sl_link.'"><img src="'.$BASENAME['img_product'].$sl_iddeveloper.'/'.$sl_id.'/'.$sl_pic.'"></a>
                                    <a href="'.$sl_linktype.'"><button class="labeling label-type">'.$TEXT['home_progressslide_type'].$sl_type.'</button></a><br>
                                    <a href="'.$sl_link.'"><button class="labeling label-title">'.$sl_title.'</button></a><br>
                                    <a href="'.$sl_linkdev.'"><button class="labeling label-dev">'.$TEXT['home_progressslide_dev'].$sl_iddeveloper.'</button></a><br>
                                    <div class="progress">
                                        <button class="label-progress" style="width:'.$sl_progress.'%'.';min-width:15%;">'.$sl_progress.'%'.'</button>
                                    </div>
                                    <button class="labeling label-label">'.$TEXT['home_progressslide_status'].$sl_label.'</button>
                                </div>
                                ';
        }
        return $result;
    }
    else return "";
}   

function getDevProductData($uid){
    include 'variable.php';
    connectDb();
    $result = array();
    $id = mysql_real_escape_string($uid);
    $query = mysql_query("SELECT @type:='game' AS type FROM tci_game WHERE id_game = '$id'
        UNION SELECT @type:='dlc' AS type FROM tci_dlc WHERE id_dlc = '$id'
        UNION SELECT @type:='content' AS type FROM tci_content WHERE id_content = '$id'
        UNION SELECT @type:='app' AS type FROM tci_app WHERE id_app = '$id'");
    if (mysql_num_rows($query) == 1){
        $rs = mysql_fetch_array($query);
        $sttp = "";
        $stpg = "";
        $stid = "";
        $x_type = $rs['type'];
        switch ($rs['type']) {
            case 'game':
                $sttp = "tci_game";
                $stpg = "tci_game_progress";
                $stid = "tci_game.id_game";
                $stpi = "tci_game_progress.id_game";
                break;
            case 'dlc':
                $sttp = "tci_dlc";
                $stpg = "tci_dlc_progress";
                $stid = "tci_dlc.id_dlc";
                $stpi = "tci_dlc_progress.id_dlc";
                break;

            case 'content':
                $sttp = "tci_content";
                $stpg = "tci_content_progress";
                $stid = "tci_content.id_content";
                $stpi = "tci_content_progress.id_content";
                break;

            case 'app':
                $sttp = "tci_app";
                $stpg = "tci_app_progress";
                $stid = "tci_app.id_app";
                $stpi = "tci_app_progress.id_app";
                break;
        }
        $query = mysql_query("SELECT $sttp.*, $stpg.*, count(user_user_transaction.id_stuff) AS total_buyer, tci_stuff_download.num AS total_download, count(user_review.id_stuff) AS total_review, count(dev_activity.object) AS total_activity
            FROM $sttp
            LEFT JOIN $stpg ON $stpi = $stid
            LEFT JOIN user_user_transaction ON user_user_transaction.id_stuff = $stid
            LEFT JOIN tci_stuff_download ON tci_stuff_download.id_stuff = $stid
            LEFT JOIN user_review ON user_review.id_stuff = $stid
            LEFT JOIN dev_activity ON dev_activity.object = $stid AND dev_activity.doing = 'Published'
            WHERE $stid = '$id'
            ") or die (mysql_error());
        if (mysql_num_rows($query) == 1){
            $result = mysql_fetch_array($query);
            $result['type'] = $x_type;
            $result['id'] = uniteId($result);
            //section link store
            if ($result['type'] == "game" || $result['type'] == "app") $x_store = $BASENAME['store'];
            else $x_store = $BASENAME['content'];
            $result['link_store'] = $x_store.'view/?id='.$result['id'];
            //section explode code
            if ($result['type'] == "dlc" || $result['type'] == "content"){
                $x_e_id = explode($result['id_game_parent'], $result['id']);
                $result['code_project'] = $x_e_id[1];
            }
            $result['gc'] = uniteGenreCategory($result);
            $result['sub_title_id'] = getStringLang('ID',$result['sub_title']);
            $result['sub_title_en'] = getStringLang('EN',$result['sub_title']);
            $result['detail_id'] = getStringLang('ID',$result['detail']);
            $result['detail_en'] = getStringLang('EN',$result['detail']);
            $x_price = floatval($result['price']);
            $fee_gateway = $x_price > 0 ? 0.31 : 0;
            $result['price'] = (100/(100+10) * ($x_price - $fee_gateway));
            $result['vote_yes'] = calculateRating($id);
            $result['vote_total'] = getTotalVote($id);
            $query = mysql_query("SELECT SUM(user_user_transaction.price) AS total_income FROM user_user_transaction WHERE id_stuff = '$id'");
            if (mysql_num_rows($query) > 0){
                $rs = mysql_fetch_array($query);
                $result['total_income'] = cvtMoney($rs['total_income']);
            } else $result['total_income'] = cvtMoney(0);
            $result['mode'] = "edit";
        }
    }
    return $result;

}

function getDevProject($dev_id,$type){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    $dev_id = mysql_real_escape_string($dev_id);
    if (!empty($_SESSION['ext_devProjectSearch'])){
        $x = mysql_real_escape_string($_SESSION['ext_devProjectSearch']);
        $q = explode(" ", $x);
        $num = count($q);
        $xx = 1;
        while ($xx <= $num){
            if ($xx == 1){
                $to_add_g = "(tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_a = "(tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_c = "(tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_d = "(tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
            }
            else if ($xx != 1){
                $to_add_g = $to_add_g." AND (tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_a = $to_add_a." AND (tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_c = $to_add_c." AND (tci_content.id_content LIKE '%".$q[$xx-1]."%' OR tci_content.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_content.title LIKE '%".$q[$xx-1]."%' OR tci_content.id_developer LIKE '%".$q[$xx-1]."%' OR tci_content.category LIKE '%".$q[$xx-1]."%' OR tci_content.tags LIKE '%".$q[$xx-1]."%')";
                $to_add_d = $to_add_d." AND (tci_dlc.id_dlc LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_game_parent LIKE '%".$q[$xx-1]."%' OR tci_dlc.title LIKE '%".$q[$xx-1]."%' OR tci_dlc.id_developer LIKE '%".$q[$xx-1]."%' OR tci_dlc.category LIKE '%".$q[$xx-1]."%' OR tci_dlc.tags LIKE '%".$q[$xx-1]."%')";
            }
            $xx++;
        }
        mysql_query("SET @ptype:='game'");
        if ($type == "game")$query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label FROM tci_game,tci_game_progress WHERE tci_game.id_game = tci_game_progress.id_game AND tci_game.id_developer = '$dev_id' AND (".$to_add_g.") LIMIT 6");
        else if ($type == "dlc")$query = mysql_query("SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label FROM tci_dlc,tci_dlc_progress WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc.id_developer = '$dev_id' AND (".$to_add_d.") LIMIT 6");
        else if ($type == "content")$query = mysql_query("SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label FROM tci_content,tci_content_progress WHERE tci_content.id_content = tci_content_progress.id_content AND tci_content.id_developer = '$dev_id' AND (".$to_add_c.") LIMIT 6");
        else if ($type == "app")$query = mysql_query("SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label FROM tci_app,tci_app_progress WHERE tci_app.id_app = tci_app_progress.id_app AND tci_app.id_developer = '$dev_id' AND (".$to_add_a.") LIMIT 6");
    }else{
        mysql_query("SET @ptype:='game'");
        if ($type == "game")$query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label,tci_game_progress.date_published AS date_published, tci_game_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game.id_game = tci_game_progress.id_game AND tci_game_progress.show_public = 1 AND tci_game.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_game.id_game LIMIT 6");
        else if ($type == "dlc")$query = mysql_query("SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label,tci_dlc_progress.date_published AS date_published, tci_dlc_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_dlc,tci_dlc_progress,tci_stuff_download WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND tci_dlc_progress.show_public = 1 AND tci_dlc.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_dlc.id_dlc LIMIT 6");
        else if ($type == "content")$query = mysql_query("SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label,tci_content_progress.date_published AS date_published, tci_content_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_content,tci_content_progress,tci_stuff_download WHERE tci_content.id_content = tci_content_progress.id_content AND tci_content_progress.show_public = 1 AND tci_content.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_content.id_content LIMIT 6");
        else if ($type == "app")$query = mysql_query("SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label,tci_app_progress.date_published AS date_published, tci_app_progress.date_started AS date_started, tci_stuff_download.num AS num FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app.id_app = tci_app_progress.id_app AND tci_app_progress.show_public = 1 AND tci_app.id_developer = '$dev_id' AND tci_stuff_download.id_stuff = tci_app.id_app LIMIT 6");
    }
    $num = mysql_num_rows($query);
    if (!$num < 1){
        $result = '';
        while ($rs = mysql_fetch_array($query)){
            $sl_type = $rs['@ptype'];
            $sl_pic = $rs['pic_title'];
            $sl_id = $rs['id'];
            $sl_title = $rs['title'];
            $sl_iddeveloper = $rs['id_developer'];
            $sl_progress = $rs['progress'];
            $sl_label = $rs['label'];
            if ($rs['@ptype'] == "game") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs['@ptype'] == "dlc") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs['@ptype'] == "content") {
                $sl_link = $BASENAME['content'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['content'];
            }
            else if ($rs['@ptype'] == "app") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            $sl_linkdev = $BASENAME['developer'].'view/?id='.$sl_iddeveloper;
            $result = $result.  '<div class="progress-slide">
                                    <a href="'.$sl_link.'"><img src="'.$BASENAME['img_product'].$sl_iddeveloper.'/'.$sl_id.'/'.$sl_pic.'"></a>
                                    <a href="'.$sl_linktype.'"><button class="labeling label-type">'.$TEXT['home_progressslide_type'].$sl_type.'</button></a><br>
                                    <a href="'.$sl_link.'"><button class="labeling label-title">'.$sl_title.'</button></a><br>
                                    <a href="'.$sl_linkdev.'"><button class="labeling label-dev">'.$TEXT['home_progressslide_dev'].$sl_iddeveloper.'</button></a><br>
                                    <div class="progress">
                                        <button class="label-progress" style="width:'.$sl_progress.'%'.';min-width:15%;">'.$sl_progress.'%'.'</button>
                                    </div>
                                    <button class="labeling label-label">'.$TEXT['home_progressslide_status'].$sl_label.'</button>
                                </div>
                                ';
        }
        return $result;
    }
    else return "";
}   

function getDevProjectSearchPlaceholder(){
    if (!empty($_SESSION['ext_devProjectSearch'])){
        return $_SESSION['ext_devProjectSearch'];
    }else{
        return "";
    }
}

function getDevStats($dev_id){
    connectDb();
    $dev_id = mysql_real_escape_string($dev_id);
    $query = mysql_query("SELECT COUNT(tci_game.id_game) AS num FROM tci_game WHERE tci_game.id_developer = '$dev_id'
        UNION SELECT COUNT(tci_dlc.id_dlc) AS num FROM tci_dlc WHERE tci_dlc.id_developer = '$dev_id'
        UNION SELECT COUNT(tci_content.id_content) AS num FROM tci_content WHERE tci_content.id_developer = '$dev_id'
        UNION SELECT COUNT(tci_app.id_app) AS num FROM tci_app WHERE tci_app.id_developer = '$dev_id'") or die (mysql_error());
    $x_tot_product = 0;
    while ($rs = mysql_fetch_array($query)) {
        if (!empty($rs['num'])) $x_tot_product = $x_tot_product + intval($rs['num']);
    }
    $result['total_product'] = $x_tot_product;
    $query = mysql_query("SELECT COUNT(tci_game.id_game) AS num FROM tci_game WHERE id_developer = '$dev_id'");
    $rs = mysql_fetch_array($query);
    $result['total_game'] = $rs['num'];
    $query = mysql_query("SELECT COUNT(tci_dlc.id_dlc) AS num FROM tci_dlc WHERE id_developer = '$dev_id'");
    $rs = mysql_fetch_array($query);
    $result['total_dlc'] = $rs['num'];
    $query = mysql_query("SELECT COUNT(tci_content.id_content) AS num FROM tci_content WHERE id_developer = '$dev_id'");
    $rs = mysql_fetch_array($query);
    $result['total_content'] = $rs['num'];
    $query = mysql_query("SELECT COUNT(tci_app.id_app) AS num FROM tci_app WHERE id_developer = '$dev_id'");
    $rs = mysql_fetch_array($query);
    $result['total_app'] = $rs['num'];
    $result['rate_yes'] = calculateDevRating($dev_id);
    $result['total_rate'] = getDevTotalVote($dev_id);
    return $result;
}

function getDevTotalVote($uid){
    $id = mysql_real_escape_string($uid);
    $qu_total = mysql_query("SELECT COUNT(id_stuff) AS num FROM user_vote_stuff
    LEFT JOIN tci_game ON id_stuff = id_game AND tci_game.id_developer = '$id'
    LEFT JOIN tci_dlc ON id_stuff = id_dlc AND tci_dlc.id_developer = '$id'
    LEFT JOIN tci_content ON id_stuff = id_content AND tci_content.id_developer = '$id'
    LEFT JOIN tci_app ON id_stuff = id_app AND  tci_app.id_developer = '$id'
    WHERE id_stuff = id_game OR id_stuff = id_dlc OR id_stuff = id_content OR id_stuff = id_app");
    $rs = mysql_fetch_array($qu_total);
    $result = $rs['num'];
    return $result;
}

function getErrorTxt(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	$result = $TEXT['error_txt_default'];
	if (!empty($_GET['err'])){
		if ($_GET['err'] == "404") $result = $TEXT['error_txt_404'];
		else if ($_GET['err'] == "data") $result = $TEXT['error_txt_data'];
		else if ($_GET['err'] == "db") $result = $TEXT['error_txt_db'];
		else if ($_GET['err'] == "paging") $result = $TEXT['error_txt_paging'];
		else if ($_GET['err'] == "profileerror"){
			session_destroy();
			$result = $TEXT['error_txt_profile'];
		}
	}
	return $result;
}

function getHeaderNews(){ //get the header news
	connectDb();
	$query = mysql_query("SELECT * FROM tci_config");
	$rs = mysql_fetch_array($query);
	$result = getStringFromArray($rs['header_news']);
	return $result;
}

function getHomeNews(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	include 'variable.php';
	connectDb();
	$query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user ORDER BY tci_news.datetime DESC LIMIT 9");
	$result = "";
	while ($rs = mysql_fetch_array($query)){
		$n_newslink = $BASENAME['news'].'view/?id='.$rs['id_news'];
		$n_title = getStringFromArray($rs['title']);
		$n_text = mb_strimwidth(getStringFromArray($rs['text']), 0, 100, "...");
		$n_pic = $BASENAME['img_news'].$rs['title_photo'];
		$n_datetime = $rs['datetime'];
		$n_developerlink = $BASENAME['developer'].'view/?id='.$rs['id_developer'];
		$n_iddeveloper = $rs['id_developer'];
		$n_userlink = $BASENAME['user'].'view/?id='.$rs['u_iduser'];
		$n_user = $rs['front_name']." ".$rs['back_name'];
		$result = $result.	'
							<a href="'.$n_newslink.'">
							<div class="news-box">
								<img src="'.$n_pic.'">
								<p class="text-white-shadow">'.$n_title.'</p>
								<p>'.$n_text.'</p>
								</a><a href="'.$n_newslink.'" class="info"><p>'.$n_datetime." ".$TEXT['home_content_news_by1']."</a> <a href='".$n_userlink."' class='info'>".$n_user."</a> <a href='".$n_developerlink."' class='info'>".$TEXT['home_content_news_by2']." ".$n_iddeveloper.'</a></p>
							</a>
							</div>
							';
	}
	return $result;
}

function getHomeProductTrending(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	include 'variable.php';
	connectDb();
	$query = mysql_query("SELECT trending_id FROM tci_config");
	$rs = mysql_fetch_array($query);
	$ts = explode(";", $rs['trending_id']);
	$num_ts = count($ts);
	if (!$num_ts < 1){
		$to_add_game = "";
		$to_add_dlc = "";
		$to_add_content = "";
		$to_add_app = "";
		$xx = 1;
		while ($xx <= $num_ts){
			if ($xx == 1){
			 	$to_add_game = $to_add_game."tci_game.id_game = '".$ts[$xx-1]."' ";
			 	$to_add_dlc = $to_add_dlc."tci_dlc.id_dlc = '".$ts[$xx-1]."' ";
			 	$to_add_content = $to_add_content."tci_content.id_content = '".$ts[$xx-1]."' ";
			 	$to_add_app = $to_add_app."tci_app.id_app = '".$ts[$xx-1]."' ";
			}else{
				$to_add_game = $to_add_game."OR tci_game.id_game = '".$ts[$xx-1]."' ";
				$to_add_dlc = $to_add_dlc."OR tci_dlc.id_dlc = '".$ts[$xx-1]."' ";
				$to_add_content = $to_add_content."OR tci_content.id_content = '".$ts[$xx-1]."' ";
				$to_add_app = $to_add_app."OR tci_app.id_app = '".$ts[$xx-1]."' ";
			}
			$xx++;
		}
		mysql_query("SET @ptype='game'");
		$query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game.price AS price FROM tci_game,tci_game_progress WHERE tci_game.id_game = tci_game_progress.id_game AND (".$to_add_game.")
		UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc.price AS price FROM tci_dlc,tci_dlc_progress WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND (".$to_add_dlc.")
		UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content.price AS price FROM tci_content,tci_content_progress WHERE tci_content.id_content = tci_content_progress.id_content AND (".$to_add_content.")
		UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app.price AS price FROM tci_app,tci_app_progress WHERE tci_app.id_app = tci_app_progress.id_app AND (".$to_add_app.")");
		$result = "";
		$num = mysql_num_rows($query);
		$xx = 1;
		while ($rs = mysql_fetch_array($query)){
			$pr_pic = $rs['pic_title'];
			$pr_id = $rs['id'];
			$pr_title = $rs['title'];
			$pr_iddeveloper = $rs['id_developer'];
			$pr_progress = $rs['progress'];
			$pr_price = $rs['price'];
			if ($rs['@ptype'] == "game") {
				$pr_link = $BASENAME['store'].'view/?id='.$pr_id;
			}
			else if ($rs['@ptype'] == "dlc") {
				$pr_link = $BASENAME['store'].'view/?id='.$pr_id;
			}
			else if ($rs['@ptype'] == "content") {
				$pr_link = $BASENAME['content'].'view/?id='.$pr_id;
			}
			else if ($rs['@ptype'] == "app") {
				$pr_link = $BASENAME['store'].'view/?id='.$pr_id;
			}
			$result = 	$result.'<div class="product-box">
							<a href="'.$pr_link.'"><img src="'.$BASENAME['img_product'].$pr_iddeveloper.'/'.$pr_id.'/'.$pr_pic.'">
							<h5 class="text-big-black">'.$pr_title.'</h5> <h4 class="text-green">'.getSym().cvtMoney($pr_price).'</h4>
							</a>
						</div>
						';
		}
		return $result;
	}
}

function getHomeProgressList(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	include 'variable.php';
	connectDb();
	mysql_query("SET @ptype:='game'");
	$query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer FROM tci_game,tci_game_progress WHERE NOT tci_game_progress.progress = 100 AND tci_game.id_game = tci_game_progress.id_game
		UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer FROM tci_dlc,tci_dlc_progress WHERE NOT tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc
		UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer FROM tci_content,tci_content_progress WHERE NOT tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content
		UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer FROM tci_app,tci_app_progress WHERE NOT tci_app_progress.progress = 100 AND tci_app.id_app = tci_app_progress.id_app");
	$num = mysql_num_rows($query);
	if (!$num < 1){
		$result = '';
		$xx = 1;
		while (($rs = mysql_fetch_array($query)) && $xx <= 6){
			$sl_type = $rs['@ptype'];
			$sl_id = $rs['id'];
			$sl_title = $rs['title'];
			$sl_iddeveloper = $rs['id_developer'];
			if ($rs['@ptype'] == "game") {
				$sl_link = $BASENAME['store'].'view/?id='.$sl_id;
			}
			else if ($rs['@ptype'] == "dlc") {
				$sl_link = $BASENAME['store'].'view/?id='.$sl_id;
			}
			else if ($rs['@ptype'] == "content") {
				$sl_link = $BASENAME['content'].'view/?id='.$sl_id;
			}
			else if ($rs['@ptype'] == "app") {
				$sl_link = $BASENAME['store'].'view/?id='.$sl_id;
			}
			$result = $result.	'<a href="'.$sl_link.'" class="text-white-shadow">'.$sl_iddeveloper.":".$sl_title.'</a><br>
								';
			$xx++;
		} 
		if ($num > 6){
			$result = $result.	'<b>'.$num.' '.$TEXT['home_content_progressmore'].'</b><br>
								';
		}
		return $result;
	}else{
		return "";
	}
}

function getHomeProgressSlide(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	include 'variable.php';
	connectDb();
	mysql_query("SET @ptype:='game'");
	$query = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_progress AS pic_progress,tci_game_progress.progress AS progress,tci_game_progress.label AS label FROM tci_game,tci_game_progress WHERE NOT tci_game_progress.progress = 100 AND tci_game.id_game = tci_game_progress.id_game
		UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_progress AS pic_progress,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label FROM tci_dlc,tci_dlc_progress WHERE NOT tci_dlc_progress.progress = 100 AND tci_dlc.id_dlc = tci_dlc_progress.id_dlc
		UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_progress AS pic_progress,tci_content_progress.progress AS progress,tci_content_progress.label AS label FROM tci_content,tci_content_progress WHERE NOT tci_content_progress.progress = 100 AND tci_content.id_content = tci_content_progress.id_content
		UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_progress AS pic_progress,tci_app_progress.progress AS progress,tci_app_progress.label AS label FROM tci_app,tci_app_progress WHERE NOT tci_app_progress.progress = 100 AND tci_app.id_app = tci_app_progress.id_app");
	$num = mysql_num_rows($query);
	if (!$num < 1){
		$result = '';
		while ($rs = mysql_fetch_array($query)){
			$sl_type = $rs['@ptype'];
			$sl_pic = $rs['pic_progress'];
			$sl_id = $rs['id'];
			$sl_title = $rs['title'];
			$sl_iddeveloper = $rs['id_developer'];
			$sl_progress = $rs['progress'];
			$sl_label = $rs['label'];
			if ($rs['@ptype'] == "game") {
				$sl_link = $BASENAME['store'].'view/?id='.$sl_id;
				$sl_linktype = $BASENAME['store'];
			}
			else if ($rs['@ptype'] == "dlc") {
				$sl_link = $BASENAME['store'].'view/?id='.$sl_id;
				$sl_linktype = $BASENAME['store'];
			}
			else if ($rs['@ptype'] == "content") {
				$sl_link = $BASENAME['content'].'view/?id='.$sl_id;
				$sl_linktype = $BASENAME['content'];
			}
			else if ($rs['@ptype'] == "app") {
				$sl_link = $BASENAME['store'].'view/?id='.$sl_id;
				$sl_linktype = $BASENAME['store'];
			}
			$sl_linkdev = $BASENAME['developer'].'view/?id='.$sl_iddeveloper;
			$result = $result.	'<div class="progress-slide">
									<a href="'.$sl_link.'"><img src="'.$BASENAME['img_product'].$sl_iddeveloper.'/'.$sl_id.'/'.$sl_pic.'"></a>
									<a href="'.$sl_linktype.'"><button class="labeling label-type">'.$TEXT['home_progressslide_type'].$sl_type.'</button></a><br>
									<a href="'.$sl_link.'"><button class="labeling label-title">'.$sl_title.'</button></a><br>
									<a href="'.$sl_linkdev.'"><button class="labeling label-dev">'.$TEXT['home_progressslide_dev'].$sl_iddeveloper.'</button></a><br>
									<div class="progress">
										<button class="label-progress" style="width:'.$sl_progress.'%'.';min-width:15%;">'.$sl_progress.'%'.'</button>
									</div>
									<button class="labeling label-label">'.$TEXT['home_progressslide_status'].$sl_label.'</button>
								</div>
								';
		}
		return $result;
	}
	else return "";
}	

function getHomeRecentActivities(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	include 'variable.php';
	connectDb();
	$query = mysql_query("SELECT * FROM tci_recent_activity ORDER BY datetime DESC");
	$num = mysql_num_rows($query);
	if (!$num < 1){
		$result = "";
		$xx = 1;
		while (($rs = mysql_fetch_array($query)) && $xx <= 10){
			$ra_text = getStringFromArray($rs['text']);
			$ra_datetime = $rs['datetime'];
			$ra_link = $rs['link'];
			$result = $result. 	'<a href="'.$ra_link.'" class="text-blue">'.$ra_text.'</a><br>
								<b class="label-date">'.$ra_datetime.'</b><br>
								';
			$xx++;
		}
		$result = $result.	'<button class="btn-green btn-height-medium" onclick="showRecentActivity()">'.$TEXT['home_content_ra_more'].'</button>
							';
		return $result;
	}else{
		return "";
	}
}

function getHomeSlideShow(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	include 'variable.php';
	connectDb();
	$query = mysql_query("SELECT * FROM tci_slide");
	$num = mysql_num_rows($query);
	if (!$num < 1){
		$result = "";
		while ($rs = mysql_fetch_array($query)){
			$sl_pic = $rs['filename'];
			$sl_title = $rs['title'];
			$sl_text = $rs['text'];
			$sl_href = $rs['href'];
			$result = 	$result.'<div class="index-slide">
									<img src="'.$BASENAME['img_slide'].$sl_pic.'">
									<center>
									<h1><b>'.$sl_title.'</b></h1>
									<h3><i>'.getStringFromArray($sl_text).'</i></h3>
									<a href="'.$sl_href.'"><b>'.$TEXT['home_slide_view'].'</b></a><br>
									<div class="indicator">
									<button class="indBtn btnPrev" onclick="slider(-1)">&#60;</button><button class="indBtn btnNext" onclick="slider(+1)">&#62;</button>
									</div>
									</center>
									<button class="progress"></button>
								</div>
								';
		}
		return $result;
	}else{
		return "";
	}
}

function getMyAdminPosition(){ //get the position data of user(admin)
	if (!empty($_SESSION['adm_position'])) return $_SESSION['adm_position'];
	else return null;
}

function getMyBalance(){
    if (!empty($_SESSION['usr_id'])){
        $usr_id = $_SESSION['usr_id'];
        if (!$query = mysql_query("SELECT balance FROM user_user_balance WHERE id_user = $usr_id")) goPage("error","?err=data");
        $rs = mysql_fetch_array($query);
        $result = formatMoney($rs['balance']);
        return $result;
    }
}

function getNavProfile(){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    $result = "";
    if (empty($_SESSION['usr_id'])){
        $result =   '
                    <img src="'.$BASENAME['img_html'].'navbar-default-avatar.png'.'">
                        <div class="dropdown-content">
                            <a href="'.$BASENAME['login'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left text-small" style="width: 80%;padding-top: 3%;">'.$TEXT['navbar_login'].'</div></a>
                            <a href="'.$BASENAME['signup'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left" style="width: 80%;padding-top: 3%;">'.$TEXT['navbar_signup'].'</div></a>
                        </div>
                    ';
    }else{
        $usr_id = $_SESSION['usr_id'];
        $query = mysql_query("SELECT user_user.id_user,user_user.front_name,user_user.back_name,user_user.avatar_link,user_user_balance.balance FROM user_user,user_user_balance WHERE user_user.id_user='$usr_id' AND user_user_balance.id_user='$usr_id'");
        if (mysql_num_rows($query) == 1){
            $rs = mysql_fetch_array($query);
            if ($rs['avatar_link'] != ""){
                $usr_linkPic = $BASENAME['img_avatar'].$rs['avatar_link'];
            }
            $usr_name = $rs['front_name'].' '.$rs['back_name'];
            $result =   '
                        <img src="'.$usr_linkPic.'">
                            <div class="dropdown-content">
                                <a href="'.$BASENAME['my_profile'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left text-small" style="width: 80%;padding-top: 3%;">'.$TEXT2['nav_myprofile'].'</div></a>
                                <a href="'.$BASENAME['my_profile_topup'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left" style="width: 80%;padding-top: 0%;">'.$TEXT2['nav_balance'].' :'.'<br>'.getSym().cvtMoney($rs['balance']).'</div></a>
                                <a href="'.$BASENAME['my_transaction'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left text-small" style="width: 80%;padding-top: 3%;">'.$TEXT2['nav_product'].'</div></a>
                                <a href="'.$BASENAME['cart'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left text-small" style="width: 80%;padding-top: 3%;">'.$TEXT2['nav_cart'].'</div></a>
                                <a href="'.$BASENAME['devconsole'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left text-small" style="width: 80%;padding-top: 3%;">'.$TEXT2['nav_dev'].'</div></a>
                                <a href="'.$BASENAME['logout'].'"><div class="left" style="width:20%;"><img src="'.$BASENAME['img_html'].'about.png'.'"></div><div class="left text-small" style="width: 80%;padding-top: 3%;">'.$TEXT2['nav_logout'].'</div></a>
                            </div>
                        ';
        }else goPage("error","?err=profileerror");
    }
    return $result;
}

function getNewsArray(){
    include 'variable.php';
    connectDb();
    $result['avail'] = false;
    if (!empty($_GET['id'])){
        $n_id = mysql_real_escape_string($_GET['id']);
        if (!$query = mysql_query("SELECT tci_news.id_news AS id,tci_news.title,tci_news.text,tci_news.photos,tci_news.title_photo,tci_news.category,tci_news.tags,tci_news.datetime,tci_news.id_developer,tci_news.id_user,tci_news_visit.num AS visit,tci_news.href,user_user.front_name,user_user.back_name FROM tci_news,tci_news_visit,user_user WHERE tci_news.id_user = user_user.id_user AND tci_news_visit.id_news = tci_news.id_news AND tci_news.id_news = $n_id")) goPage("error","?err=data");
        $row = mysql_num_rows($query);
        if ($row == 1){
            $rs = mysql_fetch_array($query);
            $result['id'] = $rs['id'];
            $result['title'] = getStringFromArray($rs['title']);
            $result['text'] = getStringFromArray($rs['text']);
            $result['photos'] = $rs['photos'];
            $result['title_photo'] = $BASENAME['img_news'].$rs['title_photo'];
            $result['category'] = $rs['category'];
            $result['tags'] = $rs['tags'];
            $result['datetime'] = $rs['datetime'];
            $result['id_developer'] = $rs['id_developer'];
            $result['link_developer'] = $BASENAME['developer'].'view/?id='.$result['id_developer'];
            $result['link_writer'] = $BASENAME['user'].'view/?id='.$rs['id_user'];
            $result['visit'] = $rs['visit'];
            if ($rs['href'] != ""){
                $result['href'] = $rs['href'];
                $result['href_avail']= true;
            }else $result['href_avail'] = false;
            $result['user_name'] = $rs['front_name'].' '.$rs['back_name'];
            $result['title_html'] = $result['title'].' - '.'TCI';
            $result['avail'] = true;
        }else {
            $result['id'] = 0;
            $result['title_html'] = 'News not found - TCI';
        }
    }else {
        $result['id'] = 0;
        $result['title_html'] = 'News not found - TCI';
    }
    return $result;
}

function getNewsAvailNext(){
    connectDb();
    $result = false;
    if (!empty($_GET['id'])){
        $n_id = mysql_real_escape_string($_GET['id']);
        if (!$query = mysql_query("SELECT * FROM tci_news WHERE id_news = $n_id + 1")) goPage("error","?err=data");
        $row = mysql_num_rows($query);
        if ($row == 1) $result = true;
    }
    return $result;
}

function getNewsAvailPrev(){
    connectDb();
    $result = false;
    if (!empty($_GET['id'])){
        $n_id = mysql_real_escape_string($_GET['id']);
        if (!$query = mysql_query("SELECT * FROM tci_news WHERE id_news = $n_id - 1")) goPage("error","?err=data");
        $row = mysql_num_rows($query);
        if ($row == 1) $result = true;
    }
    return $result;
}

function getNewsBtnVoted($nid,$v){
    connectDb();
    $result = "";
    if (!empty($_SESSION['usr_id'])){
        $usr_id = $_SESSION['usr_id'];
        $n_id = mysql_real_escape_string($nid);
        $query = mysql_query("SELECT * FROM user_vote_news WHERE id_news='$n_id' AND id_user='$usr_id'");
        if (mysql_num_rows($query) == 1) {
            $rs = mysql_fetch_array($query);
            switch ($v) {
            case 'y':
                if ($rs['vote'] == 'y') $result = "btn-vote-yes-apy";
                else $result = $result = "btn-vote-yes";
                break;
            
            case 'n':
                if ($rs['vote'] == 'n') $result = "btn-vote-no-apy";
                else $result = $result = "btn-vote-no";
                break;
            }
        }else{
            switch ($v) {
            case 'y':
                $result = $result = "btn-vote-yes";
                break;
            
            case 'n':
                $result = $result = "btn-vote-no";
                break;
            }
        }
    }
    return $result;
}

function getNewsNews(){
    connectDb();
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    if (!empty($_GET['f'])){
        switch ($_GET['f']) {
            case 'n':
                $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user ORDER BY tci_news.datetime DESC LIMIT 8");
                break;

            case 'o':
                $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user ORDER BY tci_news.datetime ASC");
                break;    
            
            case 'i':
                $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user AND tci_news.category LIKE '%important%' ORDER BY tci_news.datetime DESC");
                break;

            case 'tci':
                $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user AND tci_news.id_developer = 'TCI' ORDER BY tci_news.datetime DESC");
                break;

            default:
                $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user ORDER BY tci_news.datetime DESC");
                break;
        }
    }else if (!empty($_GET['c'])){
        $n_c = mysql_real_escape_string($_GET['c']);
        $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user AND tci_news.category LIKE '%$n_c%' ORDER BY tci_news.datetime DESC");
    }else if (!empty($_GET['q'])){
        $x = mysql_real_escape_string($_GET['q']);
        $q = explode(" ", $x);
        $num = count($q);
        $xx = 1;
        while ($xx <= $num){
            if ($xx == 1){
                $to_add = "(tci_news.title LIKE '%".$q[$xx-1]."%' OR tci_news.text LIKE '%".$q[$xx-1]."%' OR tci_news.category LIKE '%".$q[$xx-1]."%' OR tci_news.datetime LIKE '%".$q[$xx-1]."%' OR tci_news.id_developer LIKE '%".$q[$xx-1]."%' OR tci_news.tags LIKE '%".$q[$xx-1]."%' OR user_user.front_name LIKE '%".$q[$xx-1]."%' OR user_user.back_name LIKE '%".$q[$xx-1]."%')";
            }
            else if ($xx != 1){
                $to_add = $to_add." AND (tci_news.title LIKE '%".$q[$xx-1]."%' OR tci_news.text LIKE '%".$q[$xx-1]."%' OR tci_news.category LIKE '%".$q[$xx-1]."%' OR tci_news.datetime LIKE '%".$q[$xx-1]."%' OR tci_news.id_developer LIKE '%".$q[$xx-1]."%' OR tci_news.tags LIKE '%".$q[$xx-1]."%' OR user_user.front_name LIKE '%".$q[$xx-1]."%' OR user_user.back_name LIKE '%".$q[$xx-1]."%')";
            }
            $xx++;
        }
        $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user AND (".$to_add.") ORDER BY tci_news.datetime DESC"); 
    }else{
        $query = mysql_query("SELECT tci_news.id_news AS id_news,tci_news.title AS title,tci_news.text AS text,tci_news.title_photo AS title_photo,tci_news.datetime AS datetime,tci_news.id_developer AS id_developer,tci_news.id_user AS n_iduser,user_user.id_user AS u_iduser,user_user.front_name AS front_name,user_user.back_name AS back_name FROM tci_news,user_user WHERE tci_news.id_user = user_user.id_user ORDER BY tci_news.datetime DESC");
    }
    $result = "";
    while ($rs = mysql_fetch_array($query)){
        $n_newslink = $BASENAME['news'].'view/?id='.$rs['id_news'];
        $n_title = getStringFromArray($rs['title']);
        $n_text = mb_strimwidth(getStringFromArray($rs['text']), 0, 100, "...");
        $n_pic = $BASENAME['img_news'].$rs['title_photo'];
        $n_datetime = $rs['datetime'];
        $n_developerlink = $BASENAME['developer'].'view/?id='.$rs['id_developer'];
        $n_iddeveloper = $rs['id_developer'];
        $n_userlink = $BASENAME['user'].'view/?id='.$rs['u_iduser'];
        $n_user = $rs['front_name']." ".$rs['back_name'];
        $result = $result.  '
                            <a href="'.$n_newslink.'">
                            <div class="news-box">
                                <img src="'.$n_pic.'">
                                <p class="text-white-shadow">'.$n_title.'</p>
                                <p>'.$n_text.'</p>
                                </a><a href="'.$n_newslink.'" class="info"><p>'.$n_datetime." ".$TEXT['home_content_news_by1']."</a> <a href='".$n_userlink."' class='info'>".$n_user."</a> <a href='".$n_developerlink."' class='info'>".$TEXT['home_content_news_by2']." ".$n_iddeveloper.'</a></p>
                            </a>
                            </div>
                            ';
    }
    return $result;
}

function getNewsPicLeft($tp,$p){ // $tp : Title Photo, $p : Photos
    include 'variable.php';
    $result = "";
    if (!empty($tp)){
        $result = '<img class="img-w100-h400" id="show-img-1" src="'.$tp.'">';
        if ($p != ""){
            $e_p = explode(";", $p);
            $num = count($e_p);
            for ($xx=0; $xx < $num; $xx++) { 
                $l_p = $BASENAME['img_news'].$e_p[$xx];
                $id_p = "show-img-".($xx+2);
                $result = $result.'<img class="img-w100-h400" id="'.$id_p.'" src="'.$l_p.'">';
            }
        }
    }
    return $result;
}

function getNewsPicRight($tp,$p){ // $tp : Title Photo, $p : Photos
    include 'variable.php';
    $result = "";
    if (!empty($tp)){
        $result = '<img class="img-w45-h70-hoverable cursor-hand" style="margin-bottom:10px;margin-left:2.5px;margin-right:2.5px" id="option-img-1" src="'.$tp.'" onclick="changePic(\''.'show-img-1'.'\',\''.'option-img-1'.'\')">';
        if ($p != ""){
            $e_p = explode(";", $p);
            $num = count($e_p);
            for ($xx=0; $xx < $num; $xx++) { 
                $l_p = $BASENAME['img_news'].$e_p[$xx];
                $id_p = "show-img-".($xx+2);
                $id_o_p = "option-img-".($xx+2);
                $result = $result.'<img class="img-w45-h70-hoverable cursor-hand" style="margin-bottom:10px;margin-left:2.5px;margin-right:2.5px" id="'.$id_o_p.'" src="'.$l_p.'" onclick="changePic(\''.$id_p.'\',\''.$id_o_p.'\')">';
            }
        }else $result = '<img class="img-w70-h120 cursor-hand" style="margin-bottom:10px;" id="option-img-1" src="'.$tp.'">';
    }
    return $result;
}

function getNewsSearchPlaceholder(){
    if (!empty($_GET['q'])){
        return $_GET['q'];
    }else{
        return "";
    }
}

function getNewsTitleNext(){
    connectDb();
    $result = "";
    if (!empty($_GET['id'])){
        $n_id = mysql_real_escape_string($_GET['id']);
        if (!$query = mysql_query("SELECT title FROM tci_news WHERE id_news = $n_id + 1")) goPage("error","?err=data");
        $rs = mysql_fetch_array($query);
        $result = getStringFromArray($rs['title']);
    }
    return $result;
}

function getNewsTitlePrev(){
    connectDb();
    $result = "";
    if (!empty($_GET['id'])){
        $n_id = mysql_real_escape_string($_GET['id']);
        if (!$query = mysql_query("SELECT title FROM tci_news WHERE id_news = $n_id - 1")) goPage("error","?err=data");
        $rs = mysql_fetch_array($query);
        $result = getStringFromArray($rs['title']);
    }
    return $result;
}

function getProductArray(){
	include 'variable.php';
    $result['avail'] = false;
    if (!empty($_GET['id'])){
        $pr_id = mysql_real_escape_string($_GET['id']);
        $query = mysql_query("SELECT @type:='game' AS type FROM tci_game WHERE tci_game.id_game ='$pr_id'
            UNION SELECT @type:='app' AS type FROM tci_app WHERE tci_app.id_app='$pr_id'");
        if (mysql_num_rows($query) == 1){
            $rs = mysql_fetch_array($query);
            $pr_type = $rs['type'];
            if ($pr_type == "game"){
                $query = mysql_query("SELECT * FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game.id_game='".$pr_id."' AND tci_game_progress.id_game='".$pr_id."' AND tci_stuff_download.id_stuff='".$pr_id."'");
            }else if ($pr_type == "app"){
                $query = mysql_query("SELECT * FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app.id_app='".$pr_id."' AND tci_app_progress.id_app='".$pr_id."' AND tci_stuff_download.id_stuff='".$pr_id."'");
            }
            $rs = mysql_fetch_array($query);
            $rs['id'] = uniteId($rs);
            $rs['gc'] = uniteGenreCategory($rs);
            $rs['linkPic'] = $BASENAME['img_product'].$rs['id_developer'].'/'.$rs['id'].'/'.$rs['pic_title'];
            $rs['avail'] = true;
            $rs['avail_ATC'] = getAvailATC($rs['id']);
            $rs['ATC_added'] = getAddedATC($rs['id']);
            $rs['avail_dl'] = getAvailDl($rs['id']);
            $rs['vote_yes'] = calculateRating($rs['id']);
            $rs['vote_total'] = getTotalVote($rs['id']);
            $rs['title_html'] = $rs['title'].' - TCI';
            $result = $rs;
        }else $result['title_html'] = 'No product found - TCI';
    }else $result['title_html'] = 'No product found - TCI';
    return $result;
}

function getProductArrayAvail(){
    $result = false;
    if (!empty($_GET['id'])){
        $pr_id = mysql_real_escape_string($_GET['id']);
        $query = mysql_query("SELECT @type:='game' AS type FROM tci_game WHERE tci_game.id_game ='$pr_id'
            UNION SELECT @type:='app' AS type FROM tci_app WHERE tci_app.id_app='$pr_id'");
        if (mysql_num_rows($query) == 1){
            $result = true;
        }
    }
    return $result;
}

function getRateExchange(){
    connectDb();
    $query = mysql_query("SELECT kurs_rupiah FROM tci_config");
    $rs = mysql_fetch_array($query);
    return $rs['kurs_rupiah'];
}

function getRemoteSize($site){
     $ch = curl_init($site);
     curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
     curl_setopt($ch, CURLOPT_HEADER, TRUE);
     curl_setopt($ch, CURLOPT_NOBODY, TRUE);
     $data = curl_exec($ch);
     $size = intval(curl_getinfo($ch, CURLINFO_CONTENT_LENGTH_DOWNLOAD));
     curl_close($ch);
     return $size;
}

function getSearchPlaceholder(){
    if (!empty($_GET['q'])){
        return $_GET['q'];
    }else{
        return "";
    }
}

function getSignupReWrong(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	$result = "";
	if (!empty($_GET['re'])){
		$result = '<p class="text-red">'.$TEXT['signup_re_wrong'].'</p>';
	}
	return $result;
}

function getStoreBtnVoted($uid,$v){
    if (!empty($_SESSION['usr_id'])){
    	$usr_id = $_SESSION['usr_id'];
    	$pr_id = mysql_real_escape_string($uid);
    	$query = mysql_query("SELECT * FROM user_vote_stuff WHERE id_stuff='$pr_id' AND id_user='$usr_id'");
    	if (mysql_num_rows($query) == 1) {
    		$rs = mysql_fetch_array($query);
    		switch ($v) {
                case 'y':
                    if ($rs['vote'] == 'y') $result = "btn-vote-yes-apy";
                    else $result = "btn-vote-yes";
                    break;
                
                case 'n':
                    if ($rs['vote'] == 'n') $result = "btn-vote-no-apy";
                    else $result = "btn-vote-no";
                    break;
            }
    	}else{
            switch ($v) {
                case 'y':
                    $result = "btn-vote-yes";
                    break;
                
                case 'n':
                    $result = "btn-vote-no";
                    break;
            }
        }
    }else{
        switch ($v) {
        case 'y':
            $result = $result = "btn-vote-yes";
            break;
        
        case 'n':
            $result = "btn-vote-no";
            break;
        }
    }
	return $result;
}

function getStoreGASearchPlaceholder(){
    if (!empty($_GET['q'])){
        return $_GET['q'];
    }else{
        return "";
    }
}

function getStoreProductReview(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    $result = "";
    if (!empty($_GET['id'])){
    	if (!empty($_SESSION['usr_id'])) {
    		$usr_id = $_SESSION['usr_id'];
    	}else $usr_id = 0;
    	$pr_id = mysql_real_escape_string($_GET['id']);
    	if (!empty($_SESSION['usr_id'])) $usr_id = $_SESSION['usr_id']; else $usr_id = 0;
    	$query = mysql_query("SELECT * FROM user_review WHERE id_stuff='$pr_id' ORDER BY datetime DESC");
    	if (mysql_num_rows($query) >= 1){
    		$delete = "<br>
            <div style='text-align: right;margin-bottom: -30px;'>
                <input type='submit' name='usr_store_comment_delete' class='btn-red' value='".$TEXT['delete']."'>
            </div>";
    		while ($rs = mysql_fetch_array($query)){
                $usrData = getUserData($rs['id_user']);
    			$result =	$result.'
    						<div class="bar-comment-posted">
    							<form method="POST" action="'.$BASENAME['dr_form_function'].'">
    							<input type="hidden" name="usr_store_comment_id" value="'.$rs['id_review'].'">
    							<textarea name="usr_store_posted_comment" readonly disabled>'.$rs['text'].'</textarea>';
    							if ($rs['id_user'] == $usr_id) $result = $result.$delete;
    							$result = $result.'
    							</form>
                                <a href="'.$BASENAME['user'].'view/?id='.$usrData['id_user'].'">
                                <div style="width: 100%;height: 45%;margin-top: 2%;">
                                    <div class="left" style="width: 10%;">
                                        <img class="img-fw-fh-circle-hoverable img-border-shine-tiny cursor-hand" style="width:100%;height:50px;" src="'.$BASENAME['img_avatar'].$usrData['avatar_link'].'">
                                    </div>
                                    <div class="left" style="width: 87%;margin-left: 3%;">
                                        <label class="text-small text-white-shadow cursor-hand">'.$usrData['front_name'].' '.$usrData['back_name'].'</label><br>
                                        <label class="text-tiny text-white-shadow cursor-hand">'.$TEXT2['data_id'].' : '.$usrData['id_user'].'</label><br>
                                        <label class="text-tiny text-white-shadow cursor-hand">'.mb_strimwidth($usrData['signature'], 0, 75, "...").'</label>
                                    </div>
                                </div>
                                </a>
    						</div>
    						';
    		}
    	}
    }
    return $result;
}

function getStoreProductView(){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    $result = "";
    $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game_progress.progress = 100 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff
                UNION SELECT @type:='app' AS type,tci_app.id_app AS id,tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.pic_progress AS pic_progress,tci_app.price AS price,tci_app_progress.progress AS progress,tci_app_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app_progress.show_public = 1 AND tci_app_progress.progress = 100 AND tci_app.id_app = tci_app_progress.id_app AND tci_app.id_app = tci_stuff_download.id_stuff ORDER BY download DESC";
    if (empty($_GET['s']) && empty($_GET['q'])){
        $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff
                UNION SELECT @type:='app' AS type,tci_app.id_app AS id,tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.pic_progress AS pic_progress,tci_app.price AS price,tci_app_progress.progress AS progress,tci_app_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app_progress.show_public = 1 AND tci_app.id_app = tci_app_progress.id_app AND tci_app.id_app = tci_stuff_download.id_stuff ORDER BY download DESC";
    }else if (!empty($_GET['s']) && empty($_GET['q'])){
        if ($_GET['s'] == "g"){
            if (empty($_GET['c']) && empty($_GET['g'])){
                $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff ORDER BY download DESC";
            }else if(!empty($_GET['c']) && empty($_GET['g'])){
                if ($_GET['c'] == "trending"){
                    $str_to_add = "";
                    $rs = mysql_fetch_array(mysql_query("SELECT trending_id FROM tci_config"));
                    $idf = explode(";", $rs['trending_id']);
                    $num_idf = count($idf);
                    $xx = 1;
                    if ($num_idf > 0){
                        while ($xx <= $num_idf){
                            if ($xx == 1) $str_to_add = "tci_game.id_game = '".$idf[$xx-1]."' ";
                            else $str_to_add = $str_to_add."OR tci_game.id_game = '".$idf[$xx-1]."'";
                            $xx++;
                        }
                        $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff AND (".$str_to_add.")";
                    }else{
                        $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff ORDER BY download DESC";
                    }
                }else if ($_GET['c'] == "bs"){
                    $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game_progress.progress = 100 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff ORDER BY download DESC LIMIT 10";
                }else if ($_GET['c'] == "progress"){
                    $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND NOT tci_game_progress.progress = 100 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff ORDER BY tci_game_progress.date_started DESC";
                }
            }else if(!empty($_GET['g']) && empty($_GET['c'])){
                $genre = mysql_real_escape_string($_GET['g']);
                $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff AND tci_game.genre LIKE '%$genre%' ORDER BY download DESC";
            }
        }else if ($_GET['s'] == "a"){
            $sql = "SELECT @type:='app' AS type,tci_app.id_app AS id,tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.pic_progress AS pic_progress,tci_app.price AS price,tci_app_progress.progress AS progress,tci_app_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app_progress.show_public = 1 AND tci_app_progress.progress = 100 AND tci_app.id_app = tci_app_progress.id_app AND tci_app.id_app = tci_stuff_download.id_stuff ORDER BY download DESC LIMIT 10";
        }else if ($_GET['s'] == "trending"){
        	$str_to_add = "";
            $rs = mysql_fetch_array(mysql_query("SELECT trending_id FROM tci_config"));
            $idf = explode(";", $rs['trending_id']);
            $num_idf = count($idf);
            $xx = 1;
            if ($num_idf > 0){
                while ($xx <= $num_idf){
                    if ($xx == 1) {
                    	$str_to_add_g = "tci_game.id_game = '".$idf[$xx-1]."' ";
                    	$str_to_add_a = "tci_app.id_app = '".$idf[$xx-1]."' ";
                    }
                    else {
                    	$str_to_add_g = $str_to_add_g."OR tci_game.id_game = '".$idf[$xx-1]."'";
                    	$str_to_add_a = $str_to_add_a."OR tci_app.id_app = '".$idf[$xx-1]."' ";
                    }
                    $xx++;
                }
                $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff AND (".$str_to_add_g.")
                		UNION SELECT @type:='app' AS type,tci_app.id_app AS id,tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.pic_progress AS pic_progress,tci_app.price AS price,tci_app_progress.progress AS progress,tci_app_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app_progress.show_public = 1 AND tci_app.id_app = tci_app_progress.id_app AND tci_app.id_app = tci_stuff_download.id_stuff AND (".$str_to_add_a.") ORDER BY download DESC";
            }
        }else if ($_GET['s'] == "bs"){
            $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND tci_game_progress.progress = 100 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff
                UNION SELECT @type:='app' AS type,tci_app.id_app AS id,tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.pic_progress AS pic_progress,tci_app.price AS price,tci_app_progress.progress AS progress,tci_app_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app_progress.show_public = 1 AND tci_app_progress.progress = 100 AND tci_app.id_app = tci_app_progress.id_app AND tci_app.id_app = tci_stuff_download.id_stuff ORDER BY download DESC";
        }else if ($_GET['s'] == "progress"){
            $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download, tci_game_progress.date_started AS date_started FROM tci_game,tci_game_progress,tci_stuff_download WHERE tci_game_progress.show_public = 1 AND NOT tci_game_progress.progress = 100 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff
             UNION SELECT @type:='app' AS type,tci_app.id_app AS id,tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.pic_progress AS pic_progress,tci_app.price AS price,tci_app_progress.progress AS progress,tci_app_progress.date_published AS date_published,tci_stuff_download.num AS download, tci_app_progress.date_started AS date_started FROM tci_app,tci_app_progress,tci_stuff_download WHERE tci_app_progress.show_public = 1 AND NOT tci_app_progress.progress = 100 AND tci_app.id_app = tci_app_progress.id_app AND tci_app.id_app = tci_stuff_download.id_stuff ORDER BY date_started DESC";
        }
    }else if (empty($_GET['s']) && !empty($_GET['q'])){
        $x = mysql_real_escape_string($_GET['q']);
        $q = explode(" ", $x);
        $num = count($q);
        $xx = 1;
        while ($xx <= $num){
        	if ($xx == 1){
        		$to_add_g = "(tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
        		$to_add_a = "(tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
        	}
        	else if ($xx != 1){
        		$to_add_g = $to_add_g." AND (tci_game.id_game LIKE '%".$q[$xx-1]."%' OR tci_game.title LIKE '%".$q[$xx-1]."%' OR tci_game.id_developer LIKE '%".$q[$xx-1]."%' OR tci_game.genre LIKE '%".$q[$xx-1]."%' OR tci_game.tags LIKE '%".$q[$xx-1]."%')";
        		$to_add_a = $to_add_a." AND (tci_app.id_app LIKE '%".$q[$xx-1]."%' OR tci_app.title LIKE '%".$q[$xx-1]."%' OR tci_app.id_developer LIKE '%".$q[$xx-1]."%' OR tci_app.category LIKE '%".$q[$xx-1]."%' OR tci_app.tags LIKE '%".$q[$xx-1]."%')";
        	}
        	$xx++;
        }
        $sql = "SELECT @type:='game' AS type,tci_game.id_game AS id,tci_game.title AS title,tci_game.sub_title AS sub_title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game.pic_progress AS pic_progress,tci_game.price AS price,tci_game_progress.progress AS progress,tci_game_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_game,tci_game_progress,tci_stuff_download WHERE (tci_game_progress.show_public = 1 AND tci_game.id_game = tci_game_progress.id_game AND tci_game.id_game = tci_stuff_download.id_stuff) AND (".$to_add_g.")
        UNION SELECT @type:='app' AS type,tci_app.id_app AS id,tci_app.title AS title,tci_app.sub_title AS sub_title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app.pic_progress AS pic_progress,tci_app.price AS price,tci_app_progress.progress AS progress,tci_app_progress.date_published AS date_published,tci_stuff_download.num AS download FROM tci_app,tci_app_progress,tci_stuff_download WHERE (tci_app_progress.show_public = 1 AND tci_app.id_app = tci_app_progress.id_app AND tci_app.id_app = tci_stuff_download.id_stuff) AND (".$to_add_a.") LIMIT 10";
    }
    $query = mysql_query($sql);
    while ($rs = mysql_fetch_array($query)){
        $pr_pic = $rs['pic_title'];
        $pr_id = $rs['id'];
        $pr_title = $rs['title'];
        $pr_subtitle = getStringFromArray($rs['sub_title']);
        $pr_iddeveloper = $rs['id_developer'];
        $pr_progress = $rs['progress'];
        $pr_price = $rs['price'];
        $pr_datepublished = $rs['date_published'];
        $pr_download = $rs['download'];
        $pr_link = $BASENAME['store'].'view/?id='.$pr_id;
        $result =   $result.'
                            <div class="product-box">
                            <div class="pic-title">
                                <a href="'.$pr_link.'"><img src="'.$BASENAME['img_product'].$pr_iddeveloper.'/'.$pr_id.'/'.$pr_pic.'">
                            </div>
                                <div class="inner">
                                <h5 class="text-big-black">'.$pr_title.'</h5> 
                                <label="text-gray title">'.$TEXT['developed_by'].' '.$pr_iddeveloper.'</label><br>
                                <label="text-gray sub-title">'.$pr_subtitle.'</label>
                                <h4 class="text-green">'.getSym().cvtMoney($pr_price).'</h4>
                                </div>
                                </a>
                            </div>
                            ';
    }
    return $result;
}

function getStoreVoteUnable($uid){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	$result = "";
	if (!empty($_GET['err']) && $_GET['err'] == "buy") $result = "<label class='text-red'>".$TEXT['store_vote_unable']."</label>";
	return $result;
}

function getStringFromArray($data){
	$arr1 = explode(";",$data);
	$lang = getLang();
	if ($lang == "ID"){
		$arr2 = explode("/", $arr1[0]);
		return $arr2[1];
	}else if ($lang == "EN"){
		$arr2 = explode("/", $arr1[1]);
		return $arr2[1];
	}	
}

function getStringLang($lang,$data){
    $arr1 = explode(";",$data);
    if ($lang == "ID"){
        $arr2 = explode("/", $arr1[0]);
        return $arr2[1];
    }else if ($lang == "EN"){
        $arr2 = explode("/", $arr1[1]);
        return $arr2[1];
    }   
}

function getSym(){
	if ($_SESSION['curr'] == "USD") return "$";
	else if ($_SESSION['curr'] == "IDR") return "Rp.";
}

function getTotalVote($uid){
	$id = mysql_real_escape_string($uid);
	$query = mysql_query("SELECT * FROM user_vote_stuff WHERE id_stuff='$id'");
	return mysql_num_rows($query);
}

function getTotalReview($uid){
    $id = mysql_real_escape_string($uid);
    $query = mysql_query("SELECT @temp FROM user_review WHERE id_stuff = '$id'");
    return mysql_num_rows($query);
}

function getUserData($id){ // It will return User data from user_user table to array.
    $usr_id = mysql_real_escape_string($id);
    if (!$query = mysql_query("SELECT * FROM user_user LEFT JOIN user_user_balance ON user_user_balance.id_user = user_user.id_user WHERE user_user.id_user = $usr_id")) goPage("error","?err=data");
    $result = mysql_fetch_array($query);
    return $result;
}

function getUserIdByUsername($username){
    connectDb();
    $username = mysql_real_escape_string($username);
    $query = mysql_query("SELECT id_user FROM user_login WHERE username = '$username'");
    if (mysql_num_rows($query) == 1){
        $rs = mysql_fetch_array($query);
        return $rs['id_user'];
    }else return false;
}

function getUserLoginData($usr_id){
    connectDb();
    $usr_id = mysql_real_escape_string($usr_id);
    $query = mysql_query("SELECT * FROM user_login WHERE id_user = $usr_id");
    return mysql_fetch_array($query);
}

function getSignupUsernameExist(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	$result = "";
	if (!empty($_GET['username'])){
		$result = '<p class="text-red">'.$TEXT['signup_username_exist'].'</p>';
	}
	return $result;
}

function getUsrProductBought($usr_id){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    $usr_id = mysql_real_escape_string($usr_id);
    $result = false;
    $query = mysql_query("SELECT id_stuff FROM user_user_transaction WHERE id_user = $usr_id LIMIT 6");
    if (mysql_num_rows($query) > 0){
        while ($rs = mysql_fetch_array($query)) {
            $x = $rs['id_stuff'];
            $to_add_g = "tci_game.id_game = '$x'";
            $to_add_a = "tci_app.id_app = '$x'";
            $to_add_c = "tci_content.id_content = '$x'";
            $to_add_d = "tci_dlc.id_dlc = '$x'";
            mysql_query("SET @ptype:='game'");
            $query2 = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label FROM tci_game,tci_game_progress WHERE tci_game.id_game = tci_game_progress.id_game AND (".$to_add_g.")
            UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label FROM tci_dlc,tci_dlc_progress WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND (".$to_add_d.")
            UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label FROM tci_content,tci_content_progress WHERE tci_content.id_content = tci_content_progress.id_content AND (".$to_add_c.")
            UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label FROM tci_app,tci_app_progress WHERE tci_app.id_app = tci_app_progress.id_app AND (".$to_add_a.") LIMIT 10") or die (mysql_error());
            $rs2 = mysql_fetch_array($query2);
            $sl_type = $rs2['@ptype'];
            $sl_pic = $rs2['pic_title'];
            $sl_id = $rs2['id'];
            $sl_title = $rs2['title'];
            $sl_iddeveloper = $rs2['id_developer'];
            $sl_progress = $rs2['progress'];
            $sl_label = $rs2['label'];
            if ($rs2['@ptype'] == "game") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs2['@ptype'] == "dlc") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs2['@ptype'] == "content") {
                $sl_link = $BASENAME['content'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['content'];
            }
            else if ($rs2['@ptype'] == "app") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            $sl_linkdev = $BASENAME['developer'].'view/?id='.$sl_iddeveloper;
            echo    '<div class="progress-slide">
                    <a href="'.$sl_link.'"><img src="'.$BASENAME['img_product'].$sl_iddeveloper.'/'.$sl_id.'/'.$sl_pic.'"></a>
                    <a href="'.$sl_linktype.'"><button class="labeling label-type">'.$TEXT['home_progressslide_type'].$sl_type.'</button></a><br>
                    <a href="'.$sl_link.'"><button class="labeling label-title">'.$sl_title.'</button></a><br>
                    <a href="'.$sl_linkdev.'"><button class="labeling label-dev">'.$TEXT['home_progressslide_dev'].$sl_iddeveloper.'</button></a><br>
                    <div class="progress">
                        <button class="label-progress" style="width:'.$sl_progress.'%'.';min-width:15%;">'.$sl_progress.'%'.'</button>
                    </div>
                    <button class="labeling label-label">'.$TEXT['home_progressslide_status'].$sl_label.'</button>
                    </div>
                    ';
            $result = true;
        }
    }
    return $result;
}

function getUsrProductBoughtAll($usr_id){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    $usr_id = mysql_real_escape_string($usr_id);
    $result = false;
    $query = mysql_query("SELECT id_stuff FROM user_user_transaction WHERE id_user = $usr_id");
    if (mysql_num_rows($query) > 0){
        while ($rs = mysql_fetch_array($query)) {
            $x = $rs['id_stuff'];
            $to_add_g = "tci_game.id_game = '$x'";
            $to_add_a = "tci_app.id_app = '$x'";
            $to_add_c = "tci_content.id_content = '$x'";
            $to_add_d = "tci_dlc.id_dlc = '$x'";
            mysql_query("SET @ptype:='game'");
            $query2 = mysql_query("SELECT @ptype,tci_game.id_game AS id,tci_game.title AS title,tci_game.id_developer AS id_developer,tci_game.pic_title AS pic_title,tci_game_progress.progress AS progress,tci_game_progress.label AS label FROM tci_game,tci_game_progress WHERE tci_game.id_game = tci_game_progress.id_game AND (".$to_add_g.")
            UNION SELECT @ptype:='dlc',tci_dlc.id_dlc AS id,tci_dlc.title AS title,tci_dlc.id_developer AS id_developer,tci_dlc.pic_title AS pic_title,tci_dlc_progress.progress AS progress,tci_dlc_progress.label AS label FROM tci_dlc,tci_dlc_progress WHERE tci_dlc.id_dlc = tci_dlc_progress.id_dlc AND (".$to_add_d.")
            UNION SELECT @ptype:='content',tci_content.id_content AS id,tci_content.title AS title,tci_content.id_developer AS id_developer,tci_content.pic_title AS pic_title,tci_content_progress.progress AS progress,tci_content_progress.label AS label FROM tci_content,tci_content_progress WHERE tci_content.id_content = tci_content_progress.id_content AND (".$to_add_c.")
            UNION SELECT @ptype:='app',tci_app.id_app AS id,tci_app.title AS title,tci_app.id_developer AS id_developer,tci_app.pic_title AS pic_title,tci_app_progress.progress AS progress,tci_app_progress.label AS label FROM tci_app,tci_app_progress WHERE tci_app.id_app = tci_app_progress.id_app AND (".$to_add_a.") LIMIT 10") or die (mysql_error());
            $rs2 = mysql_fetch_array($query2);
            $sl_type = $rs2['@ptype'];
            $sl_pic = $rs2['pic_title'];
            $sl_id = $rs2['id'];
            $sl_title = $rs2['title'];
            $sl_iddeveloper = $rs2['id_developer'];
            $sl_progress = $rs2['progress'];
            $sl_label = $rs2['label'];
            if ($rs2['@ptype'] == "game") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs2['@ptype'] == "dlc") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            else if ($rs2['@ptype'] == "content") {
                $sl_link = $BASENAME['content'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['content'];
            }
            else if ($rs2['@ptype'] == "app") {
                $sl_link = $BASENAME['store'].'view/?id='.$sl_id;
                $sl_linktype = $BASENAME['store'];
            }
            $sl_linkdev = $BASENAME['developer'].'view/?id='.$sl_iddeveloper;
            echo    '<div class="progress-slide">
                    <a href="'.$sl_link.'"><img src="'.$BASENAME['img_product'].$sl_iddeveloper.'/'.$sl_id.'/'.$sl_pic.'"></a>
                    <a href="'.$sl_linktype.'"><button class="labeling label-type">'.$TEXT['home_progressslide_type'].$sl_type.'</button></a><br>
                    <a href="'.$sl_link.'"><button class="labeling label-title">'.$sl_title.'</button></a><br>
                    <a href="'.$sl_linkdev.'"><button class="labeling label-dev">'.$TEXT['home_progressslide_dev'].$sl_iddeveloper.'</button></a><br>
                    <div class="progress">
                        <button class="label-progress" style="width:'.$sl_progress.'%'.';min-width:15%;">'.$sl_progress.'%'.'</button>
                    </div>
                    <button class="labeling label-label">'.$TEXT['home_progressslide_status'].$sl_label.'</button>
                    </div>
                    ';
            $result = true;
        }
    }
    return $result;
}

function getUsrReviewActivity($usr_id){
    if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
    else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
    else include 'lang/EN.php';
    include 'variable.php';
    connectDb();
    $usr_id = mysql_real_escape_string($usr_id);
    $query = mysql_query("SELECT * FROM user_review WHERE id_user = $usr_id LIMIT 6");
    $result = false;
    if (mysql_num_rows($query) > 0){
        $result = true;
        echo "<table class='table-w100-h20-blue center'>
        <tr>
            <th>".$TEXT2['data_product']."</th>
            <th>".$TEXT2['data_review']."</th>
        </th>";
        while ($rs = mysql_fetch_array($query)) {
            $t_review = mb_strimwidth($rs['text'], 0, 150, "...");
            $x_pid = $rs['id_stuff'];
            $x_pdata = getDevProductData($x_pid);
            $t_pic = $BASENAME['img_product'].$x_pdata['id_developer'].'/'.$x_pid.'/'.$x_pdata['pic_title'];
            if ($x_pdata['type'] == "game") {
                $t_link = $BASENAME['store'].'view/?id='.$x_pid;
            }
            else if ($x_pdata['type'] == "dlc") {
                $t_link = $BASENAME['store'].'view/?id='.$x_pid;
            }
            else if ($x_pdata['type'] == "content") {
                $t_link = $BASENAME['content'].'view/?id='.$x_pid;
            }
            else if ($x_pdata['type'] == "app") {
                $t_link = $BASENAME['store'].'view/?id='.$x_pid;
            }
            echo "<tr>
                    <td style='width: 35%;'><a href='$t_link'><img src='".$t_pic."' class='img-w70-h80-hoverable cursor-hand'></a></td>
                    <td>".$t_review."</td>
                </tr>";
        }
        echo "</table>";
    }
    return $result;
}

function getUsrUsername(){
	if (!empty($_SESSION['usr_id'])){
		$usr_id = $_SESSION['usr_id'];
		$query = mysql_query("SELECT username FROM user_login WHERE id_user='$usr_id'");
		$rs = mysql_fetch_array($query);
		return $rs['username'];
	}else{
		return "";
	}
}

function getWrongImgLabel(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	$result = "";
	if (!empty($_GET['img'])){
		if ($_GET['img'] == "not"){
			$result = '<p class="text-red">'.$TEXT['signup_img_wrong_label'].'</p>';
		}
	}
	return $result;
}

function getWrongLoginLabel(){
	if ($_SESSION['lang'] == "ID") include 'lang/ID.php';
	else if ($_SESSION['lang'] == "EN") include 'lang/EN.php';
	else include 'lang/EN.php';
	$result = "";
	if (!empty($_GET['false'])){
		$result = '<p class="text-red">'.$TEXT['login_label_wrong'].'</p>';
	}
	return $result;
}

// END OF GETTER - FUNCTION
// ----------SEPERATOR----------
// BEGINNING OF SETTER - FUNCTION