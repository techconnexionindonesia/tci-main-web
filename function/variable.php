<?php
/*Variable models
--Tech Connexion Indonesia Website--
--Created by TCI Programming Division--
--Tech Connexion Indonesia Group--
--(c) Andreas Alexander Kaawoan (Executive Director) TCI Group--
--Process with function PHP 5 Language--
*/

/* Environment */
defined('ENVIRONMENT') OR define('ENVIRONMENT', 'production');
// define('ENVIRONMENT', 'production');

//BASENAME ; Link Location
//Server Address

/* Develop */
if (ENVIRONMENT == 'development')
	$BASENAME['server'] = "http://tci.local/";

/* Sandbox */
if (ENVIRONMENT == 'sandbox')
	$BASENAME['server'] = "http://sandboxtci.esy.es/";

/* Production */
if (ENVIRONMENT == 'production')
	$BASENAME['server'] = "https://www.techconnexionindonesia.com/";

//Main Folder -- Sorted by a-Z
//dr prefix means direct file access link
$BASENAME['about_us'] = $BASENAME['server']."aboutus/";
$BASENAME['about_us_aboutus'] = $BASENAME['server']."aboutus/?s=aboutus";
$BASENAME['about_us_project'] = $BASENAME['server']."aboutus/?s=project";
$BASENAME['about_us_rule'] = $BASENAME['server']."aboutus/?s=rule";
$BASENAME['cart'] = $BASENAME['server']."cart/";
$BASENAME['ch_lang'] = $BASENAME['server']."language/";
$BASENAME['content'] = $BASENAME['server']."content/";
$BASENAME['devconsole'] = $BASENAME['server']."devconsole/";
$BASENAME['devconsole_activity'] = $BASENAME['server']."devconsole/activity/";
$BASENAME['devconsole_config'] = $BASENAME['server']."devconsole/config/";
$BASENAME['devconsole_confirm'] = $BASENAME['server']."devconsole/confirm/";
$BASENAME['devconsole_finance'] = $BASENAME['server']."devconsole/finstas/";
$BASENAME['devconsole_login'] = $BASENAME['server']."devconsole/login/";
$BASENAME['devconsole_product'] = $BASENAME['server']."devconsole/product/";
$BASENAME['devconsole_signup'] = $BASENAME['server']."devconsole/signup/";
$BASENAME['devconsole_wait'] = $BASENAME['server']."devconsole/wait/";
$BASENAME['developer'] = $BASENAME['server']."developer/";
$BASENAME['download'] = $BASENAME['server']."download/?id=";
$BASENAME['dr_css'] = $BASENAME['server']."css/style.css";
$BASENAME['dr_form_function'] = $BASENAME['server']."function/form.php";
$BASENAME['dr_response_payment_ipaymu'] = $BASENAME['server']."function/payment/ipaymu.php";
$BASENAME['error'] = $BASENAME['server']."error/";
$BASENAME['index'] = $BASENAME['server']."";
$BASENAME['js'] = $BASENAME['server']."js/";
$BASENAME['login'] = $BASENAME['server']."login/";
$BASENAME['logout'] = $BASENAME['server']."logout/";
$BASENAME['my_profile'] = $BASENAME['server']."myprofile/";
$BASENAME['my_profile_security'] = $BASENAME['server']."myprofile/security/";
$BASENAME['my_profile_topup'] = $BASENAME['server']."myprofile/topup/";
$BASENAME['my_transaction'] = $BASENAME['server']."myprofile/transaction/";
$BASENAME['news'] = $BASENAME['server']."news/";
$BASENAME['product'] = $BASENAME['server']."product/";
$BASENAME['restricted'] = $BASENAME['server']."restricted/";
$BASENAME['restricted-admin'] = $BASENAME['server']."admin/restricted/";
$BASENAME['signup'] = $BASENAME['server']."signup/";
$BASENAME['signup_developer'] = $BASENAME['server']."devconsole/signup/";
$BASENAME['signup_success'] = $BASENAME['signup']."success/";
$BASENAME['store'] = $BASENAME['server']."store/";
$BASENAME['temp'] = $BASENAME['server']."temp_dir/";
$BASENAME['user'] = $BASENAME['server']."user/";
//Other Bases Folder -- Sorted by a-Z
$BASENAME['img_avatar'] = $BASENAME['server']."img/avatar/";
$BASENAME['img_crew'] = $BASENAME['server']."img/crew/";
$BASENAME['img_dev'] = $BASENAME['server']."img/dev/";
$BASENAME['img_html'] = $BASENAME['server']."img_html/";
$BASENAME['img_news'] = $BASENAME['server']."img/news/";
$BASENAME['img_product'] = $BASENAME['server']."img/product/";
$BASENAME['img_slide'] = $BASENAME['server']."img/slide/";	
$BASENAME['img_structure'] = $BASENAME['server']."img/structure/";	

/* --- Site outside TCI --- */
if (ENVIRONMENT == 'production'){
	$SITE['ipaymu_api'] = "https://my.ipaymu.com/";
	
} else {
	$SITE['ipaymu_api'] = "https://sandbox.ipaymu.com/";
}
$SITE['ipaymu_api_payment'] = $SITE['ipaymu_api']."payment";
$SITE['ipaymu_check_transaction'] = $SITE['ipaymu_api']."api/transaksi";
?>