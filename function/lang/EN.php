<?php
/*
--English Language Variable Texts--
--Tech Connexion Indonesia Website--
--Created by TCI Programming Division--
--Tech Connexion Indonesia Group--
--(c) Andreas Alexander Kaawoan (Executive Director) TCI Group--
*/

//English
//Sorted by a-Z
//Note:Fixed Word mean it shouldn't or no need to change by language
$TEXT['about_menu_about'] = "About Us";
$TEXT['about_menu_contact'] = "Contact Us";
$TEXT['about_menu_director'] = "Director Words";
$TEXT['about_menu_relation'] = "Businnes Relation";
$TEXT['about_menu_faq'] = "FAQ"; //Fixed Word
$TEXT['about_menu_joinasdev'] = "Join As Developer";
$TEXT['about_menu_joinasuser'] = "Join As User";
$TEXT['about_menu_project'] = "Our Project";
$TEXT['about_menu_rule'] = "Terms and Conditions";
$TEXT['about_menu_service'] = "TCI Services";
$TEXT['about_menu_structure'] = "Organization Structure";
$TEXT['about_project_search'] = "Search for TCI project here";
$TEXT['about_project_sort_dp'] = "Date Published";
$TEXT['about_project_sort_ds'] = "Date Started";
$TEXT['about_project_sort_p'] = "Popularity";
$TEXT['about_rule_fordev'] = "Developer Terms & Conditions";
$TEXT['about_rule_foruser'] = "User Terms & Conditions";
$TEXT['about_rule_title'] = "What T&C Used For?";
$TEXT['about_structure_select'] = "Select Structure :";
$TEXT['activity'] = "Activity";
$TEXT['add_to_cart'] = "Add to cart";
$TEXT['addupdate'] = "Add/Update";
$TEXT['alert'] = "Alert";
$TEXT['alert_en'] = "Should be in English";
$TEXT['app'] = "Application";
$TEXT['automatic'] = "Automatic";
$TEXT['balance'] = "Balance";
$TEXT['cart'] = "Cart";
$TEXT['cart_alert_delete'] = "You will delete this single product from your cart";
$TEXT['cart_alert_delete_all'] = "You will delete all of product of your cart";
$TEXT['cart_alert_pay'] = "You will pay this single product of your cart";
$TEXT['cart_alert_pay_all'] = "You will pay all of product of your cart";
$TEXT['cart_balance'] = "Your current balance:";
$TEXT['cart_balance_alert'] = "Your balance is not enought to buy. Please recharge your balance";
$TEXT['cart_bought'] = "just bought";
$TEXT['cart_delete'] = "Remove All Cart";
$TEXT['cart_delete_single'] = "Remove";
$TEXT['cart_none'] = "No cart here";
$TEXT['cart_pay'] = "Pay All";
$TEXT['cart_pay_single'] = "Pay";
$TEXT['cart_search'] = "Search Cart";
$TEXT['cart_total'] = "Total of your cart:";
$TEXT['category'] = "Category";
$TEXT['close'] = "Close";
$TEXT['content'] = "Content";
$TEXT['content_all'] = "Content & DLC";
$TEXT['content_bs'] = "Best Seller";
$TEXT['content_c_gtasa'] = "GTA:SA"; //fixed word
$TEXT['content_c_trs'] = "Trainz Simulator"; //fixed work
$TEXT['content_c'] = "Content";
$TEXT['content_d'] = "DLC";
$TEXT['content_newest'] = "Newest";
$TEXT['content_of'] = "of";
$TEXT['content_progress'] = "Progress";
$TEXT['content_search'] = "Search for Content and DLC here";
$TEXT['content_showall'] = "All";
$TEXT['content_trending'] = "Trending";
$TEXT['continue'] = "Continue";
$TEXT['delete'] = "Delete";
$TEXT['devconsole_activity'] = "Activity";
$TEXT['devconsole_agree'] = "By registering as Developer, you agreed to terms and condition that apply";
$TEXT['devconsole_bank'] = "Bank Name";
$TEXT['devconsole_bankchange_info'] = "*Bank account data only can be changed by owner";
$TEXT['devconsole_bankname'] = "Bank Account Holder Name";
$TEXT['devconsole_bankno'] = "Bank Account Number";
$TEXT['devconsole_createdev'] = "Create Developer";
$TEXT['devconsole_createp'] = "Create for";
$TEXT['devconsole_config'] = "Configuration";
$TEXT['devconsole_config_admin'] = "Administrator Configuration";
$TEXT['devconsole_err_usrpw'] = "The password for verification is wrong";
$TEXT['devconsole_err_exist'] = "Sorry, ID or name of developer is used by other";
$TEXT['devconsole_err_create'] = "Please check the item on the checkbox for product that you create";
$TEXT['devconsole_err_imgnot'] = "File that you upload is not an image";
$TEXT['devconsole_err_imgsize'] = "Image that you upload is too large, maximal size is 1MB";
$TEXT['devconsole_err_imgtype'] = "File type that you upload is not the format that approved";
$TEXT['devconsole_finance'] = "Finance Statistic";
$TEXT['devconsole_iddev'] = "ID Developer";
$TEXT['devconsole_logo'] = "Developer Logo";
$TEXT['devconsole_password'] = "Password Developer";
$TEXT['devconsole_personaldata'] = "Data Pribadi";
$TEXT['devconsole_place'] = "Place of Work";
$TEXT['devconsole_product'] = "Product";
$TEXT['devconsole_signup'] = "Signup As Developer";
$TEXT['devconsole_site'] = "Developer Site<br>(In case you have)";
$TEXT['devconsole_structure'] = "Developer Structure";
$TEXT['devconsole_type'] = "Developer type";
$TEXT['devconsole_typeplc_any'] = "Anywhere";
$TEXT['devconsole_typeplc_home'] = "Home";
$TEXT['devconsole_typeplc_office'] = "Office";
$TEXT['devconsole_typestc_company'] = "Company";
$TEXT['devconsole_typestc_single'] = "Single";
$TEXT['devconsole_typestc_team'] = "Team";
$TEXT['devconsole_wait_validation'] = "At this time, the Developer that you register is waiting for TCI to validate it. If your Developer data have been validated, so you could use the Developer console to publish your product. At the time you redirected to Developer login page, that mean your Developer data have been validated. Please check it by later.";
$TEXT['devconsole_welcome'] = "Welcome to the Developer Console";
$TEXT['developed_by'] = "Developed By";
$TEXT['dlc'] = "DLC";
$TEXT['download'] = "Download";
$TEXT['error_title'] = "ERROR";
$TEXT['error_txt_404'] = "Sorry, the page you accessed is not available. It is possibly because the page was removed or expired or even under construction.";
$TEXT['error_txt_data'] = "Sorry, you can't access this page because there is problem with the central data. Please contact administrator at admin@techconnexionindonesia.com .";
$TEXT['error_txt_db'] = "Sorry, there is problem with the Database. Please contact administrator at admin@techconnexionindonesia.com .";
$TEXT['error_txt_default'] = "Sorry, there is an error. Please contact administrator at admin@techconnexionindonesia.com .";
$TEXT['error_txt_paging'] = "Sorry, you can't access this page. If any problem, please contact administrator at admin@techconnexionindonesia.com . Please go back to home page";
$TEXT['error_txt_paging'] = "Sorry, you can't access this page. If any problem, please contact administrator at admin@techconnexionindonesia.com . Please go back to home page";
$TEXT['error_txt_profile'] = "Sorry, error occured with your profile data. For security reason, you are logged out automacilly, but your profile data not deleted. Please contact administrator at admin@techconnexionindonesia.com";
$TEXT['filter'] = "Filter";
$TEXT['game'] = "Game";
$TEXT['gamedlc'] = "Game & Dlc";
$TEXT['genre'] = "Genre";
$TEXT['home_content_select_language'] = "Select Language & Currency";
$TEXT['home_content_news'] = "News";
$TEXT['home_content_news_by1'] = "written by";
$TEXT['home_content_news_by2'] = "from";
$TEXT['home_content_news_more'] = "View all news";
$TEXT['home_content_onprogress'] = "On Progress";
$TEXT['home_content_progressmore'] = "Projects on progress";
$TEXT['home_content_ra_more'] = "View more";
$TEXT['home_content_trending'] = "Trending";
$TEXT['home_content_recentactivities'] = "Recent Activities";
$TEXT['home_progressslide_dev'] = "Dev:"; //Fixed Word
$TEXT['home_progressslide_status'] = "Status:";
$TEXT['home_progressslide_type'] = "Type:";
$TEXT['home_slide_view'] = "View Now";
$TEXT['id'] = "ID"; //Fixed Word
$TEXT['login_label_wrong'] = "Your username or password is incorrect";
$TEXT['login_ph_password'] = "Please input Password";
$TEXT['login_ph_username'] = "Please input Username";
$TEXT['login_signup'] = "Don't have TCI account yet ? Create now !";
$TEXT['login_title'] = "LOGIN"; //Fixed Word
$TEXT['navbar_aboutus'] = "ABOUT TCI";
$TEXT['navbar_content'] = "GAME CONTENT";
$TEXT['navbar_home'] = "HOME";
$TEXT['navbar_login'] = "Login";
$TEXT['navbar_news'] = "NEWS";
$TEXT['navbar_signup'] = "Signup";
$TEXT['navbar_store'] = "GAME & APP";
$TEXT['news'] = "News";
$TEXT['news_all'] = "All";
$TEXT['news_app'] = "App";
$TEXT['news_content'] = "Content";
$TEXT['news_developer'] = "Developer";
$TEXT['news_dlc'] = "DLC";
$TEXT['news_game'] = "Game";
$TEXT['news_general'] = "General";
$TEXT['news_important'] = "Important";
$TEXT['news_newest'] = "Newest";
$TEXT['news_older'] = "Older";
$TEXT['news_open_link'] = "Open link";
$TEXT['news_open_profile_writer'] = "Open writer profile";
$TEXT['news_photos'] = "Photos";
$TEXT['news_project'] = "Project";
$TEXT['news_reaction'] = "How is your reaction?";
$TEXT['news_release'] = "Release";
$TEXT['news_search'] = "Search for news here";
$TEXT['news_server'] = "Server";
$TEXT['news_share'] = "Share this news!";
$TEXT['news_tci'] = "From TCI";
$TEXT['news_update'] = "Update";
$TEXT['news_visited1'] = "Visited";
$TEXT['news_visited2'] = "times";
$TEXT['news_voted'] = "did a vote at a news";
$TEXT['no'] = "No";
$TEXT['open_profile_dev'] = "Open Developer profile";
$TEXT['option'] = "Option";
$TEXT['otomatic'] = "Automatic";
$TEXT['devconsole_paypal_a'] = "Paypal Account";
$TEXT['price'] = "Price";
$TEXT['product_g_a_search'] = "Search for Game and App here";
$TEXT['project_published'] = "Date published";
$TEXT['project_started'] = "Date started project";
$TEXT['reset'] = "Reset";
$TEXT['signup_address'] = "Address";
$TEXT['signup_answer1'] = "Answer";
$TEXT['signup_answer2'] = "Answer";
$TEXT['signup_back_name'] = "Back name";
$TEXT['signup_birth'] = "Birth date";
$TEXT['signup_country'] = "Country";
$TEXT['signup_data'] = "Login data";
$TEXT['signup_description'] = "Description";
$TEXT['signup_email'] = "E-mail"; //Fixed Word
$TEXT['signup_front_name'] = "Front name";
$TEXT['signup_img'] = "Profile picture";
$TEXT['signup_img_wrong_label'] = "The file you has been upload is not an image";
$TEXT['signup_joined'] = "joined with TCI";
$TEXT['signup_name'] = "Name";
$TEXT['signup_password'] = "Password";
$TEXT['signup_password_re'] = "Verifiy password";
$TEXT['signup_personal'] = "Your personal data";
$TEXT['signup_phone'] = "Phone Number";
$TEXT['signup_question1'] = "Security question 1";
$TEXT['signup_question2'] = "Security question 2";
$TEXT['signup_re_wrong'] = "Password are not same";
$TEXT['signup_required'] = "* (Must be filled)";
$TEXT['signup_signature'] = "Signature";
$TEXT['signup_subscription'] = "I want to get current news from TCI by Email";
$TEXT['signup_success_text1'] = "Congratulations, you have finish your registration with the following data :";
$TEXT['signup_success_text2'] = "Your ID";
$TEXT['signup_success_text3'] = "Your Username";
$TEXT['signup_success_text4'] = "Please remember, by registering to TCI, you agreed to our Terms and Condision. You now can access your account by open Profile menu and buy a new game. If you have any question or problem, please contact administrator at admin@techconnexionindonesia.com";
$TEXT['signup_success_title'] = "SIGNUP SUCCESS";
$TEXT['signup_terms'] = "By registering on the TCI website, you agreed to the Terms and Conditions that apply";
$TEXT['signup_title'] = "SIGNUP"; //Fixed Word
$TEXT['signup_username'] = "Username";
$TEXT['signup_username_exist'] = "Sorry, username has been used";
$TEXT['signup_zipcode'] = "Zipcode";
$TEXT['sort_by'] = "Sort By :";
$TEXT['statistic'] = "Statistic";
$TEXT['status'] = "Status";
$TEXT['store_a'] = "App";
$TEXT['store_bs'] = "Best Seller";
$TEXT['store_g'] = "Games";
$TEXT['store_g_a_all'] = "All Product";
$TEXT['store_g_a_showall'] = "All";
$TEXT['store_g_all'] = "All Genre";
$TEXT['store_g_action'] = "Action"; // Fixed Word
$TEXT['store_g_adventure'] = "Adventure"; // Fixed Word
$TEXT['store_g_arcade'] = "Arcade"; // Fixed Word
$TEXT['store_g_all'] = "All Genre";
$TEXT['store_g_fight'] = "Fight"; // Fixed Word
$TEXT['store_g_fps'] = "FPS"; // Fixed Word
$TEXT['store_g_horror'] = "Horror"; // Fixed Word
$TEXT['store_g_multiplayer'] = "Multiplayer"; // Fixed Word
$TEXT['store_g_racing'] = "Racing"; // Fixed Word
$TEXT['store_g_rpg'] = "RPG"; // Fixed Word
$TEXT['store_g_scifi'] = "Sci Fi"; // Fixed Word
$TEXT['store_g_shooter'] = "Shooter"; // Fixed Word
$TEXT['store_g_simulation'] = "Simulation"; // Fixed Word
$TEXT['store_g_stealth'] = "Stealth"; // Fixed Word
$TEXT['store_g_story'] = "Story"; // Fixed Word
$TEXT['store_g_strategy'] = "Strategy"; // Fixed Word
$TEXT['store_g_tps'] = "TPS"; // Fixed Word
$TEXT['store_g_tycoon'] = "Tycoon"; // Fixed Word
$TEXT['store_in_cart'] = "Already in cart";
$TEXT['store_rate'] = "Give a vote !";
$TEXT['store_rating1'] = "Rating";
$TEXT['store_rating2'] = "from total";
$TEXT['store_rating3'] = "votes liked this";
$TEXT['store_review_placeholder'] = "Add your review about this product";
$TEXT['store_progress'] = "Progress";
$TEXT['store_total_download'] = "Total download";
$TEXT['store_trending'] = "Trending";
$TEXT['store_vote_unable'] = "You should buy this product before submit review or vote";
$TEXT['submit'] = "SUBMIT"; //Fixed Word
$TEXT['validation'] = "Validation";
$TEXT['tags'] = "Tags"; //Fixed Word
$TEXT['yes'] = "Yes";

//TEXT2
$TEXT2['administrator'] = "Administrator";
$TEXT2['cmd_accept'] = "Accept";
$TEXT2['cmd_apply_update'] = "Apply Update";
$TEXT2['cmd_back_prevpage'] = "Back to previous page";
$TEXT2['cmd_reject'] = "Reject";
$TEXT2['cmd_select_country_update'] = "Select country to update / leave if you wish not to update";
$TEXT2['cmd_leave_to_not'] = "Leave if you wish not to update this";
$TEXT2['confirm'] = "Confirm";
$TEXT2['confirm_1'] = "Confirmed";
$TEXT2['confirm_0'] = "Not Confirmed Yet";
$TEXT2['created'] = "Created";
$TEXT2['data_balance'] = "Balance";
$TEXT2['data_balance_e'] = "Initial Balance";
$TEXT2['data_balance_f'] = "Final Balance";
$TEXT2['data_bankdest'] = "Destination Bank";
$TEXT2['data_bankname'] = "Bank Account Holder";
$TEXT2['data_bankname_plc'] = "e.g : Abdi Abdullah";
$TEXT2['data_banknosender'] = "Bank Account Number / Transfer Number";
$TEXT2['data_banknosender_plc'] = "e.g : 0102031122331234";
$TEXT2['data_banksender'] = "Sender Bank";
$TEXT2['data_banksender_plc'] = "e.g : Bank Mandiri";
$TEXT2['data_country'] = "Country";
$TEXT2['data_currency'] = "Currency";
$TEXT2['data_datetime'] = "Date / Time";
$TEXT2['data_description'] = "Description";
$TEXT2['data_dev_rating'] = "Developer Rating";
$TEXT2['data_dev_related'] = "Developer Related";
$TEXT2['data_email'] = "E-Mail"; //Fixed Word
$TEXT2['data_id'] = "ID";
$TEXT2['data_info'] = "Info";
$TEXT2['data_name'] = "Name";
$TEXT2['data_note'] = "Note";
$TEXT2['data_password_dev'] = "Developer Password";
$TEXT2['data_password_new'] = "New Password";
$TEXT2['data_password_new_re'] = "Verify New Password";
$TEXT2['data_password_old'] = "Old Password";
$TEXT2['data_personal'] = "Personal Data";
$TEXT2['data_product'] = "Product";
$TEXT2['data_product_bought'] = "Product Bought";
$TEXT2['data_profile'] = "Profile";
$TEXT2['data_profile_security'] = "Profile Security";
$TEXT2['data_registered'] = "Registered";
$TEXT2['data_review'] = "Review";
$TEXT2['data_security'] = "Security Data";
$TEXT2['data_signature'] = "Signature";
$TEXT2['data_total_app'] = "Total Application";
$TEXT2['data_total_dlc'] = "Total DLC";
$TEXT2['data_total_content'] = "Total Content";
$TEXT2['data_total_game'] = "Total Game";
$TEXT2['data_total_product'] = "Total Product";
$TEXT2['data_total_rating'] = "Total Rating";
$TEXT2['data_total_topup'] = "Topup Balance Total";
$TEXT2['data_total_topup_count'] = "Total Activity of Topup";
$TEXT2['data_total_vote_count'] = "Total Vote Activity";
$TEXT2['data_transaction'] = "Transaction";
$TEXT2['data_transfer_date'] = "Date of Transfer";
$TEXT2['data_transfer_proof'] = "Transfer Proof (Picture)";
$TEXT2['data_transfer_total'] = "Jumlah Transfer";
$TEXT2['data_transfer_total_plc'] = "e.g : 50.000 or 5.00";
$TEXT2['data_vote'] = "Vote";
$TEXT2['data_website'] = "Website"; //Fixed Word
$TEXT2['devconsole_add_admin_username'] = "Add Administrator by Username";
$TEXT2['devconsole_admin_confirm'] = "Click 'Accept' to confirm invitation, or click 'Reject' to reject the invitation. <br>You can contact the developer by E-Mail :";
$TEXT2['devconsole_admin_invitation'] = "You have been invited to be Administrator of Developer :";
$TEXT2['devconsole_admin_joined'] = "has been joined with developer";
$TEXT2['devconsole_admin_reject'] = "rejected to joined with developer";
$TEXT2['devconsole_administrator_list'] = "Administrator List";
$TEXT2['devconsole_administrator_num_info'] = "*Administrator maximal up to 5";
$TEXT2['devconsole_avt_actdeveloper'] = "Developer Activity";
$TEXT2['devconsole_avt_activity'] = "Activity";
$TEXT2['devconsole_avt_actreview'] = "Review Activity";
$TEXT2['devconsole_avt_acttransaction'] = "Transaction Activity";
$TEXT2['devconsole_avt_actvote'] = "Voting Activity";
$TEXT2['devconsole_avt_admin'] = "Admin"; //Fixed Word
$TEXT2['devconsole_avt_close'] = "Close the display";
$TEXT2['devconsole_avt_info'] = "Information";
$TEXT2['devconsole_avt_more_admin'] = "See Full Activity";
$TEXT2['devconsole_avt_more_review'] = "See Full Review";
$TEXT2['devconsole_avt_more_transaction'] = "See Full Transaction";
$TEXT2['devconsole_avt_more_vote'] = "See Full Vote";
$TEXT2['devconsole_avt_price'] = "Price";
$TEXT2['devconsole_avt_product'] = "Product";
$TEXT2['devconsole_avt_review'] = "Review";
$TEXT2['devconsole_avt_target'] = "Target";
$TEXT2['devconsole_avt_time'] = "Time";
$TEXT2['devconsole_avt_user'] = "ID/User Name";
$TEXT2['devconsole_avt_vote'] = "Vote";
$TEXT2['devconsole_bankdata'] = "Bank Account Data";
$TEXT2['devconsole_code'] = "Project Code";
$TEXT2['devconsole_devdata'] = "Developer Data";
$TEXT2['devconsole_detail_en'] = "Product Detail (in English)";
$TEXT2['devconsole_detail_id'] = "Product Detail (in Indonesia)";
$TEXT2['devconsole_filelink'] = "Product Link";
$TEXT2['devconsole_filelink_info'] = "*Use link if you upload outside TCI. Link that inputted should be direct into the file";
$TEXT2['devconsole_fileproduct'] = "Product File";
$TEXT2['devconsole_fileproduct_info'] = "*File approved are .exe,.zip or .rar and maximal size 5MB. Please upload file to the uploader website pointed by TCI if size more than 5MB";
$TEXT2['devconsole_filereplace_info'] = "*Uploading file or changing the link will update your product file";
$TEXT2['devconsole_fileset'] = "File Setting";
$TEXT2['devconsole_gc'] = "Genre/Category";
$TEXT2['devconsole_gc_info'] = "*Should be in English.<br>If more than 2, seperate by semicolon(;)";
$TEXT2['devconsole_idgame'] = "Game ID (DLC & Content)";
$TEXT2['devconsole_idproduct'] = "Product ID";
$TEXT2['devconsole_log_add_p'] = "Create new product named";
$TEXT2['devconsole_log_del_p'] = "Delete product named";
$TEXT2['devconsole_log_upd_p'] = "Did update at product named";
$TEXT2['devconsole_logincontrol'] = "Login Control";
$TEXT2['devconsole_logincontrol_info'] = "*Please fill data if you would like to change password";
$TEXT2['devconsole_password_new'] = "New Developer Password";
$TEXT2['devconsole_picchange_info'] = "*If you upload picture, will replace that any exist";
$TEXT2['devconsole_picprogress'] = "Image to show on Progress menu";
$TEXT2['devconsole_picprogress_info'] = "*If only the project is on progress";
$TEXT2['devconsole_picreplace_info'] = "*Uploading image will replace that any exist";
$TEXT2['devconsole_picsub'] = "Other Image(s)";
$TEXT2['devconsole_pictitle'] = "Primary Image";
$TEXT2['devconsole_pricedollar'] = "Price in Dollar";
$TEXT2['devconsole_pricedollar_info'] = "*Input through this form<br>Set to 0 if free";
$TEXT2['devconsole_priceexchange'] = "With rate exchange Dollar to Rupiah : ";
$TEXT2['devconsole_pricerupiah'] = "Price in Rupiah";
$TEXT2['devconsole_pricestoredollar'] = "Price at Store in Dollar<br>*(+10% administration fee + $0.31 Payment Gateway Fee)";
$TEXT2['devconsole_pricestorerupiah'] = "Price at Store in Rupiah (+10% administration fee + Rp4030 Payment Gateway Fee)";
$TEXT2['devconsole_pricing'] = "Price Setting";
$TEXT2['devconsole_productclickinfo'] = "*Click product image to view or edit";
$TEXT2['devconsole_product_delete_confirm'] = "Alert ! You will delete the product !";
$TEXT2['devconsole_product_search'] = "Search for your product here";
$TEXT2['devconsole_progressdatefinish'] = "Project Finished";
$TEXT2['devconsole_progressdatestarted'] = "Project Started";
$TEXT2['devconsole_progresslabel'] = "Progress Status";
$TEXT2['devconsole_progressnotfinish'] = "*Automatically checked if project not finished";
$TEXT2['devconsole_progresspercentage'] = "Progress percentage";
$TEXT2['devconsole_progresspercentage_info'] = "*0-100<br>If you set to 100, status automacilly changed to 'Published'";
$TEXT2['devconsole_progressshow'] = "Show in Store";
$TEXT2['devconsole_progressing'] = "Progress Setting";
$TEXT2['devconsole_stas_totalbuyer'] = "Total Buyer";
$TEXT2['devconsole_stas_totaldownload'] = "Total Download";
$TEXT2['devconsole_stas_totalincome'] = "Total income";
$TEXT2['devconsole_stas_totalreview'] = "Total Review";
$TEXT2['devconsole_subtitle_en'] = "Sub Title in English (short description)";
$TEXT2['devconsole_subtitle_id'] = "Sub Title in Indonesia (short description)";
$TEXT2['devconsole_tags'] = "Tags";
$TEXT2['devconsole_title'] = "Title";
$TEXT2['devconsole_type'] = "Type of Product";
$TEXT2['devconsole_verification_info'] = "*Verify password once you apply update";
$TEXT2['developer_search_by'] = "Search for Product created by";
$TEXT2['in_process'] = "In Process";
$TEXT2['joined_with'] = "Joined with";
$TEXT2['label_product_vote'] = "did vote to a product";
$TEXT2['nav_balance'] = "Balance";
$TEXT2['nav_cart'] = "Cart";
$TEXT2['nav_dev'] = "Developer";
$TEXT2['nav_logout'] = "Logout";
$TEXT2['nav_myprofile'] = "My Profile";
$TEXT2['nav_product'] = "Product Bought";
$TEXT2['title_congratulation'] = "Congratulations !";
$TEXT2['search_transaction'] = "Search for your transaction here";
$TEXT2['see_more'] = "See More";
$TEXT2['select'] = "Select";
$TEXT2['topup_amount'] = "Topup Amount";
$TEXT2['topup_balance'] = "Topup Balance";
$TEXT2['topup_country_channel'] = "Payment country channel";
$TEXT2['topup_just_did'] = "just did topup";
$TEXT2['topup_no_decimal'] = "*Without decimal sign";
$TEXT2['user_product_none'] = "User didn't buy any product yet";
$TEXT2['user_product_none2'] = "You didn't buy any product yet";
$TEXT2['user_report_info'] = "*Please report this user to our E-Mail : admin@techconnexionindonesia.com if this user violate our regulation";
$TEXT2['user_review_none'] = "User didn't gave any review yet";
$TEXT2['verification'] = "Verification";

//Long text here
//Sorted by a-Z
$LONGTEXT['about_rule_dev'] = "
&nbsp;&nbsp;&nbsp;As a developer, you are required to maintain the quality of the products that you publish on the TCI website. This is because of the relationship between TCI and you as a developer. We ensure that both parties, users and developers benefit from each other without anyone feeling disadvantaged. Before joining a developer, you are required to read the terms & conditions of the user first. By becoming a developer, you understand and agree to the terms and conditions that TCI has applied as follows:<br><br>
<b>1. Regarding the authenticity of the product</b><br>
&nbsp;&nbsp;&nbsp;By publishing content, the developer must guarantee the authenticity of the product that the developer is making, ie the product the developer publishes is the result of the work of the developer, or the product of someone else that has been allowed to be published to the developer. All content in a product that is in the product published by the developer must also be original or have permission for publication. TCI will delete products that are not original or do not have permission if the TCI knows the product in question is pirated products. The party will review if there is an entry from the user who reports the authenticity of the product<br><br>
<b>2. Regarding product publications</b><br>
&nbsp;&nbsp;&nbsp;By using the TCI feature, you have the right to publish the product and set the status and percentage of its publication. Published products can be set to conditions ranging from 0%. Products with publication status below 100% have meaning not released and with that, the purchase button on the intended product page is omitted. Products which are set to 100% will be reviewed by TCI to determine whether the product can be used or not. Products that have been reviewed by TCI, if they cannot be used or downloaded, they will experience a delay in release until the developer corrects the errors stated in the publication information. Products that have been released with 100% publication status, can be set back to under 100% only if an error occurs in the product.<br><br>
<b>3. Regarding product pricing</b><br>
&nbsp;&nbsp;&nbsp;At product publication, developer can set their price at which product that publicated. That price can be set by the developer decision. Price that inputted through Rupiah at publication information form, then will be converted into US Dollar using currency exchange rate that configurated by TCI. Price that labeled in Rupiah, can be changed anytime during changing of currency exchange rate that already set.<br><br>
<b>4. Regarding Product Elements</b><br>
&nbsp;&nbsp;&nbsp;On products that are published, they must not contain sexual elements, political campaigns, speeches of hate and things that can harm users specifically such as elements that attack religion, identity etc.<br><br>
<b>5. Regarding accounts & withdrawal of profits for Indonesia</b><br>
&nbsp;&nbsp;&nbsp;Developers who register must have an account and register their account number and bank name on the form with the correct data if the developer publishes paid products. If the developer does not have a paid product, the developer is not required to have an account and register it. Developers can publish paid products first without an account and can register accounts in the future. The account number and bank that is owned is needed when it will attract profits. Withdrawal of profits is a minimum of Rp.35,000 with details of the transferred profits of Rp.30,000 and administration fees of Rp.5,000 if the profits drawn are less than Rp.100,000. If the withdrawal of profits is carried out above Rp.99,000, an administrative fee of 10% will be charged. Benefits can be drawn by going to the developer menu and making profit withdrawals. After the process of withdrawing profits, TCI will process and provide proof of transfer if the transfer has been made. The transfer process will be carried out no later than 2 days after the withdrawal process is carried out. If it exceeds, the developer can contact TCI further. Profits that have been withdrawn cannot be returned to the developer balance. If there is a disruption to the transfer process and TCI has been confirmed by e-mail, TCI will return the balance withdrawn to the developer's total balance.<br><br>
<b>6. Regarding accounts & withdrawal of profits for overseas</b><br>
&nbsp;&nbsp;&nbsp;Withdrawal of profits obtained after the product sales process where the developer comes from abroad, can be done by bank method or Paypal. The developer must have a bank account or paypal account that is registered on the data entry form in the developer registration. The profit balance that is withdrawn is at least $ 6 if the profit drawn is less than $ 100 with details of the transferred profit of $ 5 and an administrative fee of $ 1. If it exceeds $ 9, the administration fee is charged at 10% of the total profit withdrawal. Benefits can be drawn by going to the developer menu and making profit withdrawals. After the process of withdrawing profits, TCI will process and provide proof of transfer if the transfer has been made. The transfer process will be carried out no later than 2 days after the withdrawal process is carried out. If it exceeds, the developer can contact TCI further. Profits that have been withdrawn cannot be returned to the developer balance. If there is a disruption to the transfer process and TCI has been confirmed by e-mail, TCI will return the balance withdrawn to the developer's total balance.<br><br>
<b>7. Regarding information and contacts</b><br>
&nbsp;&nbsp;&nbsp;The developer must have an active email because all information such as the transfer process and others will be informed by email. The developer can contact TCI further for questions and other matters through:<br>
<b>Email</b> : <a href='mailto:admin@techconnexionindonesia.com'>admin@techconnexionindonesia.com</a><br>
<b>Facebook</b> : <a href='https://www.facebook.com/tcifp'>www.facebook.com/tcifp</a><br>
";

$LONGTEXT['about_rule_intro'] = "
&nbsp;&nbsp;&nbsp;As a trading platform service provider, TCI has terms and conditions that apply to users and developers. This is used to ensure the existing system nets, namely the buying and selling system. Terms and conditions are also made to protect customers and developers from threats of fraud, or threats that are in our services. By utilizing the existing system, we strive to ensure that the quality of the buying and selling of digital products within TCI services runs well and can cover users and developers that guarantee the security and convenience of use. Still on the same thing, as a digital platform service provider, and by using the terms and conditions that we apply, we strive to guarantee that every product in TCI is original and no products are published through piracy.
";


$LONGTEXT['about_rule_user'] = "
&nbsp;&nbsp;&nbsp;By registering as a user and using the Tech Connexion Indonesia digital website service, you understand and agree to the terms and conditions that TCI has applied as follows:<br><br>
<b>1. Regarding the process of buying and selling</b><br>
&nbsp;&nbsp;&nbsp;The buying and selling process on the TCI website fully uses the balance system stored in the user account. The balance comes from the transfer to the account owned by TCI and accompanied by proof of payment. Users can purchase game products and content that are available at TCI and can pay and download if the balance in the user account is sufficient for the price of the product purchased. If the product in question has a price of 0 (zero) or free, then the user can add to the basket and pay for it even though they have no balance at all.<br><br>
<b>2. Currency and currency exchange rates</b><br>
&nbsp;&nbsp;&nbsp;The TCI website uses US Dollar as a buying and selling process between customers and developers. The balance entered into a user account, will be fully converted to US Dollar (US Dollar) in the database structure on the TCI server. Currency changes on the website will only convert from US dollars to the currency in which the exchange rate is updated according to global data no later than once a week by TCI. While for the process of purchasing a product, the calculation process will be carried out using the United States Dollar (US Dollar).<br><br>
<b>3. Perihal pengisian saldo dan bukti pembayaran</b><br>
&nbsp;&nbsp;&nbsp;The balance on a user account can be filled by transferring money directly to the TCI account and filling in the balance form in the profile menu and attaching proof of payment in the form of photos / screenshots when transferring money. Proof of transfer must be included in order to ensure administrative data correctly. If there is no transfer proof attached, then filling out the balance must be checked in advance by the TCI, which may take longer. Fill in the form filled in the balance that has been sent, it will be processed by the TCI first. The balance entered in the filler account is processed automatically through the TCI system where the transfer amount will be divided into the exchange rate of the Rupiah to the United States Dollar (US Dollar). If the amount that has been processed is less than the value of the Rupiah transferred, then the value of the balance will be increased by 0.01 (US Dollar) so that it equals or exceeds a little of the value of the rupiah transferred. The minimum amount for filling out the balance is IDR 30,000, where the amount below will be rejected by TCI. The user who has filled out the balance has the right to file an objection if the balance added by the TCI does not equal the balance amount. The balance that has been filled in, cannot be returned. If the balance is not filled after no later than 2 hours after filling out the balance form, the user can contact TCI.<br><br>
<b>4. Regarding system interference</b><br>
&nbsp;&nbsp;&nbsp;If there is a system disruption on the TCI website, on the scope of the website, database and technical scope server, it can be reported to TCI to be corrected immediately. And if the disturbance is detrimental to the user, then the user has the right to submit improvements to the scope of the account that has been harmed by including details of the initial conditions before the disturbance.<br><br>
<b>5. Regarding behavior</b><br>
&nbsp;&nbsp;&nbsp;By using TCI services and products, you are willing to maintain attitudes and courtesy on interaction parts such as reviews of products and comment features on TCI games. If there are interactions that contain elements of speech hate, political campaigns, religion, pornography, and other things that allow other parties to be harmed. And if the user finds other user interactions as mentioned above, then the user can report the matter to the TCI.<br><br>
<b>6. Regarding service & product maintenance</b><br>
&nbsp;&nbsp;&nbsp;TCI can maintain services and products at any time by giving news to users no later than 3 hours before.<br><br>
<b>7. Regarding information</b><br>
&nbsp;&nbsp;&nbsp;Users can contact TCI if there are obstacles found in the use of services & products and to get information through<br>
<b>Email</b> : <a href='mailto:admin@techconnexionindonesia.com'>admin@techconnexionindonesia.com</a><br>
<b>Facebook</b> : <a href='https://www.facebook.com/tcifp'>www.facebook.com/tcifp</a><br>
";

$LONGTEXT['about_tci_2013'] = "
&nbsp;&nbsp;&nbsp; In 2013, TCI was initially named 'Trainz Collection Indonesia', which at that time, TCI only became a content developer who made additional content for the Trainz Railroad Simulator 2009 game. At that time, TCI was initiated and built by Ikhwan Febriansyah and Alex Andre as Founders of TCI. <br>
&nbsp;&nbsp;&nbsp; Some time after it was built in 2013, the two founders set up their first website based on Weebly on the site: <a href='http://www.trainzcollectionindonesia.weebly.com'> http://www.trainzcollectionindonesia.weebly.com</a>.
";

$LONGTEXT['about_tci_2016'] = "
&nbsp;&nbsp;&nbsp;After a long vacuum for 300 years. Sorry, we mean 3 years, TCI is back in the content world to recreate interesting content for the Trainz Railroad Simulator simulation game. Even not only the Trainz Railroad Simulator, but also TCI presents the mod for GTA: San Andreas. At the beginning, TCI made the first mod GTA: San Andreas entitled 'Voice PPKA mod GTA SA by TCI' where the mod can sound PPKA sounds in the famous game. After a long period of silence, TCI made a lot of mods for the game, starting from mod scripts, buildings to folders. Not forgetting the Trainz Railroad Simulator, TCI also created a modeled KRL mod from the Japanese private railway company, Meitetsu 800 and Meitetsu 1600. TCI also published the actual Depok-Manggarai route starting from Citayam station.
";

$LONGTEXT['about_tci_2017'] = "
&nbsp;&nbsp;&nbsp;After successfully gaining a reputation in 2016 in the world of modding, in 2017 TCI innovated to create a 3D game that was amazed by the 'Jakarta Monorail Simulator'. This is where the start of the era of Developing Games at TCI. This game is planned to be published as a local simulation game made according to the real Monorail project in Jakarta from the route to the train. By using several team members, TCI has prepared a model infrastructure such as the manufacture of the UTM-125 train model, the station model from Casablanca station to Ambasador station. However, the trip did not bear sweet fruit. A few months later, the project was not effective until it was stopped.
";

$LONGTEXT['about_tci_2018'] = "
&nbsp;&nbsp;&nbsp;Entering December 2018, TCI implements new innovations in the gaming world. By initiating the Indonesia Train Dispatcher, TCI was able to create the first game publication with the simulation game. The ITD game has succeeded in raising the reputation of TCI in the release of Beta to its Stable in 2019. The ITD game has now been widely played by Train lovers. In the same 2019, TCI also hired a new server for its website to put ITD game server data and TCI websites on the server. From that moment on, TCI changed its name to 'Tech Connexion Indonesia' which meant a technological relationship for Indonesia. TCI agrees on its new vision of creating what does not yet exist and perfecting what already exists, and has a mission to improve the reputation of Indonesian technology to be on par with the outside world. Now TCI has become a project-based organization that aims to create applications such as games and other needs, and in the future it will innovate to create the latest innovative and creative technologies.
";

$LONGTEXT['about_tci_intro'] = "
&nbsp;&nbsp;&nbsp;Welcome to Tech Connexion Indonesia. Tech Connexion Indonesia is an organization that is a game developer, application developer and also as a platform provider for publishing content and mods of games such as GTA: San Andreas & Trainz Simulator.<br>
&nbsp;&nbsp;&nbsp;As a provider of platforms for content, TCI provides an opportunity for content developers to publish additional content in the form of mods or addons to be published and become quality content, and even benefit from selling it. The enthusiasm of gamers and content developers makes TCI the provider of content publishing platforms so gamers and developers can more easily publish and access content for the games they play. And also, as a game and application developer.<br>
&nbsp;&nbsp;&nbsp;TCI has a vision and mission to advance the reputation of Indonesia's application / technology to be more advanced and on par with other countries that have already advanced. TCI creates amazing games and applications with amazing features. The game that is now created, focuses mainly on simulation, and beyond that the Action genre etc. TCI is even creating new innovations about technology and implementing creative ideas that will be useful for everyone. TCI creates what does not exist, and perfects what already exists.";

$LONGTEXT['devconsole_intro'] = "
&nbsp;&nbsp;&nbsp;Welcome to the Developer Console page. The Developer Console works for all settings that relate to your development on your product. You can choose the menu above.<br><br>
-> Products: Serves to regulate publications such as adding, updating and removing products.<br>
-> Financial Statistics: Serves to view and analyze publication statistics and product rating activities and product downloads.<br>
-> Activity: Function to see all activities such as administrator activities, purchases and downloads and others.<br>
-> Configuration: Serves as a configuration for Developer account settings such as admin configuration, updates to Developer data and so on.<br><br>
&nbsp;&nbsp;&nbsp;If you experience difficulties when using the Developer Console, please contact TCI via the contact provided in the Terms and Conditions menu in the About TCI section.
";

$LONGTEXT['topup_info'] = "
&nbsp;&nbsp;&nbsp;&nbsp;To topup your account balance, please follow the procedure below : <br><br>

<b>1. Select Country Channel</b><br>
&nbsp;&nbsp;&nbsp;&nbsp;By selecting country channel, will determine which payment method available to do your transction<br>
<b>2. Input balance amount</b><br>
&nbsp;&nbsp;&nbsp;&nbsp;Minimum balance for topup is Rp.30.000 for Indonesia and $3.00 for outside of Indonesia <br>
<b>3. Do Transaction Process</b>
&nbsp;&nbsp;&nbsp;&nbsp;After input amount of topup, you can continue by clicking Topup, after then you will be redirected to payment page. Do the transaction with available payment method. After you did a transaction, the TCI system will add the amount of topup to your account and you will be notified by email.<br><br>

<b> Attention !</b><br>
&nbsp;&nbsp;&nbsp;&nbsp;Topup balance will be converted into US Dollar. If you did a transaction using Indonesian payment channel, the amount will be increased by $0.01<br><br>

<b>Indonesia Payment Method (Using Ipaymu) :</b><br>
1. Bank Transfer dan Virtual Account : <br>
<b>BCA (Only Rp500.000+), Arta Graha , BNI , CIMB Niaga , Mandiri</b><br><br>
2. Convenience Store : <br>
<b>Alfamart , Indomart<br>
(Say the Plasamall payment and say the code you got)</b><br><br>
3. Tagpay Application : <br>
<b>By using Tagpay application, you can pay your topup transaction using mobile balance</b><br><br>
4. QR IS / Virtual Wallet : <br>
<b>BCA QR , Ovo , Gopay , Dana , Linkaja</b>
";

$LONGTEXT['topup_wait'] = "
&nbsp;&nbsp;&nbsp;You have been submit topup form. TCI system is waiting for your payment or verifying for your topup payment. TCI system will send you email either the topup is successfull or failed. You can do a topup again by fill out the form below. TCI system will remove the topup request if the topup payment has not paid off until 2 hours after register topup.
";
?>