<?php
/*
--Indonesian Language Variable Texts--
--Tech Connexion Indonesia Website--
--Created by TCI Programming Division--
--Tech Connexion Indonesia Group--
--(c) Andreas Alexander Kaawoan (Executive Director) TCI Group--
*/

//Indonesian
//Sorted by a-Z
//Note:Fixed Word mean it shouldn't or no need to change by language
$TEXT['about_menu_about'] = "Tentang Kami";
$TEXT['about_menu_contact'] = "Kontak Kami";
$TEXT['about_menu_director'] = "Kata Pengantar Pimpinan";
$TEXT['about_menu_relation'] = "Hubungan Bisnis";
$TEXT['about_menu_faq'] = "FAQ"; //Fixed Word
$TEXT['about_menu_joinasdev'] = "Bergabung Sebagai Developer";
$TEXT['about_menu_joinasuser'] = "Bergabung Sebagai User";
$TEXT['about_menu_project'] = "Proyek Kami";
$TEXT['about_menu_rule'] = "Syarat dan Ketentuan";
$TEXT['about_menu_service'] = "Layanan Jasa TCI";
$TEXT['about_menu_structure'] = "Struktur Organisasi";
$TEXT['about_project_search'] = "Cari proyek TCI di sini";
$TEXT['about_project_sort_dp'] = "Tanggal Publish";
$TEXT['about_project_sort_ds'] = "Tanggal Mulai Proyek";
$TEXT['about_project_sort_p'] = "Kepopuleran";
$TEXT['about_rule_fordev'] = "Syarat & Ketentuan Developer";
$TEXT['about_rule_foruser'] = "Syarat & Ketentuan Pengguna Website";
$TEXT['about_rule_title'] = "Untuk Apa S&K?";
$TEXT['about_structure_select'] = "Pilih Struktur :";
$TEXT['activity'] = "Aktifitas";
$TEXT['addupdate'] = "Tambah/Update";
$TEXT['add_to_cart'] = "Tambah ke keranjang";
$TEXT['alert'] = "Peringatan";
$TEXT['alert_en'] = "Harus dalam bahasa inggris";
$TEXT['app'] = "Aplikasi";
$TEXT['automatic'] = "Otomatis";
$TEXT['balance'] = "Saldo";
$TEXT['cart'] = "Keranjang";
$TEXT['cart_alert_delete'] = "Anda akan menghapus produk tersebut dari keranjang anda";
$TEXT['cart_alert_delete_all'] = "Anda akan menghapus semua produk yang terdapat dalam keranjang anda";
$TEXT['cart_alert_pay'] = "Anda akan membayar produk tersebut dalam keranjang anda";
$TEXT['cart_alert_pay_all'] = "Anda akan membayar semua produk yang terdapat dalam keranjang anda";
$TEXT['cart_balance'] = "Saldo anda saat ini :";
$TEXT['cart_balance_alert'] = "Saldo anda tidak mencukupi pembelian. Silahkan isi saldo anda";
$TEXT['cart_bought'] = "baru saja melakukan pembelian";
$TEXT['cart_delete'] = "Hapus Semua Keranjang";
$TEXT['cart_delete_single'] = "Hapus";
$TEXT['cart_none'] = "Tidak ada keranjang di sini";
$TEXT['cart_pay'] = "Bayar Semua";
$TEXT['cart_pay_single'] = "Bayar";
$TEXT['cart_search'] = "Cari Keranjang";
$TEXT['cart_total'] = "Total harga keranjang anda :";
$TEXT['category'] = "Kategori";
$TEXT['close'] = "Tutup";
$TEXT['content'] = "Konten";
$TEXT['content_all'] = "Konten & DLC";
$TEXT['content_bs'] = "Paling Laku";
$TEXT['content_c_gtasa'] = "GTA:SA"; //fixed word
$TEXT['content_c_trs'] = "Trainz Simulator"; //fixed word
$TEXT['content_c'] = "Konten";
$TEXT['content_d'] = "DLC";
$TEXT['content_newest'] = "Paling Baru";
$TEXT['content_of'] = "dari";
$TEXT['content_progress'] = "Progress";
$TEXT['content_search'] = "Cari Konten dan DLC di sini";
$TEXT['content_showall'] = "Semua";
$TEXT['content_trending'] = "Trending";
$TEXT['continue'] = "Lanjutkan";
$TEXT['delete'] = "Hapus";
$TEXT['devconsole_activity'] = "Aktifitas";
$TEXT['devconsole_agree'] = "Dengan mendaftar sebagai Developer, anda menyetujui persyaratan yang berlaku";
$TEXT['devconsole_bank'] = "Nama Bank";
$TEXT['devconsole_bankchange_info'] = "*Data akun bank hanya bisa diganti oleh owner";
$TEXT['devconsole_bankname'] = "Nama Pemegang Rekening";
$TEXT['devconsole_bankno'] = "Nomor Rekening Bank"; 
$TEXT['devconsole_createdev'] = "Buat Developer";
$TEXT['devconsole_createp'] = "Membuat";
$TEXT['devconsole_config'] = "Konfigurasi";
$TEXT['devconsole_config_admin'] = "Konfigurasi Administrator";
$TEXT['devconsole_err_usrpw'] = "Password untuk verifikasi pengguna salah";
$TEXT['devconsole_err_exist'] = "Mohon maaf, ID atau nama developer sudah terpakai";
$TEXT['devconsole_err_create'] = "Silahkan pilih produk yang akan dibuat pada checkbox";
$TEXT['devconsole_err_imgnot'] = "File yang anda upload bukan berupa gambar";
$TEXT['devconsole_err_imgsize'] = "Gambar yang anda upload terlalu besar, maksimal ukuran 1MB";
$TEXT['devconsole_err_imgtype'] = "Format file yang anda upload bukan tipe yang diizinkan";
$TEXT['devconsole_finance'] = "Finansial Statistik";
$TEXT['devconsole_iddev'] = "ID Developer";
$TEXT['devconsole_logo'] = "Logo Developer";
$TEXT['devconsole_password'] = "Password Developer";
$TEXT['devconsole_paypal_a'] = "Akun Paypal";
$TEXT['devconsole_personaldata'] = "Data Pribadi";
$TEXT['devconsole_place'] = "Tempat Pekerjaan";
$TEXT['devconsole_product'] = "Produk";
$TEXT['devconsole_signup'] = "Daftar Sebagai Developer";
$TEXT['devconsole_site'] = "Situs website<br>(Jika anda punya)";
$TEXT['devconsole_structure'] = "Struktur Developer";
$TEXT['devconsole_type'] = "Tipe Developer";
$TEXT['devconsole_typeplc_any'] = "Di mana saja";
$TEXT['devconsole_typeplc_home'] = "Rumah";
$TEXT['devconsole_typeplc_office'] = "Kantor";
$TEXT['devconsole_typestc_company'] = "Perusahaan";
$TEXT['devconsole_typestc_single'] = "Perseorangan";
$TEXT['devconsole_typestc_team'] = "Tim Kerja";
$TEXT['devconsole_wait_validation'] = "Saat ini Developer yang anda daftarkan sedang menunggu pihak TCI untuk validasi. Apabila data Developer anda sudah divalidasi, maka anda dapat menggunakan Developer console untuk mempublikasi produk anda. Saat anda diarahkan menuju halaman login Developer, berarti data Developer anda sudah divalidasi. Silahkan cek kembali nanti.";
$TEXT['devconsole_welcome'] = "Selamat datang di Developer Console";
$TEXT['developed_by'] = "Dikembangkan oleh";
$TEXT['dlc'] = "DLC";
$TEXT['download'] = "Download";
$TEXT['error_title'] = "Kesalahan";
$TEXT['error_txt_404'] = "Mohon maaf, halaman yang anda akses tidak tersedia. Hal ini mungkin karena halaman tersebut sudah dihapus atau kadaluarsa atau bahkan sedang dalam pengerjaan.";
$TEXT['error_txt_data'] = "Mohon maaf, anda tidak dapat mengakses halaman ini karena terjadi kesalahan pada data pusat. Mohon hubungi administrator di admin@techconnexionindonesia.com .";
$TEXT['error_txt_db'] = "Mohon maaf, terjadi kesalahan pada database. Mohon hubungi administrator di admin@techconnexionindonesia.com .";
$TEXT['error_txt_default'] = "Mohon maaf, terjadi kesalahan. Mohon hubungi administrator di admin@techconnexionindonesia.com .";
$TEXT['error_txt_paging'] = "Mohon maaf, anda tidak dapat mengakses halaman ini. Bila ada kesalahan, mohon hubungi administrator di admin@techconnexionindonesia.com . Silahkan kembali ke halaman utama";
$TEXT['error_txt_profile'] = "Mohon maaf, terjadi kesalahan dengan data profil anda. Demi keamanan dan kenyamanan, anda telah logout secara otomatis, namun data profil anda tidak dihapus. Mohon hubungi administrator di admin@techconnexionindonesia.com";
$TEXT['filter'] = "Filter";
$TEXT['game'] = "Game";
$TEXT['gamedlc'] = "Game & DLC";
$TEXT['genre'] = "Genre";
$TEXT['home_content_select_language'] = "Pilih Bahasa dan Mata Uang";
$TEXT['home_content_news'] = "Berita";
$TEXT['home_content_news_by1'] = "ditulis oleh";
$TEXT['home_content_news_by2'] = "dari";
$TEXT['home_content_news_more'] = "Lihat semua berita";
$TEXT['home_content_onprogress'] = "Dalam Pengerjaan";
$TEXT['home_content_progressmore'] = "Projek dalam pengerjaan";
$TEXT['home_content_ra_more'] = "Lihat lebih banyak";
$TEXT['home_content_recentactivities'] = "Aktifitas Terakhir";
$TEXT['home_content_trending'] = "Trending";
$TEXT['home_progressslide_dev'] = "Dev:"; //Fixed Word
$TEXT['home_progressslide_status'] = "Status:";
$TEXT['home_progressslide_type'] = "Tipe:";
$TEXT['home_slide_view'] = "Lihat Sekarang";
$TEXT['id'] = "ID"; //Fixed Word
$TEXT['login_label_wrong'] = "Username atau password yang anda masukan salah";
$TEXT['login_ph_password'] = "Silahkan masukan Password";
$TEXT['login_ph_username'] = "Silahkan masukan Username";
$TEXT['login_signup'] = "Tidak punya akun TCI ? buat sekarang juga !";
$TEXT['login_title'] = "LOGIN"; //Fixed Word
$TEXT['navbar_aboutus'] = "TENTANG TCI";
$TEXT['navbar_content'] = "KONTEN GAME";
$TEXT['navbar_home'] = "BERANDA";
$TEXT['navbar_login'] = "Login";
$TEXT['navbar_news'] = "BERITA";
$TEXT['navbar_signup'] = "Daftar";
$TEXT['navbar_store'] = "GAME & APP";
$TEXT['news'] = "Berita";
$TEXT['news_all'] = "Semua";
$TEXT['news_app'] = "Aplikasi";
$TEXT['news_content'] = "Konten";
$TEXT['news_developer'] = "Developer";
$TEXT['news_dlc'] = "DLC";
$TEXT['news_game'] = "Game";
$TEXT['news_general'] = "Umum";
$TEXT['news_important'] = "Penting";
$TEXT['news_newest'] = "Paling baru";
$TEXT['news_older'] = "Lebih lama";
$TEXT['news_open_link'] = "Buka link";
$TEXT['news_open_profile_writer'] = "Buka profil penulis";
$TEXT['news_photos'] = "Foto";
$TEXT['news_project'] = "Proyek";
$TEXT['news_reaction'] = "Bagaimana reaksi anda?";
$TEXT['news_release'] = "Perilisan";
$TEXT['news_search'] = "Cari berita di sini";
$TEXT['news_server'] = "Server";
$TEXT['news_share'] = "Bagikan berita ini !";
$TEXT['news_tci'] = "Dari TCI";
$TEXT['news_update'] = "Update";
$TEXT['news_visited1'] = "Dikunjungi sebanyak";
$TEXT['news_visited2'] = "kali";
$TEXT['news_voted'] = "melakukan voting pada sebuah berita";
$TEXT['no'] = "Tidak";
$TEXT['open_profile_dev'] = "Buka profil Developer";
$TEXT['option'] = "Pilihan";
$TEXT['price'] = "Harga";
$TEXT['product_g_a_search'] = "Cari Game dan Aplikasi di sini";
$TEXT['project_published'] = "Tanggal publish";
$TEXT['project_started'] = "Tanggal mulai proyek";
$TEXT['reset'] = "Reset";
$TEXT['signup_address'] = "Alamat";
$TEXT['signup_answer1'] = "Jawaban";
$TEXT['signup_answer2'] = "Jawaban";
$TEXT['signup_back_name'] = "Nama belakang";
$TEXT['signup_birth'] = "Tanggal Lahir";
$TEXT['signup_country'] = "Negara";
$TEXT['signup_data'] = "Data login";
$TEXT['signup_description'] = "Deskripsi";
$TEXT['signup_email'] = "E-mail"; //Fixed Word
$TEXT['signup_front_name'] = "Nama depan";
$TEXT['signup_img'] = "Foto profil";
$TEXT['signup_img_wrong_label'] = "File yang anda upload bukan gambar";
$TEXT['signup_joined'] = "baru bergabung bersama TCI";
$TEXT['signup_name'] = "Nama";
$TEXT['signup_password'] = "Password";
$TEXT['signup_password_re'] = "Verifikasi password";
$TEXT['signup_personal'] = "Data personal anda";
$TEXT['signup_phone'] = "No. Telp";
$TEXT['signup_question1'] = "Pertanyaan keamanan 1";
$TEXT['signup_question2'] = "Pertanyaan keamanan 2";
$TEXT['signup_re_wrong'] = "Password tidak sama";
$TEXT['signup_required'] = "* (Harus diisi)";
$TEXT['signup_signature'] = "Tanda tangan";
$TEXT['signup_subscription'] = "Saya ingin mendapat kabar baru dari TCI melalui Email";
$TEXT['signup_success_text1'] = "Selamat, anda berhasil menyelasaikan registrasi anda dengan data sebagai berikut :";
$TEXT['signup_success_text2'] = "ID Anda";
$TEXT['signup_success_text3'] = "Username Anda";
$TEXT['signup_success_text4'] = "Harap diingat, dengan meregistrasi pada website TCI, anda telah setuju kepada Syarat dan Ketentuan yang berlaku. Sekarang anda dapat mengakses akun anda dengan membuka menu Profile. Anda juga dapat melakukan pembelian game. Jika anda mempunyai pertanyaan atau keluhan, harap kontak admin di admin@techconnexionindonesia.com";
$TEXT['signup_success_title'] = "PENDAFTARAN BERHASIL";
$TEXT['signup_terms'] = "Dengan mendaftar ke website TCI , anda menyetujui Syarat dan Ketentuan yang berlaku";
$TEXT['signup_title'] = "SIGNUP"; //Fixed Word
$TEXT['signup_username'] = "Username";
$TEXT['signup_username_exist'] = "Mohon maaf,username sudah digunakan";
$TEXT['signup_zipcode'] = "Kode pos";
$TEXT['sort_by'] = "Urut Berdasarkan :";
$TEXT['statistic'] = "Statistik";
$TEXT['status'] = "Status";
$TEXT['store_a'] = "Aplikasi";
$TEXT['store_bs'] = "Paling Laku";
$TEXT['store_g'] = "Games";
$TEXT['store_g_a_all'] = "Semua Produk";
$TEXT['store_g_a_showall'] = "Semua";
$TEXT['store_g_action'] = "Action"; // Fixed Word
$TEXT['store_g_adventure'] = "Adventure"; // Fixed Word
$TEXT['store_g_arcade'] = "Arcade"; // Fixed Word
$TEXT['store_g_all'] = "Semua Genre";
$TEXT['store_g_fight'] = "Fight"; // Fixed Word
$TEXT['store_g_fps'] = "FPS"; // Fixed Word
$TEXT['store_g_horror'] = "Horror"; // Fixed Word
$TEXT['store_g_multiplayer'] = "Multiplayer"; // Fixed Word
$TEXT['store_g_racing'] = "Racing"; // Fixed Word
$TEXT['store_g_rpg'] = "RPG"; // Fixed Word
$TEXT['store_g_scifi'] = "Sci Fi"; // Fixed Word
$TEXT['store_g_shooter'] = "Shooter"; // Fixed Word
$TEXT['store_g_simulation'] = "Simulation"; // Fixed Word
$TEXT['store_g_stealth'] = "Stealth"; // Fixed Word
$TEXT['store_g_story'] = "Story"; // Fixed Word
$TEXT['store_g_strategy'] = "Strategy"; // Fixed Word
$TEXT['store_g_tps'] = "TPS"; // Fixed Word
$TEXT['store_g_tycoon'] = "Tycoon"; // Fixed Word
$TEXT['store_in_cart'] = "Ditambahkan ke keranjang";
$TEXT['store_progress'] = "Progress";
$TEXT['store_rate'] = "Berikan Vote !";
$TEXT['store_rating1'] = "Rating";
$TEXT['store_rating2'] = "dari total";
$TEXT['store_rating3'] = "voting menyukai ini";
$TEXT['store_review_placeholder'] = "Tuliskan pendapat anda tentang produk ini";
$TEXT['store_total_download'] = "Total unduhan";
$TEXT['store_trending'] = "Trending";
$TEXT['store_vote_unable'] = "Anda harus membeli produk ini sebelum review atau vote";
$TEXT['submit'] = "SUBMIT"; //Fixed Word
$TEXT['tags'] = "Tags"; //Fixed Word
$TEXT['validation'] = "Validasi";
$TEXT['yes'] = "Ya";

//TEXT2
$TEXT2['administrator'] = "Administrator";
$TEXT2['cmd_accept'] = "Terima";
$TEXT2['cmd_apply_update'] = "Lakukan Update";
$TEXT2['cmd_back_prevpage'] = "Kembali ke halaman sebelumnya";
$TEXT2['cmd_reject'] = "Reject";
$TEXT2['cmd_select_country_update'] = "Pilih negara untuk diperbarui / Lewatkan jika tidak ingin diperbarui";
$TEXT2['cmd_leave_to_not'] = "Tinggalkan jika tidak ingin diperbarui";
$TEXT2['confirm'] = "Konfirmasi";
$TEXT2['confirm_1'] = "Dikonfirmasi";
$TEXT2['confirm_0'] = "Belum Dikonfirmasi";
$TEXT2['created'] = "Membuat";
$TEXT2['data_balance'] = "Saldo";
$TEXT2['data_balance_e'] = "Saldo Akhir";
$TEXT2['data_balance_f'] = "Saldo Awal";
$TEXT2['data_bankdest'] = "Bank Tujuan";
$TEXT2['data_bankname'] = "Nama Pemegang Rekening";
$TEXT2['data_bankname_plc'] = "Misal : Abdi Abdullah";
$TEXT2['data_banknosender'] = "Nomor Rekening Pengirim / Resi Transfer";
$TEXT2['data_banknosender_plc'] = "Misal : 0102031122331234";
$TEXT2['data_banksender'] = "Bank Pengirim";
$TEXT2['data_banksender_plc'] = "Misal : Bank Mandiri";
$TEXT2['data_country'] = "Negara";
$TEXT2['data_currency'] = "Mata Uang";
$TEXT2['data_datetime'] = "Tanggal / Waktu";
$TEXT2['data_description'] = "Deskripsi";
$TEXT2['data_dev_rating'] = "Rating Developer";
$TEXT2['data_dev_related'] = "Developer Terhubung";
$TEXT2['data_email'] = "E-Mail"; //Fixed Word
$TEXT2['data_id'] = "ID";
$TEXT2['data_info'] = "Info";
$TEXT2['data_name'] = "Nama";
$TEXT2['data_note'] = "Catatan";
$TEXT2['data_password_dev'] = "Password Developer";
$TEXT2['data_password_new'] = "Password Baru";
$TEXT2['data_password_new_re'] = "Verifikasi Password Baru";
$TEXT2['data_password_old'] = "Password Lama";
$TEXT2['data_personal'] = "Data Pribadi";
$TEXT2['data_product'] = "Produk";
$TEXT2['data_product_bought'] = "Produk yang Dibeli";
$TEXT2['data_profile'] = "Profil";
$TEXT2['data_profile_security'] = "Pengamanan Profil";
$TEXT2['data_registered'] = "Registrasi";
$TEXT2['data_review'] = "Review";
$TEXT2['data_security'] = "Data Keamanan";
$TEXT2['data_signature'] = "Tanda Tangan";
$TEXT2['data_total_app'] = "Total Aplikasi";
$TEXT2['data_total_dlc'] = "Total DLC";
$TEXT2['data_total_content'] = "Total Konten";
$TEXT2['data_total_game'] = "Total Game";
$TEXT2['data_total_product'] = "Total Produk";
$TEXT2['data_total_rating'] = "Total Rating";
$TEXT2['data_total_topup'] = "Total Isi Saldo";
$TEXT2['data_total_topup_count'] = "Jumlah Aktifitas Pengisian Saldo";
$TEXT2['data_total_vote_count'] = "Jumlah Aktifitas Voting";
$TEXT2['data_transaction'] = "Transaksi";
$TEXT2['data_transfer_date'] = "Tanggal Transfer";
$TEXT2['data_transfer_proof'] = "Bukti Transfer (Foto)";
$TEXT2['data_transfer_total'] = "Jumlah Transfer";
$TEXT2['data_transfer_total_plc'] = "Misal : 50.000 atau 5.00";
$TEXT2['data_vote'] = "Vote";
$TEXT2['data_website'] = "Website"; //Fixed Word
$TEXT2['devconsole_add_admin_username'] = "Tambah Administrator menggunakan Username";
$TEXT2['devconsole_admin_confirm'] = "Klik 'Terima' untuk mengkonfirmasikan undangan, atau klik 'Tolak' untuk menolak undangan.<br> Anda dapat menghubungi E-Mail developer di :";
$TEXT2['devconsole_admin_invitation'] = "Anda diundang menjadi Administrator dari Developer :";
$TEXT2['devconsole_admin_joined'] = "telah bergabung bersama developer";
$TEXT2['devconsole_admin_reject'] = "menolak bergabung bersama developer";
$TEXT2['devconsole_administrator_list'] = "Daftar Administrator";
$TEXT2['devconsole_administrator_num_info'] = "*Maksimal administrator adalah 5 pengguna";
$TEXT2['devconsole_avt_actdeveloper'] = "Aktifitas Developer";
$TEXT2['devconsole_avt_activity'] = "Aktifitas";
$TEXT2['devconsole_avt_actreview'] = "Aktifitas Review";
$TEXT2['devconsole_avt_acttransaction'] = "Aktifitas Transaksi";
$TEXT2['devconsole_avt_actvote'] = "Aktifitas Voting";
$TEXT2['devconsole_avt_admin'] = "Admin"; //Fixed Word
$TEXT2['devconsole_avt_close'] = "Tutup Tampilan";
$TEXT2['devconsole_avt_info'] = "Informasi";
$TEXT2['devconsole_avt_more_admin'] = "Lihat Aktifitas Lengkap";
$TEXT2['devconsole_avt_more_review'] = "Lihat Review Lengkap";
$TEXT2['devconsole_avt_more_transaction'] = "Lihat Transaksi Lengkap";
$TEXT2['devconsole_avt_more_vote'] = "Lihat Voting Lengkap";
$TEXT2['devconsole_avt_price'] = "Harga";
$TEXT2['devconsole_avt_product'] = "Produk";
$TEXT2['devconsole_avt_review'] = "Review";
$TEXT2['devconsole_avt_target'] = "Target";
$TEXT2['devconsole_avt_time'] = "Waku";
$TEXT2['devconsole_avt_user'] = "ID/Nama Pengguna";
$TEXT2['devconsole_avt_vote'] = "Vote";
$TEXT2['devconsole_bankdata'] = "Akun Rekening";
$TEXT2['devconsole_code'] = "Kode Proyek";
$TEXT2['devconsole_detail_en'] = "Detail Produk (Inggris)";
$TEXT2['devconsole_detail_id'] = "Detail Produk (Indonesia)";
$TEXT2['devconsole_devdata'] = "Data Developer";
$TEXT2['devconsole_filelink'] = "Link Produk";
$TEXT2['devconsole_filelink_info'] = "*Gunakan link jika anda mengupload file di luar TCI. Link yang dimasukan harus langsun menuju bentuk file.";
$TEXT2['devconsole_fileproduct'] = "File Produk";
$TEXT2['devconsole_fileproduct_info'] = "*File yang diizinkan berupa .exe,.zip atau .rar & maksimal 5MB. Silahkan upload di website uploader yang ditunjuk TCI jika ukuran lebih dari 5MB.";
$TEXT2['devconsole_filereplace_info'] = "*Mengupload file atau mengganti link akan memperbarui file produk anda";
$TEXT2['devconsole_fileset'] = "Pengaturan File";
$TEXT2['devconsole_gc'] = "Genre/Kategori";
$TEXT2['devconsole_gc_info'] = "*Harus dalam bahasa Inggris<br>Jika lebih dari 2,pisahkan dengan titik koma(;)";
$TEXT2['devconsole_idgame'] = "ID Game (DLC & Content)";
$TEXT2['devconsole_idproduct'] = "ID Produk";
$TEXT2['devconsole_log_add_p'] = "Membuat produk baru berjudul";
$TEXT2['devconsole_log_del_p'] = "Menghapus produk berjudul";
$TEXT2['devconsole_log_upd_p'] = "Melakukan update pada produk berjudul";
$TEXT2['devconsole_logincontrol'] = "Kontrol Login";
$TEXT2['devconsole_logincontrol_info'] = "*Silahkan isi data jika ingin mengganti password";
$TEXT2['devconsole_password_new'] = "Password Developer Baru";
$TEXT2['devconsole_picchange_info'] = "*Jika anda upload gambar, akan mengganti yang telah ada";
$TEXT2['devconsole_picprogress'] = "Gambar untuk ditampilkan pada menu Progress";
$TEXT2['devconsole_picprogress_info'] = "*Hanya jika projek dalam progress";
$TEXT2['devconsole_picreplace_info'] = "*Mengupload gambar akan mengganti gambar yang sudah ada";
$TEXT2['devconsole_picsub'] = "Gambar Lainya";
$TEXT2['devconsole_pictitle'] = "Gambar Utama";
$TEXT2['devconsole_pricedollar'] = "Harga dalam Dollar";
$TEXT2['devconsole_pricedollar_info'] = "*Input melalui form ini<br>Set 0 jika gratis";
$TEXT2['devconsole_priceexchange'] = "Dengan nilai tukar Dollar ke Rupiah sebesar : ";
$TEXT2['devconsole_pricerupiah'] = "Harga dalam Rupiah";
$TEXT2['devconsole_pricestoredollar'] = "Harga di Store dalam Dollar<br>*(+10% biaya administrasi + $0.31 biaya Payment Gateway)";
$TEXT2['devconsole_pricestorerupiah'] = "Harga di Store dalam Rupiah<br>*(+10% biaya administrasi + Rp4030 biaya Payment Gateway)";
$TEXT2['devconsole_pricing'] = "Pengaturan Harga";
$TEXT2['devconsole_productclickinfo'] = "*Klik gambar produk untuk edit atau lihat";
$TEXT2['devconsole_product_delete_confirm'] = "Perhatian ! Anda akan menghapus produk !";
$TEXT2['devconsole_product_search'] = "Cari produk anda di sini";
$TEXT2['devconsole_progressdatestarted'] = "Tanggal Mulai Proyek";
$TEXT2['devconsole_progresslabel'] = "Status Progress";
$TEXT2['devconsole_progressnotfinish'] = "*Otomatis tercentang jika proyek belum selesai";
$TEXT2['devconsole_progresspercentage'] = "Persentase Progress";
$TEXT2['devconsole_progresspercentage_info'] = "*0-100<br>Jika anda mengatur progress ke 100, status otomatis menjadi 'Published'";
$TEXT2['devconsole_progressshow'] = "Tampilkan di Store";
$TEXT2['devconsole_progressing'] = "Pengaturan Progress";
$TEXT2['devconsole_stas_totalbuyer'] = "Total Pembeli";
$TEXT2['devconsole_stas_totaldownload'] = "Total Unduhan";
$TEXT2['devconsole_stas_totalincome'] = "Pendapatan Total";
$TEXT2['devconsole_stas_totalreview'] = "Total Review";
$TEXT2['devconsole_subtitle_en'] = "Sub Judul Inggris (keterangan singkat)";
$TEXT2['devconsole_subtitle_id'] = "Sub Judul Indonesia (keterangan singkat)";
$TEXT2['devconsole_tags'] = "Tags";
$TEXT2['devconsole_title'] = "Judul";
$TEXT2['devconsole_type'] = "Tipe Produk";
$TEXT2['devconsole_verification_info'] = "*Verifikasikan password setiap kali melakukan perubahan";
$TEXT2['developer_search_by'] = "Cari Produk yang dibuat oleh";
$TEXT2['in_process'] = "Dalam Proses";
$TEXT2['joined_with'] = "Bergabung bersama";
$TEXT2['label_product_vote'] = "melakukan vote pada sebuah produk";
$TEXT2['nav_balance'] = "Saldo";
$TEXT2['nav_cart'] = "Keranjang";
$TEXT2['nav_dev'] = "Developer";
$TEXT2['nav_logout'] = "Logout";
$TEXT2['nav_myprofile'] = "Profil Saya";
$TEXT2['nav_product'] = "Produk Dibeli";
$TEXT2['search_transaction'] = "Cari transaksi anda di sini";
$TEXT2['see_more'] = "Lihat Lebih Banyak";
$TEXT2['select'] = "Pilih";
$TEXT2['title_congratulation'] = "Selamat !";
$TEXT2['topup_amount'] = "Jumlah Saldo";
$TEXT2['topup_balance'] = "Tambah Saldo";
$TEXT2['topup_country_channel'] = "Channel Negara Pembayaran";
$TEXT2['topup_just_did'] = "baru saja melakukan topup";
$TEXT2['topup_no_decimal'] = "*Tanpa menggunakan tanda titik/desimal";
$TEXT2['user_product_none'] = "Pengguna belum membeli produk apapun";
$TEXT2['user_product_none2'] = "Anda belum membeli produk apapun";
$TEXT2['user_report_info'] = "*Harap laporkan pengguna ini kepada kami melalui E-Mail : admin@techconnexionindonesia.com apabila pengguna ini telah melanggar peraturan kami";
$TEXT2['user_review_none'] = "Pengguna belum review produk apapun";
$TEXT2['verification'] = "Verifikasi";

//Long text here
//Sorted by a-Z
$LONGTEXT['about_rule_dev'] = "
&nbsp;&nbsp;&nbsp;Sebagai pengembang, anda diwajibkan untuk menjaga kualitas produk yang anda publikasikan di website TCI. Hal ini karena adanya hubungan antara pihak TCI dan anda sebagai pengembang. Kami memastikan agar kedua pihak yaitu pengguna dan pengembang mendapatkan keuntungan masing-masing tanpa ada yang merasa dirugikan. Sebelum bergabung menjadi pengembang, anda diharuskan untuk membaca syarat & ketentuan pengguna terlebih dahulu. Dengan menjadi pengembang, anda telah memahami dan menyetujui syarat dan ketentuan yang telah diberlakukan pihak TCI sebagai berikut :<br><br>
<b>1. Perihal keaslian produk</b><br>
&nbsp;&nbsp;&nbsp;Dengan mempublikasikan konten, pengembang harus menjamin keaslian produk yang pengembang buat, yaitu produk yang pengembang publikasikan adalah hasil dari pekerjaan pengembang, ataupun produk milik orang lain yang telah diizinkan untuk dipublikasikan kepada pihak pengembang. Segala konten di dalam suatu produk yang ada di dalam produk yang dipublikasikan pengembang juga harus bersifat orisinil atau memiliki izin untuk publikasi. Pihak TCI akan menghapus produk yang tidak orisinil atau tidak memiliki izin apabila pihak TCI mengetahui produk yang dimaksud adalah produk bajakan. Pihak akan mengkaji apabila ada masuknya laporan dari pengguna yang melaporkan perihal keaslian produk<br><br>
<b>2. Perihal publikasi produk</b><br>
&nbsp;&nbsp;&nbsp;Dengan menggunakan fitur TCI, anda berhak untuk mempublikasikan produk dan mengatur status dan persentase publikasinya. Produk yang dipublikasikan dapat diatur kondisi statusnya mulai dari 0%. Produk dengan status publikasi di bawah 100% memiliki arti belum rilis dan dengan hal itu, tombol pembelian pada laman produk yang dimaksud ditiadakan. Produk yang diatur status publikasinya menjadi 100% akan ditinjau pihak TCI terlebih dahulu untuk menentukan apakah produk dapat digunakan atau tidak. Produk yang telah ditinjau oleh pihak TCI, apabila tidak dapat digunakan atau diunduh, maka akan mengalami penundaan perilisan hingga pengembang memperbaiki kesalahan yang diterakan pada informasi publikasi. Produk yang sudah dirilis dengan status publikasi 100%, dapat diatur kembali menjadi di bawah 100% hanya apabila terjadi kesalahan pada produk.<br><br>
<b>3. Perihal Penetapan harga produk</b><br>
&nbsp;&nbsp;&nbsp;Pada publikasi produk, pengembang dapat mengatur harga pada produk yang dipublikasikan. Harga tersebut dapat diatur menurut kehendak pengembang. Harga yang dimasukan melalui Rupiah pada form informasi publikasi, selanjutnya akan dikonversikan menjadi Dollar Amerika Serikat (US Dollar) menggunakan nilai tukar mata uang yang dikonfigurasikan pihak TCI. Harga yang tertera pada Rupiah, dapat berubah sewaktu-waktu seiring perubahan nilai tukar mata uang yang ditetapkan.<br><br>
<b>4. Perihal Unsur Produk</b><br>
&nbsp;&nbsp;&nbsp;Pada produk yang dipublikasikan, tidak boleh mengandung unsur seksual, kampanye politik, ujaran kebencian dan hal-hal yang dapat merugikan pengguna secara spesifik seperti unsur yang menyerang agama, identitas dll.<br><br>
<b>5. Perihal rekening & penarikan keuntungan untuk Indonesia</b><br>
&nbsp;&nbsp;&nbsp;Pengembang yang mendaftar harus memiliki rekening dan mendaftarkan nomor rekening dan nama bank yang dimilikinya pada form dengan data yang benar apabila pengembang mempublikasikan produk berbayar. Apabila pengembang tidak memiliki produk berbayar, pengembang tidak diharuskan untuk memiliki rekening dan mendaftarkanya. Pengembang dapat mempublikasikan produk berbayar terlebih dahulu tanpa rekening dan dapat mendaftarkan rekening pada waktu mendatang. Nomor rekening dan bank yang dimiliki dibutuhkan saat akan menarik keuntungan. Penarikan keuntungan adalah seminimal-minimalnya Rp.35.000 dengan rincian keuntungan yang ditransfer adalah Rp.30.000 dan biaya administrasi sebesar Rp.5000 bilamana keuntungan yang ditarik kurang dari Rp.100.000 . Apabila penarikan keuntungan dilakukan di atas Rp.99.000 maka dikenai biaya administrasi sebesar 10%. Keuntungan dapat ditarik dengan menuju menu developer dan melakukan penarikan keuntungan. Setelah melakukan proses penarikan keuntungan, pihak TCI akan memproses dan memberikan bukti transfer apabila telah dilakukan transfer. Proses transfer akan dilakukan selambat-lambatnya 2 hari setelah proses penarikan dilakukan. Apabila melebihi, maka pihak pengembang dapat menghubungi pihak TCI lebih lanjut. Keuntungan yang sudah ditarik tidak dapat dikembalikan ke saldo pengembang. Apabila sedang terjadi gangguan terhadap proses transfer dan sudah dikonfirmasi pihak TCI melalui email, maka pihak TCI akan mengembalikan saldo yang ditarik semula ke total saldo pengembang.<br><br>
<b>6. Perihal rekening & penarikan keuntungan untuk luar negeri</b><br>
&nbsp;&nbsp;&nbsp;Penarikan keuntungan yang didapatkan setelah proses penjualan produk di mana pengembang berasal dari luar negeri, dapat dilakukan dengan metode bank atau Paypal. Pengembang harus memiliki rekening bank atau rekening paypal yang didaftarkan pada form pengisian data di pendaftaran pengembang. Saldo keuntungan yang ditarik minimal $6 bilamana keuntungan yang ditarik kurang dari $100 dengan rincian keuntungan yang ditransfer sebesar $5 dan biaya administrasi sebesar $1 . Apabila melebihi $9 , maka biaya administrasi dikenakan sebesar 10% dari total penarikan keuntungan. Keuntungan dapat ditarik dengan menuju menu developer dan melakukan penarikan keuntungan. Setelah melakukan proses penarikan keuntungan, pihak TCI akan memproses dan memberikan bukti transfer apabila telah dilakukan transfer. Proses transfer akan dilakukan selambat-lambatnya 2 hari setelah proses penarikan dilakukan. Apabila melebihi, maka pihak pengembang dapat menghubungi pihak TCI lebih lanjut. Keuntungan yang sudah ditarik tidak dapat dikembalikan ke saldo pengembang. Apabila sedang terjadi gangguan terhadap proses transfer dan sudah dikonfirmasi pihak TCI melalui email, maka pihak TCI akan mengembalikan saldo yang ditarik semula ke total saldo pengembang.<br><br>
<b>7. Perihal informasi dan kontak</b><br>
&nbsp;&nbsp;&nbsp;Pengembang harus mempunyai email aktif dikarenakan segala informasi seperti proses transfer dan lainya akan diinformasikan melalui email. Pengembang dapat menghubungi pihak TCI lebih lanjut untuk pertanyaan dan hal lainya melalui :<br>
<b>Email</b> : <a href='mailto:admin@techconnexionindonesia.com'>admin@techconnexionindonesia.com</a><br>
<b>Facebook</b> : <a href='https://www.facebook.com/tcifp'>www.facebook.com/tcifp</a><br>
";

$LONGTEXT['about_rule_intro'] = "
&nbsp;&nbsp;&nbsp;Sebagai penyedia layanan platform dagang, TCI mempunyai syarat dan ketentuan yang diberlakukan bagi pengguna dan pengembang. Hal ini digunakan untuk memastikan jalanya sistem yang ada yaitu sistem jual beli. Syarat dan ketentuan juga dibuat untuk melindungi pelanggan dan pengembang dari ancaman penipuan, atau ancaman yang berada dalam layanan kami. Dengan memanfaatkan sistem yang ada, kami berusaha sepenuhnya agar kualitas jual beli produk digital di dalam layanan TCI berjalan dengan baik dan dapat melingkup pengguna dan pengembang yang menjamin keamanan dan kenyamanan pengunaan. Masih pada hal yang sama, sebagai penyedia layanan platform digital, dan dengan menggunakan syarat dan ketentuan yang kami berlakukan, kami berupaya untuk menjaminan agar setiap produk yang ada di TCI bersifat original dan tidak ada produk yang dipublikasi melalui pembajakan.
";


$LONGTEXT['about_rule_user'] = "
&nbsp;&nbsp;&nbsp;Dengan anda mendaftar menjadi pengguna dan menggunakan layanan digital website Tech Connexion Indonesia, anda telah memahami dan menyetujui syarat dan ketentuan yang telah diberlakukan pihak TCI sebagai berikut :<br><br>
<b>1. Perihal proses jual beli</b><br>
&nbsp;&nbsp;&nbsp;Proses jual beli pada website TCI sepenuhnya menggunakan siste saldo yang tersimpan dalam akun pengguna. Saldo tersebut berasal dari hasil transfer ke rekening yang dimiliki oleh TCI dan disertai bukti pembayaran. Pengguna dapat membeli produk game dan konten yang telah tersedia di TCI dan dapat membayar serta mengunduh apabila saldo di akun pengguna mencukupi terhadap harga produk yang dibeli. Apabila produk yang dimaksud memiliki harga 0 (nol) atau gratis, maka pengguna dapat menambahkan ke keranjang dan membayarnya walaupun tidak memiliki saldo sama sekali.<br><br>
<b>2. Perihal mata uang dan nilai tukar mata uang</b><br>
&nbsp;&nbsp;&nbsp;Website TCI menggunakan mata uang Dollar Amerika Serikat (US Dollar) sebagai proses jual beli antara pelanggan dan pengembang. Saldo yang dimasukan ke dalam suatu akun pengguna, akan sepenuhnya dikonversi ke bentuk Dollar Amerika Serikat (US Dollar) dalam struktur basis data di server TCI. Penggantian mata uang pada website, hanya akan mengkonversikan dari mata uang Dollar Amerika Serikat (US Dollar) ke mata uang yang dituju dengan perhitungan nilai tukar yang diperbarui menurut data global pada selambat-lambatnya 1 minggu sekali oleh pihak TCI. Sementara untuk proses pembelian suatu produk, maka akan dilakukan proses perhitungan menggunakan mata uang Dollar Amerika Serikat (US Dollar).<br><br>
<b>3. Perihal pengisian saldo dan bukti pembayaran</b><br>
&nbsp;&nbsp;&nbsp;Saldo pada suatu akun pengguna dapat diisi dengan cara transfer uang secara langsung ke rekening milik pihak TCI dan mengisi form isi saldo pada menu profil serta melampirkan bukti pembayaran berupa foto/tangkapan layar saat transfer uang. Bukti transfer harus disertakan agar dapat memastikan data administrasi dengan benar. Apabila tidak ada pelampiran bukti transfer, maka pengisian saldo harus dicek terlebih dahulu oleh pihak TCI yang mungkin akan memakan waktu lebih lama. Pengisian form isi saldo yang telah dikirimkan, akan diproses oleh pihak TCI terlebih dahulu. Saldo yang masuk pada akun pengisi, diproses secara otomatis melalui sistem TCI di mana jumlah transfer akan dibagi nilai tukar mata uang Rupiah ke Dollar Amerika Serikat (US Dollar). Apabila jumlah yang telah diproses kurang dari nilai Rupiah yang ditransfer, maka nilai saldo akan ditingkatkan sebesar 0,01 (US Dollar) sehingga sama atau melebihi sedikit dari nilai rupiah yang ditransfer. Jumlah minimal untuk pengisian saldo adalah Rp.30.000,- di mana jumlah di bawah itu akan ditolak oleh pihak TCI. Pengguna yang telah mengisi saldo berhak mengajukan keberatan apabila saldo yang telah ditambahkan oleh pihak TCI tidak sama dengan jumlah saldo. Saldo yang sudah diisi, tidak dapat dikembalikan. Apabila pengisian saldo tidak dilakukan setelah selambat-lambatnya 2 jam setelah pengisian form isi saldo dilakukan, maka pengguna dapat menghubungi pihak TCI.<br><br>
<b>4. Perihal gangguan sistem</b><br>
&nbsp;&nbsp;&nbsp;Jika ada gangguan sistem di website TCI, pada lingkup server website, basis data, dan halnya yang melingkup teknis, dapat dilaporkan kepada pihak TCI agar segera diperbaiki. Dan apabila gangguan tersebut merugikan pengguna, maka pengguna berhak mengajukan perbaikan pada lingkup akun yang dirugikannya dengan menyertakan detail kondisi awal sebelum gangguan.<br><br>
<b>5. Perihal perilaku</b><br>
&nbsp;&nbsp;&nbsp;Dengan menggunakan layanan dan produk TCI, anda bersedia untuk menjaga sikap dan sopan santun pada bagian-bagian yang bersifat interaksi seperti Review tentang produk dan fitur komentar pada game TCI. Apabila terdapat interaksi yang mengandung unsur ujaran kebencian, kampanye politik, agama, pornografi, dan hal lainya yang memungkinkan pihak lain dirugikan. Dan apabila pengguna menemukan interaksi pengguna lain seperti yang disebutkan di atas, maka pengguna dapat melaporkan hal tersebut kepada pihak TCI.<br><br>
<b>6. Perihal pemeliharaan layanan & produk</b><br>
&nbsp;&nbsp;&nbsp;Pihak TCI dapat melakukan pemeliharaan layanan dan produk sewaktu-waktu dengan memberi kabar berita kepada pengguna paling lambat 3 jam sebelumnya.<br><br>
<b>7. Perihal informasi</b><br>
&nbsp;&nbsp;&nbsp;Pengguna dapat menghubungi pihak TCI apabila ada kendala yang ditemukan dalam pengunaan layanan & produk serta untuk mendapatkan informasi melalui<br>
<b>Email</b> : <a href='mailto:admin@techconnexionindonesia.com'>admin@techconnexionindonesia.com</a><br>
<b>Facebook</b> : <a href='https://www.facebook.com/tcifp'>www.facebook.com/tcifp</a><br>
";

$LONGTEXT['about_tci_2013'] = "
&nbsp;&nbsp;&nbsp;Pada tahun 2013, TCI awalnya bernama 'Trainz Collection Indonesia' di mana pada masa itu, TCI hanya menjadi developer konten yang membuat konten tambahan untuk game Trainz Railroad Simulator 2009. Pada masa tersebut juga, TCI digagas dan dibangun oleh Ikhwan Febriansyah dan Alex Andre selaku Founder dari TCI.<br>
&nbsp;&nbsp;&nbsp;Beberapa lama setelah dibangun pada 2013, para kedua pendiri tersebut mendirikan website pertamanya yang berbasis di Weebly pada situs : <a href='http://www.trainzcollectionindonesia.weebly.com'>http://www.trainzcollectionindonesia.weebly.com</a> .
";

$LONGTEXT['about_tci_2016'] = "
&nbsp;&nbsp;&nbsp;Setelah lama vakum selama 300 tahun. Maaf maksud kami 3 tahun, TCI hadir kembali di dunia konten untuk menciptakan kembali konten-konten yang menarik untuk game simulasi Trainz Railroad Simulator. Bahkan bukan hanya Trainz Railroad Simulator, melainkan juga TCI menghadirkan mod untuk GTA:San Andreas. Pada awal mulanya, TCI membuat mod pertama GTA:San Andreasnya yang berjudul 'Suara PPKA mod GTA SA by TCI' di mana mod itu dapat membunyikan suara PPKA di game terkenal tersebut. Setelah lama bersilam, TCI membuat mod untuk game itu dengan jumlah yang sangat banyak, dimulai dari mod script, bangunan hingga map. Tidak lupa dengan Trainz Railroad Simulator, TCI juga menciptakan mod KRL yang bermodelkan dari perusahaan kereta api swasta Jepang, Meitetsu 800 dan Meitetsu 1600. TCI juga mempublikasi rute Depok-Manggarai yang sebenarnya dimulai dari stasiun Citayam.
";

$LONGTEXT['about_tci_2017'] = "
&nbsp;&nbsp;&nbsp;Setelah sukses meraih reputasi pada tahun 2016 di bidang dunia modding, pada 2017 TCI berinovasi untuk menciptakan suatu game 3D yang bertakjub 'Jakarta Monorail Simulator'. Di sinilah cikal bakal dimulainya era Developing Game pada TCI. Game ini direncanakan untuk dipublikasikan sebagai game simulasi lokal yang dibuat menurut proyek Monorail di Jakarta sungguhan dari rute hingga keretanya. Dengan menggunakan beberapa anggota tim, TCI telah menyiapkan infrastruktur model seperti pembuatan model kereta UTM-125, model stasiun dari stasiun Casablanca hingga stasiun Ambbasador. Namun, perjalanan itu tak berbuah manis. Pada beberapa bulan kemudian, tidak keefektifan pada proyek tersebut hingga dihentikan.
";

$LONGTEXT['about_tci_2018'] = "
&nbsp;&nbsp;&nbsp;Memasuki Desember 2018, TCI melakukan implementasi dari inovasi baru pada dunia game. Dengan menggagas Indonesia Train Dispatcher, TCI mampu menciptakan publikasi game pertamanya dengan game simulasi tersebut. Game ITD itu, telah berhasil menaikan reputasi TCI pada perilisan Beta hingga Stablenya di 2019. Game ITD kini telah banyak dimainkan oleh para pecinta Kereta Api. Pada tahun 2019 yang sama, TCI juga menyewa server baru untuk websitenya untuk menaruh data server game ITD dan website TCI pada server tersebut. Dari saat itu juga, TCI merubah namanya menjadi 'Tech Connexion Indonesia' yang berartikan suatu hubungan teknologi untuk Indonesia. TCI mengaggas visi barunya yaitu menciptakan apa yang belum ada dan menyempurnakan apa yang telah ada, serta memiliki misi untuk meningkatkan reputasi teknologi Indonesia agar setara dengan dunia luar. Kini TCI telah menjadi organisasi berbasis proyek yang bertujuan untuk menciptakan aplikasi-aplikasi seperti game dan kebutuhan lain, dan kedepanya akan berinovasi untuk menciptakan teknologi-teknologi terbaru yang inovatif dan kreatif.
";

$LONGTEXT['about_tci_intro'] = "
&nbsp;&nbsp;&nbsp;Selamat datang di Tech Connexion Indonesia. Tech Connexion Indonesia adalah sebuah organisasi yang menjadi developer game, developer aplikasi dan juga sebagai penyedia platform untuk publikasi konten dan mod game-game seperti GTA:San Andreas & Trainz Simulator.<br>
&nbsp;&nbsp;&nbsp;Sebagai penyedia platform untuk konten, TCI memberikan kesempatan bagi para developer konten untuk mempublikasikan konten tambahannya berupa mod ataupun addons untuk dipublikasikan dan menjadi konten yang berkualitas, bahkan bisa mendapat keuntungan dengan menjualnya. Antusias para gamer dan developer konten yang menjadikan TCI penyedia platform publikasi konten agar para gamer dan developer dapat lebih mudah untuk mempublikasi dan mengakses konten-konten untuk game yang mereka mainkan. Dan juga, sebagai developer game dan aplikasi.<br>
&nbsp;&nbsp;&nbsp;TCI memiliki visi misi untuk memajukan reputasi aplikasi/teknologi Indonesia agar lebih maju dan setara dengan negara lain yang telah lebih dulu maju. TCI menciptakan game dan aplikasi yang luar biasa dengan fitur-fitur yang menakjubkan. Game yang kini dibuat, berfokus utama pada simulasi, dan di luar itu bergenre Action dll. TCI bahkan sedang menciptakan inovasi-inovasi baru tentang teknologi dan mengimplementasikan ide-ide kreatif yang akan berguna untuk semua orang. TCI menciptakan apa yang belum ada, dan menyempurnakan apa yang telah ada.";

$LONGTEXT['devconsole_intro'] = "
&nbsp;&nbsp;&nbsp;Selamat datang di laman Developer Console. Developer Console berfungsi untuk segala pengaturan yang berhubungan pengembangan anda pada produk anda. Anda dapat memilih menu yang ada di atas.<br><br>
-> Produk : Berfungsi untuk mengatur publikasi seperti penambahan, pembaharuan dan penghapusan produk.<br>
-> Finansial Statistik : Berfungsi untuk melihat dan menganalisa statistik publikasi dan aktifitas rating produk serta pengunduhan produk.<br>
-> Aktifitas : Berfungsi melihat semua aktifitas seperti aktifitas administrator, pembelian dan pengunduhan serta lainya.<br>
-> Konfigurasi : Berfungsi sebagai konfigurasi untuk pengaturan akun Developer seperti konfigurasi admin, pembaharuan data Developer dan sebagainya.<br><br>
&nbsp;&nbsp;&nbsp;Jika anda mengalami kesulitan saat menggunakan Developer Console, silahkan hubungi pihak TCI melalui kontak yang disediakan di menu Syarat dan Ketentuan pada bagian Tentang TCI.
";

$LONGTEXT['topup_info'] = "
&nbsp;&nbsp;&nbsp;&nbsp;Untuk menambah saldo akun anda, anda dapat melakukan prosedur di bawah ini : <br><br>

<b>1. Pilih Channel Negara</b><br>
&nbsp;&nbsp;&nbsp;&nbsp;Dengan memilih channel negara, akan menentukan channel pembayaran yang tersedia untuk melakukan proses transaksi<br>
<b>2. Masukan Jumlah Saldo</b><br>
&nbsp;&nbsp;&nbsp;&nbsp;Saldo minimal untuk melakukan topup adalah Rp.30.000 untuk channel Indonesia dan $3.00 untuk luar negeri. <br>
<b>3. Lakukan Proses Transaksi</b>
&nbsp;&nbsp;&nbsp;&nbsp;Setelah anda memasukan saldo, anda dapat klik topup dan anda akan diarahkan ke halaman pembayaran. Lakukan pembayaran dengan metode yang tersedia. Setelah anda melakukan pembayaran, sistem TCI otomatis akan menambah jumlah saldo yang anda bayar dan anda akan mendapatkan notifikasi melalui email.<br><br>

<b> Perhatian !</b><br>
&nbsp;&nbsp;&nbsp;&nbsp;Saldo topup akan dikonversikan menjadi US Dollar. Jika menggunakan channel pembayaran Indonesia, jumlah topup akan ditambah $0.01.<br><br>

<b>Metode Pembayaran Indonesia (Menggunakan Ipaymu) :</b><br>
1. Transfer Bank dan Virtual Account : <br>
<b>BCA (khusus Rp500.000+) , Arta Graha , BNI , CIMB Niaga , Mandiri</b><br><br>
2. Convenience Store : <br>
<b>Alfamart , Indomart<br>
(Sebutkan pembayaran Plasamall dan sebutkan kode pembayaran yang didapat)</b><br><br>
3. Aplikasi Tagpay : <br>
<b>Dengan menggunakan aplikasi tagpay, anda bisa bayar menggunakan pulsa</b><br><br>
4. QR IS / Virtual Wallet : <br>
<b>BCA QR, Ovo , Shopee Pay, Gopay , Dana , Linkaja</b>
";

$LONGTEXT['topup_wait'] = "
&nbsp;&nbsp;&nbsp;Anda telah melakukan pengisian saldo. Sistem TCI sedang menunggu pembayaran anda atau sedang melakukan verifikasi pada pembayaran topup anda. Sistem TCI akan menigirmkan email apabila topup anda berhasil atau gagal. Anda bisa melakukan isi saldo kembali dengan mengisi form di bawah. Sistem TCI akan menghapus tagihan pembayaran apabila belum terbayar hingga 2 jam setelah pengisian saldo.
";
?>