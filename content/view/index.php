<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "content-view-user";
include '../../function/model.php';
$x_pdata = getDevProductData($_GET['id']);
if ($x_pdata['type'] == "game" || $x_pdata['type'] == "app") goPage("store","view/?id=".$_GET['id']);
$pr = getContentProductArray();
if (!empty($pr['id'])) {
	$pr['id'] = $pr['id'];
}else{
	$pr['id'] = 0;
}
if (!empty($pr['show_public']) && $pr['show_public'] != true) goBack();
$devData = getDevData($pr['id_developer']);
$_SESSION['refferPage'] = $BASENAME['content'].'view/?id='.$pr['id'];
if (!empty($_SESSION['usr_id']) && isDevAlreadySignup($_SESSION['usr_id'])){
	$x_dev = getDevDataByUsrId($_SESSION['usr_id']);
}else{
	$x_dev = array();
	$x_dev['id_developer'] = "";
}
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title><?php echo $pr['title_html']?></title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-80-default content-element">
			<?php if ($pr['avail'] == true){?>
				<div class="img-big">
					<img src="<?php echo $pr['linkPic']?>">
				</div>
				<div class="slide-selector content-element-dark">
					
				</div>
				<div class="inner-big-left content-element">
					<h1 class="title-white-big"><?php echo $pr['title']?></h1>
					<p><?php echo getStringFromArray($pr['sub_title'])?></p>
					<p><?php echo getStringFromArray($pr['detail'])?></p>
					<br>
					<hr>
					<label><?php echo $TEXT['developed_by'].' : '.$devData['name']?></label>
					<hr>
					<label style="float: left"><?php echo $TEXT['project_started'].' : '.$pr['date_started']?></label><label style="float: right;"><?php echo $TEXT['project_published'].' : '.$pr['date_published']?></label><br>
					<hr>
					<label style="float: left"><?php echo $TEXT['store_total_download'].' : '.number_format($pr['num'])?></label><label style="float: right;"><?php echo $TEXT['store_rating1'].' : '.$pr['vote_yes'].'% '.$TEXT['store_rating2'].' '.$pr['vote_total'].' '.$TEXT['store_rating3']?></label>
					<br>
					<hr>
					<center>
						<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
							<input type="hidden" name="usr_store_id" value="<?php echo $pr['id']?>">
							<input type="submit" name="usr_store_vote_yes" value="Yes" class="<?php echo getStoreBtnVoted($pr['id'],'y')?>"><input type="submit" name="usr_store_vote_no" value="No" class="<?php echo getStoreBtnVoted($pr['id'],'n')?>">
						</form>
						<label class="text-white-shadow"><?php echo $TEXT['store_rate']?></label><br>
						<?php echo getStoreVoteUnable($pr['id'])?>
					</center>
				</div>
				<div class="inner-small-right content-element">
					<label><?php echo $TEXT['id'].' : '.$pr['id']?></label><br>
					<hr>
					<label><?php echo $TEXT['genre'].'/'.$TEXT['category'].' : '.str_replace(";",", ",$pr['gc'])?></label>
					<hr>
					<label><?php echo $TEXT['status'].' : '.$pr['label']?></label><br>
					<div class="progress-base">
						<button class="label-progress" style="width:<?php echo $pr['progress'].'%'?>;min-width: 35px"><?php echo $pr['progress'].'%';?></button>
					</div>
					<hr>
					<?php echo $TEXT['tags'].' :'?><br><textarea class="textarea-bright" disabled><?php echo $pr['tags']?></textarea>
					<hr>
					<label><?php echo $TEXT['price'].' : '?><a class="text-green"><?php echo getSym().cvtMoney($pr['price'])?></a></label>
					<hr>
					<center>
						<?php if (intval($pr['progress']) == 100 && $pr['show_public'] == 1){?>
							<?php if ($x_dev['id_developer'] == $pr['id_developer']){?>
									<input type="submit" name="usr_store_dl" value="<?php echo $TEXT['download']?>" class="btn-green btn-big" onclick="window.open(`<?php echo $BASENAME['download'].$pr['id']?>`,'_SELF')">
							<?php }else if ($pr['avail_ATC']){?>
								<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
									<input type="hidden" name="usr_store_stuff_id" value="<?php echo $pr['id']?>">
									<input type="submit" name="usr_store_ATC" value="<?php echo $TEXT['add_to_cart'].' '.getSym().cvtMoney($pr['price'])?>" class="btn-green btn-big">
								</form>
							<?php }if ($pr['ATC_added']){?>
								<a href="<?php echo $BASENAME['cart']?>"><button class="btn-green btn-big"><?php echo $TEXT['store_in_cart']?></button></a>
							<?php }if ($pr['avail_dl']){?>
								<input type="submit" name="usr_store_dl" value="<?php echo $TEXT['download']?>" class="btn-green btn-big" onclick="window.open(`<?php echo $BASENAME['download'].$pr['id']?>`,'_SELF')">
							<?php }?>
						<?php }?>
					</center>
				</div>
				<div class="inner-big-left content-element">
					<?php if (!empty($_SESSION['usr_id'])){?>
						<div class="bar-comment-self">
							<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
								<input type="hidden" name="usr_store_id" value="<?php echo $pr['id']?>">
								<textarea name="usr_store_review_text" placeholder="<?php echo $TEXT['store_review_placeholder']?>"></textarea>
								<input type="submit" name="usr_store_comment_submit" value="<?php echo $TEXT['submit']?>" class="btn-green">
							</form>
						</div>
						<br>
						<hr>
					<?php }?>
					<label class="text-medium text-white-shadow"><?php echo getTotalReview($pr['id']).' '.$TEXT2['devconsole_stas_totalreview']?></label>
					<?php echo getStoreProductReview()?>
				</div>
			<?php }?>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>