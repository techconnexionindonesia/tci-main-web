<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "content-user";
include '../function/model.php';
$_SESSION['refferPage'] = $BASENAME['content'];
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Content - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
		<script type="text/javascript" src="<?php echo $BASENAME['js'].'search.js'?>"></script>
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-100-default content-element">
				<input type="text" name="content_search"  placeholder="<?php echo $TEXT['content_search']?>" class="search-100" id="content_search" value="<?php echo getContentSearchPlaceholder()?>" onChange="content_search()">
			<div class="container-mini-left">
				<a class="title-white label-title"><?php echo $TEXT['content_all']?></a><br>
				<a href="<?php echo $BASENAME['content']?>" class="text-white-shadow"><?php echo $TEXT['content_showall']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=c'?>" class="text-white-shadow"><?php echo $TEXT['content_c']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=d'?>" class="text-white-shadow"><?php echo $TEXT['content_d']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=trending'?>" class="text-white-shadow"><?php echo $TEXT['content_trending']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=bs'?>" class="text-white-shadow"><?php echo $TEXT['content_bs']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=newest'?>" class="text-white-shadow"><?php echo $TEXT['content_newest']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=progress'?>" class="text-white-shadow"><?php echo $TEXT['content_progress']?></a><br>
				<br>
				<a class="title-white label-title"><?php echo $TEXT['content_c']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=c&c=trending'?>" class="text-white-shadow"><?php echo $TEXT['content_trending']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=c&c=bs'?>" class="text-white-shadow"><?php echo $TEXT['content_bs']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=c&c=newest'?>" class="text-white-shadow"><?php echo $TEXT['content_newest']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=c&c=progress'?>" class="text-white-shadow"><?php echo $TEXT['content_progress']?></a><br>
				<hr>
				<a href="<?php echo $BASENAME['content'].'?s=c&g=gtasa'?>" class="text-white-shadow"><?php echo $TEXT['content_c_gtasa']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=c&g=trs'?>" class="text-white-shadow"><?php echo $TEXT['content_c_trs']?></a><br>
				<br>
				<a class="title-white label-title"><?php echo $TEXT['content_d']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=d&c=trending'?>" class="text-white-shadow"><?php echo $TEXT['content_trending']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=d&c=bs'?>" class="text-white-shadow"><?php echo $TEXT['content_bs']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=d&c=newest'?>" class="text-white-shadow"><?php echo $TEXT['content_newest']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=d&c=progress'?>" class="text-white-shadow"><?php echo $TEXT['content_progress']?></a><br>
				<a href="<?php echo $BASENAME['content'].'?s=d'?>" class="text-white-shadow"><?php echo $TEXT['content_all']?></a><br>
				<hr>
				<?php echo getContentDlcGameList();?>
			</div>
			<div class="container-big-right content-element">
				<?php echo getContentProductView();?>
			</div>
			<center>All Product Copyright by Their Developer</center>
			<div style="height: 50px;width: 1%;"></div>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	</body>
</html>