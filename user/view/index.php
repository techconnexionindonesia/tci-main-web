<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
if (empty($_GET['id'])) goBack();
$_SESSION['thisPage'] = "user-view-user";
include '../../function/model.php';
if (!isUserExist($_GET['id'])) goBack();
$_SESSION['refferPage'] = $BASENAME['user'].'view/?id='.$_GET['id'];
$userData = getUserData($_GET['id']);
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8"/>
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title><?php echo $userData['front_name'].' '.$userData['back_name']?> - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>" onLoad='setImgFlag(<?php echo "`".$_SESSION['lang']."`"?>);'>
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-80-default">
			<!-- Left Section-->
			<div class="inner-big-left content-element">
				<!--Description Section-->
				<div class="textbox-w100-1">
					<label class="text-small text-white-shadow"><?php echo $userData['front_name'].' '.$userData['back_name'].' '.$TEXT2['data_description']?> :</label><br>
					<hr>
					<label class="text-low-small text-white-shadow"><?php echo $userData['description']?></label>
				</div>
				<br>
				<!--Signature Section-->
				<div class="textbox-w100-1">
					<label class="text-small text-white-shadow"><?php echo $TEXT2['data_signature']?> :</label><br>
					<hr>
					<label class="text-low-small text-white-shadow"><?php echo $userData['signature']?></label>
				</div>
				<br>
				<?php if (isDevAlreadySignup($userData['id_user']) && !isDevWaitValid($userData['id_user'])){ ?>
				<!--Developer Section-->
				<div>
					<?php
					$devData = getDevDataByUsrId($userData['id_user']);
					if (getDevPosition($userData['id_user']) == "owner") $x_t = $TEXT2['created'].' '.$devData['name'];
					else $x_t = $TEXT2['joined_with'].' '.$devData['name'];
					?>
					<center><a href="<?php echo $BASENAME['developer'].'view/?id='.$devData['id_developer']?>"><?php createBoxUblue1(60,120,150,'Developer',$BASENAME['img_dev'].$devData['pic_logo'],$x_t);?></a></center>
				</div>
				<br>
				<?php }?>
				<!-- Product Section-->
				<div class="textbox-w100-1">
					<label class="text-small text-white-shadow"><?php echo $TEXT2['data_product_bought']?> :</label><br>
					<hr>
					<?php 
					$x_p = getUsrProductBought($_GET['id']);
					if (!$x_p) echo $TEXT2['user_product_none'];?>
					___________________________________________________________________
				</div>
				<br>
				<!-- Vote Section-->
				<div class="textbox-w100-1">
					<label class="text-small text-white-shadow"><?php echo $TEXT2['devconsole_avt_actreview']?> :</label><br>
					<hr>
					<?php
					$x_v = getUsrReviewActivity($_GET['id']);
					if (!$x_v) echo $TEXT2['user_review_none'];?>
				</div>
				<br>
				<br>
				<label class="text-low-small text-white-shadow"><?php echo $TEXT2['user_report_info']?></label>
			</div>
			<!-- Right Section-->
			<div class="inner-small-right content-element">
				<center>
					<img class="img-w90-h190-circle-hoverable" src="<?php echo $BASENAME['img_avatar'].$userData['avatar_link']?>">
					<hr>
					<label class="text-medium text-white-shadow"><?php echo $userData['front_name'].' '.$userData['back_name']?></label>
					<br>
					<label class="text-tiny text-white-shadow"><?php echo 'ID : '.$userData['id_user']?></label>
					<br><hr>
					<br>
					<div class="textbox-w100-1">
						<label class="text-small text-white-shadow"><?php echo $TEXT2['data_country']?> :</label><br>
						<hr>
						<label class="text-low-small text-white-shadow"><?php echo cvtCodeCountry($userData['country'])?></label>
					</div>
					<br>
					<div class="textbox-w100-1">
						<label class="text-small text-white-shadow"><?php echo $TEXT2['data_registered']?> :</label><br>
						<hr>
						<label class="text-low-small text-white-shadow"><?php echo $userData['registered']?></label>
					</div>
				</center>
			</div>
			<center>______________________________________</center>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>
	<script type="text/javascript" src="<?php echo $BASENAME['js'].'news-view.js'?>"></script>