function showFullTransaction(){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-transaction").style.display = "block";
}

function showFullActivity(){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-activity").style.display = "block";
}

function showFullVote(){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-vote").style.display = "block";
}

function showFullReview(){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-review").style.display = "block";
}

function hide(){
	document.getElementById("webmask").style.display = "none";
	document.getElementById("div-transaction").style.display = "none";
	document.getElementById("div-activity").style.display = "none";
	document.getElementById("div-vote").style.display = "none";
	document.getElementById("div-review").style.display = "none";
}