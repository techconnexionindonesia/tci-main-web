var basename = "https://www.techconnexionindonesia.com/";
var baseStore = "store/";
var baseCart = "cart/";
var baseNews = "news/";
var baseContent = "content/";
var baseAbout = "aboutus/";
var baseDevConsoleProduct = "devconsole/product/";

function storeGASearch(){
	var q = document.getElementById("product_g_a_search").value;
	window.open(basename+baseStore+"?q="+q,"_SELF");
}

function cart_search(){
	var q = document.getElementById("usr_cart_search").value;
	window.open(basename+baseCart+"?q="+q,"_SELF");
}

function news_search(){
	var q = document.getElementById("news_search").value;
	window.open(basename+baseNews+"?q="+q,"_SELF");
}

function content_search(){
	var q = document.getElementById("content_search").value;
	window.open(basename+baseContent+"?q="+q,"_SELF");
}

function devconsoleProduct_search(){
	var q = document.getElementById("devconsole_product_search").value;
	window.open(basename+baseDevConsoleProduct+"?q="+q,"_SELF");
}