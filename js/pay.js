var selectId = NULL;

function payCart(id){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-pay").style.display = "block";
	document.getElementById("usr_cart_pay_id").value = id;
}

function payAll(id){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-pay-all").style.display = "block";
}

function deleteCart(id){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-delete").style.display = "block";
	document.getElementById("usr_cart_delete_id").value = id;
}

function deleteAll(id){
	document.getElementById("webmask").style.display = "block";
	document.getElementById("div-delete-all").style.display = "block";
}

function closeDiv(){
	document.getElementById("webmask").style.display = "none";
	document.getElementById("div-pay").style.display = "none";
	document.getElementById("div-delete").style.display = "none";
}