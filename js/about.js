function show(div){
	closeDiv();
	openDiv(div);
}

function closeDiv(){
	document.getElementById("about-div-aboutus").style.display = "none";
	document.getElementById("about-div-project").style.display = "none";
	document.getElementById("about-div-structure").style.display = "none";
	document.getElementById("about-div-director").style.display = "none";
	document.getElementById("about-div-rule").style.display = "none";
}

function openDiv(div){
	document.getElementById(div).style.display = "block";
}

function showStructure(stc){
	closeStructure();
	openStructure(stc);
}

function closeStructure(){
	var structures = document.getElementsByClassName("div-structures");
	for (i=0;i<structures.length;i++){
		structures[i].style.display = "none";
	}
}

function openStructure(stc){
	document.getElementById(stc).style.display = "block";
}

function structureSelection(){
	var stc = document.getElementById('about-structure-selection').value;
	showStructure(stc);
}