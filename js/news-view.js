newsImgSet();

function newsImgSet(){
  var showNum = 0;
  var onCheck = true;
  while (onCheck){
    showNum++;
    var showStr = "show-img-"+showNum;
    var optStr = "option-img-"+showNum;
    if (document.getElementById(showStr) != null){
        if (showNum == 1) {
          document.getElementById(showStr).style.display = "block";
          document.getElementById(optStr).style.opacity = "1.0";
        }
        else {
          document.getElementById(showStr).style.display = "none";
          document.getElementById(optStr).style.opacity = "0.5";
        }
    }else onCheck = false;
  }
}

function changePic(picId,optId){
  var showNum = 0;
  var onCheck = true;
  while (onCheck){
    showNum++;
    var showStr = "show-img-"+showNum;
    var optStr = "option-img-"+showNum;
    if (document.getElementById(showStr) != null){
        document.getElementById(showStr).style.display = "none";
        document.getElementById(optStr).style.opacity = "0.5";
    }else onCheck = false;
  }
  document.getElementById(picId).style.display = "block";
  document.getElementById(optId).style.opacity = "1.0";
}

function setImgFlag(lang){
  document.getElementById("flagId").style.opacity = "0.5";
  document.getElementById("flagEn").style.opacity = "0.5";
  if (lang == "ID") document.getElementById("flagId").style.opacity = "1.0";
  else if (lang == "EN") document.getElementById("flagEn").style.opacity = "1.0";
}