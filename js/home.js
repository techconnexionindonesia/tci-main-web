//Javascript Function
//Tech Connexion Indonesia Website

//Home Index Javascript
//Declaration
var slideIndex = 0;
var slideIndex2 = 0;
var timer = 0;
var timer2 = 0;
var i;
var j;
autoSlide();
autoSlideProgress();

function autoSlide() {
    if (document.getElementsByClassName("index-slide") != null){
      var x = document.getElementsByClassName("index-slide");
      for (i = 0; i < x.length; i++) {
        x[i].style.display = "none";
      }
      slideIndex++;
      if (slideIndex > x.length) {slideIndex = 1}
      x[slideIndex-1].style.display = "block";
      timer = setTimeout(autoSlide, 10000); // Change image every 10 seconds
    }
}

function autoSlideProgress() {
    if (document.getElementsByClassName("progress-slide") != null){
      var y = document.getElementsByClassName("progress-slide");
      for (j = 0; j < y.length; j++) {
        y[j].style.display = "none";
      }
      slideIndex2++;
      if (slideIndex2 > y.length) {slideIndex2 = 1}
      y[slideIndex2-1].style.display = "block";
      timer2 = setTimeout(autoSlideProgress, 5000); // Change image every 5 seconds
    }
}

function slider(n) {
  showDivs(slideIndex += n);
}

function showDivs(n) {
  clearTimeout(timer);
  timer = 0;
  var x = document.getElementsByClassName("index-slide");
  if (n > x.length) {slideIndex = 1}
  if (n < 1) {slideIndex = x.length} ;
  for (i = 0; i < x.length; i++) {
    x[i].style.display = "none";
  }
  x[slideIndex-1].style.display = "block";
  timer = setTimeout(autoSlide, 10000); // Change image every 10 seconds
}

function showRecentActivity(){
  document.getElementById("webmask").style.display = "block";
  document.getElementById("div-recentactivity").style.display = "block";
}

function closeRecentActivity(){
  document.getElementById("webmask").style.display = "none";
  document.getElementById("div-recentactivity").style.display = "none";
}