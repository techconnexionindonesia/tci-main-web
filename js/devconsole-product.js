var set_cachelabel = "";
var set_exchange = 13000;
var set_iddeveloper = "";
autoSetting();

function autoSetting(){
	if (document.getElementById("dev_ps_type") != null){	
		var label_idgame = document.getElementById("label_idgame");
		var set_code = document.getElementById("dev_ps_code");
		var set_detail = document.getElementById("dev_ps_detail");
		var set_detail_en = document.getElementById("dev_ps_detail_en");
		var set_detail_id = document.getElementById("dev_ps_detail_id");
		var set_idgame = document.getElementById("dev_ps_idgame");
		var set_idproduct_d = document.getElementById("dev_ps_idproduct_d");
		var set_label = document.getElementById("dev_ps_label");
		var set_mode = document.getElementById("dev_ps_mode");
		var set_picprogress = document.getElementById("dev_ps_picprogress");
		var set_picsub1 = document.getElementById("dev_ps_picsub1");
		var set_picsub2 = document.getElementById("dev_ps_picsub2");
		var set_picsub3 = document.getElementById("dev_ps_picsub3");
		var set_pictitle = document.getElementById("dev_ps_pictitle");
		var set_pricedollar = document.getElementById("dev_ps_pricedollar");
		var set_pricerupiah = document.getElementById("dev_ps_pricerupiah");
		var set_pricestoredollar = document.getElementById("dev_ps_pricestoredollar");
		var set_pricestorerupiah = document.getElementById("dev_ps_pricestorerupiah");
		var set_progress = document.getElementById("dev_ps_progress");
		var set_progressnotfinish = document.getElementById("dev_ps_notfinish");
		var set_subtitle = document.getElementById("dev_ps_subtitle");
		var set_subtitle_en = document.getElementById("dev_ps_subtitle_en");
		var set_subtitle_id = document.getElementById("dev_ps_subtitle_id");
		var set_type = document.getElementById("dev_ps_type");		
		var text_filereplace = document.getElementById("dev_ps_filereplace_info");
		var text_picreplace = document.getElementById("dev_ps_picreplaceinfo");
		var text_type_disabled = document.getElementById("label_type_disabled");
		//Section Id Game hiding
		if (set_type.value == "dlc" || set_type.value == "content"){
			set_idgame.style.display = "";
			label_idgame.style.display = "";
		}
		else {
			set_idgame.style.display = "none";
			label_idgame.style.display = "none";
		}
		//End of Id Game hiding section
		//Section Id Product setting
		if (document.getElementById("dev_ps_code").value != ""){
			var a = set_iddeveloper;
			var b = "";
			if (set_type.value == "game") b = "1";
			else if (set_type.value == "dlc") b = "2";
			else if (set_type.value == "content") b = "3";
			else b = "4";
			var c = document.getElementById("dev_ps_idgame");
			var d = document.getElementById("dev_ps_code");
			if (set_type.value == "game" || set_type.value == "app"){
				var e = a+b+d.value.toUpperCase();
				if (set_type.value != "edit") set_idproduct_d.value = e;
			}else{
				var e = a+b+c.value+d.value.toUpperCase();
				if (set_type.value != "edit") set_idproduct_d.value = e;
			}
		}else{
			document.getElementById("dev_ps_idproduct_d").value = "";
		}
		//End of Id Product setting section
		//Section Upper Case
		set_code.value = set_code.value.toUpperCase();
		set_idgame.value = set_idgame.value.toUpperCase();
		//End of Upper Case section
		//Section pricing
		if (set_pricedollar.value != ""){
			var a = parseFloat(set_pricedollar.value);
			var fee_gateway = a > 0 ? 0.31 : 0;
			var b = a*set_exchange;
			var c = (a+(a*10/100))+fee_gateway;
			var d = (((a*set_exchange)*10/100)+(fee_gateway*set_exchange))+(a*set_exchange);
			set_pricerupiah.value = "Rp."+b.toLocaleString("ID");
			set_pricestoredollar.value = "$"+c.toFixed(2);
			set_pricestorerupiah.value = "Rp."+d.toLocaleString("ID");
			var x = set_pricedollar.value.split(".");
			if (x.length > 1){
				if (x[1].length > 2) set_pricedollar.value = a.toFixed(2);
			}
		}else{
			set_pricerupiah.value = "";
			set_pricestoredollar.value = "";
			set_pricestorerupiah.value = "";
		}
		//End of pricing section
		//Section progress labeling
		setCacheLabel(set_label.value);
		if (set_progress.value == "100"){
			set_label.value = "Published";
		}else{
			set_label.value = set_cachelabel;
		}
		//End of progress labeling section
		//Section progress checkbox & pic
		if (dev_ps_progress.value == 100) dev_ps_notfinish.checked = null;
		else dev_ps_notfinish.checked = "true";
		if (dev_ps_notfinish.checked) set_picprogress.style.display = "";
		else set_picprogress.style.display = "none";
		//End of progress checkbox & pic section
		//Section joining text
		set_subtitle.value = "ID/"+set_subtitle_id.value+";EN/"+set_subtitle_en.value;
		set_detail.value = "ID/"+set_detail_id.value+";EN/"+set_detail_en.value;
		//End of joining text session
		//Section check mode edit
		if (set_mode.value == "edit"){
			text_filereplace.style.display = "";
			text_picreplace.style.display = "";
			text_type_disabled.style.display = "";
			set_pictitle.required = null;
			set_picsub1.required = null;
			set_picsub2.required = null;
			set_picsub3.required = null;
			set_type.disabled = "true";
		}else{
			text_filereplace.style.display = "none";
			text_picreplace.style.display = "none";
			text_type_disabled.style.display = "none";
			set_pictitle.required = "true";
			set_picsub1.required = "true";
			set_picsub2.required = "true";
			set_picsub3.required = "true";
			set_type.disabled = null;
			if (dev_ps_notfinish.checked) set_picprogress.required = "true";
			else set_picprogress.required = null;
		}
		//Section no space
		set_code.value = set_code.value.replace(" ","");
		set_idgame.value = set_idgame.value.replace(" ","");
	}
	setTimeout(autoSetting, 100);
}

function setIdDeveloper(dev_id){
	set_iddeveloper = dev_id;
}

function setRateExchange(ex){
	set_exchange = ex;
}

function setCacheLabel(label){
	if (label.toLowerCase() != "published") set_cachelabel = label;
}

function submitted(){
	var set_type = document.getElementById("dev_ps_type");
	set_type.disabled = null;
}