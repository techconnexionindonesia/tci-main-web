<?php
//Index Page - Signup - Tech Connexion Indonesia Website
session_start();
$_SESSION['thisPage'] = "signup-success-user";
include '../../function/model.php';
if (empty($_SESSION['signup_success'])) goBack();
?>
<!DOCTYPE HTML>
<html>
	<head>
		<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
		<meta id="Viewport" name="viewport" content="initial-scale=-100, width=320 maximum-scale=1, minimum-scale=-100, user-scalable=yes">
		<meta name="author" content="Tech Connexion Indonesia">
		<title>Signup Success - TCI</title>
		<link rel="stylesheet" href="<?php echo $BASENAME['dr_css']?>">
		<link rel="icon" href="<?php echo $BASENAME['img_html'].'icon.png'?>">
	</head>
	<body background="<?php echo $BASENAME['img_html'].'bg1.png'?>">
		<?php if (getBooleanHeaderNews()){
			?><div class="headerNews"><marquee><?php echo getHeaderNews()?></marquee></div><?php
		}?>
		<div class="navbar">
			<div class="logo">
				<img src="<?php echo $BASENAME['img_html'].'navbar-logo.png'?>"><br>
				<h1>TECH CONNEXION INDONESIA</h1>
			</div>
			<div class="menu">
				<div class="login">
					<div class="dropdown">
  						<?php echo getNavProfile()?>
					</div>
				</div>
				<div class="link">
					<a href="<?php echo $BASENAME['index']?>"><?php echo $TEXT['navbar_home']?></a>
					<a href="<?php echo $BASENAME['news']?>"><?php echo $TEXT['navbar_news']?></a>
					<a href="<?php echo $BASENAME['store']?>"><?php echo $TEXT['navbar_store']?></a>
					<a href="<?php echo $BASENAME['content']?>"><?php echo $TEXT['navbar_content']?></a>
					<a href="<?php echo $BASENAME['about_us']?>"><?php echo $TEXT['navbar_aboutus']?></a>
				</div>
			</div>
		</div>
		<div class="content-center-big-50-default content-element">
			<center><h1 class="title-white-big"><?php echo $TEXT['signup_success_title']?></h1></center>
			<div class="textbox">
				<?php 
				echo $TEXT['signup_success_text1'].'<br><br>';
				echo $TEXT['signup_success_text2'].': '.$_SESSION['usr_id'].'<br>'.$TEXT['signup_success_text3'].': '.getUsrUsername().'<br><br>';
				echo $TEXT['signup_success_text4'];
				?>
			</div>
			<br>
			<form method="POST" action="<?php echo $BASENAME['dr_form_function']?>">
				<label class="text-white-shadow"><input type="checkbox" name="usr_signup_subscription"> <?php echo $TEXT['signup_subscription']?></label>
				<br><br>
				<input type="submit" name="usr_signup_success_submit" value="<?php echo $TEXT['continue']?>" class='btn-green btn-height-medium'>
			</form>
		</div>
		<div class="footer">
			(c)2019 Developed by Tech Connexion Indonesia
		</div>